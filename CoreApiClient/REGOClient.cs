﻿using CyberaniumModels;
using CyberaniumModels.Procedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {

        public async Task<Message<int>> SaveupdateREGO(RegoMaster LawRegulation)
        {
            Message<int> message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/REGO/SaveupdateREGO"));
            message = await PostAsync<int, RegoMaster>(requestUrl, LawRegulation);
            return message;
        }
        public async Task<List<RegoMaster>> GetAllREGO()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/REGO/GetAllREGOData"));
            var data = await GetAsync<List<RegoMaster>>(requestUrl);
            return data;
        }
        public async Task<Message<RegoMaster>> BlockDeleteREGO(RegoMaster regoMaster)
        {
            Message<RegoMaster> message = new Message<RegoMaster>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/REGO/BlockDeleteREGO"));
            message = await PostAsync(requestUrl, regoMaster);
            return message;
        }
        public async Task<List<RegulationbySectorAndCountry>> GetAllRegulationbySectorAndCountryWise(string SectorId, string CountryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/REGO/GetAllRegulationbySectorAndCountryWise"));
            var data = await GetAsync<List<RegulationbySectorAndCountry>,string,string>(requestUrl,  SectorId, CountryId, "SectorId", "CountryId");
            return data;
        }
    }
}
