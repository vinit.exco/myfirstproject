﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region LawRegulation

        public async Task<Message<LawRegulation>> SaveupdateLawRegulation(LawRegulation LawRegulation)
        {
            Message<LawRegulation> message = new Message<LawRegulation>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Regulation/SaveupdateLawRegulation"));
            message = await PostAsync(requestUrl, LawRegulation);
            return message;
        }
        public async Task<List<LawRegulation>> GetAllRegulation()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Regulation/GetAllRegulation"));
            var data = await GetAsync<List<LawRegulation>>(requestUrl);
            return data;
        }

        public async Task<Message<LawRegulation>> BlockDeleteLawRegulation(LawRegulation LawRegulation)
        {
            Message<LawRegulation> message = new Message<LawRegulation>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Regulation/BlockDeleteLawRegulation"));
            message = await PostAsync(requestUrl, LawRegulation);
            return message;
        }
        public async Task<List<LawRegulation>> GetAllLawRegulation()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Regulation/GetAllLawRegulation"));
            var data = await GetAsync<List<LawRegulation>>(requestUrl);
            return data;
        }

        public async Task<List<LawRegulation>> GetMapRegulationByID(int ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Regulation/GetMapRegulationByID"));
            var result = await GetAsync<List<LawRegulation>, int>(requestUrl, ID, "ID");
            return result;
        }
        #endregion
    }
}
