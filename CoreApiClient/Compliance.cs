﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region Compliance
        public async Task<Message<Compliance>> SubmitCompliance(Compliance Compliance)
        {
            Message<Compliance> message = new Message<Compliance>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Compliance/SubmitCompliance"));
            message = await PostAsync(requestUrl, Compliance);
            return message;
        }
        public async Task<Message<int>> BlockDeleteCompliance(int id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Compliance/BlockDeleteCompliance"));
            var res = await GetAsync<Message<int>, int>(requestUrl, id, "id");
            return res;
        }
        public async Task<Message<int>> SetIsPrimary(int id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Compliance/SetIsPrimary"));
            var res = await GetAsync<Message<int>, int>(requestUrl, id, "id");
            return res;
        }
        public async Task<List<Compliance>> GetCompliance()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Compliance/GetCompliance"));
            var data = await GetAsync<List<Compliance>>(requestUrl);
            return data;
        }
        #endregion
    }
}
