﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region Assesment
        public async Task<Message<Assesment>> SubmitAssesment(Assesment Assesment)
        {
            Message<Assesment> message = new Message<Assesment>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Assesment/SubmitAssesment"));
            message = await PostAsync(requestUrl, Assesment);
            return message;
        }
        public async Task<Message<Assesment>> BlockDeleteAssesment(Assesment Assesment)
        {
            Message<Assesment> message = new Message<Assesment>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Assesment/BlockDeleteAssesment"));
            message = await PostAsync(requestUrl, Assesment);
            return message;
        }
        public async Task<List<Assesment>> GetAssesment()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Assesment/GetAssesment"));
            var data = await GetAsync<List<Assesment>>(requestUrl);
            return data;
        }
        public async Task<Message<ClientEnablers>> SubmitEnablerAssesments(ClientEnablers Assesment)
        {
            Message<ClientEnablers> message = new Message<ClientEnablers>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Assesment/SubmitEnablerAssesments"));
            message = await PostAsync(requestUrl, Assesment);
            return message;
        }

        public async Task<Message<OrgAssignedAssessment>> SubmitAssignedAssessment(OrgAssignedAssessment model)
        {
            var message = new Message<OrgAssignedAssessment>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Assesment/SubmitAssignedAssessment"));
            message = await PostAsync(requestUrl, model);
            return message;
        }
        public async Task<Message<List<GetEnablerQuestion>>> GetEnablerQuestionByClientCategory(long parentOrgId, long childOrgId, int categoryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Assesment/GetEnablerQuestionByClientCategory"));
            var data = await GetAsync<Message<List<GetEnablerQuestion>>, long, long, int>(requestUrl, parentOrgId, childOrgId, categoryId, "parentOrgId", "childOrgId", "categoryId");
            return data;
        }
        public async Task<List<ChildOrg>> GetChildOrg()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Assesment/GetChildOrg"));
            var data = await GetAsync<List<ChildOrg>>(requestUrl);
            return data;
        }
        public async Task<Message<ScopeAssessments>> SubmitScopeAssessment(ScopeAssessments model)
        {
            var message = new Message<ScopeAssessments>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Assesment/SubmitScopeAssessment"));
            message = await PostAsync(requestUrl, model);
            return message;
        }
        #endregion
    }
}
