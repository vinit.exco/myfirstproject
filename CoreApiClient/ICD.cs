﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region ICD
        public async Task<Message<ICD>> SubmitICD(ICD ICD)
        {
            try
            {
                Message<ICD> message = new Message<ICD>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/ICD/SubmitICD"));
                message = await PostAsync(requestUrl, ICD);
                return message;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }
        public async Task<Message<ICDJson>> SubmitICDByExcel(ICDJson model)
        {
            var message = new Message<ICDJson>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/SubmitICDByExcel"));
            message = await PostAsync(requestUrl, model);
            return message;
        }
        public async Task<Message<ICD>> BlockDeleteICD(ICD ICD)
        {
            Message<ICD> message = new Message<ICD>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/BlockDeleteICD"));
            message = await PostAsync(requestUrl, ICD);
            return message;
        }
        public async Task<List<ICD>> GetICD(long ICDID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetICD"));
            var data = await GetAsync<List<ICD>,long>(requestUrl, ICDID, "ICDID");
            return data;
        }
        public async Task<List<ICD>> GetICDBySubDomainID(long SubDomainID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetICDBySubDomainID"));
            var data = await GetAsync<List<ICD>, long>(requestUrl, SubDomainID, "SubDomainID");
            return data;
        }
        public async Task<Message<ICD>> CodeById(int DomainId, string SubDomain)
        {
            Message<ICD> message = new Message<ICD>();
            var icd = new ICD();
            icd.DomainId = DomainId;
            icd.ControlName = SubDomain;
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/CodeById"));
            message = await PostAsync(requestUrl, icd);
            return message;
        }
        public async Task<Message<ICD>> ControlById(int DomainId, int SubDomainId)
        {
            Message<ICD> message = new Message<ICD>();
            var icd = new ICD();
            icd.DomainId = DomainId;
            icd.SubDomainId = SubDomainId;
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/ControlById"));
            message = await PostAsync(requestUrl, icd);
            return message;
        }
        public async Task<List<ICD>> GetICDThreatMap()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetICDThreatMap"));
            var data = await GetAsync<List<ICD>>(requestUrl);
            return data;
        }
        public async Task<List<ICD>> GetICDRiskDomainMap()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetICDRiskDomainMap"));
            var data = await GetAsync<List<ICD>>(requestUrl);
            return data;
        }
        public async Task<List<ICD>> GetICDCategoryMap()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetICDCategoryMap"));
            var data = await GetAsync<List<ICD>>(requestUrl);
            return data;
        }
        public async Task<List<ICD>> GetICDControlTypeMap()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetICDControlTypeMap"));
            var data = await GetAsync<List<ICD>>(requestUrl);
            return data;
        }
        public async Task<Message<List<ICD>>> GetControlsByThreatId(long id)
        {
            Message<List<ICD>> message = new Message<List<ICD>>();
            var icd = new ICD();
            icd.Id = id;
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetControlsByThreatId"));
            message = await PostAsync<List<ICD>, ICD>(requestUrl, icd);
            return message;
        }
        public async Task<List<ICD>> GetControlsByVersion(string StandardIds, string RegulationIds)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetControlsByVersion"));
            var data = await GetAsync<List<ICD>,string,string>(requestUrl, StandardIds, RegulationIds, "StandardIds", "RegulationIds");
            return data;
        }
        public async Task<List<ICD>> GetRegulationMapByICDID(long ICDId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ICD/GetRegulationMapByICDID"));
            var data = await GetAsync<List<ICD>,long>(requestUrl, ICDId, "ICDId");
            return data;
        }
        #endregion
    }
}
