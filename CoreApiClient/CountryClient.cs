﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        public async Task<List<Country>> GetAllCountry()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/GetAllCountry"));
            var data = await GetAsync<List<Country>>(requestUrl);
            return data;
        }
        public async Task<List<Country>> LawRegulationCountry()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/LawRegulationCountry"));
            var data = await GetAsync<List<Country>>(requestUrl);
            return data;
        }
        public async Task<List<State>> GetAllState(long CID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/GetAllStates"));
            var data = await GetAsync<List<State>,long>(requestUrl, CID, "CID");
            return data;
        }
        public async Task<List<City>> GetAllCity(long CID,long SID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/GetAllCity"));
            var data = await GetAsync<List<City>,long,long>(requestUrl, CID, SID, "CID", "SID");
            return data;
        }
        public async Task<Message<Country>> SaveCountry(Country Model)
        {
            Message<Country> message = new Message<Country>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/SaveCountry"));
            return await PostAsync(requestUrl, Model);
        }
        public async Task<Message<State>> SaveState(State model)
        {
            Message<State> message = new Message<State>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/SaveState"));
            return await PostAsync(requestUrl, model);
        }
        public async Task<Message<City>> SaveCity(City model)
        {
            Message<City> message = new Message<City>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/SaveCity"));
            return await PostAsync(requestUrl, model);
        }
        public async Task<Message<Country>> CheckCountry(Country model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/CheckCountry"));
            return await PostAsync(requestUrl, model);
        }
        public async Task<Message<State>> CheckState(State model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/CheckState"));
            return await PostAsync(requestUrl, model);
        }
        public async Task<Message<City>> CheckCity(City Model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/CheckCity"));
            return await PostAsync(requestUrl, Model);
        }
        public async Task<List<State>> BindStateById(long countryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/BindStateById"));
            return await GetAsync<List<State>, long>(requestUrl, countryId, "countryId");
        }
        public async Task<List<Country>> FindCountryByName(string countryName)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/FindCountryByName"));
            return await GetAsync<List<Country>, string>(requestUrl, countryName, "countryName");
        }
        public async Task<List<State>> FindStateByName(string stateName)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/FindStateByName"));
            return await GetAsync<List<State>, string>(requestUrl, stateName, "stateName");
        }
        public async Task<List<City>> FindCityByName(string cityName)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Country/FindCityByName"));
            return await GetAsync<List<City>, string>(requestUrl, cityName, "cityName");
        }
    }
}
