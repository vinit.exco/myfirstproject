﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region LogIn
        public async Task<Message<UserLogin>> GetUserLogin(UserLogin model)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Login/authenticate"));
            message = await PostAsync(requestUrl, model);
            return message;
        }
        #endregion
        public async Task<List<UserLogin>> GetUsers()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Login/GetAllUsers"));
            return await GetAsync<List<UserLogin>>(requestUrl);
        }

        public async Task<Message<UserLogin>> SaveupdateUser(UserLogin userLogin)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Login/UserCreation"));
            message = await PostAsync(requestUrl, userLogin);
            return message;
        }
        public async Task<Message<UserLogin>> SaveOrganizationUser(UserLogin userLogin)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Login/SaveOrganizationUser"));
            message = await PostAsync(requestUrl, userLogin);
            return message;
        }

        public async Task<Message<UserLogin>> SubmitClientAdmin(UserLogin userLogin)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Login/SubmitClientAdmin"));
            message = await PostAsync(requestUrl, userLogin);
            return message;
        }
        public async Task<List<UserLogin>> getOrganizationUsers(long OrgID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/getOrganizationUsers"));
            var data = await GetAsync<List<UserLogin>, long>(requestUrl, OrgID, "OrgID");
            return data;
        }
        public async Task<List<UserLogin>> EditOrganizationUsers(int ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/EditOrganizationUsers"));
            var data = await GetAsync<List<UserLogin>, int>(requestUrl, ID, "ID");
            return data;
        }
        public async Task<List<UserLogin>> OrganizationUsersPassword(int ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/OrganizationUsersPassword "));
            var data = await GetAsync<List<UserLogin>, int>(requestUrl, ID, "ID");
            return data;
        }
        public async Task<List<UserLogin>> OrganizationUsersEmail(string Email)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/OrganizationUsersEmail "));
            var data = await GetAsync<List<UserLogin>, string>(requestUrl, Email, "Email");
            return data;
        }
        public async Task<Message<UserLogin>> SaveUsername(string UserName = "")
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/SaveUsername "));
            var data = await GetAsync<Message<UserLogin>, string>(requestUrl, UserName, "UserName");
            return data;
        }
        public async Task<List<UserLogin>> PasswordResetOrganizationUsers(int ID , string Password, int ModifiedBy)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/PasswordResetOrganizationUsers "));
            var data = await GetAsync<List<UserLogin>, int, string, int>(requestUrl, ID, Password, ModifiedBy, "ID", "Password", "ModifiedBy");
            return data;
        }
        public async Task<List<UserLogin>> DeleteOrganizationUsers(int ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/DeleteOrganizationUsers "));
            var data = await GetAsync<List<UserLogin>, int>(requestUrl, ID, "ID");
            return data;
        }
        public async Task<List<UserLogin>> DisableOrganizationUsers(int ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/DisableOrganizationUsers "));
            var data = await GetAsync<List<UserLogin>, int>(requestUrl, ID, "ID");
            return data;
        }

        public async Task<List<ClientAdmin>> getClientadmin()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/getClientadmin"));
            var data = await GetAsync<List<ClientAdmin>>(requestUrl);
            return data;
        }

        public async Task<List<ClientAdmin>> getClientadminFillData(long OrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/getClientadminFillData"));
            var data = await GetAsync<List<ClientAdmin>, long>(requestUrl, OrgId, "OrgId");
            return data;
        }


        public async Task<Message<ClientAdmin>> DeleteClientAdmin(int Id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Login/DeleteClientAdmin"));
            var data = await GetAsync<Message<ClientAdmin>, int>(requestUrl, Id, "Id");
            return data;
        }

        public async Task<List<RBAC>> GetRoleMenuById(int id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/Login/GetRoleMenuByRoleId"));
            var data = await GetAsync<List<RBAC>, int>(requestUrl, id, "id");
            return data;
        }
        public async Task<Message<UserLogin>> DeleteActiveUser(int id, int Type)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            try
            {
                var user = new UserLogin();
                user.Id = id;
                user.UserType = Type;
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/Login/DeleteActiveUser"));
                message = await PostAsync(requestUrl, user);
                return message;
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                return message;
            }
        }
    }
}
