﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region EnumType
        public async Task<Message<EnumType>> SaveEnumType(EnumType EnumType)
        {
            Message<EnumType> message = new Message<EnumType>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/EnumType/SaveEnumType"));
            message = await PostAsync(requestUrl, EnumType);
            return message;
        }
        public async Task<Message<EnumType>> UpdateEnumType(EnumType EnumType)
        {
            Message<EnumType> message = new Message<EnumType>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/EnumType/UpdateEnumType"));
            message = await PostAsync(requestUrl, EnumType);
            return message;
        }
        public async Task<Message<EnumType>> BlockDeleteEnumType(EnumType EnumType)
        {
            try
            {
                Message<EnumType> message = new Message<EnumType>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/EnumType/BlockDeleteEnumType"));
                message = await PostAsync(requestUrl, EnumType);
                return message;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<EnumType>> GetAllEnumType()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/EnumType/GetAllEnumType"));
            var data = await GetAsync<List<EnumType>>(requestUrl);
            return data;
        }

        #endregion
        #region Enum
        public async Task<Message<Enum>> SaveEnum(Enum Enum)
        {
            Message<Enum> message = new Message<Enum>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/SaveEnum"));
            message = await PostAsync(requestUrl, Enum);
            return message;
        }
        public async Task<Message<Enum>> UpdateEnum(Enum Enum)
        {
            try
            {
                Message<Enum> message = new Message<Enum>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/Enum/UpdateEnum"));
                message = await PostAsync(requestUrl, Enum);
                return message;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }
        public async Task<Message<Enum>> BlockDeleteEnum(Enum Enum)
        {
            try
            {
                Message<Enum> message = new Message<Enum>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/Enum/BlockDeleteEnum"));
                message = await PostAsync(requestUrl, Enum);
                return message;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Enum>> GetAllEnum()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetAllEnum"));
            var data = await GetAsync<List<Enum>>(requestUrl);
            return data;
        }
        public async Task<List<Enum>> GetEnumByType(int EnumType)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetEnumByType"));
            var data = await GetAsync<List<Enum>, int>(requestUrl, EnumType, "EnumType");
            return data;
        }
        public async Task<List<Enum>> GetAllEnumChildOrgCategory(int OrgID,int ChilOrgID )
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/GetAllChildOrgCategory"));
            var data = await GetAsync<List<Enum>, int,int>(requestUrl, OrgID, ChilOrgID, "OrgID", "ChilOrgID");
            return data;
        }
        public async Task<List<Enum>> GetControlCategoryByOrgId(long parentOrgId, long childOrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/GetControlCategoryByOrgId"));
            var data = await GetAsync<List<Enum>, long, long>(requestUrl, parentOrgId, childOrgId, "parentOrgId", "childOrgId");
            return data;
        }
        public async Task<Message<Enum>> GetHighestValueForEnum(Enum Enum)
        {
            Message<Enum> message = new Message<Enum>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetHighestValueForEnum"));
            message = await PostAsync(requestUrl, Enum);
            return message;
        }
        public async Task<List<Enum>> GetControlCatByProcessId(ProcessesAssign model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetControlCatByProcessId"));
            var res = await PostAsync<List<Enum>, ProcessesAssign>(requestUrl, model);
            return res.Data;
        }

        public async Task<List<RBAC>> GetMISReportId(RBAC model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetMISReportId"));
            var res = await PostAsync<List<RBAC>, RBAC>(requestUrl, model);
            return res.Data;
        }

        public async Task<List<Compliance>> GetAddparticipant(int id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetAddparticipant"));
            var res = await GetAsync<List<Compliance>, int>(requestUrl, id,"id");
            return res;
        }
        public async Task<List<ClientEnablers>> GetQuestionnaire(int id,int OrganizationId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetQuestionnaire"));
            var res = await GetAsync<List<ClientEnablers>, int,int>(requestUrl, id, OrganizationId, "id", "OrganizationId");
            return res;
        }

        public async Task<List<ClientEnablers>> GetOrganizationId(int id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetOrganizationId"));
            var res = await GetAsync<List<ClientEnablers>, int>(requestUrl, id, "id");
            return res;
        }

        public async Task<List<Enum>> GetOrganizationOrder()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetOrganizationOrder"));
            var res = await GetAsync<List<Enum>>(requestUrl);
            return res;
        }

        public async Task<List<Enum>> GetOrganizationMainOrder()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetOrganizationMainOrder"));
            var res = await GetAsync<List<Enum>>(requestUrl);
            return res;
        }

        public async Task<List<RBAC>> GetBaseRiskScore(RBAC model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetBaseRiskScore"));
            var res = await PostAsync<List<RBAC>, RBAC>(requestUrl, model);
            return res.Data;
        }
        public async Task<List<RBAC>> GetProcesssCount(RBAC model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Enum/GetProcesssCount"));
            var res = await PostAsync<List<RBAC>, RBAC>(requestUrl, model);
            return res.Data;
        }
        public async Task<List<RequestArtefacts>> GetArtefactTasks(long userId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetArtefactTasks"));
            var res = await GetAsync<List<RequestArtefacts>, long>(requestUrl, userId, "userId");
            return res;
        }
        #endregion
    }
}
