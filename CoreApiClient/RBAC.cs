﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region RBAC
        public async Task<Message<RBAC>> SubmitRole(RBAC Menu)
        {
            Message<RBAC> message = new Message<RBAC>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/RBAC/SubmitRole"));
            message = await PostAsync(requestUrl, Menu);
            return message;
        }
        public async Task<List<RBAC>> GetRoleById(long id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/RBAC/GetRoleById"));
            var data = await GetAsync<List<RBAC>, long>(requestUrl, id, "id");
            return data;
        }
        
        #endregion
    }
}