﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region Menu
        public async Task<Message<Menu>> SubmitMenu(Menu Menu)
        {
            Message<Menu> message = new Message<Menu>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Menu/SubmitMenu"));
            message = await PostAsync(requestUrl, Menu);
            return message;
        }
        public async Task<Message<Menu>> BlockDeleteMenu(Menu Menu)
        {
            Message<Menu> message = new Message<Menu>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Menu/BlockDeleteMenu"));
            message = await PostAsync(requestUrl, Menu);
            return message;
        }
        public async Task<List<Menu>> GetMenu()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Menu/GetMenu"));
            var data = await GetAsync<List<Menu>>(requestUrl);
            return data;
        }
        #endregion
    }
}
