﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region Artefact
        public async Task<Message<Artefact>> SubmitArtefact(Artefact Artefact)
        {
            Message<Artefact> message = new Message<Artefact>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Artefact/SubmitArtefact"));
            message = await PostAsync(requestUrl, Artefact);
            return message;
        }
        public async Task<Message<Artefact>> BlockDeleteArtefact(Artefact Artefact)
        {
            Message<Artefact> message = new Message<Artefact>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Artefact/BlockDeleteArtefact"));
            message = await PostAsync(requestUrl, Artefact);
            return message;
        }
        public async Task<List<Artefact>> GetArtefact()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Artefact/GetArtefact"));
            var data = await GetAsync<List<Artefact>>(requestUrl);
            return data;
        }
        #endregion
    }
}
