﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region Threat Type
        public async Task<Message<ThreatType>> SaveThreatType(ThreatType threatType)
        {
            Message<ThreatType> message = new Message<ThreatType>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatType/SaveThreatType"));
            message = await PostAsync(requestUrl, threatType);
            return message;
        }
        //public async Task<Message<ThreatType>> UpdateThreatType(ThreatType threatType)
        //{
        //    Message<ThreatType> message = new Message<ThreatType>();
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "/api/ThreatType/UpdateThreatType"));
        //    message = await PostAsync(requestUrl, threatType);
        //    return message;
        //}
        public async Task<Message<ThreatType>> BlockDeleteThreatType(ThreatType threatType)
        {
            try
            {
                Message<ThreatType> message = new Message<ThreatType>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/ThreatType/BlockDeleteThreatType"));
                message = await PostAsync(requestUrl, threatType);
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<ThreatType>> GetAllThreatType()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatType/GetAllThreatType"));
            var data = await GetAsync<List<ThreatType>>(requestUrl);
            return data;
        }
        #endregion
        #region Threat
        public async Task<Message<Threat>> SaveupdateThreat(Threat Threat)
        {
            try
            {
                Message<Threat> message = new Message<Threat>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/Threat/SaveupdateThreat"));
                message = await PostAsync(requestUrl, Threat);
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Message<Threat>> UpdateThreat(Threat Threat)
        {
            Message<Threat> message = new Message<Threat>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/UpdateThreat"));
            message = await PostAsync(requestUrl, Threat);
            return message;
        }
        public async Task<Message<Threat>> BlockDeleteThreat(Threat Threat)
        {
            Message<Threat> message = new Message<Threat>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/BlockDeleteThreat"));
            message = await PostAsync(requestUrl, Threat);
            return message;
        }
        public async Task<List<Threat>> GetAllThreat()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/GetAllThreat"));
            var data = await GetAsync<List<Threat>>(requestUrl);
            return data;
        }
        public async Task<Message<List<Threat>>> GetThreatsByClientId(DomainBaseRiskScore model)
        {
            Message<List<Threat>> message = new Message<List<Threat>>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/GetThreatsByClientId"));
            message = await PostAsync<List<Threat>, DomainBaseRiskScore>(requestUrl, model);
            return message;
        }
        public async Task<Message<int>> GetProbabilityScore(DomainBaseRiskScore model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/GetProbabilityScore"));
            message = await PostAsync<int, DomainBaseRiskScore>(requestUrl, model);
            return message;
        }
        public async Task<List<Threat>> GetControlCategoriesByThreatId(long threatId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/GetControlCategoriesByThreatId"));
            var data = await GetAsync<List<Threat>, long>(requestUrl, threatId, "threatId");
            return data;
        }
        public async Task<List<Threat>> GetThreatsByControlCategoryId(int controlCategoryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/GetThreatsByControlCategoryId"));
            var data = await GetAsync<List<Threat>, int>(requestUrl, controlCategoryId, "controlCategoryId");
            return data;
        }
        public async Task<bool> ValidateThreatName(int ThreatTypeId, string ThreatName)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/ValidateThreatName"));
            var data = await GetAsync<bool, int, string>(requestUrl, ThreatTypeId, ThreatName, "ThreatTypeId", "ThreatName");
            return data;
        }
        #endregion
    }
}
