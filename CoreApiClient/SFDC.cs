﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region SFDC
        public async Task<List<DomainBaseRiskScore>> GetDomainBaseRiskScore(ProcessesAssign model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/SFDC/GetDomainBaseRiskScore"));
            var res = await PostAsync<List<DomainBaseRiskScore>, ProcessesAssign>(requestUrl, model);
            return res.Data;
        }
        public async Task<Message<int>> SubmitDataByAnswer(DomainBaseRiskScore model)
        {
            Message<int> message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/SFDC/SubmitDataByAnswer"));
            message = await PostAsync<int, DomainBaseRiskScore>(requestUrl, model);
            return message;
        }

        public async Task<List<DomainBaseRiskScore>> GetDomainCritiScoreByfunIDAndProcessID(ProcessesAssign model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/SFDC/GetDomainCritiScoreByfunIDAndProcessID"));
            var res = await PostAsync<List<DomainBaseRiskScore>, ProcessesAssign>(requestUrl, model);
            return res.Data;
        }
        #endregion
    }
}
