﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region InternationalStandard
        public async Task<Message<InternationalStandard>> SaveInternationalStandard(InternationalStandard InternationalStandard)
        {
            Message<InternationalStandard> message = new Message<InternationalStandard>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/InternationalStandard/SaveInternationalStandard"));
            message = await PostAsync(requestUrl, InternationalStandard);
            return message;
        }
        public async Task<Message<InternationalStandard>> UpdateInternationalStandard(InternationalStandard InternationalStandard)
        {
            Message<InternationalStandard> message = new Message<InternationalStandard>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/InternationalStandard/UpdateInternationalStandard"));
            message = await PostAsync(requestUrl, InternationalStandard);
            return message;
        }
        public async Task<Message<InternationalStandard>> BlockDeleteInternationalStandard(InternationalStandard InternationalStandard)
        {
            try
            {
                Message<InternationalStandard> message = new Message<InternationalStandard>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/InternationalStandard/BlockDeleteInternationalStandard"));
                message = await PostAsync(requestUrl, InternationalStandard);
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task SaveOrganizationFunction(UserLogin model)
        {
            throw new NotImplementedException();
        }

        public async Task<List<InternationalStandard>> GetAllInternationalStandard()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/InternationalStandard/GetAllInternationalStandard"));
            var data = await GetAsync<List<InternationalStandard>>(requestUrl);
            return data;
        }
        public async Task<InternationalStandard> Edit(long id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/InternationalStandard/Edit"));
            return await GetAsync<InternationalStandard, long>(requestUrl, id, "id");
        }

        public async Task<List<InternationalStandard>> GetInternationalStandard()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/InternationalStandard/GetInternationalStandard"));
            var data = await GetAsync<List<InternationalStandard>>(requestUrl);
            return data;
        }
        public async Task<List<InternationalStandard>> GetInternationalStandardVersion(string StandardID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/InternationalStandard/GetInternationalStandardVersion"));
            var data = await GetAsync<List<InternationalStandard>,string>(requestUrl, StandardID, "StandardID");
            return data;
        }

        public async Task<List<InternationalStandard>> GetMappedStandardByICDId(long ICDId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/InternationalStandard/GetMappedStandardByICDId"));
            var data = await GetAsync<List<InternationalStandard>,long>(requestUrl, ICDId, "ICDId");
            return data;
        }
        #endregion
    }
}
