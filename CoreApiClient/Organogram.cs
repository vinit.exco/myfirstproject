﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region Organogram
        public async Task<List<Organization>> GetOrganogram()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetOrganization"));
            var data = await GetAsync<List<Organization>>(requestUrl);
            return data;
        }
        public async Task<Message<Organization>> SaveThreatIdentified(Organization organization)
        {
            Message<Organization> message = new Message<Organization>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/SaveThreatIdentified"));
            message = await PostAsync(requestUrl, organization);
            return message;
        }

        public async Task<List<Organization>> CheckThreatById(string OrgName, string ChildOrgName)
        {
            //try
            //{
            var model = new Organization();
            model.ParentOrgName = OrgName;
            model.ChildOrgName = ChildOrgName;
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/CheckThreatById"));
            var result = await PostAsync<List<Organization>, Organization>(requestUrl, model);
            return result.Data;
            //}
            //catch (System.Exception ex)
            //{
            //    throw ex.InnerException;
            //}
        }

        public async Task<Message<Organization>> UpdateAssigntoConsultant(Organization Organization)
        {
            Message<Organization> message = new Message<Organization>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/UpdateAssigntoConsultant"));
            message = await PostAsync(requestUrl, Organization);
            return message;
        }
        public async Task<Message<Organization>> SaveAssignedOrganizationUser(Organization Organization)
        {
            Message<Organization> message = new Message<Organization>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/SaveAssignedOrganizationUser"));
            message = await PostAsync(requestUrl, Organization);
            return message;
        }
        #endregion


        #region Child Organization
        public async Task<Message<int>> SubmitChildOrganization(OrganizationChild model)
        {
            Message<int> message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/SubmitChildOrganization"));
            message = await PostAsync<int, OrganizationChild>(requestUrl, model);
            return message;
        }

        public async Task<List<OrganizationChild>> GetChildCompanyListOnClientWise(int OrganizationId)
        {
            //try
            //{
            var model = new OrganizationChild();
            model.Id = OrganizationId;

            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetChildCompanyListOnClientWise"));
            var result = await PostAsync<List<OrganizationChild>, OrganizationChild>(requestUrl, model);
            return result.Data;
            //}
            //catch (System.Exception ex)
            //{
            //    throw ex.InnerException;
            //}
        }

        public async Task<List<OrganizationChild>> GetChildOrganizationList()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetChildOrganizationList"));
            var data = await GetAsync<List<OrganizationChild>>(requestUrl);
            return data;
        }

        public async Task<List<OrganizationChild>> GetChildOrganizationByParentOrgID(long ParentOrgID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetChildOrganizationByParentOrgID"));
            var data = await GetAsync<List<OrganizationChild>, long>(requestUrl, ParentOrgID, "ParentOrgID");
            return data;
        }

        public async Task<List<ClientAudity>> GetChildOrganizationAudityListByID(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetChildOrganizationAudityListByID"));
            var data = await GetAsync<List<ClientAudity>, long>(requestUrl, ID, "ID");
            return data;
        }

        public async Task<Message<int>> updateOrgSignStaus(OrganizationChild model)
        {
            Message<int> message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/updateOrgSignStaus"));
            message = await PostAsync<int, OrganizationChild>(requestUrl, model);
            return message;
        }
        #endregion



        #region Organization Function
        public async Task<Message<OrganizationFunctions>> SaveOrganizationFunction(OrganizationFunctions Model)
        {
            Message<OrganizationFunctions> message = new Message<OrganizationFunctions>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/SaveOrganizationFunction"));
            message = await PostAsync(requestUrl, Model);
            return message;
        }

        public async Task<List<OrganizationFunctions>> GetOrganizationFunctionList(int EmployeeId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetOrganizationFunctionList"));
            var data = await GetAsync<List<OrganizationFunctions>, int>(requestUrl, EmployeeId, "EmployeeId");
            return data;
        }

        public async Task<OrganizationFunctions> GetOrganizationFunctionByID(int ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetOrganizationFunctionByID"));
            var data = await GetAsync<OrganizationFunctions, int>(requestUrl, ID, "ID");
            return data;
        }

        public async Task<List<OrganizationFunctions>> GetFunctionByOrganization(long OrgID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetFunctionByOrganization"));
            var result = await GetAsync<List<OrganizationFunctions>, long>(requestUrl, OrgID, "OrgID");
            return result;
        }
        public async Task<List<RiskAssessment>> GetTaskCountByEmpId(int EmpId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetTaskCountByEmpId"));
            var data = await GetAsync<List<RiskAssessment>, int>(requestUrl, EmpId, "EmpId");
            return data;
        }
        public async Task<List<OrganizationFunctions>> GetFunctionByChildOrganization(long ChildId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetFunctionByChildOrganization"));
            var result = await GetAsync<List<OrganizationFunctions>, long>(requestUrl, ChildId, "ChildId");
            return result;
        }
        #endregion

        #region function Process
        public async Task<Message<OrganizationProcesss>> SubmitFunctionProcess(OrganizationProcesss Organization)
        {
            Message<OrganizationProcesss> message = new Message<OrganizationProcesss>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/SubmitFunctionProcess"));
            message = await PostAsync(requestUrl, Organization);
            return message;
        }

        public async Task<List<OrganizationProcesss>> GetFunctionProcessList()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetFunctionProcessList"));
            var result = await GetAsync<List<OrganizationProcesss>>(requestUrl);
            return result;
        }

        public async Task<OrganizationProcesss> GetFunctionProcessByID(int ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetFunctionProcessByID"));
            var data = await GetAsync<OrganizationProcesss, int>(requestUrl, ID, "ID");
            return data;
        }

        public async Task<List<FunctionProcess>> GetProcessesByOrgId(long orgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetProcessesByOrgId"));
            return await GetAsync<List<FunctionProcess>, long>(requestUrl, orgId, "orgId");
        }

        public async Task<List<FunctionProcess>> GetProcessesForMis(long orgId ,int FunctionId, int ProcessId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetProcessesForMis"));
            return await GetAsync<List<FunctionProcess>, long,int,int>(requestUrl, orgId, FunctionId, ProcessId, "orgId","FunctionId","ProcessId");
        }

        public async Task<List<FunctionProcess>> GetFunctionForMis(long orgId,long FunctionId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetFunctionForMis"));
            return await GetAsync<List<FunctionProcess>, long,long>(requestUrl, orgId, FunctionId,"orgId", "FunctionId");
        }

        public async Task<List<FunctionProcess>> GetOraganizationForMis(long orgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetOraganizationForMis"));
            return await GetAsync<List<FunctionProcess>, long>(requestUrl, orgId, "orgId");
        }


        public async Task<FunctionProcess> CheckFunProDuplicate(long orgId = 0, string FunProName = "")
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/CheckFunProDuplicate"));
            var data = await GetAsync<FunctionProcess, long, string>(requestUrl, orgId, FunProName, "orgId", "FunProName");
            return data;
        }
        public async Task<List<OrganizationProcesss>> GetProcesses(int employeeId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetProcesses"));
            var result = await GetAsync<List<OrganizationProcesss>, int>(requestUrl, employeeId, "employeeId");
            return result;
        }
        #endregion


        #region Compliance RCM
        public async Task<List<RiskAssessmentRCM>> GetQuestionBanklist()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetQuestionBanklist"));
            return await GetAsync<List<RiskAssessmentRCM>>(requestUrl);
        }

        public async Task<List<RiskAssessmentRCM>> GetQuestionStandardWise(string QuestionsID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetQuestionStandardWise"));
            return await GetAsync<List<RiskAssessmentRCM>, string>(requestUrl, QuestionsID, "QuestionsID");
        }

        public async Task<List<RiskAssessmentRCM>> GetQuestionRegoWise(string SectorId, string CountryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetQuestionRegoWise"));
            return await GetAsync<List<RiskAssessmentRCM>, string, string>(requestUrl, SectorId, CountryId, "SectorId", "CountryId");
        }

        public async Task<List<RiskAssessmentRCM>> GetRegulationBySectorAndCountryWise(string SectorId, string CountryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetRegulationBySectorAndCountryWise"));
            return await GetAsync<List<RiskAssessmentRCM>, string, string>(requestUrl, SectorId, CountryId, "SectorId", "CountryId");
        }

        public async Task<Message<RiskAssessmentRCM>> SubmitStandardRegulationRCM(RiskAssessmentRCM Model)
        {
            Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/SubmitStandardRegulationRCM"));
            message = await PostAsync(requestUrl, Model);
            return message;
        }

        //public async Task<Message<RiskAssessmentRCM>> SaveAssignRiskAssessment(RiskAssessmentRCM Model)
        //{
        //    Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
        //    var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
        //        "/api/Organogram/SaveAssignRiskAssessment"));
        //    message = await PostAsync(requestUrl, Model);
        //    return message;
        //}

        public async Task<List<RiskAssessmentRCM>> GetAssignRiskAssessmentList(int TypeID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetAssignRiskAssessmentList"));
            var data = await GetAsync<List<RiskAssessmentRCM>, int>(requestUrl, TypeID, "TypeID");
            return data;
        }

        public async Task<Message<RiskAssessmentRCM>> SubmitRCM(RiskAssessmentRCM Model)
        {
            Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/SubmitRCM"));
            message = await PostAsync(requestUrl, Model);
            return message;
        }
        public async Task<Message<RiskAssessmentRCM>> SaveUpdateRCMICD(RiskAssessmentRCM Model)
        {
            Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/SaveUpdateRCMICD"));
            message = await PostAsync(requestUrl, Model);
            return message;
        }

        public async Task<List<RiskAssessmentRCM>> GetRCMListByOrgID(long OrgID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetRCMListByOrgID"));
            return await GetAsync<List<RiskAssessmentRCM>, long>(requestUrl, OrgID, "OrgID");
        }
        public async Task<Message<RiskAssessmentRCM>> GenerateRCMVersion(RiskAssessmentRCM model)
        {
            Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GenerateRCMVersion"));
            message= await PostAsync(requestUrl,model);
            return message;
        }

        public async Task<Message<RCM>> SubmitRCMByExcel(RCM model)
        {
            var message = new Message<RCM>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/SubmitRCMByExcel"));
            message = await PostAsync(requestUrl, model);
            return message;
        }

        public async Task<List<ICD>> GetRCMControlListByOrg(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetRCMControlListByOrg"));
            return await GetAsync<List<ICD>, long>(requestUrl, ID, "ID");
        }
        public async Task<List<ICD>> GetControlLisCustomRCM(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetControlLisCustomRCM"));
            return await GetAsync<List<ICD>, long>(requestUrl, ID, "ID");
        }
        public async Task<List<ICD>> GetRCMControlListByStdRegOrg(long ID, string StandardIDs)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetRCMControlListByStdRegOrg"));
            return await GetAsync<List<ICD>, long, string>(requestUrl, ID, StandardIDs, "ID", "StandardIDs");
        }

        public async Task<Message<RiskAssessmentRCM>> DeleteRCM(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/DeleteRCM"));
            var data = await GetAsync<Message<RiskAssessmentRCM>, long>(requestUrl, ID, "ID");
            return data;
        }

        public async Task<List<ICD>> GetSubDomainRCMBID(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetSubDomainRCMBID"));
            return await GetAsync<List<ICD>, long>(requestUrl, ID, "ID");
        }
        public async Task<List<ICD>> GetSubDomainCustomRCM(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetSubDomainCustomRCM"));
            return await GetAsync<List<ICD>, long>(requestUrl, ID, "ID");
        }
        public async Task<List<ICD>> GetSubDomainStdRegRCMBID(long ID,string StandardIDs)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetSubDomainStdRegRCMBID"));
            return await GetAsync<List<ICD>, long,string>(requestUrl, ID, StandardIDs, "ID", "StandardIDs");
        }
        public async Task<RiskAssessmentRCM> GetRCMByID(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetRCMByID"));
            var data = await GetAsync<RiskAssessmentRCM, long>(requestUrl, ID, "ID");
            return data;
        }

        public async Task<Message<RiskAssessmentRCM>> DeleteControlByID(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/DeleteControlByID"));
            var data = await GetAsync<Message<RiskAssessmentRCM>, long>(requestUrl, ID, "ID");
            return data;
        }

        public async Task<RiskAssessmentRCM> GetRCMControlDetails(long ControlID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organogram/GetRCMControlDetails"));
            return await GetAsync<RiskAssessmentRCM, long>(requestUrl, ControlID, "ControlID");
        }


        #endregion
    }
}
