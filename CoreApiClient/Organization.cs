﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region New Organization
        public async Task<Message<Organization>> AddUpadeteOrganization(Organization Organization)
        {
            var message = new Message<Organization>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/AddUpadeteOrganization"));
            message = await PostAsync(requestUrl, Organization);
            return message;
        }
        public async Task<List<Organization>> GetOrganizationList()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetOrganizationList"));
            var data = await GetAsync<List<Organization>>(requestUrl);
            return data;
        }

        public async Task<List<Organization>> GetAssessmentOrganizationList(int AssesmentValue)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetAssessmentOrganizationList"));
            var data = await GetAsync<List<Organization>,int>(requestUrl, AssesmentValue, "AssesmentValue");
            return data;
        }

        public async Task<List<OrgAssignedAssessment>> AssessmentListByOrgId(long orgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/AssessmentListByOrgId"));
            var data = await GetAsync<List<OrgAssignedAssessment>, long>(requestUrl, orgId, "orgId");
            return data;
        }

        public async Task<List<OrganizationProcesss>> BindFunctionProcess(long OrgID, int FunId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/BindFunctionProcess"));
            var data = await GetAsync<List<OrganizationProcesss>, long,int>(requestUrl, OrgID, FunId, "OrgID", "FunId");
            return data;
        }
        public async Task<Message<Organization>> DeleteOrganizationById(long parentOrgId = 0)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/DeleteOrganizationById"));
            var data = await GetAsync<Message<Organization>, long>(requestUrl, parentOrgId, "parentOrgId");
            return data;
        }
        public async Task<Message<Organization>> CheckOrgDuplicate(string OrgName = "")
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/CheckOrgDuplicate"));
            var data = await GetAsync<Message<Organization>, string>(requestUrl, OrgName, "OrgName");
            return data;
        }
        #endregion

        #region Old Organization
        public async Task<Message<Organization>> SubmitOrganization(Organization Organization)
        {
            Message<Organization> message = new Message<Organization>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/SubmitOrganization"));
            message = await PostAsync(requestUrl, Organization);
            return message;
        }
        public async Task<Message<Organization>> BlockDeleteOrganization(Organization Organization)
        {
            Message<Organization> message = new Message<Organization>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/BlockDeleteOrganization"));
            message = await PostAsync(requestUrl, Organization);
            return message;
        }
        public async Task<List<Organization>> GetOrganizationByUserId(long userId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetOrganizationByUserId"));
            return await GetAsync<List<Organization>, long>(requestUrl, userId, "userId");
        }
        public async Task<List<Organization>> GetParentOrgListByEmployeeId(int EmployeeId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetParentOrgListByEmployeeId"));
            var data = await GetAsync<List<Organization>, int>(requestUrl, EmployeeId, "EmployeeId");
            return data;
        }
        public async Task<List<Organization>> GetChildOrgListByEmployeeId(int EmployeeId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetChildOrgListByEmployeeId"));
            var data = await GetAsync<List<Organization>, int>(requestUrl, EmployeeId, "EmployeeId");
            return data;
        }
        public async Task<List<OrganizationFunctions>> GetOrgFunctions(int employeeId, long id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetOrgFunctions"));
            var data = await GetAsync<List<OrganizationFunctions>, int, long>(requestUrl, employeeId, id, "employeeId", "id");
            return data;
        }
        public async Task<List<Consultant>> GetConsultants()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetConsultants"));
            var data = await GetAsync<List<Consultant>>(requestUrl);
            return data;
        }
        public async Task<List<Consultant>> GetAssignedConsultantsByOrgId(long parentOrgId, long childOrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetAssignedConsultantsByOrgId"));
            var data = await GetAsync<List<Consultant>, long, long>(requestUrl, parentOrgId, childOrgId, "parentOrgId", "childOrgId");
            return data;
        }
        public async Task<List<Consultant>> GetAssignedConsultantsByFunctionId(int functionId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetAssignedConsultantsByFunctionId"));
            var data = await GetAsync<List<Consultant>, int>(requestUrl, functionId, "functionId");
            return data;
        }
        public async Task<List<Consultant>> GetAssignedConsultantsByFunctionMapId(long functionId = 0)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetAssignedConsultantsByFunctionMapId"));
            var data = await GetAsync<List<Consultant>, long>(requestUrl, functionId, "functionId");
            return data;
        }
        public async Task<List<Consultant>> GetAssignedConsultantsByProcessId(int processId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetAssignedConsultantsByProcessId"));
            var data = await GetAsync<List<Consultant>, int>(requestUrl, processId, "processId");
            return data;
        }
        public async Task<Message<int>> CheckParentOrgName(Organization organization)
        {
            Message<int> message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/CheckParentOrgName"));
            message = await PostAsync<int, Organization>(requestUrl, organization);
            return message;
        }
        public async Task<List<Organization>> GetOrganizationByEmplyoyeeID(int EmployeeId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetOrganizationByEmplyoyeeID"));
            var data = await GetAsync<List<Organization>, int>(requestUrl, EmployeeId, "EmployeeId");
            return data;
        }
        public async Task<List<Organization>> GetOrganizationNotAssigned()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetOrganizationNotAssigned"));
            var data = await GetAsync<List<Organization>>(requestUrl);
            return data;
        }
        public async Task<List<Organization>> AssignedOrgList()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/AssignedOrgList"));
            return await GetAsync<List<Organization>>(requestUrl);
        }
        public async Task<List<Organization>> GetAssignedOrganizationByEmplyoyeeID(int EmployeeId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetAssignedOrganizationByEmplyoyeeID"));
            var data = await GetAsync<List<Organization>, int>(requestUrl, EmployeeId, "EmployeeId");
            return data;
        }
        public async Task<List<ClientAudity>> GetOrganizationAudityListByID(long ID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetOrganizationAudityListByID"));
            var data = await GetAsync<List<ClientAudity>, long>(requestUrl, ID, "ID");
            return data;
        }
        public async Task<List<Organization>> EditAssignedOrg(long OrgID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/EditAssignedOrg"));
            var data = await GetAsync<List<Organization>, long>(requestUrl, OrgID, "OrgID");
            return data;
        }
        #endregion
    }
}