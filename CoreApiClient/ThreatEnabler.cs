﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        public async Task<List<ThreatEnabler>> GetAllThreats()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/GetAllThreats"));
            var data = await GetAsync<List<ThreatEnabler>>(requestUrl);
            return data;
        }
        public async Task<List<ThreatEnabler>> GetAllEnumTypes(long Id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/GetAllEnumTypes"));
            return await GetAsync<List<ThreatEnabler>,long>(requestUrl,Id,"Id");
        }
        public async Task<List<ThreatEnabler>> GetTypeOfCategory(int OrgID, int ChilOrgID,long CatID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/GetTypeOfCategory"));
            return await GetAsync<List<ThreatEnabler>, int,int,long>(requestUrl, OrgID, ChilOrgID, CatID, "OrgID", "ChilOrgID", "CatID");
        }
        public async Task<Message<ThreatEnabler>> SubmitThreatsEnabler(ThreatEnabler model)
        {
            Message<ThreatEnabler> message = new Message<ThreatEnabler>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/SubmitThreatsEnabler"));
            message = await PostAsync(requestUrl, model);
            return message;
        }
        public async Task<List<ThreatEnabler>> GetFirstQuesAnswersByConCatId(int controlCategoryId, long OrganizationId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Threat/GetFirstQuesAnswersByConCatId"));
            var data = await GetAsync<List<ThreatEnabler>, int, long>(requestUrl, controlCategoryId, OrganizationId, "controlCategoryId", "OrganizationId");
            return data;
        }
        public async Task<Message<ThreatEnablers>> SubmitThreatEnablerScore(ThreatEnablers model)
        {
            var message = new Message<ThreatEnablers>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/SubmitThreatEnablerScore"));
            message = await PostAsync(requestUrl, model);
            return message;
        }
        public async Task<List<ThreatEnabler>> GetThreatEnablerByParentIDAndChildId(long ParentOrgId, long ChildOrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/GetThreatEnablerByParentIDAndChildId"));
            var data = await GetAsync<List<ThreatEnabler>, long, long>(requestUrl, ParentOrgId, ChildOrgId, "ParentOrgId", "ChildOrgId");
            return data;
        }
        public async Task<List<ThreatEnabler>> GetThreatEnablerMapByParentIDAndChildId(long ParentOrgId, long ChildOrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ThreatEnabler/GetThreatEnablerMapByParentIDAndChildId"));
            var data = await GetAsync<List<ThreatEnabler>, long, long>(requestUrl, ParentOrgId, ChildOrgId, "ParentOrgId", "ChildOrgId");
            return data;
        }
    }
}
