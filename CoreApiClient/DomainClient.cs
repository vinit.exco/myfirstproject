﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        public async Task<Message<Domain>> SaveDomain(Domain domain)
        {
            Message<Domain> message = new Message<Domain>();
            domain.CreatedBy = 1;
            domain.CreatedOn = DateTime.Now;
            domain.Deleted = false;
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Domain/CreateDomain"));
            message = await PostAsync(requestUrl, domain);
            return message;
        }
        public async Task<List<Domain>> GetAllDomain()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Domain/GetAllDomain"));
            var data = await GetAsync<List<Domain>>(requestUrl);
            return data;
        }

        public async Task<Message<Domain>> BlockDeleteDomain(Domain domain)
        {
            try
            {
                Message<Domain> message = new Message<Domain>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/Domain/BlockDeleteDomain"));
                message = await PostAsync(requestUrl, domain);
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> CheckCode(string code)
        {
            try
            {
                Message<Domain> message = new Message<Domain>();
                var domain = new Domain();
                domain.Code = code;
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/Domain/CheckCode"));
                message = await PostAsync(requestUrl, domain);
                return message.IsSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> CheckSubDomain(string SubDomain)
        {
            try
            {
                Message<Domain> message = new Message<Domain>();
                var domain = new Domain();
                domain.SubDomain = SubDomain;
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/Domain/CheckSubDomain"));
                var result = await PostAsync(requestUrl, domain);
                return message.IsSuccess = result.IsSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Domain>> GetDomain()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Domain/GetDomain"));
            var data = await GetAsync<List<Domain>>(requestUrl);
            return data;
        }

        public async Task<List<Domain>> GetDomainList()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Domain/GetDomainList"));
            var data = await GetAsync<List<Domain>>(requestUrl);
            return data;
        }
        public async Task<Message<Domain>> DomainSave(Domain domain)
        {
            Message<Domain> message = new Message<Domain>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Domain/DomainSave"));
            return await PostAsync(requestUrl, domain);
        }
        public async Task<bool> CheckDomain(string DomainName)
        {
            Message<Domain> message = new Message<Domain>();
            var model = new Domain();
            model.DomainName = DomainName;
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Domain/CheckDomain"));
            var result =  await PostAsync(requestUrl,model);
            return message.IsSuccess = result.IsSuccess;
        }
        public async Task<bool> CheckDomainCode(string DomainCode)
        {
            Message<Domain> message = new Message<Domain>();
            var model = new Domain();
            model.DomainCode = DomainCode;
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Domain/CheckDomainCode"));
            var result = await PostAsync(requestUrl, model);
            return message.IsSuccess = result.IsSuccess;
        }
    }
}
