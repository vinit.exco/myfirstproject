﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CyberaniumModels;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region DomainThreatMapping
        public async Task<Message<List<DomainThreatMapping>>> GetThreatByDomainId(DomainThreatMapping data)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/DomainThreatMapping/GetThreatByDomainId"));
            var result = await PostAsync<List<DomainThreatMapping>, DomainThreatMapping>(requestUrl, data);
            return result;
        }
        public async Task<Message<DomainThreatMapping>> SubmitDomainThreatMapping(DomainThreatMapping data)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/DomainThreatMapping/SubmitDomainThreatMapping"));
            var result = await PostAsync(requestUrl, data);
            return result;
        }
        public async Task<Message<DomainThreatMapping>> BlockDeleteDomainThreatMapping(DomainThreatMapping data)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/DomainThreatMapping/BlockDeleteDomainThreatMapping"));
            var result = await PostAsync(requestUrl, data);
            return result;
        }
        public async Task<List<DomainThreatMapping>> GetDomainThreatMapList(string domainIds)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/DomainThreatMapping/GetDomainThreatMapList"));
            var data = await GetAsync<List<DomainThreatMapping>, string>(requestUrl, domainIds, "domainIds");
            return data;
        }
        #endregion
    }
}
