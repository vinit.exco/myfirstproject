﻿using CyberaniumModels;
using CyberaniumModels.Comman;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        #region IRA
        public async Task<Message<IRA>> SaveAssignedProcess(IRA model)
        {
            try
            {
                var message = new Message<IRA>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/IRA/SaveAssignedProcess"));
                message = await PostAsync(requestUrl, model);
                return message;
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                throw;
            }
        }
        public async Task<List<IRA>> GetEnablerAssessmentByEmpId(int EmpId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetEnablerAssessmentByEmpId"));
            var data = await GetAsync<List<IRA>, int>(requestUrl, EmpId, "EmpId");
            return data;
        }
        public async Task<List<IRA>> GetComplianceRCMByEmpId(int EmpId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetComplianceRCMByEmpId"));
            var data = await GetAsync<List<IRA>, int>(requestUrl, EmpId, "EmpId");
            return data;
        }
        public async Task<IRA> GetAssignedUserByProcessesId(long processId = 0)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetAssignedUserByProcessesId"));
            return await GetAsync<IRA, long>(requestUrl, processId, "processId");
        }
        public async Task<List<DomainCriticalityScore>> GetDomainCriticalityScoreByProcessesId(long orgId, long functionId, long processId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetDomainCriticalityScoreByProcessesId"));
            return await GetAsync<List<DomainCriticalityScore>, long, long, long>(requestUrl, orgId, functionId, processId, "orgId", "functionId", "processId");
        }
        public async Task<List<CyberaniumModels.Enum>> GetEnumsByEnumTypeId(long enumTypeId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetEnumsByEnumTypeId"));
            return await GetAsync<List<CyberaniumModels.Enum>, long>(requestUrl, enumTypeId, "enumTypeId");
        }
        public async Task<Message<int>> SubmitDomainCriticalityScore(DomainCriticalityScore model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SubmitDomainCriticalityScore"));
            message = await PostAsync<int, DomainCriticalityScore>(requestUrl, model);
            return message;
        }
        public async Task<Message<int>> SubmitProcessEnablerMap(Base model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SubmitProcessEnablerMap"));
            message = await PostAsync<int, Base>(requestUrl, model);
            return message;
        }
        public async Task<List<long>> GetQuestionnaireIdByProcessId(long orgId = 0, long functionId = 0, long processId = 0, int conCatId = 0)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetQuestionnaireIdByProcessId"));
            return await GetAsync<List<long>, long, long, long, int>(requestUrl, orgId, functionId, processId, conCatId, "orgId", "functionId", "processId", "conCatId");
        }
        public async Task<Message<IRA>> SaveAssessmentScope(IRA model)
        {
            try
            {
                var message = new Message<IRA>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/IRA/SaveAssessmentScope"));
                message = await PostAsync(requestUrl, model);
                return message;
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                throw;
            }
        }
        public async Task<Message<IRA>> SaveComplianceRCM(IRA model)
        {
            try
            {
                var message = new Message<IRA>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/IRA/SaveComplianceRCM"));
                message = await PostAsync(requestUrl, model);
                return message;
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                throw;
            }
        }
        public async Task<Message<IRA>> SaveCustomRCM(IRA model)
        {
            try
            {
                var message = new Message<IRA>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/IRA/SaveCustomRCM"));
                message = await PostAsync(requestUrl, model);
                return message;
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                throw;
            }
        }
        public async Task<Message<IRA>> SaveStandardAndRegulationRCM(IRA model)
        {
            try
            {
                var message = new Message<IRA>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/IRA/SaveStandardAndRegulationRCM"));
                message = await PostAsync(requestUrl, model);
                return message;
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                throw;
            }
        }

        public async Task<List<IRA>> getAssessmentScope(long OrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/IRA/getAssessmentScope"));
            var data = await GetAsync<List<IRA>, long>(requestUrl, OrgId, "OrgId");
            return data;
        }
        public async Task<List<IRA>> GetComplianceRCM(long OrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/IRA/GetComplianceRCM"));
            var data = await GetAsync<List<IRA>, long>(requestUrl, OrgId, "OrgId");
            return data;
        }
        public async Task<List<IRA>> GetCustomRCM(long OrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/IRA/GetCustomRCM"));
            var data = await GetAsync<List<IRA>, long>(requestUrl, OrgId, "OrgId");
            return data;
        }
        public async Task<List<IRA>> GetStandardAndRegulationRCM(long OrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/IRA/GetStandardAndRegulationRCM"));
            var data = await GetAsync<List<IRA>, long>(requestUrl, OrgId, "OrgId");
            return data;
        }
        public async Task<List<ClientEnablers>> GetEnablerCategoryByOrgID(long OrgID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetEnablerCategoryByOrgID"));
            return await GetAsync<List<ClientEnablers>, long>(requestUrl, OrgID, "OrgID");
        }

        public async Task<List<ClientEnablers>> GetConCatListByIds(long orgId, int functionId, int processId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetConCatListByIds"));
            return await GetAsync<List<ClientEnablers>, long, int, int>(requestUrl, orgId, functionId, processId, "orgId", "functionId", "processId");
        }

        public async Task<List<ClientEnablers>> GetDomainBaseRiskScore(long orgId, int functionId, int processId, int ConCatId, int QuestionnaireId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetDomainBaseRiskScore"));
            return await GetAsync<List<ClientEnablers>, long, int, int, int, int>(requestUrl, orgId, functionId, processId, QuestionnaireId, ConCatId, "orgId", "functionId", "processId", "QuestionnaireId", "ConCatId");
        }

        public async Task<Message<IRA>> SaveEnablerAssessment(IRA model)
        {
            try
            {
                var message = new Message<IRA>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/IRA/SaveEnablerAssessment"));
                message = await PostAsync(requestUrl, model);
                return message;
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                throw;
            }
        }

        public async Task<List<IRA>> GetAssignedEnalbersDetails(long QuestionnaireID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetAssignedEnalbersDetails"));
            var data = await GetAsync<List<IRA>, long>(requestUrl, QuestionnaireID, "QuestionnaireID");
            return data;
        }

        public async Task<List<IRA>> GetAssignedEnablerCategory(long UserID, long orgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetAssignedEnablerCategory"));
            var data = await GetAsync<List<IRA>, long, long>(requestUrl, UserID, orgId, "UserID", "orgId");
            return data;
        }

        public async Task<List<IRA>> GetAssignedEnalbersCategoryType(long UserID, long orgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetAssignedEnalbersCategoryType"));
            var data = await GetAsync<List<IRA>, long, long>(requestUrl, UserID, orgId, "UserID", "orgId");
            return data;
        }

        public async Task<List<IRA>> GetAssignedEnablerByOrgAndCategory(long OrgId, long CategoryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetAssignedEnablerByOrgAndCategory"));
            var data = await GetAsync<List<IRA>, long, long>(requestUrl, OrgId, CategoryId, "OrgId", "CategoryId");
            return data;
        }

        public async Task<List<IRA>> GetAllThreadsByEnablerCategory(string EnablerCategory)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetAllThreadsByEnablerCategory"));
            var data = await GetAsync<List<IRA>, string>(requestUrl, EnablerCategory, "EnablerCategory");
            return data;
        }
        public async Task<Message<int>> UpadateProcess(FunctionProcess model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/UpadateProcess"));
            message = await PostAsync<int, FunctionProcess>(requestUrl, model);
            return message;
        }

        public async Task<List<RiskAssessment>> GetRCMControlByOrgLatest(long OrgID, long QuestionnaireID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetRCMControlByOrgLatest"));
            return await GetAsync<List<RiskAssessment>, long, long>(requestUrl, OrgID, QuestionnaireID, "OrgID", "QuestionnaireID");
        }
        public async Task<List<UserLogin>> GetClientUser()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetClientUser"));
            return await GetAsync<List<UserLogin>>(requestUrl);
        }
        public async Task<DateTime> GetEndDateOfAS(long OrgId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetEndDateOfAS"));
            return await GetAsync<DateTime,long>(requestUrl, OrgId, "OrgId");
        }
        public async Task<Message<int>> SubmitThreatProbabilityScore(Base model)
        {
            try
            {
                var message = new Message<int>();
                var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    "/api/IRA/SubmitThreatProbabilityScore"));
                message = await PostAsync<int, Base>(requestUrl, model);
                return message;
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                throw;
            }
        }
        public async Task<List<ThreatEnabler>> GetThreatEnablerListByOrgId(long orgId, int conCatId, long questionnaireId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetThreatEnablerListByOrgId"));
            var data = await GetAsync<List<ThreatEnabler>, long, int, long>(requestUrl, orgId, conCatId, questionnaireId, "orgId", "conCatId", "questionnaireId");
            return data;
        }

        public async Task<Message<int>> SaveUpdateControlAssessment(RiskAssessment model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SaveUpdateControlAssessment"));
            message = await PostAsync<int, RiskAssessment>(requestUrl, model);
            return message;
        }
        public async Task<Message<int>> SaveUpdateArtefacts(RiskAssessment model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SaveUpdateArtefacts"));
            message = await PostAsync<int, RiskAssessment>(requestUrl, model);
            return message;
        }
        public async Task<Message<int>> SaveComment(RiskAssessment model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SaveComment"));
            message = await PostAsync<int, RiskAssessment>(requestUrl, model);
            return message;
        }
        public async Task<List<RiskAssessment>> GetAllComments(long OrgId, long CategoryId, long QuestionnaireID, long CreatedBy)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetAllComments"));
            var res = await GetAsync<List<RiskAssessment>, long, long, long, long>(requestUrl, OrgId, CategoryId, QuestionnaireID, CreatedBy, "OrgId", "CategoryId", "QuestionnaireID", "CreatedBy");
            return res;
        }
        public async Task<Message<RiskAssessment>> SaveApprovalfor(RiskAssessment RiskAssessment)
        {
            Message<RiskAssessment> message = new Message<RiskAssessment>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SaveApprovalfor"));
            message = await PostAsync(requestUrl, RiskAssessment);
            return message;
        }
        public async Task<int> RemoveTask(long OrgId, long CategoryId, long QuestionnaireID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/RemoveTask"));
            return await GetAsync<int, long, long, long>(requestUrl, OrgId, CategoryId, QuestionnaireID, "OrgID", "CategoryId", "QuestionnaireID");
        }

        public async Task<List<RiskAssessment>> GetControlAssessmentListByOrg(long OrgID, long QuestionnaireID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetControlAssessmentListByOrg"));
            return await GetAsync<List<RiskAssessment>, long, long>(requestUrl, OrgID, QuestionnaireID, "OrgID", "QuestionnaireID");
        }
        public async Task<Base> GetApprovalFor(long OrgID, long QuestionnaireID, long CategoryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetApprovalFor"));
            var res = await GetAsync<Base, long, long, long>(requestUrl, OrgID, QuestionnaireID, CategoryId, "OrgID", "QuestionnaireID", "CategoryId");
            return res;
        }
        public async Task<List<Threat>> GetThreatListById(long orgId, int categoryId, long questionnaireId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetThreatListById"));
            return await GetAsync<List<Threat>, long, int, long>(requestUrl, orgId, categoryId, questionnaireId, "orgId", "categoryId", "questionnaireId");
        }
        public async Task<List<RiskAssessment>> GetControlListByIds(long orgId = 0, int conCatId = 0, long questionnaireId = 0)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetControlListByIds"));
            return await GetAsync<List<RiskAssessment>, long, int, long>(requestUrl, orgId, conCatId, questionnaireId, "orgId", "conCatId", "questionnaireId");
        }

        public async Task<Message<IRA>> SubmitComplainceAssessmentRCM(IRA Model)
        {
            Message<IRA> message = new Message<IRA>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SubmitComplainceAssessmentRCM"));
            message = await PostAsync(requestUrl, Model);
            return message;
        }
        public async Task<Message<IRA>> SubmitCustomRCM(IRA Model)
        {
            Message<IRA> message = new Message<IRA>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SubmitCustomRCM"));
            message = await PostAsync(requestUrl, Model);
            return message;
        }
        public async Task<Message<IRA>> SubmitStandardAndRegulationAssessmentRCM(IRA Model)
        {
            Message<IRA> message = new Message<IRA>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SubmitStandardAndRegulationAssessmentRCM"));
            message = await PostAsync(requestUrl, Model);
            return message;
        }

        public async Task<List<RiskAssessment>> GetControlAssessmentControl(long OrgID, long QuestionnaireID)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetControlAssessmentControl"));
            return await GetAsync<List<RiskAssessment>, long, long>(requestUrl, OrgID, QuestionnaireID, "OrgID", "QuestionnaireID");
        }

        public async Task<Message<int>> SaveUpdateRiskTreatmentPlan(RiskAssessment model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SaveUpdateRiskTreatmentPlan"));
            message = await PostAsync<int, RiskAssessment>(requestUrl, model);
            return message;
        }
        public async Task<List<CyberaniumModels.Enum>> GetRiskDomainListByOrgId(long orgId = 0)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/GetRiskDomainListByOrgId"));
            return await GetAsync<List<CyberaniumModels.Enum>, long>(requestUrl, orgId, "orgId");
        }
        public async Task<Message<int>> SubmitDomainBaseRiskScore(DomainBaseRiskScore model)
        {
            var message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/IRA/SubmitDomainBaseRiskScore"));
            message = await PostAsync<int, DomainBaseRiskScore>(requestUrl, model);
            return message;
        }
        #endregion
    }
}
