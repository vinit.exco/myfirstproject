﻿using CyberaniumModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ProgrammingInterface
    {
        public async Task<List<OrganizationFunctions>> GetFuncation(long id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/ProcessEnabler/GetFuncation"));
            var data = await GetAsync<List<OrganizationFunctions>, long>(requestUrl, id, "OrgId");
            return data;
        }
        public async Task<List<OrganizationProcesss>> GetProcess(long Orgid)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/ProcessEnabler/GetProcess"));
            var data = await GetAsync<List<OrganizationProcesss>, long>(requestUrl, Orgid, "Orgid");
            return data;
        }
        public async Task<List<OrganoGrams>> GetEnabler(long Orgid, long ChildOrgId, long CategoryId)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/ProcessEnabler/GetEnabler"));
            var data = await GetAsync<List<OrganoGrams>, long, long, long>(requestUrl, Orgid, ChildOrgId, CategoryId, "Orgid", "ChildOrgId", "CategoryId");
            return data;
        }
        public async Task<List<OrganoGrams>> GetCategoryDetails(long id)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/ProcessEnabler/GetCategoryDetails"));
            var data = await GetAsync<List<OrganoGrams>, long>(requestUrl, id, "id");
            return data;
        }
        public async Task<Message<OrganoGrams>> PostProcess(OrganoGrams organoGrams)
        {
            Message<OrganoGrams> message = new Message<OrganoGrams>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ProcessEnabler/PostProcess"));
            message = await PostAsync(requestUrl, organoGrams);
            return message;
        }
        public async Task<List<OrganoGrams>> GetEnablerList(long Orgid)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
            "/api/ProcessEnabler/GetEnablerList"));
            var data = await GetAsync<List<OrganoGrams>, long>(requestUrl, Orgid, "Orgid");
            return data;
        }
        public async Task<List<RiskDomain>> GetRiskDomainsByOrgId(RiskDomain model)
        {
            var res = new Message<List<RiskDomain>>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/GetRiskDomainsByOrgId"));
            res = await PostAsync<List<RiskDomain>, RiskDomain>(requestUrl, model);
            return res.Data;
        }
        public async Task<Message<int>> SubmitDomainCriticalityScore(RiskDomain model)
        {
            Message<int> message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/Organization/SubmitDomainCriticalityScore"));
            message = await PostAsync<int, RiskDomain>(requestUrl, model);
            return message;
        }
        public async Task<Message<ProcessEnabler>> SubmitProcessEnabler(ProcessEnabler model)
        {
            var message = new Message<ProcessEnabler>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ProcessEnabler/SubmitProcessEnabler"));
            message = await PostAsync(requestUrl, model);
            return message;
        }
        public async Task<List<DomainCriticalityScore>> GetDomainCriticalityScoreByProcessId(ProcessesAssign model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ProcessEnabler/GetDomainCriticalityScoreByProcessId"));
            var res = await PostAsync<List<DomainCriticalityScore>, ProcessesAssign>(requestUrl, model);
            return res.Data;
        }
        public async Task<Message<int>> SubmitDCEnablerFinalScore(ProcessesAssign model)
        {
            Message<int> message = new Message<int>();
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "/api/ProcessEnabler/SubmitDCEnablerFinalScore"));
            message = await PostAsync<int, ProcessesAssign>(requestUrl, model);
            return message;
        }
    }
}


//using CyberaniumModels;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace CoreApiClient
//{
//    public partial class ProgrammingInterface
//    {
//        public async Task<List<OrganoGrams>> GetFuncation(long id)
//        {
//            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
//            "/api/ProcessEnabler/GetFuncation"));
//            var data = await GetAsync<List<OrganoGrams>, long>(requestUrl, id, "OrgId");
//            return data;
//        }
//        public async Task<List<OrganoGrams>> GetProcess(long Orgid)
//        {
//            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
//            "/api/ProcessEnabler/GetProcess"));
//            var data = await GetAsync<List<OrganoGrams>, long>(requestUrl, Orgid, "Orgid");
//            return data;
//        }
//        public async Task<List<OrganoGrams>> GetEnabler(long Orgid, long ChildOrgId, long CategoryId)
//        {
//            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
//            "/api/ProcessEnabler/GetEnabler"));
//            var data = await GetAsync<List<OrganoGrams>, long,long,long>(requestUrl, Orgid, ChildOrgId, CategoryId, "Orgid", "ChildOrgId", "CategoryId");
//            return data;
//        }
//        public async Task<List<OrganoGrams>> GetCategoryDetails(long id)
//        {
//            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
//            "/api/ProcessEnabler/GetCategoryDetails"));
//            var data = await GetAsync<List<OrganoGrams>, long>(requestUrl, id, "id");
//            return data;
//        }
//        public async Task<Message<OrganoGrams>> PostProcess(OrganoGrams organoGrams)
//        {
//            Message<OrganoGrams> message = new Message<OrganoGrams>();
//            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
//                "/api/ProcessEnabler/PostProcess"));
//            message = await PostAsync(requestUrl, organoGrams);
//            return message;
//        }
//    }
//}
