﻿using CyberaniumModels.Enumeration;
using System.Collections.Generic;
using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;

namespace CyberaniumModels
{
    public class Organization : Base
    {
        public List<Organization> employeeList;

        public long Id { get; set; }
        public long ChildOrgId { get; set; }
        public long SubChildOrgId { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string OrganizationXML { get; set; }
        public List<Organization> OrganizationList { get; set; }
        public long OrganizationId { get; set; }
        public string ParentOrgName { get; set; }
        public string CorporateOfficeLocation { get; set; }
        public string SectorIds { get; set; }
        public int RiskDomain { get; set; }
        public int ISoStandard { get; set; }
        public string CountryIds { get; set; }
        public string ChildOrgName { get; set; }
        public string ChildOrgLocation { get; set; }
        public string ComplianceIds { get; set; }
        public string QuestionnaireXML { get; set; }
        public string ChildOrgXML { get; set; }
        public string FunctionProcessXML { get; set; }
        public long ControlCategory { get; set; }
        // public List<Enum> Enums { get; set; }
        // public List<Country> Countries { get; set; }
        public SelectList DdlCountry { get; set; }
        public SelectList DdlRiskDomain { get; set; }
        public SelectList DdlInternational { get; set; }
        public SelectList DdlSector { get; set; }
        public SelectList DdlControlCat { get; set; }
        public List<Compliance> Compliances { get; set; }
        public SelectList DdlOrganization { get; set; }
        public SelectList DdlParentOrg { get; set; }
        public SelectList DdlRole { get; set; }
        public SelectList UserSelectList { get; set; }
        public long OrgID { get; set; }
        public DateTime AssignDate { get; set; }
        public int RoleId { get; set; }
        public long CategoryId { get; set; }
        public string UserName { get; set; }
        public long EmployeId { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public long StateId { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public SelectList DdlThreat { get; set; }
        public string Probability { get; set; }
        public long ThreatId { get; set; }
        public string uniqueId { get; set; }
        public List<OrganizationChild> OrganizationChildList { get; set; }
        public int CountryID2 { get; set; }
        public int CountryID3 { get; set; }
        public long StateId2 { get; set; }
        public int CountryID { get; set; }
        public SelectList DdlState { get; set; }
        public long CityId { get; set; }
        public SelectList DdlCity { get; set; }
        public string xmlAudity { get; set; }
        public List<ClientAudity> AudityList { get; set; }
        public string ConsultantName { get; set; }
        public string ConsultantIds { get; set; }
        public string ClientAdminIds { get; set; }
        public int AssignedId { get; set; }
        public string AssgnedIDs { get; set; }
        public string OrgName { get; set; }
        public string EmployeeName { get; set; }
        public string AssignToAdminclient { get; set; }

    }

    public class OrganoGrams : Base
    {
        public long Id { get; set; }
        public long FunctionId { get; set; }
        public long ProcessId { get; set; }
        public long ControlCategoyID { get; set; }
        public string ChildOrgName { get; set; }
        public string ChildOrgLocation { get; set; }
        public string ChildOrgXML { get; set; }
        public List<Organization> OrganizationsList { get; set; }
        public long ChildOrgId { get; set; }
        public SelectList DdlChildOrg { get; set; }
        public List<Enum> EnumTypeList { get; set; }
        public long EnablerId { get; set; }
        public List<OrganizationProcesss> ProcessList { get; set; }
        public List<OrganoGrams> ProcessEnablerList { get; set; }
        public long ParentOrgId { get; set; }
        public int RoleId { get; set; }
        public long StateId { get; set; }
        public int CountryID { get; set; }
        public long CityId { get; set; }
    }

    public class OrganizationChild : Base
    {
        public int Id { get; set; }
        public int OrganizationId { get; set; }
        public string ChildOrgName { get; set; }
        public string ChildOrgLocation { get; set; }
        public List<DomainBaseRiskScore> DomainBaseRiskScoreList { get; set; }
        public List<OrganizationChild> ChildOrganizationList { get; set; }
        public SelectList DdlState { get; set; }
        public SelectList DdlCity { get; set; }
        public SelectList DdlCountry { get; set; }
        public SelectList DdlParentOrg { get; set; }
        public SelectList DdlRole { get; set; }
        public long ParentOrgId { get; set; }
        public int CountryID { get; set; }
        public long StateId { get; set; }
        public long CityId { get; set; }
        public string ParentOrgName { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public int CountryID2 { get; set; }
        public int CountryID3 { get; set; }
        public long StateId2 { get; set; }
        public List<ClientAudity> AudityList { get; set; }
        public string xmlAudity { get; set; }
        public string ConsultantIds { get; set; }
        public List<Organization> OrganizationList { get; set; }
        public List<Enum> EnumList { get; set; }
        public int AssessmentId { get; set; }
        public int AssessmentValue { get; set; }
        public string AssessmentType { get; set; }
        public List<OrgAssignedAssessment> OrgAssignedAssessmentList { get; set; }
    }

    public class OrganizationFunctions : Base
    {
        public int Id { get; set; }
        public int OrganizationId { get; set; }
        public string FunctionName { get; set; }
        public string FunctionOwner { get; set; }
        public string FunctionDescription { get; set; }
        public long ParentOrgId { get; set; }
        public SelectList DdlParentOrg { get; set; }
        public SelectList DdlChildOrg { get; set; }
        public long ChildOrgId { get; set; }
        public string xmlFunction { get; set; }
        public string Xml { get; set; }
        public List<OrganizationFunctions> OrgFunctionList { get; set; }
        public List<OrganizationFunctions> OrgFunctionListMap { get; set; }
        public string ParentOrgName { get; set; }
        public string ChildOrgName { get; set; }
        public string Consultants { get; set; }
        public List<Consultant> ConsultantList { get; set; }
        public long OrgID { get; set; }
        public SelectList DdlOrganization { get; set; }

    }

    public class OrganizationProcesss : Base
    {
        public int Id { get; set; }
        public int FunctionId { get; set; }
        public string ProcessIdentified { get; set; }
        public string FunctionalOwner { get; set; }
        public string FunctionName { get; set; }
        public string ParentOrgName { get; set; }
        public string ChildOrgName { get; set; }
        public string Description { get; set; }
        public SelectList DdlParentOrg { get; set; }
        public long ParentOrgId { get; set; }
        public int? ChildOrgId { get; set; }
        public SelectList DdlChildOrg { get; set; }
        public int RoleId { get; set; }
        public SelectList DdlRole { get; set; }
        public SelectList DdlProcessOwner { get; set; }
        public string xmlProcess { get; set; }
        public int IsType { get; set; }
        public SelectList DdlFunction { get; set; }
        public List<OrganizationProcesss> FunctionProcessList { get; set; }
        public List<OrganizationProcesss> FunctionProcessMapList { get; set; }
        public int ConsultantId { get; set; }
        public List<Consultant> ConsultantList { get; set; }
    }

    public class FunctionProcess : Base
    {
        public int Id { get; set; }
        public int FunctionID { get; set; }
        public long OrgID { get; set; }
        public string FunProName { get; set; }
        public string ProcessName { get; set; }
        public string ProcessOwner { get; set; }
        public string ProcessDescription { get; set; }
        public long AssignToUserId { get; set; }
        public int CountFunction { get; set; }
        public int CountProcess { get; set; }
        public string CreatedYear { get; set; }
        public decimal Score { get; set; }
        public int ProcessId { get; set; }
        public string FinYear { get; set; }

    }

    public class ClientEnablers : Base
    {
        public int OrganizationId { get; set; }
        public int Id { get; set; }
        public long ParentOrgId { get; set; }
        public long ChildOrgId { get; set; }
        public string QuestionnaireXML { get; set; }
        public string PeopleGovnXML { get; set; }
        public List<Organization> OrganizationsList { get; set; }
        public SelectList DdlOrganizations { get; set; }
        public SelectList DdlControlCat { get; set; }
        public int? CategoryId { get; set; }
        public long? QuestionId { get; set; }
        public string Answer { get; set; }
        public string ParentOrgName { get; set; }
        public string CorporateOfficeLocation { get; set; }
        public string CategoryName { get; set; }
        public string Question { get; set; }
        public List<Organization> OrganizationList { get; set; }
        public List<Enum> EnumList { get; set; }
        public List<Enum> EnumList2 { get; set; }
        public int RowNo { get; set; }
        public List<ClientEnablers> EnumList3 { get; set; }
        public decimal Score { get; set; }
        public int RiskDomainId { get; set; }
    }

    public class GetEnablerQuestion : Base
    {
        public long Id { get; set; }
        public string Query { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public long QuestionnaireId { get; set; }
        public long OrganizationId { get; set; }
        public long CategoryId { get; set; }
        public long QuestionId { get; set; }
        public string Answer { get; set; }
        public string ParentOrgName { get; set; }
        public string CorporateOfficeLocation { get; set; }
        public string CategoryName { get; set; }
        public string Question { get; set; }
        public string QuestionnaireXML { get; set; }
        public List<Assesment> Assesments { get; set; }
        public SelectList DdlControlCategory { get; set; }
        public SelectList DdlOrganization { get; set; }
        public List<ChildOrg> DDlchildOrgs { get; set; }
        public long ChildOrgId { get; set; }
        public string ChildOrgName { get; set; }
    }

    public class ScopeAssessments : Base
    {
        public int Id { get; set; }
        public long ParentOrgId { get; set; }
        public long ChildOrgId { get; set; }
        public long RiskDomainId { get; set; }
        public long ISOId { get; set; }
        public long RegulationId { get; set; }
        public string ScopeAssessmentXml { get; set; }
        public string ScopeAssessmentRiskDomainMapXml { get; set; }
        public string ScopeAssessmentISOMapXml { get; set; }
        public string ScopeAssessmentRegulationMapXml { get; set; }
        public List<Organization> OrganizationsList { get; set; }
        public SelectList DDlOrganizationChild { get; set; }
        public SelectList DdlCountry { get; set; }
        public SelectList DdlSector { get; set; }
        public SelectList DdlInternational { get; set; }
        public SelectList DdlRiskDomain { get; set; }
    }

    public class FunctionDashboard : Base
    {
        public long Id { get; set; }
        public long ChildOrgId { get; set; }
        public long FunctionId { get; set; }
        public long ProcessId { get; set; }
        public SelectList DdlChildOrg { get; set; }
        public List<Organization> OrganizationList { get; set; }
    }

    public class ClientAudity
    {
        public long Id { get; set; }
        public string UName { get; set; }
        public string Email { get; set; }
        public string UPassword { get; set; }
    }

    public class RiskAssessmentRCM : Base
    {
        public long ID { get; set; }
        public string ICDCategoryMapXML { get; set; }
        public string RCMThreatMapXML { get; set; }
        public string RCMArtefactMapXML { get; set; }
        public long MapId { get; set; }
        public string DomainName { get; set; }
        public string SubDomainName { get; set; }
        public string ControlTypeName { get; set; }

        public List<Artefact> Artefactlist { get; set; }
        public SelectList DdlDomain { get; set; }
        public int ControlTypeId { get; set; }
        public int DomainId { get; set; }
        public int SubDomain { get; set; }
        public SelectList DdlControlType { get; set; }
        public string ControlName { get; set; }
        public string ControlId { get; set; }
        public string ControlCategoryId { get; set; }
        public string ThreatId { get; set; }
        public string ControlDescription { get; set; }
        public SelectList DdlControlCategory { get; set; }
        public SelectList DdlThreat { get; set; }
        public SelectList ddlRiskAssessment { get; set; }
        public SelectList DdlSector { get; set; }
        public SelectList DdlCountry { get; set; }
        public long SectorID { get; set; }
        public long CountryID { get; set; }
        public List<RiskAssessmentRCM> QuestionBanklist { get; set; }
        public string Question { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public SelectList ddlTestofDesign { get; set; }
        public SelectList DdlInternationalStandard { get; set; }
        public long StandardId { get; set; }
        public string SectorIDs { get; set; }
        public string CountryIDs { get; set; }
        public string StandardIds { get; set; }
        public string xmlStandardRegulation { get; set; }
        public SelectList DdlParentOrg { get; set; }
        public SelectList DdlChildOrg { get; set; }
        public long ParentOrgId { get; set; }
        public long ChildOrgId { get; set; }
        public long AssessmentTypeID { get; set; }
        public string AssessmentTypeName { get; set; }
        public List<RiskAssessmentRCM> AssessMentList { get; set; }
        public string ParentOrgName { get; set; }
        public string ChildOrgName { get; set; }
        public long Status { get; set; }
        public long OrgID { get; set; }
        public long RegoId { get; set; }
        public string Title { get; set; }
        public string xmlRCM { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string LastUpdatedBy { get; set; }
        public List<ICD> RcmControlList { get; set; }
        public List<Domain> DomainList { get; set; }
        public List<ICD> SubDomainList { get; set; }
        public string CreateOnNew { get; set; }
        public string UpdateOnnew { get; set; }
        public bool Governance { get; set; }
        public bool Application { get; set; }
        public bool Infrastructure { get; set; }
        public bool Cloud { get; set; }
        public bool Networks { get; set; }
        public bool People { get; set; }
        public bool Location { get; set; }
        public SelectList DdlOrganization { get; set; }

        public bool IsThirdParty { get; set; }
        public bool IsCloud { get; set; }
        public bool IsIaas { get; set; }
        public bool IsPaaS { get; set; }
        public bool IsSaaS { get; set; }
        public bool PersonalInfo { get; set; }
        public string RegulationIds { get; set; }
    }
    public class OrgAssignedAssessment : Base
    {
        public long Id { get; set; }
        public long OrgId { get; set; }
        public string OrgName { get; set; }
        public short AssignedId { get; set; }
        public string AssessmentXML { get; set; }
        public List<Enum> EnumList { get; set; }
        public List<OrgAssignedAssessment> OrgAssignedAssessmentList { get; set; }
    }
}
