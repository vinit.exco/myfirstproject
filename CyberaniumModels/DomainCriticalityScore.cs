﻿using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class DomainCriticalityScore : Base
    {
        public long Id { get; set; }
        public long OrgId { get; set; }
        public long FunctionId { get; set; }
        public long ProcessId { get; set; }
        public int ParameterId { get; set; }
        public int RiskDomainId { get; set; }
        public decimal Score { get; set; }
        public string DomainCriticalityScoreXML { get; set; }
    }
}
