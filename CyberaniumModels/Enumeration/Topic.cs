﻿using System.ComponentModel;

namespace CyberaniumModels.Enumeration
{
    public enum Topic
    {
        None, Cyber_Security, Cloud_Security, Privacy, Business_Continuity, PCI__DSS, HIPAA
    }
}
