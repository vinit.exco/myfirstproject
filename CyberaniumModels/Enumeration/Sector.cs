﻿namespace CyberaniumModels.Enumeration
{
    public enum Sector
    {
        All=1, Banking, Telecom, Insurance
    }
}
