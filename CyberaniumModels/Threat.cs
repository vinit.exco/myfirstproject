﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace CyberaniumModels
{
    public class Threat : Base
    {
        public long ThreatId { get; set; }
        public long TTypeId { get; set; }
        public long TlevelId { get; set; }
        public string ThreatCode { get; set; }
        public string ThreatName { get; set; }
        public string ControlCategory { get; set; }
        public int ControlCategoryId { get; set; }
        public int Score { get; set; }
        public string ControlCategoryXML { get; set; }
        public List<ThreatType> ThreatTypes { get; set; }
        public SelectList DdlControlCategory { get; set; }
        public List<Threat> Threats { get; set; }
    }
}
