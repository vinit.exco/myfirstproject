﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace CyberaniumModels
{
    public class ThreatEnabler : Base
    {
        public long Id { get; set; }
        public long OrgId { get; set; }
        public int ThreatId { get; set; }
        public int ConCatId { get; set; }
        public long QuestionnaireId { get; set; }
        public int Score { get; set; }
        public SelectList ParentOrgDDL { get; set; }
        public string CodeName { get; set; }
        public List<ThreatEnabler> ThreatList { get; set; }
        public string ApplicationScore { get; set; }
        public string infrastructureScore { get; set; }
        public string LocationScore { get; set; }
        public string ApplicationScoreXML { get; set; }
        public string ApplicationScoreIdXML { get; set; }
        public string InfrastructureScoreXML { get; set; }
        public string InfrastructureScoreIdXML { get; set; }
        public string LocationScoreXML { get; set; }
        public string LocationScoreIdXML { get; set; }
        public string ParentOrgIdXML { get; set; }
        public string Name { get; set; }
        public List<ThreatEnabler> Categories { get; set; }
        public List<Organization> OrganizationsList { get; set; }
        public int ChildOrg { get; set; }
        public int Value { get; set; }
        public string Title { get; set; }
        public string Answer { get; set; }
        public List<ThreatEnabler> ArrList { get; set; }
        public string QuestionnaireXML { get; set; }
        public long ThreatEnablerId { get; set; }
        public string ThreatName { get; set; }
        public long ChildOrgId { get; set; }
        public string CategoryName { get; set; }
        [Allowhtml]
        public string FormHtml { get; set; }
        public List<ThreatEnabler> ThreatEnablerList { get; set; }
        public List<ThreatEnabler> ThreatEnablerMapList { get; set; }
        public long CategoryId { get; set; }
        public long QuestionId { get; set; }
    }
}
