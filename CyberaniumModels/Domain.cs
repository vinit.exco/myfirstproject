﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class Domain : BaseClass
    {
        public int Id { get; set; }
        public int DomainId { get; set; }
        public string SubDomain { get; set; }
        public string Domaintype { get; set; }
        public SelectList DDlDomain { get; set; }
        public string Code { get; set; }
        public string DomainName { get; set; }

        public string DomainCode { get; set; }
        public enum DomainEnum
        {
            Identity = 1,
            Protect,
            Detect,
            Response,
            Recover
        }
        public List<Domain> Domainlist { get; set; }
        public List<DomainEnum> Enumlist { get; set; }
        public int Eventtype { get; set; }
    }

    public class RiskDomain : Base
    {
        public long Id { get; set; }
        public long ParentOrgId { get; set; }
        public long ChildOrgId { get; set; }
        public long RiskDomainId { get; set; }
        public long FunctionId { get; set; }
        public long ProcessId { get; set; }
        public string RiskDomainName { get; set; }
        public string DomainCriticalityScoreXML { get; set; }
        public List<RiskDomain> RiskDomainList { get; set; }
    }

}
