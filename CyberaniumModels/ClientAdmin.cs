﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class ClientAdmin
    {
        public int Id { get; set; }
        public int OrganizationId { get; set; }
        public string Organization { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public int b_Deleted { get; set; }
        public int AssignRoleClientId { get; set; }
        public string AssignRoleClient { get; set; }
        public List<ClientAdmin> clientAdmins { get; set; }
    }
}
