﻿using CyberaniumModels.Comman;
using System;
using System.Collections.Generic;

namespace CyberaniumModels
{
    public class RequestArtefacts : Base
    {
        public long Id { get; set; }
        public long OrgId { get; set; }
        public int CategoryId { get; set; }
        public long QuestionnaireId { get; set; }
        public long ICDId { get; set; }
        public long RecFromId { get; set; }
        public DateTime RecByDate { get; set; }
        public int RecStatus { get; set; }
        public string Remark { get; set; }
        public string ArtefactsXML { get; set; }
        public string ImagesXML { get; set; }
        public string Answer { get; set; }
        public List<RequestArtefacts> ListRequestArtefacts { get; set; }
    }
}
