﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class Menu : Base
    {
        public long MenuId { get; set; }
        public int MenuType { get; set; }
        public long ParentMenuId { get; set; }
        public long ChildMenuId { get; set; }
        public long SubMenuId { get; set; }
        public int OrderNo { get; set; }
        public string MenuName { get; set; }
        public string MenuTitle { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public List<Menu> Menues { get; set; }
        public SelectList DdlRole { get; set; }
    }
}
