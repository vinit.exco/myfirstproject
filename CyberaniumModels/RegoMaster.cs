﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class RegoMaster : Base
    {
        public int RegoId { get; set; }
        public int CountryId { get; set; }
        public int TypeId { get; set; }
        public int Regulation { get; set; }
        public string YearPublish { get; set; }
        public string Revision { get; set; }
        public int Sector { get; set; }
        public int Section { get; set; }
        public int Domain { get; set; }
        [System.Web.Mvc.AllowHtml]
        public string Text { get; set; }
        public string Requirement_ID { get; set; }
        public List<RegoMaster> Regolist { get; set; }

        public SelectList DdlCountry { get; set; }
        public SelectList DdlSector { get; set; }
        public SelectList Ddltype { get; set; }
        public SelectList DdlRegulation { get; set; }
        public SelectList DdlSection { get; set; }
        public SelectList DdlDomain { get; set; }
        public string[] Artefacts { get; set; }
    }
}
