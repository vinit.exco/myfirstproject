﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class ProcessesAssign : Base
    {
        public long Id { get; set; }
        public long ParentOrgId { get; set; }
        public string ParentOrgName { get; set; }
        public string CorporateOfficeLocation { get; set; }
        public long ChildOrgId { get; set; }
        public string ChildOrgName { get; set; }
        public string ChildOrgLocation { get; set; }
        public long FunctionId { get; set; }
        public long ProcessId { get; set; }
        public int ConCatId { get; set; }
        public int AnswerId { get; set; }
        public SelectList DdlChildOrg { get; set; }
        public List<ProcessesAssign> ProcessAssignList { get; set; }
        public List<Organization> OrganizationList { get; set; }
        public List<Enum> RiskDomainList { get; set; }
        public List<Enum> ControlCategoryList { get; set; }
        public List<DomainBaseRiskScore> DomainBaseRiskScoreList { get; set; }
        public string ClientProcessDCScoreXML { get; set; }
        public string ClientProcessEnablerScoreXML { get; set; }
        public string ClientProcessFinalScoreXML { get; set; }
    }
    public class FunctionProcessMap : Base
    {
        public long Id { get; set; }
        public long FunctionId { get; set; }
        public string ProcessIdentified { get; set; }
        public long FunctionalOwner { get; set; }
        public string Description { get; set; }
        public List<FunctionProcessMap> FunctionProcessMapList { get; set; }
    }
}
