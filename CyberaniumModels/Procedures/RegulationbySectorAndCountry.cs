﻿using CyberaniumModels.Comman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels.Procedures
{
    public class RegulationbySectorAndCountry:RegoMaster
    {
        public string CountryName { get; set; }
        public string SectorName { get; set; }
        public string RegulationName { get; set; }
    }
}
