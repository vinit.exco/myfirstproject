﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace CyberaniumModels
{
    public class Compliance : Base
    {
        public long Id { get; set; }
        public string Query { get; set; }
        public bool IsPrimary { get; set; }
        public int OrderNo { get; set; }
        public int ControlCategory { get; set; }
        public string QuestionnaireXML { get; set; }
        public SelectList DdlControlCat { get; set; }
        public List<Enum> EnablerList { get; set; }
        public List<Compliance> ComplianceList { get; set; }
    }
}
