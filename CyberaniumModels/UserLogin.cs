﻿
using CyberaniumModels.Comman;
using CyberaniumModels.Enumeration;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class UserLogin:BaseClass
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }
        public bool RememberMe { get; set; }
        public int RoleId { get; set; }
        public DateTime LastLogin { get; set; }
        public int Hits { get; set; }
        public DateTime Hittingtime { get; set; }
        public SelectList DdlRole { get; set; }
        //public RoleEnum Roles { get; set; }
        public List<UserLogin> Userlist { get; set; }
        public long OrgID { get; set; }
        public SelectList DdlParentOrg { get; set; }
        public SelectList DdlOrganization { get; set; }
        public SelectList UserSelectList { get; set; }
        public string t_CurrentPassword { get; set; }
        public string t_ResetPassword { get; set; }
        public string t_ConfirmPassword { get; set; }
        public string t_UserID { get; set; }
        public string t_CanAccess { get; set; }
        public string t_AccessRole { get; set; }
        public int OrganizationId { get; set; }
        public string Organization { get; set; }
        public SelectList DdlOrgUserAccess { get; set; }
        public List<SelectListItem> DdlAccessRole { get; set; }
        public string AccessName { get; set; }
        public string CanAccess { get; set; }
        public string t_UserIDNew { get; set; }
        public string PasswordNew { get; set; }

    }
}
