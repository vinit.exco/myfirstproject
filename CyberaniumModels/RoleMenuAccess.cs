﻿using CyberaniumModels.Enumeration;
using System.Collections.Generic;
using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CyberaniumModels
{
    public class RoleMenuAccess : Base
    {
        public long Id { get; set; }
        public int RoleId { get; set; }
        public long MenuId { get; set; }
        public long ParentMenuId { get; set; }
        public long ChildMenuId { get; set; }
        public long SubMenuId { get; set; }
        public bool CanViewAll { get; set; }
        public bool CanCreate { get; set; }
        public bool CanView { get; set; }
        public bool CanEdit { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDeleted { get; set; }
        public bool CanApproveReject { get; set; }
        public bool CanBlockUnblock { get; set; }
        public bool CanPrint { get; set; }
        public bool CanDownload { get; set; }
        public List<RoleMenuAccess> RoleMenuAccesses { get; set; }
        public List<Menu> Menues { get; set; }
        public SelectList DDLRole { get; set; }
    }
}
