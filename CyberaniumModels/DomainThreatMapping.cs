﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class DomainThreatMapping : Base
    {
        public long Id { get; set; }
        public int DomainId { get; set; }
        public long ThreatId { get; set; }
        public bool Status { get; set; }
        public string DomainThreatMappingXML { get; set; }
        public List<DomainThreatMapping> DomainThreatMappingList { get; set; }
        public List<Threat> ThreatList { get; set; }
        public SelectList DdlRiskDomain { get; set; }
    }
}
