﻿using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class ProcessEnabler : Base
    {
        public long Id { get; set; }
        public long ParentOrgId { get; set; }
        public long ChildOrgId { get; set; }
        public long FunctionId { get; set; }
        public long ProcessId { get; set; }
        public string ProcessEnablerXML { get; set; }
    }
    public class ProcessEnablerMap : Base
    {
        public long Id { get; set; }
        public long ProcessEnablerId { get; set; }
        public int ConCatId { get; set; }
        public long AnswerId { get; set; }
    }
}
