﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace CyberaniumModels
{
    public class RiskAssessment : Base
    {
        public long Id { get; set; }
        public int EmpId { get; set; }
        public int ApprovalStatus { get; set; }
        public string EnablerName { get; set; }
        public string ApprovalFor { get; set; }
        public SelectList UserSelectList { get; set; }
        public SelectList DdlRole { get; set; }
        public int RoleId { get; set; }
        public string CategoryName { get; set; }
        public long OrgId { get; set; }
        public string OrgName { get; set; }
        public long ICDID { get; set; }
        public long CategoryId { get; set; }
        public long QuestionnaireID { get; set; }
        public string ArtefactName { get; set; }
        public List<RiskAssessment> RCMControlByOrgLatestList { get; set; }
        public List<RiskAssessment> CommentList { get; set; }
        public string ControlName { get; set; }
        public string ControlId { get; set; }
        public int ControlTypeID { get; set; }
        public string ControlDescription { get; set; }
        public long Rating { get; set; }
        public string Observation { get; set; }
        public string ObservationComment { get; set; }
        public string Comment { get; set; }
        public string Recommendation { get; set; }
        public string RecomendationComment { get; set; }
        public long DomainID { get; set; }
        public string DomainName { get; set; }
        public long SubDomainID { get; set; }
        public string SubDomainName { get; set; }
        public long Status { get; set; }
        public string xmlControlAssessment { get; set; }
        public List<RiskAssessment> ControlAssessmentList { get; set; }
        public string UserName { get; set; }
        public string CreateOnNew { get; set; }
        public List<Threat> ThreatList { get; set; }
        public List<Enum> RiskDomainList { get; set; }
        public List<DomainThreatMapping> DomainThreatMapList { get; set; }
        public List<RiskAssessment> ControlAssesmentControlList { get; set; }
        public SelectList ddlStatus { get; set; }
        public SelectList ddlDecision { get; set; }
        public long Decision { get; set; }
        public string xmlRiskTreatMent { get; set; }
        public string RiskOwner { get; set; }
        public int ManagementDecision { get; set; }
        public string Justification { get; set; }
        public string ActionPlan { get; set; }
        public string Responsibility { get; set; }
        public string TargetclosureDate { get; set; }
        public List<Enum> StatusList { get; set; }
        public List<Enum> ManagenmentDecisionList { get; set; }
        public int Counts { get; set; }
        public bool IsTask { get; set; }
        public int Status2 { get; set; }
        public string RecomedationCmnt { get; set; }
        public string ObservationCmnt { get; set; }
        public int Type { get; set; }
        public List<RiskAssessment> RAList { get; set; }
        public List<UserLogin> ClientUserList { get; set; }
        public int ClientUserId { get; set; }
        public DateTime AssScopeEndDate { get; set; }
        public string ArtefactsXML { get; set; }
        public string ImagesXML { get; set; }
    }
}
