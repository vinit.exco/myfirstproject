﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class Consultant
    {
        public int ConsultantId { get; set; }
        public string ConsultantName { get; set; }

    }
}
