﻿using CyberaniumModels.Enumeration;
using System.Collections.Generic;
using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CyberaniumModels
{

    public class LawRegulation : Base
    {
        public long Id { get; set; }
        public int SectorId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public List<LawRegulation> LawRegulations { get; set; }
        public SelectList DdlParentOrg { get; set; }
        public List<LawRegulation> MapLawRegulationsList { get; set; }
        public SelectList DdlCountry { get; set; }
        public SelectList DdlSector { get; set; }
        public int CountryId { get; set; }
        public string PublishYear { get; set; }
        public string Revision { get; set; }
        public string RequirementId { get; set; }
        public string Remarks { get; set; }
        public int TypeId { get; set; }
        public int Sector { get; set; }
        public int Section { get; set; }
        public int Domain { get; set; }
        public SelectList Ddltype { get; set; }
        public SelectList DdlSection { get; set; }
        public SelectList DdlDomain { get; set; }
        public string XmlRegulation { get; set; }
        public string CountryName { get; set; }
        public long OrgID { get; set; }
        public List<Country> CountryList { get; set; }
        public long NewCountryID { get; set; }
    }
}
