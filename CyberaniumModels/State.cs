﻿using CyberaniumModels.Comman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class State: Base
    {
        public int ID { get; set; }
        public int CID { get; set; }
        public string StateName { get; set; }
    }
}
