﻿using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class DomainBaseRiskScore : Base
    {
        public long Id { get; set; }
        public long ParentOrgId { get; set; }
        public long ChildOrgId { get; set; }
        public long CategoryId { get; set; }
        public long AnswerId { get; set; }
        public long DomainId { get; set; }
        public long ThreatId { get; set; }
        public decimal Score { get; set; }
        public long ParameterId { get; set; }
        //------------------------------------
        public long OrgId { get; set; }
        public int ConCatId { get; set; }
        public int RiskDomainId { get; set; }
        public long QuestionnaireId { get; set; }
        public string DomainBaseRiskScoreXML { get; set; }
    }
}
