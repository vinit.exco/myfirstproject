﻿using CyberaniumModels.Comman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class Country :Base
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
    }
}
