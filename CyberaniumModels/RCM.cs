﻿using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class RCM:Base
    {
        public long OrgID { get; set; }
        public string DomainName { get; set; }
        public string SubDomainName { get; set; }
        public string ControlName { get; set; }
        public string ControlDescription { get; set; }
        public string ControlID { get; set; }
        public string ControlTypeName { get; set; }
        public bool Governance { get; set; }
        public bool Application { get; set; }
        public bool Infrastructure { get; set; }
        public bool Cloud { get; set; }
        public bool Networks { get; set; }
        public bool People { get; set; }
        public bool Location { get; set; }
        public string RCMXML { get; set; }
    }
    public class RCMJson
    {
        public string DomainName { get; set; }
        public string SubDomainName { get; set; }
        public string ControlName { get; set; }
        public string ControlDescription { get; set; }
        public string ControlID { get; set; }
        public string ControlTypeName { get; set; }
        public bool Governance { get; set; }
        public bool Application { get; set; }
        public bool Infrastructure { get; set; }
        public bool Cloud { get; set; }
        public bool Networks { get; set; }
        public bool People { get; set; }
        public bool Location { get; set; }
    }
    public class ICDJson
    {
        public string Domain { get; set; }
        public string ControlType { get; set; }
        public string ControlName { get; set; }
        public string ControlDescription { get; set; }
        public int DomainId { get; set; }
        public int ControlTypeId { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedIP { get; set; }
        public string ICDXML { get; set; }
    }
}
