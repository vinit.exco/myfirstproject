﻿using CyberaniumModels.Enumeration;
using System.Collections.Generic;
using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class Enum : Base
    {
        public long Id { get; set; }
        public long EnumTypeId { get; set; }
        public long Value { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public long AnswerId { get; set; }
        public List<Enum> Enums { get; set; }
        public List<EnumType> EnumTypes { get; set; }
        public List<ThreatEnabler> ArrList { get; set; }
        public int Flag { get; set; }
    }
}
