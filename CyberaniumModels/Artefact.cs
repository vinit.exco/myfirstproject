﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class Artefact : Base
    {
        public long Id { get; set; }
        public long ICDId { get; set; }
        public string Name { get; set; }
    }
}
