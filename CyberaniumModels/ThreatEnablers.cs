﻿using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class ThreatEnablers : Base
    {
        public long Id { get; set; }
        public long ParentOrgId { get; set; }
        public long ChildOrgId { get; set; }
        public int ConCatId { get; set; }
        public string EnablerXML { get; set; }
        public string ThreatScoreXML { get; set; }
    }
    public class ThreatEnablerMap : Base
    {
        public long Id { get; set; }
        public long ThreatEnablerId { get; set; }
        public long ThreatId { get; set; }
        public long QuestionnaireMapId { get; set; }
        public int Score { get; set; }
    }
}
