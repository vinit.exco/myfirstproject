﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class ICDDB : Base
    {
        public int Domain { get; set; }
        public int SubDomain { get; set; }
        public int SectorId { get; set; }
        public int CountryId { get; set; }
        public int InternationalStandard { get; set; }
        public string ControlId { get; set; }
        public string ControlName { get; set; }
        public string ControlDescription { get; set; }
        public string ControlCategory { get; set; }
        public int ControlType { get; set; }
        public string RiskDomain { get; set; }
        public string threats { get; set; }
        public SelectList DDlDomain { get; set; }
        public SelectList DDlSubDomain { get; set; }
        public SelectList DdlSector { get; set; }
        public SelectList DdlCountry { get; set; }
        public SelectList DdlControlType { get; set; }
        public SelectList DdlControlCat { get; set; }
        public SelectList DdlRiskDomain { get; set; }
        public SelectList DdlThreat { get; set; }
        public SelectList DdlCloudService { get; set; }
        public SelectList DDlInternationalStandard { get; set; }
    }
}
