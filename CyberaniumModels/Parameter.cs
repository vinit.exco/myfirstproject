﻿namespace CyberaniumModels
{
    public class Parameter
    {
        public short Id { get; set; }
        public string Name { get; set; }
    }
}
