﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class Assesment : Base
    {
        public long Id { get; set; }
        public long OrganizationId { get; set; }
        public long CategoryId { get; set; }
        public long? QuestionId { get; set; }
        public string Answer { get; set; }
        public string ParentOrgName { get; set; }
        public string CorporateOfficeLocation { get; set; }
        public string CategoryName { get; set; }
        public string Question { get; set; }
        public string QuestionnaireXML { get; set; }
        public List<Assesment> Assesments { get; set; }
        public SelectList DdlControlCategory { get; set; }
        public SelectList DdlOrganization { get; set; }
        public List<ChildOrg> DDlchildOrgs { get; set; }
        public long ChildOrgId { get; set; }
        public string ChildOrgName { get; set; }
    }
    public class ChildOrg
    {
        public int Id { get; set; }
        public string ChildOrgName { get; set; }
    }
}
