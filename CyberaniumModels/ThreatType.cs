﻿using CyberaniumModels.Enumeration;
using System.Collections.Generic;
using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class ThreatType : Base
    {
        public long TTypeId { get; set; }
        public ThreatLevel ThreatLevel { get; set; }
         public string TTypeName { get; set; }
        public List<ThreatType> ThreatTypes { get; set; }
    }
}
