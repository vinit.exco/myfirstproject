﻿using CyberaniumModels.Comman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class City:Base
    {
        public int ID { get; set; }
        public int CID { get; set; }
        public int SID { get; set; }
        public string CityName { get; set; }
    }
}
