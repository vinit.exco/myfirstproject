﻿using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CyberaniumModels
{
    public class IRA : Base
    {
        public List<Enum> riskDomainList;

        public long OrgId { get; set; }
        public long RCMId { get; set; }
        public int ID { get; set; }
        public int RoleId { get; set; }
        public SelectList DdlInternationalStandard { get; set; }
        public SelectList DdllawRegulation { get; set; }
        public string Standard { get; set; }
        public string Regulation { get; set; }
        public string OrgName { get; set; }
         public string RegulationIDs { get; set; }
        public string DomainIds { get; set; }
        public long FunctionId { get; set; }
        public long ProcessId { get; set; }
        public long AssignToUserId { get; set; }
        public DateTime StartDate { get; set; }
        public string _StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string _EndDate { get; set; }
        public List<Organization> OrganizationList { get; set; }
        public List<OrganizationFunctions> FunctionList { get; set; }
        public List<FunctionProcess> ProcessList { get; set; }
        public List<ClientAdmin> UserList { get; set; }
        public List<Enum> ListDomains { get; set; }
        public List<ClientEnablers> CategoryList { get; set; }
        public long AssessorID { get; set; }
        public DateTime CompletedDate { get; set; }
        public SelectList ddlAssessor { get; set; }
        public long CategoryId { get; set; }
        public long QuestionId { get; set; }
        public string strCompletedDate { get; set; }
        public string StandardIDs { get; set; }
        
        public long QuestionnaireID { get; set; }
        public List<IRA> AssignedEnablerCategoryList { get; set; }
        public List<IRA> AssignedEnablerCategoryTypeList { get; set; }
        public string CategoryName { get; set; }
        public string UserName { get; set; }
        [Allowhtml]
        public string htmlTable { get; set; }
        public long ThreatId { get; set; }
        public string ThreatCode { get; set; }
        public string ThreatName { get; set; }
        public int Score { get; set; }
        public List<RiskAssessmentRCM> RCMListByOrgID { get; set; }
        public int ChildOrgId { get; set; }
        public SelectList DdlParentOrg { get; set; }
        public SelectList DdlProcess { get; set; }
        public List<Enum> ControlCategoryList { get; set; }
        public List<ICD> SubDomainList { get; set; }
        [Allowhtml]
        public string HtmlCode { get; set; }
        public string xmlFunction { get; set; }
        public string xmlPerformAssessment { get; set; }
        public SelectList DdlAllRegulation { get; set; }
        public bool IsTask { get; set; }
        public int Status { get; set; }
    }
}
