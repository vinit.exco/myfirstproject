﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumModels
{
    public class RBAC: Menu
    {
        public string RoleName { get; set; }
        public int Counts { get; set; }
        
        public string RoleDesc { get; set; }
        public int RoleId { get; set; }
         public string RBACXML { get; set; }
        public string Edit { get; set; }
        public Decimal Score { get; set; }
        public List<RBAC> processesAssignsList { get; set; }
        public List<RiskAssessment> ApprovalList { get; set; }
        public List<IRA> EnablerList { get; set; }
        public List<IRA> ComplianceList { get; set; }
        public int ClientProcessId { get; set; }
        public string ColorName { get; set; }
        public long UserID { get; set; }
        public int CategoryId { get; set; }
        public List<Organization> OrganizationList { get; set; }

    }
}
