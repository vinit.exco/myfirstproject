﻿using CyberaniumModels.Enumeration;
using System.Collections.Generic;
using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace CyberaniumModels
{
    public class InternationalStandard : Base
    {
        public long Id { get; set; }
        [Display(Name ="Services")]
        [Required(ErrorMessage ="{0} is required.")]
        public int TopicId { get; set; }
        public string Title { get; set; }
        public string Service { get; set; }
        public SelectList DDlServices { get; set; }
        public SelectList DDlInternationalStandard { get; set; }
        public List<InternationalStandard> InternationalStandards { get; set; }
        public long ParentISOId { get; set; }
        public long ChildISOId { get; set; }
        public long ISOTypeId { get; set; }
        public string ISOChild { get; set; }
        public long VersoinId { get; set; }
        public string VersionName { get; set; }
        public string ServiceName { get; set; }
        public long ICDId { get; set; }
        public List<Enum> DDlServicesList { get; set; }
    }
}
