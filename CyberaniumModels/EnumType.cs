﻿using CyberaniumModels.Enumeration;
using System.Collections.Generic;
using CyberaniumModels.Comman;
using System.ComponentModel.DataAnnotations;

namespace CyberaniumModels
{
    public class EnumType : Base
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public List<EnumType> EnumTypes { get; set; }
    }
}
