﻿using System;

namespace CyberaniumModels.Comman
{
    public class Base
    {
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedIP { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public string UpdatedIP { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int EventType { get; set; }
        public string Mode { get; set; }

        //public int ID { get; set; }
        //public bool Deleted { get; set; }
        //public bool Activated { get; set; }
        //public int ModifiedBy { get; set; }
        //public DateTime ModifiedOn { get; set; }
        //public string ModifiedIP { get; set; }
        //public string Mode { get; set; }
    }
}
