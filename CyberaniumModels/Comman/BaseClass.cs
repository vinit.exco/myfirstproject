﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberaniumModels.Comman
{
    public class BaseClass
    {
        public bool Deleted { get; set; }
        public bool Activated { get; set; }
        public int  CreatedBy  { get; set; }
        public DateTime  CreatedOn  { get; set; }
        public string  CreatedIP  { get; set; }
        public int  ModifiedBy  { get; set; }
        public DateTime ModifiedOn  { get; set; }

        public string ModifiedIP  { get; set; }
        public string Mode  { get; set; }
        public bool IsActive { get; set; }
        public string ClientAdminXml { get; set; }
    }
}
