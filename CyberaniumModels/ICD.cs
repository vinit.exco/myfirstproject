﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using CyberaniumModels.Comman;

namespace CyberaniumModels
{
    public class ICD : Base
    {
        public long Id { get; set; }
        public string ICDXML { get; set; }
        public long DomainId { get; set; }
        public long SubDomainId { get; set; }
        public string ControlId { get; set; }
        public string ControlName { get; set; }
        public string ControlDescription { get; set; }
        public string IaaS { get; set; }
        public string PaaS { get; set; }
        public string SaaS { get; set; }
        public string StandardIds { get; set; }
        public string CountryIds { get; set; }
        public string SectorIds { get; set; }
        public string ICDCategoryMapXML { get; set; }
        public string ICDStandardMapXML { get; set; }
        public string ICDArtefactMapXML { get; set; }
        public string ICDThreatMapXML { get; set; }
        public string ICDRegulationMapXML { get; set; }
        public List<ICD> ICDs { get; set; }
        public SelectList DdlDomain { get; set; }
        public List<Domain> Domainlist { get; set; }
        public List<Artefact> Artefactlist { get; set; }
        public SelectList DdlControlCategory { get; set; }
        public SelectList DdlControlType { get; set; }
        public SelectList DdlInternationalStandard { get; set; }
        public List<ICD> SubDomainList { get; set; }
        public List<Threat> ThreatList { get; set; }
        public SelectList DdlCountry { get; set; }
        public SelectList DdlSector { get; set; }
        public string Code { get; set; }
        public string CountryId { get; set; }
        public string SectorId { get; set; }
        public string ThreatId { get; set; }
        public int ControlTypeId { get; set; }
        public string ControlCategoryId { get; set; }
        public long ICDId { get; set; }
        public long CategoryId { get; set; }
        public SelectList DdlThreat { get; set; }
        public List<DomainThreatMapping> DomainThreatMapList { get; set; }
        public string SubDomainName { get; set; }
        public int Countee { get; set; }
        public long ParentOrgId { get; set; }
        public long ChildOrgId { get; set; }
        public long AnswerId { get; set; }
        public long RegulationId { get; set; }
        public string Requirement_ID { get; set; }
        public string RegulationName { get; set; }
        public string CountryName { get; set; }
        public string SectorName { get; set; }
        public List<ICD> RegulationMapList { get; set; }
        public List<InternationalStandard> VersionMapList { get; set; }
        public string DomainCode { get; set; }
        public List<LawRegulation> MapLawRegulationsList { get; set; }
        public SelectList Ddltype { get; set; }
        public SelectList DdlSection { get; set; }
        public string Title { get; set; }
        public int TypeId { get; set; }
        public int Section { get; set; }
        public int Domain { get; set; }
        public string DomainName { get; set; }
        public string xmlRegulation { get; set; }
        public string RegulationIds { get; set; }
        public string EnablerCategoryIds { get; set; }
        public string ControlTypeName { get; set; }
        public long ControlTypeIDNew { get; set; }
        public bool Governance { get; set; }
        public bool Application { get; set; }
        public bool Infrastructure { get; set; }
        public bool Cloud { get; set; }
        public bool Networks { get; set; }
        public bool People { get; set; }
        public bool Location { get; set; }
        public string DomainCompilancescore { get; set; }
        public string overalleffectiveness { get; set; }
        public string remarks { get; set; }
        public int Testofdesign { get; set; }
        public int Testofeffectiveness { get; set; }
        public List<Domain> DomainlistNew { get; set; }

    }
}
