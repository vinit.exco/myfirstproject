﻿using CoreApiClient;
using Cyberanium.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cyberanium.Factory
{
    internal static class ApiClientFactory
    {
        private static Uri apiUri;

        private static Lazy<ProgrammingInterface> restClient = new Lazy<ProgrammingInterface>(
          () => new ProgrammingInterface(apiUri),
          LazyThreadSafetyMode.ExecutionAndPublication);

        static ApiClientFactory()
        {
            apiUri = new Uri(ApplicationSettings.WebApiUrl);
        }

        public static ProgrammingInterface Instance
        {
            get
            {
                return restClient.Value;
            }
        }
    }
}
