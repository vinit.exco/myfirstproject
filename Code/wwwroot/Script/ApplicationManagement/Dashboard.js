﻿import $ from 'jquery';
import FusionCharts from 'fusioncharts';
// Load charts module to render Column2D chart
import Charts from 'fusioncharts/fusioncharts.charts';
import 'jquery-fusioncharts';

$('document').ready(function () {
    // Once the window with a document is ready, execute the Chart module
    // and pass FusionCharts as a dependency
    Charts(FusionCharts);

    // Render the chart using `insertFusionCharts` method
    $('#chart-container').insertFusionCharts({
        type: 'column2d',
        width: '600',
        height: '400',
        dataFormat: 'json',
        dataSource: {
            chart: {
                caption: 'Countries With Most Oil Reserves [2017-18]',
                subCaption: 'In MMbbl = One Million barrels',
                xAxisName: 'Country',
                yAxisName: 'Reserves (MMbbl)',
                numberSuffix: 'K'
            },
            data: [
                {
                    label: 'Venezuela',
                    value: '290'
                },
                {
                    label: 'Saudi',
                    value: '260'
                },
                {
                    label: 'Canada',
                    value: '180'
                },
                {
                    label: 'Iran',
                    value: '140'
                },
                {
                    label: 'Russia',
                    value: '115'
                },
                {
                    label: 'UAE',
                    value: '100'
                },
                {
                    label: 'US',
                    value: '30'
                },
                {
                    label: 'China',
                    value: '30'
                }
            ]
        }
    });
});