﻿
function fn_openPopup() {
    $('#UserName').val('');
    $('#t_UserID').val('');
    $('#Email').val('');
    $('#Password').val('');
    $('#t_CanAccess').val('');
    $("#t_CanAccess").multiselect('rebuild');
    $('#t_AccessRole').val('');
    $('#Id').val(0);
    $('#title').text('CREATE NEW USER');
    $("#exampleModalCenter").modal('show');
    $(".modal-backdrop").addClass('in');
}

function fn_saveUser() {
    if ($('#UserName').val() == "") {
        toastr.error('Please enter user name ')
        return false;
    }
    if ($('#Email').val() == "") {
        toastr.error('Please enter email ')
        return false;
    }
    if ($('#t_CanAccess').val() == "" || $('#t_CanAccess').val() == null) {
        toastr.error('Please select can access ')
        return false;
    }
    if ($('#t_AccessRole').val() == "") {
        toastr.error('Please select access role ')
        return false;
    }
    //CanAccess = $("#t_CanAccess").val().toString();
    //$("#CanAccess").val(CanAccess);
    //$("#PasswordNew").val($("#Password").val());
    //$("#t_UserIDNew").val($("#t_UserID").val());
    //$("#OrganizationId").val($("#OrgID").val());
    else {
        var model = {
            Id: $('#Id').val(),
            OrganizationId: $("#OrgID").val(),
            UserName: $('#UserName').val(),
            t_UserIDNew: $("#t_UserID").val(),
            Email: $('#Email').val(),
            PasswordNew: $("#Password").val(),
            t_AccessRole: $('#t_AccessRole').val(),
            CanAccess: $("#t_CanAccess").val().toString()
        }
        $.ajax({
            url: "/ApplicationManagement/OrganizationUser/SaveOrganizationUser",
            type: "POST",
            data: { model: model },
            success: function (res) {
                if (res.isSuccess) {
                    toastr.success('User saved successfully.')
                    $('#exampleModalCenter').modal('hide');
                    fn_ShowFun()
                } else {
                    toastr.error('Somthing went wrong!');
                }
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        })
    }
}

function fn_ShowFun() {
    var orgId = parseInt($("#OrgID").val());
    if (orgId > 0) {
        $.ajax({
            url: "/ApplicationManagement/OrganizationUser/getOrganizationUsers",
            type: "POST",
            data: { OrgID: orgId },
            success: function (res) {
                $("#tblOrgUsers tbody").empty();
                if (res.length > 0) {
                    for (var i = 0; i < res.length; i++) {
                        var htt = (res[i].activated == true ? "<i class='fa fa-times' aria-hidden='true'></i>" : "<i class='fa fa-check' aria-hidden='true'></i>");
                        var txt = (res[i].activated == true ? "Unblock" : "Block");
                        $("#tblOrgUsers tbody").append("<tr><td>" + (i + 1) + "</td>" +
                            "<td>" + res[i].userName + "</td>" +
                            "<td>" + res[i].t_UserID + "</td>" +
                            "<td>" + res[i].accessName + "</td>" +
                            "<td><a class='edit_btn' title='Edit' onclick='EditUser(" + res[i].id + ")'></a><a class='btn btn-info btn-xs pull-left' title='" + txt + "' onclick='fn_Activeincative(" + res[i].id + ",this)'>" + htt + "</a><a class='delete_btn' title='Delete' onclick='Confirmation(" + res[i].id + ",this)'></a><a class='btn btn-info btn-xs pull-left' title='Reset Password' onclick='ResetPassword(" + res[i].id + ")'><i class='fa fa-key' aria-hidden='true'></i></a></td>" +
                            "</tr>"
                        );
                    }
                } else {
                    $("#tblOrgUsers tbody").append("<tr><td colspan='5'>No record found..</td></tr>");
                }
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        })
    } else {
        $("#tblOrgUsers tbody").empty();
        $("#tblOrgUsers tbody").append("<tr><td colspan='5'>No record found..</td></tr>");
    }
    $("#DivMain").show();
}

function EditUser(input) {
    $.ajax({
        url: "/ApplicationManagement/OrganizationUser/EditOrganizationUsers",
        type: "POST",
        data: { ID: input },
        success: function (res) {
            debugger
            $('#title').text('EDIT USER');
            $('#exampleModalCenter').modal('show');
            $('#UserName').val(res[0].userName);
            $('#t_UserID').val(res[0].t_UserID);
            $('#Email').val(res[0].email);
            $('#Password').val(res[0].password);
            $('#t_CanAccess').val(res[0].t_CanAccess.split(",")).change();
            $('#t_AccessRole').val(res[0].t_AccessRole);
            $("#t_CanAccess").multiselect('rebuild');
            $('#Id').val(res[0].id);
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function ResetPassword(input) {
    $("#Id").val(input);
    $.ajax({
        url: "/ApplicationManagement/OrganizationUser/EditOrganizationUsers",
        type: "POST",
        data: { ID: input },
        success: function (result) {
            debugger
            $("#t_CurrentPassword").val(result[0].password);
            $('#PasswordResetCenter').modal('show');
            $('#t_ResetPassword').val('');
            $('#t_ConfirmPassword').val('');
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function Confirmation(input, e) {
    debugger
    var UserName = $(e).parent().parent().find("td").eq(1).text();
    Swal.fire({
        title: 'Are you sure?',
        text: 'You are about to delete User Name details to ' + UserName,
        showCancelButton: true,
        confirmButtonText: 'Delete',
    }).then((result) => {
        if (result.isConfirmed) {
            $('#preloader').show();
            $.get('/ApplicationManagement/OrganizationUser/DeleteOrganizationUsers', { ID: input }, function (res) {
                debugger
                var par = $(e).parent().parent();
                par.remove();
                $('#preloader').hide();
                toastr.success(UserName + ' deleted successfully');
            })
        }
    })
}

function fn_Activeincative(input, input2) {
    $.ajax({
        url: "/ApplicationManagement/OrganizationUser/DisableOrganizationUsers",
        type: "POST",
        data: { ID: input },
        success: function (res) {
            debugger
            if ($(input2).parent().find("i").hasClass("fa fa-times")) {
                $(input2).parent().find("i").removeClass("fa fa-times");
                $(input2).parent().find("i").addClass("fa fa-check");
            } else {
                $(input2).parent().find("i").removeClass("fa fa-check");
                $(input2).parent().find("i").addClass("fa fa-times");
            }
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function generateUserID(input) {
    debugger
    var res = $("#OrgID option:selected").text().substring(0, 4);
    var str = $("#UserName").val().substring(0, 4);
    $("#t_UserID").val(res + str);
}

function generatePassword(passwordLength) {
    debugger
    var numberChars = "0123456789";
    var upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var lowerChars = "abcdefghijklmnopqrstuvwxyz";
    var specialChars = "~!*#$^&";
    var allChars = numberChars + upperChars + lowerChars + specialChars;
    var randPasswordArray = Array(passwordLength);
    randPasswordArray[0] = numberChars;
    randPasswordArray[1] = upperChars;
    randPasswordArray[2] = lowerChars;
    randPasswordArray[3] = lowerChars;
    randPasswordArray = randPasswordArray.fill(allChars, 4);
    $("#Password").val(shuffleArray(randPasswordArray.map(function (x) { return x[Math.floor(Math.random() * x.length)] })).join(''));
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function fn_ViewPassword() {
    if ($("#Password").attr("type") === "password") {
        $("#Password").attr("type", "text");
    } else {
        $("#Password").attr("type", "password");
    }
}

function ConfirmPwd() {
    debugger;
    var pass = $('#t_ResetPassword').val();
    var cPass = $("#t_ConfirmPassword").val();
    if (cPass.trim() === '') {
        $("#t_ConfirmPassword").val('');
        toastr.warning('Please enter confirm password!');
        return false;
    } else if (pass !== cPass) {
        toastr.warning('Do not matched!');
        $("#t_ConfirmPassword").val('');
        return false;
    } else {
        return true;
    }
}
function generateEmail(input) {
    debugger;
    var str = $(input).val();
    if (IsValidEmail(str) == false) {
        $('#invalid_email').show();
        $('#Email').val('');
        return false;
    }
    else {
        $('#invalid_email').hide();
        $.ajax({
            url: "/ApplicationManagement/OrganizationUser/OrganizationUsersEmail",
            type: "POST",
            data: { Email: str },
            success: function (res) {
                debugger
                if (res.isValid) {
                    $('#Email').val(str);
                } else {
                    toastr.warning(res.msg);
                    $('#Email').val('');
                }
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        })
    }
}


function IsValidEmail(input) {
    debugger;
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(input)) {
        return false;
    } else {
        return true;
    }
}

function fn_savePassword() {
    var password = $('#t_ResetPassword').val();
    if (password.trim() === '') {
        toastr.warning('Please enter password!');
        $('#t_ResetPassword').val('');
        return false;
    } else if (!ConfirmPwd()) return false;
    else {
        $.ajax({
            url: "/ApplicationManagement/OrganizationUser/IsValidPassword",
            async: false,
            data: { userId: $("#Id").val(), password: password },
            success: function (res) {
                debugger;
                if (res.isValid) {
                    $.ajax({
                        url: "/ApplicationManagement/OrganizationUser/PasswordResetOrganizationUsers",
                        type: "POST",
                        data: { ID: $("#Id").val(), Password: $("#t_ConfirmPassword").val() },
                        success: function (res) {
                            toastr.success('Password Changed successfully.');
                            $('#PasswordResetCenter').modal('hide');
                        },
                        error: function (ex) {
                            console.log(ex.responseText);
                        }
                    })
                } else {
                    $('#t_ConfirmPassword').val('');
                    toastr.warning(res.msg);
                }
            }
        })
    }

}

