﻿
function fn_checkOrgDupllicate(input) {
    $.ajax({
        url: "/ApplicationManagement/Organization/CheckParentOrgName",
        type: "POST",
        data: { ParentOrgName: $(input).val() },
        success: function (res) {
            debugger
            if (res.msg == "Already exists") {
                $(input).val('');
                toastr.error(res.msg);
            }
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function hideForm() {
    $('#dvDataTable').show();
    $('#dvForm').attr('class', 'service_line');
    $('#btnBasic').trigger('click');
}

function AddButton() {
    $("#Id").val(0);
    $("#ParentOrgName").val('');
    $("#CorporateOfficeLocation").val('');
    $("#RoleId").val('');
    $("#CountryID").val('');
    $("#StateId").val('');
    $("#CityId").val('');
    $('#StateId').find('option:not(:first)').remove();
    $('#CityId').find('option:not(:first)').remove();
    $('#tblClientAudity tbody tr').remove();
    $("#tblClientAudity tbody").append("<tr><td>" + 1 + "</td>" +
        "<td hidden><input type='text' class='form-control' value='0' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td><input type='password' autocomplete='new-password' class='form-control aaaa' /></td>" +
        "<td><a class='delete_btn' onclick='removeRow($(this))' ></a></td>" +
        "</tr>");
    $(".titile_heading h1").html("Add Client");
    $(".service_line").addClass("active");
    $(".assign_panel").hide();
}

function fn_BindStateOfModel(input) {
    $.ajax({
        url: "/ApplicationManagement/ChildOrganization/BindStateById",
        type: "POST",
        data: { CountryId: $(input).val() },
        success: function (res) {
            debugger
            if (res.length > 0) {
                $('#StateId2').find('option:not(:first)').remove();
                for (var i = 0; i < res.length; i++) {
                    $("#StateId2").append("<option value=" + res[i].id + ">" + res[i].stateName + "</option>");
                }
            }
            else {
                $('#StateId2').find('option:not(:first)').remove();
            }
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function fn_BindStates(input) {
    $.ajax({
        url: "/ApplicationManagement/ChildOrganization/BindStateById",
        type: "POST",
        data: { CountryId: $(input).val() },
        success: function (res) {
            debugger
            if (res.length > 0) {
                $('#CityId').find('option:not(:first)').remove();
                $('#StateId').find('option:not(:first)').remove();
                for (var i = 0; i < res.length; i++) {
                    $("#StateId").append("<option value=" + res[i].id + ">" + res[i].stateName + "</option>");
                }
            }
            else {
                $('#CityId').find('option:not(:first)').remove();
                $('#StateId').find('option:not(:first)').remove();
            }
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function fn_BindCity(input) {
    $.ajax({
        url: "/ApplicationManagement/ChildOrganization/BindCityById",
        type: "POST",
        data: { CountryId: $("#CountryID option:selected").val(), StateId: $(input).val() },
        success: function (res) {
            debugger
            if (res.length > 0) {
                $('#CityId').find('option:not(:first)').remove();
                for (var i = 0; i < res.length; i++) {
                    $("#CityId").append("<option value=" + res[i].id + ">" + res[i].cityName + "</option>");
                }
            }
            else {
                $('#CityId').find('option:not(:first)').remove();
            }
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

$("#cBtn").click(function () {
    if ($("input[name=Country]").val().trim() != "") {
        $.ajax({
            url: "/ApplicationManagement/ChildOrganization/SaveCountry",
            type: "POST",
            cache: false,
            data: { CountryName: $("input[name=Country]").val() },
            success: function (result) {
                debugger
                if (result.msg = "Country save successfully!") {
                    $('#CountryID').find('option:not(:first)').remove();
                    for (var i = 0; i < result.country.length; i++) {
                        $("#CountryID").append("<option value=" + result.country[i].id + ">" + result.country[i].countryName + "</option>");
                    }
                    toastr.success(result.msg);
                } else {
                    toastr.error(result.msg);
                }
                $("#countryModal").modal('hide');
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        });
    } else {
        toastr.error('Country name is required');
    }
});

$("#sBtn").click(function () {
    if ($("input[name=State]").val().trim() != "" && $("select[name=CountryID2] option:selected").val() != 0) {
        $.ajax({
            url: "/ApplicationManagement/ChildOrganization/SaveState",
            type: "POST",
            cache: false,
            data: { countryId: $("select[name=CountryID2] option:selected").val(), stateName: $("input[name=State]").val() },
            success: function (result) {
                debugger
                if (result.msg = "State save successfully!") {
                    $('#StateId').find('option:not(:first)').remove();
                    for (var i = 0; i < result.states.length; i++) {
                        $("#StateId").append("<option value=" + result.states[i].id + ">" + result.states[i].stateName + "</option>");
                    }
                    toastr.success(result.msg);
                } else {
                    toastr.error(result.msg);
                }
                $("#stateModal").modal('hide');
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        });
    } else if ($("select[name=CountryID2] option:selected").val() == 0) {
        toastr.error("Please select country");
    }
    else {
        toastr.error("State is required.");
    }
});

$("#ctBtn").click(function () {
    if ($("select[name=CountryID3] option:selected").val() != 0 && $("select[name=StateId2] option:selected").val() != 0 && $("input[name=City]").val().trim() != "") {
        $.ajax({
            url: "/ApplicationManagement/ChildOrganization/SaveCity",
            type: "POST",
            cache: false,
            data: { countryId: $("select[name=CountryID3] option:selected").val(), StateId: $("select[name=StateId2] option:selected").val(), CityName: $("input[name=City]").val() },
            success: function (result) {
                if (result.msg = "City save successfully!") {
                    $('#CityId').find('option:not(:first)').remove();
                    for (var i = 0; i < result.citys.length; i++) {
                        $("#CityId").append("<option value=" + result.citys[i].id + ">" + result.citys[i].cityName + "</option>");
                    }
                    toastr.success(result.msg);
                } else {
                    toastr.error(result.msg);
                }
                $("#cityModal").modal('hide');
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        });
    } else if ($("select[name=CountryID3] option:selected").val() == 0) {
        toastr.error("Please select country");
    }
    else if ($("select[name=StateId2] option:selected").val() == 0) {
        toastr.error("Please select state");
    } else {
        toastr.error("City is required");
    }
});

function Confirmation(id) {
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to delete this organization',
        showCancelButton: true,
        confirmButtonText: 'Delete',
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $('#preloader').show();
            $.ajax({
                url: "/ApplicationManagement/Organization/BlockDeleteOrganization",
                type: "POST",
                cache: false,
                data: { id: id, Type: Type = 1 },
                success: function (result) {
                    $('#preloader').hide();
                    toastr.success("Delete successfully!!");
                    $('a[href="/ApplicationManagement/Organization/Index"]').click();
                }, error: function (ex) {
                    console.log(ex.statusText);
                }
            })
        }
    });
}

function fn_Activeincative(id, status) {
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to ' + status + ' this organization',
        showCancelButton: true,
        confirmButtonText: status,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                url: "/ApplicationManagement/Organization/BlockDeleteOrganization",
                type: "POST",
                cache: false,
                data: { id: id, Type: Type = 0 },
                success: function (result) {
                    toastr.success(status + " " + "successfully!!");
                    $('a[href="/ApplicationManagement/Organization/Index"]').click();
                }, error: function (ex) {
                    console.log(ex.statusText);
                }
            })
        }
    });
}

function fnBlockDelete(Id, Action, Type) {
    if (confirm('Are you sure you want to ' + Action + ' this?')) {
        $.get('/ApplicationManagement/Organization/BlockDeleteOrganization', { Id: Id, EventType: Type }, function (data) {
            if (data.code == 200) {
                toastr.success(Action + ' successfully');
                $('a[href="/ApplicationManagement/Organization/Index"]').click();
            } else {
                toastr.error('Something went wrong!');
            }
        });
    }
}

function fn_AddNewRow() {
    var count = $("#tblClientAudity tbody tr").length + 1;
    $("#tblClientAudity tbody").append("<tr><td>" + count + "</td>" +
        "<td hidden><input type='text' class='form-control' value='0' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td><input type='password' class='form-control' /></td>" +
        "<td><a class='delete_btn' onclick='removeRow($(this))' ></a></td>" +
        "</tr>");
}

function removeRow(elem) {
    var par = elem.parent().parent();
    par.remove();
}

function fn_SaveAudityxml() {
    var arr = new Array(), clientAdminIds = [];
    $('#tblClientAudity tbody tr').each(function (i, tr) {
        debugger;
        arr[i] = {
            Id: $(tr).find("td:nth-child(2) input[type=text]").val(),
            UName: $(tr).find("td:nth-child(3) input[type=text]").val(),
            Email: $(tr).find("td:nth-child(4) input[type=text]").val(),
            UPassword: $(tr).find("td:nth-child(5) input[type=Password]").val(),
        }
    });
    $('#DdlRole option:selected').each(function (i, option) {
        if ($(option).val() == 'multiselect-all') {
            return;
        }
        clientAdminIds.push($(option).val());
    })
    $('#ClientAdminIds').val(clientAdminIds.toString());
    $('#xmlAudity').val(JSON.stringify(arr));
}