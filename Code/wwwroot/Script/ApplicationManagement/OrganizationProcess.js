﻿function hideForm() {
    $('#dvDataTable').show();
    $(".titile_heading h1").html("Process List");
    $("#dvForm").hide();
}

function AddButton() {
    $("#Id").val(0);
    $("#ParentOrgId").val('');
    $("#ChildOrgId").val('');
    $("#FunctionId").val('');
    $('#ChildOrgId').find('option:not(:first)').remove();
    $('#FunctionId').find('option:not(:first)').remove();
    $('.ClsProcessOwner').find('option:not(:first)').remove();
    $('#tblProcess tbody tr').remove();
    var select = '<select class="form-control m_no consultant0"><option value="0">Select Consultant</option></select>';
    $("#tblProcess tbody").append("<tr><td>" + 1 + "</td>" +
        "<td hidden><input type='text' class='form-control' value='0' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td>" + select + "</td>" +
        "<td><a class='delete_btn' onclick='removeRow($(this))' ></a></td>" +
        "</tr>");
    $(".titile_heading h1").html("Add Process");
    $("#dvForm").show();
    $(".assign_panel").hide();
}

function fn_BindChildOrg(input) {
    $.ajax({
        url: "/ApplicationManagement/OrganizationFunction/BindChildOrganization",
        type: "POST",
        data: { ParentOrgId: $(input).val() },
        success: function (res) {
            debugger
            if (res.length > 0) {
                $('#ChildOrgId').find('option:not(:first)').remove();
                for (var i = 0; i < res.length; i++) {
                    $("#ChildOrgId").append("<option value=" + res[i].id + ">" + res[i].childOrgName + "</option>");
                }
            }
            else {
                $('#ChildOrgId').find('option:not(:first)').remove();
            }
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
    BindConsultantDdl()
}

function BindConsultantDdl() {
    var functionId = parseInt($('#FunctionId').val());
    if (functionId > 0) {
        $('.consultant0').empty();
        var select = '<option value="0">Select Consultant</option>';
        $.ajax({
            url: '/ApplicationManagement/OrganizationProcess/GetAssignedConsultantsByFunctionMapId',
            async: false,
            data: { functionId: functionId },
            success: function (res) {
                if (res.length > 0) {
                    $.each(res, function (i, item) {
                        select += '<option value="' + item.consultantId + '">' + item.consultantName + '</option>';
                    })
                }
            }
        })
        $('.consultant0').html(select);
    }
}

function fn_AddNewRow() {
    var count = $("#tblProcess tbody tr").length;
    var select = '<select class="form-control m_no consultant' + count + '"><option value="0">Select Consultant</option>';
    $.ajax({
        url: '/ApplicationManagement/OrganizationProcess/GetAssignedConsultantsByFunctionMapId',
        async: false,
        data: { functionId: $('#FunctionId').val() },
        success: function (res) {
            if (res.length > 0) {
                $.each(res, function (i, item) {
                    select += '<option value="' + item.consultantId + '">' + item.consultantName + '</option>';
                })
            }
        }
    })
    select += '</select>'
    $("#tblProcess tbody").append("<tr><td>" + parseInt(count = 1) + "</td>" +
        "<td hidden><input type='text' class='form-control' value='0' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td><input type='text' class='form-control' /></td>" +
        "<td>" + select + "</td>" +
        "<td><a class='delete_btn' onclick='removeRow($(this))' ></a></td>" +
        "</tr>");
}

function removeRow(elem) {
    var par = elem.parent().parent();
    if ($("#tblProcess tbody tr").length > 1) {
        par.remove();
    } else {
        toastr.error('One row is mandatory');
    }
}

function fn_SaveProcessxml() {
    if ($("#Parent").prop("checked")) {
        $("#IsType").val(1);
    } else {
        $("#IsType").val(2);
    }
    var arr = new Array();
    $('#tblProcess tbody tr').each(function (i, tr) {
        debugger;
        arr[i] = {
            Id: $(tr).find("td:nth-child(2) input[type=text]").val(),
            ProcessIdentified: $(tr).find("td:nth-child(3) input[type=text]").val(),
            FunctionalOwner: $(tr).find("td:nth-child(4) input[type=text]").val(),
            Description: $(tr).find("td:nth-child(5) input[type=text]").val(),
            ConsultantId: $('.consultant' + i).val(),
        }
    });
    $('#xmlProcess').val(JSON.stringify(arr));
}

function fn_HideChildOrg() {
    $("#hdnChildOrg").hide();
}

function fn_ShowChildOrg() {
    $("#hdnChildOrg").show();
}

function fn_BindOrganizationParentFunction(input) {
    $.ajax({
        url: "/ApplicationManagement/OrganizationProcess/BindOrganizationParentFunction",
        type: "POST",
        data: { ParentOrgId: $(input).val() },
        success: function (result) {
            debugger
            $('#FunctionId').find('option:not(:first)').remove();
            $('.ClsProcessOwner').find('option:not(:first)').remove();
            for (var i = 0; i < result.res.length; i++) {
                $("#FunctionId").append("<option value=" + result.res[i].id + ">" + result.res[i].functionName + "</option>");
            }
            //for (var i = 0; i < result.res2.length; i++) {
            //    $(".ClsProcessOwner").append("<option value=" + result.res2[i].id + ">" + result.res2[i].uName + "</option>");
            //}
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function fn_BindOrganizationChildFunction(input) {
    $.ajax({
        url: "/ApplicationManagement/OrganizationProcess/BindOrganizationChildFunction",
        type: "POST",
        data: { ChildOrgId: $(input).val() },
        success: function (result) {
            debugger
            $('#FunctionId').find('option:not(:first)').remove();
            $('.ClsProcessOwner').find('option:not(:first)').remove();
            for (var i = 0; i < result.res.length; i++) {
                $("#FunctionId").append("<option value=" + result.res[i].id + ">" + result.res[i].functionName + "</option>");
            }
            //for (var i = 0; i < result.res2.length; i++) {
            //    $(".ClsProcessOwner").append("<option value=" + result.res2[i].id + ">" + result.res2[i].uName + "</option>");
            //}
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}