﻿
function EditOrganization(id, parentOrgName) {
    var parentOrgId = parseInt(id);
    $('#Name').val('');
    $('#div_multi_child').empty();
    $('#div_multi_child').html('<div class="pb-1 my-2 border-bottom box_title" id="div_child_text" style="font-size:13px;margin-right:20px;" hidden></div>');
    if (parentOrgId > 0) {
        $('#Name').val(parentOrgName);
        $('#div_child_text').text('Name of Child Organizations for ' + parentOrgName);
        $('#div_child_text').removeAttr('hidden');
        $.ajax({
            url: '/ApplicationManagement/Organization/GetChildOrganizationList',
            async: false,
            data: { Id: parentOrgId },
            success: function (res) {
                if (res.length > 0) {
                    var html = '';
                    $.each(res, function (i, item) {
                        var _html = '';
                        $.ajax({
                            url: '/ApplicationManagement/Organization/GetSubChildOrganizationList',
                            async: false,
                            data: { Id: item.id },
                            success: function (_res) {
                                if (_res.length > 0) {
                                    $.each(_res, function (_i, _item) {
                                        _html += '<div class="row div_subchild">' +
                                            '<div class="col-9 ml-2 pr-0">' +
                                            '<div class="select_box m_bottom">' +
                                            '<input type="hidden" value="' + _item.id + '" />' +
                                            '<input class="form-control" value="' + _item.name + '" placeholder="Enter name" onchange="fn_CheckDuplicateOrg(this)" />' +
                                            '</div>' +
                                            '</div>' +
                                            '<div class="col-2" style="padding-left:32px;">' +
                                            '<div class="row">' +
                                            '<label class="remvbtn" onclick="RemoveSubChildRow(this)" style="margin-top:8px;cursor:pointer"></label>' +
                                            '</div>' +
                                            '</div>' +
                                            '</div>';
                                    })
                                }
                            }
                        })
                        var border_text = i == 0 ? '' : '<div class="pb-1 my-2 border-bottom box_title" style="font-size:13px;margin-right:65px;"></div>';
                        html += '<div class="div_multi_child">' + border_text +
                            '<div class="row">' +
                            '<div class="collapsebtn" onclick="toggleClass(this)"></div>' +
                            '<div class="col-9 pr-4">' +
                            '<div class="select_box m_bottom">' +
                            '<input type="hidden" class="childHidden" value="' + item.id + '" />' +
                            '<input class="form-control childInput" value="' + item.name + '" placeholder="Enter name" onchange="fn_CheckDuplicateOrg(this)" />' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-2 pl-2">' +
                            '<div class="row">' +
                            '<label class="addbtn" onclick="AddMoreSubChildRow(this,\'' + i + '\')" style="margin-top:8px;cursor:pointer;"></label>&emsp;' +
                            '<label class="remvbtn" onclick="RemoveChildRow(this)" style="margin-top:8px;cursor:pointer;"></label>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="row subchild" id="div_subchild' + i + '">' +
                            '<div class="col" style="padding-left:30px; padding-right:20px;">' +
                            '<div class="pb-1 ml-2 my-2 border-bottom box_title" id="text_subchild' + i + '" style="font-size:12px;margin-right:65px;">Name of Child Organizations for ' + item.name + '</div>' +
                            '<div id="div_multi_subchild' + i + '">' + _html +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    })
                    $('#div_multi_child').append(html);
                }
            }
        })
    }
}

function PrepareToSubmit() {
    var childOrgArray = [];
    if ($("#Name").val()=="") {
        toastr.warning('Parent organization name is required');
        return false;
    }
    if ($('.div_multi_child').length > 0) {
        $('.div_multi_child').each(function (i, ele) {
            var subChildOrgArray = [];
            if ($(ele).find('.div_subchild').length > 0) {
                $(ele).find('.div_subchild').each(function (_i, _ele) {
                    subChildOrgArray[_i] = {
                        SubChildOrgId: parseInt($(_ele).closest('.row').find('input:eq(0)').val()),
                        SubChildOrgName: $(_ele).closest('.row').find('input:eq(1)').val()
                    }
                })
            }
            childOrgArray[i] = {
                ChildOrgId: parseInt($(ele).find('.childHidden').eq(0).val()),
                ChildOrgName: $(ele).find('.childInput').eq(0).val(),
                SubChildOrgArray: subChildOrgArray.length > 0 ? subChildOrgArray : null
            }
        })
    }
    var str = childOrgArray.length > 0 ? JSON.stringify(childOrgArray) : null;
    var name = $('#Name').val();
    $.ajax({
        url: '/ApplicationManagement/Organization/AddUpadeteOrganization',
        type: 'post',
        data: { Id: $('#Id').val(), Name: name, OrganizationXML: str },
        success: function (res) {
            if (res.isSuccess) {
                toastr.success(name + ' saved successfully');
                setTimeout(function BackToHome() { location.reload(); },1000);
            } else {
                toastr.warning('something went wrong!');
            }
        },
        error: function (res) {
            toastr.error('internal server error.');
        }
    })
}

function AddMoreChildRow(btn) {
    var name = $('#Name').val();
    $('#div_child_text').text('Name of Child Organizations for ' + name);
    $('#div_child_text').removeAttr('hidden');
    var length = $('.div_multi_child').length;
    var border_text = length <= 0 ? '' : '<div class="pb-1 my-2 border-bottom box_title" style="font-size:13px;margin-right:20px;"></div>';
    var html = '<div class="div_multi_child">' + border_text +
        '<div class="row">' +
        '<div class="collapsebtn" onclick="toggleClass(this)"></div>' +
        '<div class="col-9 pr-4">' +
        '<div class="select_box m_bottom">' +
        '<input class="childHidden" type="hidden" value="0" />' +
        '<input class="form-control childInput" placeholder="Enter name" onchange="fn_CheckDuplicateOrg(this)"/>' +
        '</div>' +
        '</div>' +
        '<div class="col-2 pl-2">' +
        '<div class="row">' +
        '<label class="addbtn" onclick="AddMoreSubChildRow(this,\'' + length + '\')" style="margin-top:8px;cursor:pointer;"></label>&emsp;' +
        '<label class="remvbtn" onclick="RemoveChildRow(this)" style="margin-top: 8px;cursor: pointer"></label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row subchild" id="div_subchild' + length + '">' +
        '<div class="col" style="padding-left:60px;">' +
        '<div class="pb-1 ml-2 my-2 border-bottom box_title" id="text_subchild' + length + '" style="font-size:12px;margin-right:65px;"></div>' +
        '<div id="div_multi_subchild' + length + '">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $('#div_multi_child').append(html);
}

function AddMoreSubChildRow(btn, length) {
    var name = $(btn).closest('.row').parent().parent('div:eq(0)').find('input:eq(1)').val();
    $('#text_subchild' + length).text('Name of Child Organizations for ' + name);
    var html = '<div class="row div_subchild">' +
        '<div class="col-9 ml-2 pr-0">' +
        '<div class="select_box m_bottom">' +
        '<input type="hidden" value="0" />' +
        '<input class="form-control" placeholder="Enter name" onchange="fn_CheckDuplicateOrg(this)" />' +
        '</div>' +
        '</div>' +
        '<div class="col-2" style="padding-left:32px;">' +
        '<div class="row">' +
        '<label class="remvbtn" onclick="RemoveSubChildRow(this)" style="margin-top:8px;cursor:pointer"></label>' +
        '</div>' +
        '</div>' +
        '</div>';
    $(btn).closest('.div_multi_child').addClass('active');
    $('#div_multi_subchild' + length).append(html);
}

function RemoveChildRow(btn) {
    var childOrgId = parseInt($(btn).closest('.div_multi_child').children('div').find('.childHidden').val());
    var childOrgName = $(btn).closest('.div_multi_child').children('div').find('.childInput').val();
    if (childOrgId > 0) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You are about to delete all functions and process mapped to ' + childOrgName,
            showCancelButton: true,
            confirmButtonText: 'Delete',
        }).then((result) => {
            if (result.isConfirmed) {
                $(btn).closest('.div_multi_child').remove();
                if ($('.div_multi_child').length == 0) $('#div_child_text').attr('hidden', 'hidden');
            }
        })
    } else {
        $(btn).closest('.div_multi_child').remove();
        if ($('.div_multi_child').length == 0) $('#div_child_text').attr('hidden', 'hidden');
    }

}

function RemoveSubChildRow(btn) {
    var subChildOrgId = parseInt($(btn).parent().parent('div').closest('.row').find('input:eq(0)').val());
    var subChildOrgName = $(btn).parent().parent('div').closest('.row').find('input:eq(1)').val();
    if (subChildOrgId > 0) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You are about to delete all functions and process mapped to ' + subChildOrgName,
            showCancelButton: true,
            confirmButtonText: 'Delete',
        }).then((result) => {
            if (result.isConfirmed) {
                $(btn).parent().parent('div').closest('.row').remove();
            }
        })
    } else {
        $(btn).parent().parent('div').closest('.row').remove();
    }
}

function toggleClass(btn) {
    $(btn).parent().parent().toggleClass("active");
}

function fn_CheckDuplicateOrg(input) {
    var name = $(input).val();
    $.ajax({
        url: '/ApplicationManagement/Organization/CheckOrgDuplicate',
        type: 'get',
        data: { OrgName: name },
        success: function (res) {
            if (res.isSuccess) {
                toastr.warning(name + ' already exists');
                $(input).val('');
            }
        },
        error: function (res) {
            toastr.error('internal server error.');
        }
    })
}

function BackToHome() {
    location.reload()
};