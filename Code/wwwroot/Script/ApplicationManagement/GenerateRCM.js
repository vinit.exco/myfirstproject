﻿$(document).ready(function () {
    $("#roleMenu_").click(function () {
        $('#tblVersion tbody tr').find("td:eq(0) input[type=checkbox]").attr('checked', this.checked);
    })
    $("#roleMenu1_").click(function () {
        $('#tblRegulationRCM tbody tr').find("td:eq(0) input[type=checkbox]").attr('checked', this.checked);

    })

    $(".tab_ul_in > li").click(function () {
        $(".tab_ul_in > li").removeClass("active");
        $(this).addClass("active")
        var id = $(this).attr("data-id");
        $(".tab_content").removeClass("active");
        $("#" + id).addClass("active");
    //    $(".multiple").multiselect('rebuild');
    })
   
})
function fn_cancel() {
    $("#exampleModalCenter").modal('hide');
}
function fn_openPopup() {
    $('#ID').val(0);
    $("#DomainId").val('');
    $('#SubDomain').html('');
    $('#SubDomain').append('<option value="">Select SubDomain</option>');
    $("#SubDomain").val('');
    $("#ControlName").val('');
    $("#ControlTypeId").val('');
    $("#ControlId").val('');
    $("#ControlDescription").val('');
    $("#ControlCategoryId").val('');
    $("#ThreatId").val('');
    $("#multiRowDiv").html('');
    $("#multiRowDiv").append("<div class='row addRemoveDiv'><div class='col-md-4 col-sm-6 col-xs-12'><label>Name</label><div class='select_box m_bottom'>" +
        "<input class='form-control Artefacts' placeholder='Enter name' /></div></div><div class='col-2'>" +
        "<label class='addbtn' onclick='AddMoreRow()' style='cursor: pointer'></label></div></div></div>");
    $(".multiple").multiselect('rebuild');
    $("#exampleModalCenter").modal('show');
}
function fn_EditPopup(id) {
    $('#ID').val(id);
    $.get('/ApplicationManagement/GenerateRCMExisting/GetRCMControlDetails', { ControlID: id }, function (data) {
        debugger
        $("#DomainId").val(data.obj.domainId);
        $("#ControlName").val(data.obj.controlName);
        $("#ControlTypeId").val(data.obj.controlTypeId);
        $("#ControlId").val(data.obj.controlId);
        $("#ControlDescription").val(data.obj.controlDescription);
        $('#SubDomain').html('');
        $('#SubDomain').append('<option value="">Select SubDomain</option>');
        $.each(data.obj.domainList, function (i, value) {
            $('#SubDomain').append('<option value="' + value.id + '">' + value.subDomain + '</option>');
        });
        $("#multiRowDiv").html('');
        if (data.obj.artefactlist.length>0) {
            $.each(data.obj.artefactlist, function (i, value) {
                if (i==0) {
                    $("#multiRowDiv").append("<div class='row addRemoveDiv'><div class='col-md-4 col-sm-6 col-xs-12'><label>Name</label><div class='select_box m_bottom'>" +
                        "<input class='form-control Artefacts' placeholder='Enter name' value=" + value.name + " /></div></div><div class='col-2'>" +
                        "<label class='addbtn' onclick='AddMoreRow()' style='cursor: pointer'></label></div></div></div>");
                } else {
                    $("#multiRowDiv").append("<div class='row addRemoveDiv'><div class='col-md-4 col-sm-6 col-xs-12'><label>Name</label><div class='select_box m_bottom'>" +
                        "<input class='form-control Artefacts' placeholder='Enter name' value=" + value.name + " /></div></div><div class='col-2'>" +
                        "<label class='remvbtn' onclick='RemoveRow(this)' style='cursor:pointer'></label></div></div></div>");
                }
            });
        } else {
            $("#multiRowDiv").append("<div class='row addRemoveDiv'><div class='col-md-4 col-sm-6 col-xs-12'><label>Name</label><div class='select_box m_bottom'>" +
                "<input class='form-control Artefacts' placeholder='Enter name' /></div></div><div class='col-2'>" +
                "<label class='addbtn' onclick='AddMoreRow()' style='cursor: pointer'></label></div></div></div>");
        }
        if (data.obj.threatId != null) {
            $("#ThreatId").val(data.obj.threatId.split(',')).change();
        }
        if (data.obj.controlCategoryId != null) {
            $("#ControlCategoryId").val(data.obj.controlCategoryId.split(',')).change();
        }
        $("#SubDomain").val(data.obj.subDomain);
        $(".multiple").multiselect('rebuild');
        $("#exampleModalCenter").modal('show');
    });
}
function tabNextBack(tabid, li) {
    $(".tab_ul_in > li").removeClass("active");
    $("#" + li).addClass("active")
    $(".tab_content").removeClass("active");
    $("#" + tabid).addClass("active");
}
function AddMoreRow() {
    var i = $('#multiRowDiv').children('div').length;
    $('#multiRowDiv').append('<div class="row addRemoveDiv">' +
        '<div class="col-md-4 col-sm-6 col-xs-12">' +
        '<label>Name</label>' +
        '<div class="select_box m_bottom">' +
        '<input class="form-control Artefacts" placeholder="Enter name"/>' +
        '</div></div>' +
        '<div class="col-2">' +
        '<label class="remvbtn" onclick="RemoveRow(this)" style="cursor:pointer"></label>' +
        '</div></div>');
}
function RemoveRow(div) {
    $(div).parent('div').eq(0).parent('div').remove();
}
function GetSubDomain(domain) {
    $.get('/Master/Domain/GetSubDomainByDomainId', { DomainId: parseInt($(domain).val()) }, function (data) {
        $('#SubDomain').html('');
        $('#SubDomain').append('<option value="">Select SubDomain</option>');
        $.each(data, function (i, value) {
            $('#SubDomain').append('<option value="' + value.id + '">' + value.subDomain + '</option>');
        });
    });
}
function fn_getVal(subDomain) {
    $.ajax({
        url: "/master/IntregratedControl/CodeById",
        type: "POST",
        cache: false,
        data: { DomainId: $("select#DomainId option:selected").val(), SubDomain: $("select#SubDomain option:selected").text() },
        success: function (result) {
            var code = result.returnMessage;
            $.ajax({
                url: "/master/IntregratedControl/ControlById",
                type: "POST",
                cache: false,
                data: { DomainId: $("select#DomainId option:selected").val(), SubDomainId: $("select#SubDomain option:selected").val() },
                success: function (res) {
                    $("input[name=ControlId]").val(code + "-" + (res.returnMessage == null ? 1 : parseInt(res.returnMessage) + 1));
                },
                error: function (ex) {
                    console.log(ex.responseText);
                }
            })
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    });
}
function fnSetMultiselectValue() {
    var RCMThreatMapXML = [], RCMArtefactMapXML = []
    $('#ThreatId option:selected').each(function (i) {
        if ($(this).val() == 'multiselect-all') {
            return;
        }
        RCMThreatMapXML[i] = {
            Id: 0,
            ThreatId: parseInt($(this).val())
        }
    });
    $('.Artefacts').each(function (i, v) {
        RCMArtefactMapXML[i] = {
            Id: 0,
            Name: $(this).val()
        }
    });

    if ($("#DomainId").val() == "") {
        toastr.warning("Please select domain.");
        return false;
    }
    if ($("#SubDomain").val() == "") {
        toastr.warning("Please select sub domain.");
        return false;
    }
    if ($('#ControlId').val() == '' || $('#ControlId').val() == null) {
        toastr.warning('Please enter control id!');
        return false;
    }
    if ($('#ControlName').val() == '' || $('#ControlName').val() == null) {
        toastr.warning('Please enter control name!');
        return false;
    }
    if ($('#ControlDescription').val() == '' || $('#ControlDescription').val() == null) {
        toastr.warning('Please enter control description!');
        return false;
    }
    if ($("#ControlCategoryId").val() == null) {
        toastr.warning('Please select atleast one control category!');
        return false;
    }
    if ($("#ControlTypeId").val() == 0) {
        toastr.warning('Please select a control type!');
        return false;
    }
    if (RCMThreatMapXML.length == 0) {
        toastr.warning('Please select atleast one threat!');
        return false;
    }
    else {
        var model = {
            ID: $('#ID').val(),
            MapId: $('#MapId').val(),
            OrgID: $("#OrgID").val(),
            DomainId: $("#DomainId").val(),
            DomainName: $("#DomainId option:selected").text(),
            SubDomain: $("#SubDomain").val(),
            SubDomainName: $("#SubDomain option:selected").text(),
            ControlId: $('#ControlId').val(),
            ControlName: $("#ControlName").val(),
            ControlDescription: $('#ControlDescription').val(),
            ControlTypeId: $('#ControlTypeId').val(),
            ControlTypeName: $('#ControlTypeId option:selected').text(),
            ControlCategoryId: $("#ControlCategoryId").val().toString(),
            RCMThreatMapXML: JSON.stringify(RCMThreatMapXML),
            RCMArtefactMapXML: JSON.stringify(RCMArtefactMapXML),

        }
        $.ajax({
            url: "/ApplicationManagement/GenerateRCMExisting/SaveUpdateRCMICD",
            type: "POST",
            data: { model: model },
            success: function (res) {
                if (res.isSuccess) {
                    toastr.success('Control saved successfully.')
                    $('#exampleModalCenter').modal('hide');
                } else {
                    toastr.error('Somthing went wrong!');
                }
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        })
    }
}

function fn_ShowRCMICD(input) {
    $('#ICDFirstView').show();
    $('#ICDSecondView').hide();
    $("#ICDThirdView").hide();
    $("#ICDFourthView").hide();
    $('#divUploadRCM').hide();
}

function fn_ShowOrgICD(input) {
    $('#ICDFirstView').hide();
    $('#ICDSecondView').hide();
    $("#ICDThirdView").hide();
    $("#ICDFourthView").hide();
    $('#divUploadRCM').show();
}

function fn_MainDiv(input) {
    $("#MainDiv").show();
    var OrgName = $("#OrgID option:selected").text();
    $("#lblRCMICD").text('Generate RCM for ' + OrgName + ' from Integrated Control DB');
    $("#lblOrgICD").text('Use RCM provided by ' + OrgName);
}

function fn_MoveForward(ID) {
    if (ID == 1) {
        var sectorIDs = $("#SectorID").val();
        var CountryIDs = $("#CountryID").val();
        if (sectorIDs == null) {
            toastr.error('Please select sector');
        } else if (CountryIDs == null) {
            toastr.error('Please select country');
        } else {
            $("#tblQuestionBank tbody").empty();
            $.ajax({
                url: "/ApplicationManagement/GenerateRCM/BindQuestionBanklist",
                type: "POST",
                data: { CountryId: CountryIDs.toString() },
                success: function (result) {
                    $("#tblQuestionBank tbody").append(result);
                    $('#ICDFirstView').hide();
                    $('#ICDSecondView').show();
                    $("#ICDThirdView").hide();
                    $("#ICDFourthView").hide();
                },
                error: function (ex) {
                    console.log(ex.responseText);
                }
            })
        }
    }
    else if (ID == 2) {
        var QuestionsID = fn_GetQuestionIDs();
        //if (QuestionsID == "") {
        //    toastr.error('Please select at least one question');
        //} else {
            $("#tblVersion tbody").empty();
            $("#tblRegulationRCM tbody").empty();
            var SectorIDs = $("#SectorID").val();
            var CountryIDs = $("#CountryID").val();
            var IsAadhar = $('#tblQuestionBank tbody tr').eq(2).find("#chkYes").is(":checked");
            var PCIDss = $('#tblQuestionBank tbody tr').eq(3).find("#chkYes").is(":checked");
            var Hipaa = $('#tblQuestionBank tbody tr').eq(4).find("#chkYes").is(":checked");
            var PersonalInfo = $('#tblQuestionBank tbody tr').eq(5).find("#chkYes").is(":checked");

            $.ajax({
                url: "/ApplicationManagement/GenerateRCM/BindQuestionStandardAndRegulation",
                type: "POST",
                data: { QuestionsID: QuestionsID, SectorId: SectorIDs.toString().replace("multiselect-all,", ""), CountryId: CountryIDs.toString().replace("multiselect-all,", ""), IsAadhar: IsAadhar, PCIDss: PCIDss, Hipaa: Hipaa, PersonalInfo:PersonalInfo },
                success: function (result) {
                    $("#tblVersion tbody").append(result.str2);
                    $("#tblRegulationRCM tbody").append(result.str);
                    $('#ICDFirstView').hide();
                    $('#ICDSecondView').hide();
                    $("#ICDThirdView").show();
                    $("#ICDFourthView").hide();
                },
                error: function (ex) {
                    console.log(ex.responseText);
                }
            })
       // }
    }
    else {
        if ($('#tblVersion :checked').length == 0) {
            toastr.warning('Select atleast one standard!');
            return false;
        }
        if ($('#tblRegulationRCM :checked').length == 0) {
            toastr.warning('Select atleast one regulation!');
            return false;
        }
        var IsThirdParty = $('#tblQuestionBank tbody tr').eq(0).find("#chkYes").is(":checked");
        var IsCloud = $('#tblQuestionBank tbody tr').eq(1).find("#chkYes").is(":checked");
        var PersonalInfo = $('#tblQuestionBank tbody tr').eq(5).find("#chkYes").is(":checked");
        var StandardIds = "";
        var RegulationIds = "";
        $('#tblVersion tbody tr').each(function (i, tr) {
            if ($(tr).find("td:eq(0) input[type=checkbox]").is(":checked")) {
                StandardIds = StandardIds + $(tr).find("td:eq(0) input[type=checkbox]").val() + ',';
            }
        });
        $('#tblRegulationRCM tbody tr').each(function (i, tr) {
            if ($(tr).find("td:eq(0) input[type=checkbox]").is(":checked")) {
                RegulationIds = RegulationIds + $(tr).find("td:eq(0) input[type=checkbox]").val() + ',';
            }
        });
        StandardIds = StandardIds.slice(0, -1);
        RegulationIds = RegulationIds.slice(0, -1);
        var IsIaas = $("#IsIaas").is(":checked");
        var IsPaaS = $("#IsPaaS").is(":checked");
        var IsSaaS = $("#IsSaaS").is(":checked");
        $("#tblComplianceScore tbody").empty();
        console.log(StandardIds, RegulationIds);
        var model = {
            IsThirdParty: IsThirdParty,
            IsCloud: IsCloud,
            PersonalInfo: PersonalInfo,
            StandardIds: StandardIds,
            RegulationIds: (RegulationIds == "" ? "0" : RegulationIds),
            IsIaas: IsIaas,
            IsPaaS: IsPaaS,
            IsSaaS: IsSaaS
        }
        debugger
        $.ajax({
            url: "/ApplicationManagement/GenerateRCM/BindComplianceScore",
            type: "POST",
            data: { model: model },
            success: function (result) {
                debugger
                if (result.html2 != "") {
                    $("#tblComplianceScore tbody").append(result.html2);
                    $('#ICDFirstView').hide();
                    $('#ICDSecondView').hide();
                    $("#ICDThirdView").hide();
                    $("#ICDFourthView").show();
                }
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        })

    }
}

function fn_MoveBackward(ID) {
    if (ID == 1) {
        $('#ICDFirstView').show();
        $('#ICDSecondView').hide();
        $("#ICDThirdView").hide();
        $("#ICDFourthView").hide();
    } else if (ID == 2) {
        $('#ICDFirstView').hide();
        $('#ICDSecondView').show();
        $("#ICDThirdView").hide();
        $("#ICDFourthView").hide();
    } else {
        $('#ICDFirstView').hide();
        $('#ICDSecondView').hide();
        $("#ICDThirdView").show();
        $("#ICDFourthView").hide();
    }
}

function fn_GetQuestionIDs() {
    var QuestionId = "";
    $('#tblQuestionBank tbody tr').each(function (i, tr) {
        debugger
        if ($(tr).find("#chkYes").is(":checked")) {
            QuestionId = QuestionId + $(tr).find("#chkYes").val() + ',';
        }
    });
    return QuestionId.slice(0, -1);
}

//function fn_bindOverallEffectiveness(input) {
//    debugger
//    if ($(input).val() == 1) {
//        $(input).parent().parent().find('td:eq(8) input[type="text"]').val('NA');
//        $(input).parent().parent().find('td:eq(7) select').val(0);
//        $(input).parent().parent().find('td:eq(7) select').prop('disabled', true);
//    } else if ($(input).val() == 3) {
//        $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Not Effective');
//        $(input).parent().parent().find('td:eq(7) select').val(0);
//        $(input).parent().parent().find('td:eq(7) select').prop('disabled', true);
//    } else if ($(input).val() == 2) {
//        $(input).parent().parent().find('td:eq(7) select').prop('disabled', false);
//        var ChildID = $(input).parent().parent().find('td:eq(7) option:selected').val();
//        if (ChildID == 2) {
//            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Effective');
//        } else if (ChildID == 3) {
//            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Partially Effective');
//        } else {
//            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('');
//        }
//    } else {
//        $(input).parent().parent().find('td:eq(7) select').val(0);
//        $(input).parent().parent().find('td:eq(8) input[type="text"]').val('');
//    }
//}

//function fn_bindOverallEffectiveness2(input) {
//    var parentID = $(input).parent().parent().find('td:eq(6) option:selected').val();
//    if (parentID == 2) {
//        if ($(input).val() == 3) {
//            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Partially Effective');
//        } else if ($(input).val() == 2) {
//            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Effective');
//        } else {
//            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('');
//        }
//    }
//}

//function Calc(input) {
//    debugger
//    var Count = 0;
//    var cEffective = 0;
//    var cPartiallyEffective = 0;
//    var cNA = 0;
//    var SubDomainID = $(input).parent().parent().find('td:eq(0)').text();
//    $('#tblComplianceScore tbody tr').each(function (i, tr) {
//        if ($(tr).find("td:eq(0)").text() == SubDomainID) {
//            Count += 1;
//            var Name = $(tr).find("td:eq(8) input[type=text]").val();
//            if (Name == "Effective") {
//                cEffective += 1;
//            } else if (Name == "Partially Effective") {
//                cPartiallyEffective += 1;
//            } else if (Name == "NA") {
//                cNA += 1;
//            }
//        }
//    });
//    debugger
//    var tcEffective = (cEffective * 5);
//    var tcPartiallyEffective = (cPartiallyEffective * 3);
//    var tCount = (Count - cNA);
//    var finlaScore = (((tcEffective + tcPartiallyEffective) / tCount)) / 5;
//    var Percentage = parseInt(finlaScore * 100);
//    if (isNaN(Percentage)) {
//        Percentage = 0;
//    }
//    if (Percentage < 60) {
//        $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").css('background-color', '#f4c2c2');
//    } else if (Percentage >= 60 && Percentage < 80) {
//        $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").css('background-color', '#ffff99');
//    } else if (Percentage >= 80 && Percentage < 100) {
//        $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").css('background-color', '#92d050');
//    } else {
//        $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").css('background-color', '#00b050 ');
//    }
//    $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").text(Percentage + '%');
//}

function fn_SaveMultipleCheckbox() {
    debugger
    var arr = new Array();
    $('#tblComplianceScore tbody tr').each(function (i, tr) {
        debugger;
        arr[i] = {
            DomainID: $(tr).find("td:nth-child(1) input[type=hidden]").val(),
            DomainName: $(tr).find("td:nth-child(1)").text(),
            SubDomainID: $(tr).find("td:nth-child(2) input[type=hidden]").val(),
            SubDomainName: $(tr).find("td:nth-child(2)").text(),
            ICDID: $(tr).find("td:nth-child(3) input[type=hidden]").val(),
            ControlName: $(tr).find("td:nth-child(3)").text(),
            ControlDescription: $(tr).find("td:nth-child(4)").text(),
            ControlID: $(tr).find("td:nth-child(5)").text(),
            ControlTypeID: $(tr).find("td:nth-child(6) input[type=hidden]").val(),
            ControlTypeName: $(tr).find("td:nth-child(6)").text(),
            Governance: ($(tr).find("td:nth-child(7)").text() == "Yes" ? 1 : 0),
            Application: ($(tr).find("td:nth-child(8)").text() == "Yes" ? 1 : 0),
            Infrastructure: ($(tr).find("td:nth-child(9)").text() == "Yes" ? 1 : 0),
            Cloud: ($(tr).find("td:nth-child(10)").text() == "Yes" ? 1 : 0),
            Networks: ($(tr).find("td:nth-child(11)").text() == "Yes" ? 1 : 0),
            People: ($(tr).find("td:nth-child(12)").text() == "Yes" ? 1 : 0),
            Location: ($(tr).find("td:nth-child(13)").text() == "Yes" ? 1 : 0),
        }
    });
    $("#xmlRCM").val(JSON.stringify(arr));
    toastr.success('saved successfully');
}

function fn_showRCmListByOrg(input) {
    $("#MainDiv").show();
    var OrgID = $("#OrgID").val();
    $.ajax({
        url: "/ApplicationManagement/GenerateRCMExisting/GetRCMListByOrgID",
        type: "POST",
        data: { OrgID: OrgID },
        success: function (result) {
            $("#tblRCm tbody").empty();
            if (result.length > 0) {
                for (var i = 0; i < result.length; i++) {
                    $("#tblRCm tbody").append("<tr><td style=width:30%>" + result[i].name + "</td><td style='text-align:center'>" + (result[i].userName + ' <hr/> ' + result[i].createOnNew) +
                        "</td><td style='text-align:center'>" + (result[i].lastUpdatedBy + '<hr/>' + result[i].updateOnnew) + "</td><td><a class='clsDownload' href='/ApplicationManagement/GenerateRCMExisting/DrawingWinnersExcel?Id=" + result[i].id + "' title='Download RCM'><i class='fa fa-download' style='font-size:26px'></i></a><a class='view_btn' title='View RCM' hidden onclick='fn_ViewRCM(" + result[i].id + ")'></a><a title='Edit RCM' onclick='fn_EditRCM(" + result[i].id + ")' class='edit_btn'></a><a title='Delete RCM' onclick='fn_DeleteRCM(" + result[i].id + ",this)' class='delete_btn'></a><a class='clsDupCopy' title='Generate RCM Version' onclick='fn_GenerateRCMVersion(" + result[i].id +")'><i class='fa fa-clone' style='font-size:26px'></i></a></td></tr>");
                }
            } else {
                $("#tblRCm tbody").append("<tr><td colspan=3 style='text-align:center'>No record found..</td></tr>");
            } 
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function fn_ViewRCM(ID) {
    $.ajax({
        url: "/ApplicationManagement/GenerateRCMExisting/Index2",
        type: "POST",
        data: { ID: ID },
        success: function (result) {
            $("#ajaxresult").html(result);
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}


function fn_DownloadRCM(ID) {
    $.ajax({
        url: "/ApplicationManagement/GenerateRCMExisting/DrawingWinnersExcel",
        type: "POST",
        data: { drawingId: ID },
        success: function (result) {
            $("#ajaxresult").html(result);
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function fn_EditRCM(ID) {
    $.ajax({
        url: "/ApplicationManagement/GenerateRCMExisting/Index3",
        type: "POST",
        data: { ID: ID },
        success: function (result) {
            $("#ajaxresult").html(result);
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}
function fn_GenerateRCMVersion(ID) {
    $.ajax({
        url: "/ApplicationManagement/GenerateRCMExisting/GenerateRCMVersion",
        type: "POST",
        data: { ID: ID },
        success: function (result) {
            toastr.success('RCM version generated successfully');
            fn_showRCmListByOrg();
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function fn_DeleteRCM(ID,input) {
    Swal.fire({
        title: 'Are you sure?',
        text: 'You are about to delete RCM',
        showCancelButton: true,
        confirmButtonText: 'Delete',
    }).then((result) => {
        if (result.isConfirmed) {
            $('#preloader').show();
            $.get('/ApplicationManagement/GenerateRCMExisting/DeleteRCM', { ID: ID }, function (res) {
                debugger
                if (res.isSuccess) {
                    $('#preloader').hide();
                    toastr.success('deleted successfully');
                    $(input).parent().parent().remove();
                } else {
                    toastr.warning('something went wrong!');
                }
            })
        }
    })
}

$(".orgmap .Event").click(function () {
    if ($(this).parent().parent().hasClass("inactive")) {
        $(".orgmap li.first").addClass("inactive");
        $(this).parent().parent().removeClass("inactive");
    } else {
        $(this).parent().parent().addClass("inactive");
    }
    
})

function fn_BindRCmControls(SubDomainName, input) {
    $(".orgmap li span.text").removeClass("active");
    $(input).addClass("active");
    $.ajax({
        url: "/ApplicationManagement/GenerateRCMExisting/GetRCMListBySubDomain",
        type: "POST",
        data: { ID: $("#MapId").val(), SubDomainName: SubDomainName },
        success: function (result) {
            $("#tblRCm tbody").empty();
            if (result.length > 0) {
                for (var i = 0; i < result.length; i++) {
                    $("#tblRCm tbody").append("<tr><td>" + result[i].controlName + "</td><td style=width:25%>" + result[i].controlDescription + "</td>" +
                        "<td>" + result[i].controlId + "</td><td>" + result[i].controlTypeName + "</td>" +
                        "<td hidden>" + (result[i].governance == true ? "Yes" : "No") + "</td><td hidden>" + (result[i].application == true ? "Yes" : "No") + "</td>" +
                        "<td hidden>" + (result[i].infrastructure == true ? "Yes" : "No") + "</td><td hidden>" + (result[i].cloud == true ? "Yes" : "No") + "</td>" +
                        "<td hidden>" + (result[i].networks == true ? "Yes" : "No") + "</td><td hidden>" + (result[i].people == true ? "Yes" : "No") + "</td><td hidden>" + (result[i].location == true ? "Yes" : "No") + "</td>" +
                        "<td style=width:10%><a title='Edit Control' onclick='fn_EditPopup(" + result[i].id +")' class='edit_btn'></a><a title='Delete Control' onclick='fn_DeleteControl(" + result[i].id + ",this)' class='delete_btn'></a></td></tr>");
                }
            } else {
                $("#tblRCm tbody").append("<tr><td colspan='13' class='text-center'></td></tr>");
            }
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function fnValidateFile(input) {
    debugger;
    var fileSize = input.files[0].size;
    var filePath = $(input).val().toLowerCase();
    var fileName = filePath.substr((filePath.lastIndexOf('\\') + 1));
    var fileExtension = filePath.substr((filePath.lastIndexOf('.') + 1));
    if (!(fileExtension === 'xls' || fileExtension === 'xlsx' || fileExtension === 'xlsm')) {
        $('#excelFile').val('');
        $('#strongAlert').css('color', 'red').text('Alert! Please choose correct file format.');
    }
    //else if (fileName != 'templatecustomisedrcm.xlsx') {
    //    $('#excelFile').val('');
    //    $('#strongAlert').css('color', 'red').text('Alert! choose only downloaded excel file.');
    //}
    else {
        $('#divBtnSubmit').show();
        $('#strongAlert').css('color', 'green').text('Correct file format, click submit to upload this file.');
    }
}

function UploadExcel() {
    $('#divBtnSubmit').hide();
    var val = $('#excelFile').val();
    var files = $("#excelFile").get(0).files;
    if (window.FormData !== undefined) {
        var fileData = new FormData();
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
        }
        $.ajax({
            url: '/ApplicationManagement/GenerateRCM/GetJsonFromExcelFile',
            type: 'post',
            contentType: false,
            processData: false, 
            data: fileData,
            success: function (res) {
                if (res.code == 200) {
                    $.post('/ApplicationManagement/GenerateRCM/SubmitRCMByExcel', { orgId: $('#OrgID').val(), str: JSON.stringify(res.data) }, function (res) {
                        debugger;
                        if (res.isSuccess) {
                            $('#strongAlert').css('color', 'sandybrown').text('Please choose only downloaded excel file.');
                            toastr.success('RCM uploaded successfully.');
                        } else {
                            toastr.error('internal server error!');
                        }
                    })
                } else {
                    $('#strongAlert').css('color', 'sandybrown').text('Please choose only downloaded excel file.');
                    toastr.warning(res.message);
                }
                $('#excelFile').val('');
            },
            error: function (err) {
                toastr.error(err.statusText);
            }
        });
    } else {
        alert("FormData is not supported.");
    }
}

function fn_DeleteControl(ControlID, input) {
    Swal.fire({
        title: 'You are about to delete Control',
        text: 'Do you want to proceed',
        showCancelButton: true,
        confirmButtonText: 'Delete',
    }).then((result) => {
        debugger
        if (result.isConfirmed) {
            $('#preloader').show();
            $.get('/ApplicationManagement/GenerateRCMExisting/DeleteControlByID', { ControlID: ControlID }, function (res) {
                debugger
                if (res.isSuccess) {
                    $('#preloader').hide();
                    toastr.success('Control deleted successfully.');
                    $(input).parent().parent().remove();
                } else {
                    toastr.warning('something went wrong!');
                }
            })
        }
    })
}