﻿$(".orgmap .text").click(function () {
    $(this).parent().parent().toggleClass("inactive");
})

function SaveAssignedProcess() {
    var orgId = $('#OrgId').val();
    var orgName = $('#OrgName').val();
    var functionId = $('#FunctionId').val();
    var processId = $('#ProcessId').val();
    var assignToUserId = $('#UserId').val();
    if (functionId <= 0) {
        toastr.warning('Please select a function!');
    } else if (processId <= 0) {
        toastr.warning('Please select a process!');
    } else if (assignToUserId <= 0) {
        toastr.warning('Please select a user!');
    } else {
        var model = {
            OrgId: orgId,
            FunctionId: functionId,
            ProcessId: processId,
            AssignToUserId: assignToUserId
        }
        $.post('/ApplicationManagement/IRA/SaveAssignedProcess', { model: model }, function (res) {
            if (res.isSuccess) {
                toastr.success('Process assigned successfully.');
                $.get('/ApplicationManagement/IRA/AssProcess', { orgId: orgId, orgName: orgName }, (res) => $('#ajaxresult').html(res))
            } else {
                toastr.error('Something went wrong!');
            }
        })
    }
}

function GetProcesses(functionId) {
    var orgId = $('#OrgId').val();
    $('#ProcessId').empty();
    var options = '<option value="0">Select Process</option>';
    $.get('/ApplicationManagement/IRA/GetProcessesByFunctionId', { orgId: orgId, functionId: functionId }, function (res) {
        if (res.length > 0) {
            $.each(res, function (i, item) {
                options += '<option value="' + item.id + '">' + item.processName + '</option>'
            })
        }
        $('#ProcessId').html(options);
    })
}

function GetAssignToUserId(processId) {
    if (processId > 0) {
        $.get('/ApplicationManagement/IRA/GetAssignedUserByProcessesId', { processId: processId }, function (res) {
            if (res.assignToUserId > 0) {
                $('#UserId').val(res.assignToUserId);
            } else {
                $('#UserId').val(0);
            }
        })
    } else {
        $('#UserId').val(0);
    }
}

function GetDomainCriticalityScore(orgId, functionId, processId, processName, processOwner, processDescription) {
    $('#_OrgId').val(orgId);
    $('#_FunctionId').val(functionId);
    $('#_ProcessId').val(processId);
    $('#ProcessName').val(processName);
    $('#_ProcessName').val(processName); //for divMapEnabler process name.
    $('#ProcessOwner').val(processOwner);
    $('#ProcessDescription').val(processDescription);
    $.get('/ApplicationManagement/IRA/GetDomainCriticalityScoreByProcessesId', { orgId: orgId, functionId: functionId, processId: processId }, function (res) {
        if (res.html != "") {
            $('#DomainCritical').html(res.html);
            ManageDivHideShow('#divDomainCriticalityScore');
            $('#divDomainCriticalityScore2').show();
        }
        CalculateImpactScore()
    })
}

function CalculateAverage(input) {
    debugger
    var className = $(input).attr('class').split(' ')[1];
    var length = $('.' + className).length - 1;
    var value = 0;
    if (parseInt($(input).val()) < 1 || parseInt($(input).val()) > 5) {
        toastr.warning('Score should be between 1 to 5.');
        $(input).val('')
    } else {
        $('.' + className).each(function (i, ele) {
            if (i == length) {
                $(ele).val((value / length).toFixed(2));
            } else {
                value += parseInt($(ele).val());
            }
        })
        CalculateImpactScore()
    }
}

function CalculateImpactScore() {
    var score = 0.0;
    $('.parameter5').each((i, ele) => {
        var val = $(ele).find('input').val();
        score += parseFloat(val == '' ? 0 : val);
    })
    if (isNaN(score)) {
        $('.arrowicon').css('left', '1px');
        $('#impactScore').text('Impact Score : 0');
    } else {
        var avg = (score / $('.parameter5').length).toFixed(2);
        $('#impactScore').text('Impact Score : ' + avg);
        $('.arrowicon').css('left', (avg * (avg > 1 ? 69 : 1)) + 'px');
    }
}

function SubmitDomainCriticalityScore() {
    var orgId = $('#_OrgId').val();
    var orgName = $('#OrgName').val();
    var functionId = $('#_FunctionId').val();
    var processId = $('#_ProcessId').val();
    var DomainCriticalityScoreXML = [];
    $('.parameter').each(function (pI, pEle) {
        var parameterId = parseInt($(pEle).attr('id'));
        var domainScore = [];
        $('.parameter' + parameterId).each(function (dI, dEle) {
            var riskDomainId = parseInt($(dEle).find('input').attr('class').split(' ')[1].split('_')[1]);
            var score = parseFloat($(dEle).find('input').val());
            domainScore[dI] = {
                RiskDomainId: riskDomainId,
                Score: score
            }
        })
        DomainCriticalityScoreXML[pI] = {
            ParameterId: parameterId,
            RiskDomainScore: domainScore
        }
    })
    var model = {
        OrgId: orgId,
        FunctionId: functionId,
        ProcessId: processId,
        DomainCriticalityScoreXML: JSON.stringify(DomainCriticalityScoreXML)
    }
    $.post('/ApplicationManagement/IRA/SubmitDomainCriticalityScore', { model: model }, function (res) {
        res.isSuccess ? toastr.success('Saved successfully.') : toastr.error('Something went wrong!');
    })
    var process = {
        Id: processId,
        ProcessName: $('#ProcessName').val(),
        ProcessOwner: $('#ProcessOwner').val(),
        ProcessDescription: $('#ProcessDescription').val(),
    }
    $.post('/ApplicationManagement/IRA/UpadateProcess', { model: process }, function (res) {
        $.get('/ApplicationManagement/IRA/AssProcess', { orgId: orgId, orgName: orgName }, (res) => $('#ajaxresult').html(res))
    })
}

function ManageDivHideShow(divId) {
    debugger;
    if (divId === '#divMapEnabler') {
        $.get('/ApplicationManagement/IRA/GetControlCategoryListByOrgId', { orgId: $('#_OrgId').val() }, res => {
            $('#divEnableList').empty();
            if (res.length > 0) {
                $.each(res, (i, item) => {
                    $('#divEnableList').append('<div class="col-4"><button class="btn_enabler" onclick="ShowModalMapEnabler(' + item.categoryId + ')">' + item.categoryName + '</button></div>')
                })
            }
        })
    }
    $('.addApplications').removeClass("active");
    setTimeout(() => {
        $('.addApplications').removeClass("in");
    }, 300)
    setTimeout(() => {
        $(divId).addClass("in");
        setTimeout(() => {
            $(divId).addClass("active");
        }, 200)
    }, 400)
}

SearchQuestionnaire = (input, ulId) => {
    if ($(input).val() == '') $(ulId + ' li').removeAttr('hidden');
    else $(ulId + ' li').each((i, ele) => { if ($(ele).find('label').text().toLowerCase().includes($(input).val().toLowerCase().trim())) $(ele).removeAttr('hidden'); else $(ele).attr('hidden', 'hidden') })
}

function ShowModalMapEnabler(conCatId) {
    debugger;
    $('#ConCatId').val(conCatId);
    var orgId = $('#_OrgId').val();
    var functionId = $('#_FunctionId').val();
    var processId = $('#_ProcessId').val();
    var conCatId = $('#ConCatId').val();
    $.ajax({
        async: false,
        url: '/ApplicationManagement/IRA/GetFirstQuesAnswersByConCatId',
        data: { conCatId: conCatId, orgId: orgId },
        success: function (res) {
            $('#liQuestion').empty();
            var li = '';
            $.each(res, function (i, item) {
                li += '<li><input id="id' + item.value + '" class="customcheckbtn conCat' + conCatId + '" type="checkbox" value="' + item.value + '" /><label for="id' + item.value + '">' + item.answer + '</label></li>';
            });
            $.ajax({
                async: false,
                url: '/ApplicationManagement/IRA/GetQuestionnaireIdByProcessId',
                data: { orgId: orgId, functionId: functionId, processId: processId, conCatId: conCatId },
                success: function (res) {
                    $('#liQuestion').html(li);
                    if (res.length > 0) {
                        $.each(res, function (i, item) {
                            $('#id' + item).prop('checked', true);
                        })
                    }
                }
            })
            $('#modalMapEnabler').modal('show');
            $(".modal-backdrop").addClass("in");
        }
    })
}

function SubmitProcessEnablerMap() {
    var orgId = $('#_OrgId').val();
    var functionId = $('#_FunctionId').val();
    var processId = $('#_ProcessId').val();
    var conCatId = $('#ConCatId').val();
    var array = [];
    $('.conCat' + conCatId + ':checked').each(function (i, ele) {
        array[i] = {
            OrgId: orgId,
            FunctionId: functionId,
            ProcessId: processId,
            ConCatId: conCatId,
            QuestionnaireId: $(ele).val()
        }
    })
    $.ajax({
        type: 'post',
        url: '/ApplicationManagement/IRA/SubmitProcessEnablerMap',
        data: { strJson: JSON.stringify(array) },
        success: function (res) {
            if (res.isSuccess) {
                toastr.success('Saved successfully.');
                $('#modalMapEnabler').modal('hide');
            } else {
                toastr.error('Something went wrong!');
            }
        }
    })
}

function AddQuestionnaire() {
    var orgId = $('#_OrgId').val();
    var conCatId = $('#ConCatId').val();
    if (conCatId == 1 || conCatId == 6) {
        toastr.warning('You can add more questionnaire for this enabler category!');
        return;
    }
    $.ajax({
        url: "/ApplicationManagement/ClientEnabler/GetAddPartiipate",
        type: "POST",
        data: { id: conCatId, OrganizationId: orgId },
        success: function (result) {
            $('#tblApplications').empty();
            $('#tblApplications').html(result);
            $('#modalAddQuestionnaire').modal('show');
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function AddMoreAnswers(input) {
    debugger;
    var srNo = 1;
    var tdLength = $(input).closest('tr').find('td').length;
    var sn = parseInt($(input).closest('tbody').find("tr").length + 1);
    var html = '<tr><td style="color:black">' + sn + '</td>';

    for (var i = 0; i < tdLength; i++) {
        if (i == 0 || i == tdLength - 1) { continue; }
        html += '<td>';
        html += $(input).closest('tbody').find('tr:first td').eq(i).html();
        html += '</td>';
    }
    html += '<td><span class="clickIcon remove value="-" onclick="$(this).closest(\'tr\').remove()"></span></td></tr>';
    $(input).closest('tbody').append(html);
    $(input).closest('tbody').find('tr:last td input[type=text]').val('');
    $(input).closest('tbody').find('tr:last td input[type=hidden]').val(0);
}

$('#tblApplications').on('click', '.remove', function () {
    $(this).closest('tr').remove();
})

function SaveQuestionnaire() {
    $('#preloader').show();
    var conCatId = $('#ConCatId').val();
    var QuestionnaireXML = [], Sn = 0;
    $('.hidden').each(function (i, element) {
        var str = $(element).val();
        $('.questionId' + $(element).val()).each(function (j, item) {
            debugger
            QuestionnaireXML[Sn++] = {
                QuestionId: $(element).val(),
                Id: $(this).next('input').val(),
                Answer: $(item).val(),
                RowNo: (j + 1)
            }
        })

    });
    var model = {
        Id: $('#_OrgId').val(),
        CategoryId: conCatId,
        QuestionnaireXML: JSON.stringify(QuestionnaireXML),
    }
    $.post('/ApplicationManagement/ClientEnabler/SubmitClientEnabler', { model: model }, function (res) {
        if (res.isSuccess) {
            ShowModalMapEnabler(conCatId)
            $('#modalAddQuestionnaire').modal('hide');
            $('#tblApplications').empty();
            toastr.success('Saved successfully');
        } else {
            toastr.error('Something went wrong!');
        }
    });
}
