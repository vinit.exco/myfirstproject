﻿
function fn_ShowFun(input) {
    $('#multiFunctionDiv').html('');
    if ($(input).val() > 0) {
        $.ajax({
            url: '/ApplicationManagement/OrganizationFunction/GetFunctionByOrganization',
            data: { OrgID: $('#OrgID').val() },
            type: "POST",
            success: function (result) {
                debugger;
                $('#Id').val(result.id);
                $('#multiFunctionDiv').append(result.str);
                $(".submitbtn").show();
                $("#formFunctionsProcesses").show();
                $("#spnFunName").text('Enter Functions of ' + $("#OrgID option:selected").text());
            }
        })
    } else {
        $(".submitbtn").hide();
        $("#formFunctionsProcesses").hide();
    }
}

function fn_AddFunction(btn) {
    var functionHTML = '<div class="row functions">' +
        '<div class="col-12">' +
        '<div class="row">' +
        '<div class="collapsebtn" onclick="toggleClass(this)"></div>' +
        '<div class="col-3">' +
        '<label class="required">Name</label>' +
        '<div class="select_box m_bottom">' +
        '<input type="hidden" class="functionId" value="0" />' +
        '<input type="text" class="form-control Functionname" required placeholder="Function Name" onchange="fn_CheckDuplicateFun(this)" />' +
        '</div>' +
        '</div>' +
        '<div class="col-3">' +
        '<label>Owner</label>' +
        '<div class="select_box m_bottom">' +
        '<input type="text" class="form-control FunctionOwner" />' +
        '</div>' +
        '</div>' +
        '<div class="col-4">' +
        '<label>Description</label>' +
        '<div class="select_box m_bottom">' +
        '<input type="text" class="form-control FunctionDescription" />' +
        '</div>' +
        '</div>' +
        '<div class="col-1">' +
        '<label class="addbtn" onclick="fn_AddProcess(this)" title="Add Process" style="cursor:pointer"></label>' +
        '<label class="remvbtn htn" onclick="fn_DeleteFunction(this)" title="Delete Function" style="cursor:pointer"></label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $('#multiFunctionDiv').append(functionHTML);
}

function fn_AddProcess(btn) {
    var processHTML = '<div class="col-12 divProcess">' +
        '<div class="adddivProcess">' +
        '<div class="row">' +
        '<div class="col-1">' +
        '</div>' +
        '<div class="col-3">' +
        '<label class="required">Name</label>' +
        '<div class="select_box m_bottom">' +
        '<input type="hidden" class="processId" value="0" />' +
        '<input type="text" class="form-control m_no" name="ProcessName" required placeholder="Process Name" onchange="fn_CheckDuplicateFun(this)">' +
        '</div>' +
        ' </div>' +
        '<div class="col-3">' +
        '<label>Owner</label>' +
        '<div class="select_box m_bottom">' +
        '<input type="text" class="form-control m_no" name="ProcessOwner">' +
        '</div>' +
        '</div>' +
        '<div class="col-4">' +
        '<label>Description</label>' +
        '<input type="text" class="form-control m_no" name="ProcessDescription">' +
        '</div>' +
        '<div class="col-1">' +
        '<label class="remvbtn" onclick="fn_DeleteProcess(this)" title="Delete Process" style="cursor:pointer"></label>' +
        ' </div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $(btn).closest('.functions').addClass('active');
    $(btn).parent().parent().parent().append(processHTML);
}

function fn_CheckDuplicateFun(input) {
    var name = $(input).val();
    //var classs = $(input).attr('class').split(' ')[1];
    //debugger;
    //$('.' + classs).each((i, ele) => {
    //    var namee = $(ele).val();
    //    if (i === Number($('.' + classs).index() - 1)) return;
    //    $(ele).val(name === namee ? '' : namee);
    //});
    $.ajax({
        url: '/ApplicationManagement/OrganizationFunction/CheckFunProDuplicate',
        type: 'get',
        data: { orgId: $('#OrgID').val(), FunProName: name },
        success: function (res) {
            if (res.countFunction > 0 || res.countProcess > 0) {
                toastr.warning(name + ' already exists');
                $(input).val('');
            }
        },
        error: function (res) {
            toastr.error('internal server error.');
        }
    })
}

function fn_DeleteFunction(input) {
    var functionId = parseInt($(input).parent().parent('div').find('.functionId').val());
    var functionName = $(input).parent().parent('div').find('.Functionname').val();
    if (functionId > 0) {
        Swal.fire({
            title: 'Confirm!',
            text: 'Are you sure? You want to delete ' + functionName,
            showCancelButton: true,
            confirmButtonText: 'Delete',
        }).then((result) => {
            if (result.isConfirmed) {
                $(input).parent().parent().parent().parent().remove();
            }
        })
    } else {
        $(input).parent().parent().parent().parent().remove();
    }
}

function fn_DeleteProcess(input) {
    var processId = parseInt($(input).parent().parent('div').find('.processId').val());
    var processName = $(input).parent().parent('div').find('[name="ProcessName"]').val();
    if (processId > 0) {
        Swal.fire({
            title: 'Confirm!',
            text: 'Are you sure? You want to delete ' + processName,
            showCancelButton: true,
            confirmButtonText: 'Delete',
        }).then((result) => {
            if (result.isConfirmed) {
                $(input).parent().parent().remove();
            }
        })
    } else {
        $(input).parent().parent().remove();
    }
}

function fn_SaveFunctionxml() {
    var FunctionProcessXML = [];
    $('.functions').each(function (i, ele) {
        var processArray = [];
        var functionId = $(ele).find(".functionId").val();
        var functionName = $(ele).find(".Functionname").val();
        var functionOwner = $(ele).find(".FunctionOwner").val();
        var functionDescription = $(ele).find(".FunctionDescription").val();
        var sn = 0;
        $(ele).find('.divProcess').each(function (iProc, eleProc) {
            debugger;
            var processId = $(eleProc).find('.processId').val();
            var ProcessName = $(eleProc).find('[name="ProcessName"]').val();
            var ProcessOwner = $(eleProc).find('[name="ProcessOwner"]').val();
            var ProcessDescription = $(eleProc).find('[name="ProcessDescription"]').val();
            if (typeof (processId) === "undefined") return;
            processArray[sn] = {
                ProcessId: processId,
                ProcessName: ProcessName,
                ProcessOwner: ProcessOwner,
                ProcessDescription: ProcessDescription,
            }
            sn++;
        })
        FunctionProcessXML[i] = {
            FunctionId: functionId,
            FunctionName: functionName,
            FunctionOwner: functionOwner,
            FunctionDescription: functionDescription,
            Processses: processArray.length > 0 ? processArray : null
        }
    })
    $("#xmlFunction").val(JSON.stringify(FunctionProcessXML));
    var model = {
        Id: $("#Id").val(),
        OrgID: $("#OrgID").val(),
        xmlFunction: $("#xmlFunction").val(),
    }
    $.ajax({
        url: "/ApplicationManagement/OrganizationFunction/SaveOrganizationFunction",
        type: "POST",
        data: { model: model },
        success: function (res) {
            if (res.isSuccess) {
                toastr.success('Function and Process saved successfully.');
                $.get('/ApplicationManagement/OrganizationFunction/Index', {}, (res) => $('#ajaxresult').html(res))
            }
            else {
                toastr.error('Somthing went wrong!');
            }
        }, error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function toggleClass(btn) {
    $(btn).parent().parent().parent().toggleClass("active");
}
