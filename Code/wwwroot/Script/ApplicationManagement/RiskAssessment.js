﻿
function AddButton() {
    $("#Id").val(0);
    $("#CorporateOfficeLocation").val('');
    $("#ParentOrgId").val('');
    $("#ChildOrgId").val('');
    $('#StateId').find('option:not(:first)').remove();
    $(".titile_heading h1").html("Assign Assessment");
    $(".service_line").addClass("active");
    $(".assign_panel").hide();
}

function fn_BindChildOrg(input) {
    $.ajax({
        url: "/ApplicationManagement/OrganizationFunction/BindChildOrganization",
        type: "POST",
        data: { ParentOrgId: $(input).val() },
        success: function (res) {
            debugger
            if (res.length > 0) {
                $('#ChildOrgId').find('option:not(:first)').remove();
                for (var i = 0; i < res.length; i++) {
                    $("#ChildOrgId").append("<option value=" + res[i].id + ">" + res[i].childOrgName + "</option>");
                }
            }
            else {
                $('#ChildOrgId').find('option:not(:first)').remove();
            }
        },
        error: function (ex) {
            console.log(ex.responseText);
        }
    })
}

function hideForm() {
    $('#dvDataTable').show();
    $(".titile_heading h1").html("Risk Assessment");
    $('#dvForm').attr('class', 'service_line');
}