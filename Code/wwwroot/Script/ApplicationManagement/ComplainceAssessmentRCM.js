﻿function fn_SaveMultipleCheckbox() {
    debugger
    var arr = new Array();
    var SectorIDs = $("#SectorID").val();
    var CountryIDs = $("#CountryID").val();
    $("#SectorIDs").val(SectorIDs.toString());
    $("#CountryIDs").val(CountryIDs.toString());
}


function fn_MoveForward(ID) {
    if (ID == 1) {
        $('#FirstView').hide();
        $('#SecondView').show();
        $("#ThirdView").hide();
        $("#FourthView").hide();
    } else if (ID == 2) {
        var QuestionsID = fn_GetQuestionIDs();
        if (QuestionsID == "") {
            toastr.error('Please select at least one question')
        } else {
            $("#tblStandardRCM tbody").empty();
            $("#tblRegulationRCM tbody").empty();
            var SectorIDs = $("#SectorID").val();
            var CountryIDs = $("#CountryID").val();
            $.ajax({
                url: "/ApplicationManagement/ComplainceAssessmentRCM/BindQuestionStandardAndRegulation",
                type: "POST",
                data: { QuestionsID: QuestionsID, SectorId: SectorIDs.toString(), CountryId: CountryIDs.toString() },
                success: function (result) {
                    debugger
                    for (var i = 0; i < result.res.length; i++) {
                        $("#tblStandardRCM tbody").append("<tr><td><input type='checkbox' checked value=" + result.res[i].id + "></td><td>" + result.res[i].title + "</td></tr>");
                    }
                    for (var i = 0; i < result.res2.length; i++) {
                        $("#tblRegulationRCM tbody").append("<tr><td><input type='checkbox' checked value=" + result.res2[i].id + "></td><td>" + result.res2[i].title + "</td></tr>");
                    }
                    $('#FirstView').hide();
                    $('#SecondView').hide();
                    $("#ThirdView").show();
                    $("#FourthView").hide();
                },
                error: function (ex) {
                    console.log(ex.responseText);
                }
            })
        }
        
    } else {
        $.ajax({
            url: "/ApplicationManagement/ComplainceAssessmentRCM/BindComplianceScore",
            type: "POST",
            data: { },
            success: function (result) {
                debugger
                if (result.html2 != "") {
                    $("#tblComplianceScore tbody").append(result.html2);
                    $('#FirstView').hide();
                    $('#SecondView').hide();
                    $("#ThirdView").hide();
                    $("#FourthView").show();
                }
            },
            error: function (ex) {
                console.log(ex.responseText);
            }
        })
    }
}

function fn_bindOverallEffectiveness(input) {
    debugger
    if ($(input).val() == 1) {
        $(input).parent().parent().find('td:eq(8) input[type="text"]').val('NA');
        $(input).parent().parent().find('td:eq(7) select').val(0); 
        $(input).parent().parent().find('td:eq(7) select').prop('disabled', true); 
    } else if ($(input).val() == 3) {
        $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Not Effective');
        $(input).parent().parent().find('td:eq(7) select').val(0); 
        $(input).parent().parent().find('td:eq(7) select').prop('disabled', true); 
    } else if ($(input).val() == 2) {
        $(input).parent().parent().find('td:eq(7) select').prop('disabled', false); 
        var ChildID = $(input).parent().parent().find('td:eq(7) option:selected').val();
        if (ChildID == 2) {
            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Effective');
        } else if (ChildID == 3) {
            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Partially Effective');
        } else {
            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('');
        }
    } else {
        $(input).parent().parent().find('td:eq(7) select').val(0); 
        $(input).parent().parent().find('td:eq(8) input[type="text"]').val('');
    }
}

function fn_bindOverallEffectiveness2(input) {
    var parentID = $(input).parent().parent().find('td:eq(6) option:selected').val();
    if (parentID==2) {
        if ($(input).val() == 3) {
            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Partially Effective');
        } else if ($(input).val() == 2) {
            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('Effective');
        } else {
            $(input).parent().parent().find('td:eq(8) input[type="text"]').val('');
        }
    }
}

function fn_GetQuestionIDs() {
    var QuestionId = "";
    $('#tblQuestionBank tbody tr').each(function (i, tr) {
        debugger
        if ($(tr).find("#chkYes").is(":checked")) {
            QuestionId = QuestionId + $(tr).find("#chkYes").val() + ',';
        }
    });
    return QuestionId.slice(0,-1);
}

function fn_MoveBackward(ID) {
    if (ID == 1) {
        $('#FirstView').show();
        $('#SecondView').hide();
        $("#ThirdView").hide();
        $("#FourthView").hide();
    } else if (ID == 2) {
        $('#FirstView').hide();
        $('#SecondView').show();
        $("#ThirdView").hide();
        $("#FourthView").hide();
    } else {
        $('#FirstView').hide();
        $('#SecondView').hide();
        $("#ThirdView").show();
        $("#FourthView").hide();
    }
}

function Calc(input) {
    debugger
    var Count=0;
    var cEffective = 0;
    var cPartiallyEffective = 0;
    var cNA = 0;
    var SubDomainID = $(input).parent().parent().find('td:eq(0)').text();
    $('#tblComplianceScore tbody tr').each(function (i, tr) {
        if ($(tr).find("td:eq(0)").text() == SubDomainID) {
            Count +=1;
           var Name = $(tr).find("td:eq(8) input[type=text]").val();
            if (Name == "Effective") {
                cEffective += 1;
            } else if (Name == "Partially Effective") {
                cPartiallyEffective += 1;
            } else if (Name =="NA") {
                cNA += 1;
            }
        }
    });
    debugger
    var tcEffective = (cEffective * 5);
    var tcPartiallyEffective = (cPartiallyEffective * 3);
    var tCount = (Count - cNA);
    var finlaScore = (((tcEffective + tcPartiallyEffective) / tCount)) / 5;
    var Percentage = parseInt(finlaScore * 100);
    if (isNaN(Percentage)) {
        Percentage = 0;
    }
    if (Percentage < 60) {
        $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").css('background-color', '#f4c2c2');
    } else if (Percentage >= 60 && Percentage < 80) {
        $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").css('background-color', '#ffff99');
    } else if (Percentage >= 80 && Percentage < 100) {
        $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").css('background-color', '#92d050');
    } else {
        $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").css('background-color', '#00b050 ');
    }
    $('#tblComplianceScore tbody').find("#trid_" + SubDomainID).find(".clsScore").text(Percentage + '%');
}

