﻿function fn_CheckDuplicateValidation(input) {
    debugger
    var UserName = $(input).val();
    $.ajax({
        url: '/ApplicationManagement/User/CheckDuplicateuser',
        type: 'get',
        data: { UserName: UserName },
        success: function (res) {
            if (res.isSuccess) {
                toastr.success(UserName + ' already exists');
                $('#UserName').val('');
            }
        },
        error: function (res) {
            toastr.error('internal server error.');
        }
    })
}