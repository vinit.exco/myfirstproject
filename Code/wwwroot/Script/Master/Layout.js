﻿$(document).ready(function () {
    $('#example2').DataTable();
  
});

$(function () {
    $('.multiple').multiselect({
        enableFiltering: true,
        includeSelectAllOption: true,
        nonSelectedText: 'Select an option',
        filterPlaceholder: 'Search for something...',
        maxHeight: 400,
    });
});

function fn_Onsuccess(msg, Url) {
    $.ajax(
        {
            url: "/" + Url,
            beforeSend: function () {
                $('#spinner').show();
            },
            type: "Get",
            success: function (data) {
                $('#spinner').hide();
                $("#ajaxresult").html(data);
                $('#Error').removeClass("errormsg");
                $('#Error').addClass("confirmationmsg");
                $('#Error').html(msg);
                $('#Error').show().delay(2000).hide(0);
            }
        });
}

function Active(anchor) {
    $('.cardbtnIn').removeClass('active');
    $(anchor).children('div').addClass('active');
}