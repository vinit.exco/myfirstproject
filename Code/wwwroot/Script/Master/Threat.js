﻿function fn_Getthreattype(input) {
    $("#TTypeId").empty();
    $("#TTypeId").append($("<option></option>").val(0).html("Select"));
    $.ajax({
        type: "GET",
        url: "/Master/Threat/GetLevelWiseThreat",
        data: jQuery.param({ LevelId: $(input).val() }),
        dataType: "json",
        contentType: "application/json",
        success: function (res) {
            $.each(res, function (data, value) {

                $("#TTypeId").append($("<option></option>").val(value.tTypeId).html(value.tTypeName));
            })
        }

    });
}