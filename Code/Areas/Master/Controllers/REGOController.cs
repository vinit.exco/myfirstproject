﻿using Cyberanium.Factory;
using CyberaniumModels;
using CyberaniumModels.Enumeration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class REGOController : Controller
    {
        private readonly ILogger<REGOController> _logger;
        public REGOController(ILogger<REGOController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index(int Regoid = 0, int Country = 0, int type = 0, int Section = 0, int Sector = 0, int Regulation = 0)
        {
            RegoMaster regoMaster = new();
            var regoMasterlist = await ApiClientFactory.Instance.GetAllREGO();
            if (Regoid > 0)
            {
                regoMaster = regoMasterlist.Where(x => x.RegoId == Regoid).FirstOrDefault();
            }
            regoMaster.Regolist = regoMasterlist;
            regoMaster.DdlCountry = new SelectList(await ApiClientFactory.Instance.GetAllCountry(), "Id", "CountryName");
            regoMaster.DdlDomain = new SelectList(await ApiClientFactory.Instance.GetEnumByType(4), "Value", "Name");
            regoMaster.Ddltype = new SelectList(await ApiClientFactory.Instance.GetEnumByType(7), "Value", "Name");
            regoMaster.DdlSector = new SelectList(await ApiClientFactory.Instance.GetEnumByType(1), "Value", "Name");
            regoMaster.DdlSection = new SelectList(await ApiClientFactory.Instance.GetEnumByType(5), "Value", "Name");

            regoMaster.DdlRegulation = new SelectList(await ApiClientFactory.Instance.GetAllLawRegulation(), "Id", "Title");
            //regoMaster.ddlSection = new SelectList(Enum.GetValues<Section>(), "Id", "Value");
            //regoMaster.ddlSector = new SelectList(Enum.GetValues<Sector>(), "Id", "Value");
            //regoMaster.ddltype = new SelectList(Enum.GetValues<Regotype>(), "Id", "Value");

            if (Country != 0)
            {
                regoMaster.Regolist = regoMaster.Regolist.Where(m => m.CountryId == Country).ToList();

            }
            if (type != 0)
            {
                regoMaster.Regolist = regoMaster.Regolist.Where(m => m.TypeId == type).ToList();

            }
            if (Section != 0)
            {
                regoMaster.Regolist = regoMaster.Regolist.Where(m => m.Section == Section).ToList();

            }
            if (Sector != 0)
            {
                regoMaster.Regolist = regoMaster.Regolist.Where(m => m.Sector == Sector).ToList();

            }
            //if (Regulation != 0)
            //{
            //    regoMaster.Regolist = regoMaster.Regolist.Where(m => m.Regulation == Regulation).ToList();

            //}
            ViewBag.message = TempData["success"];
            ViewBag.message = TempData["error"];
            return View(regoMaster);
        }
        [HttpPost]
        public async Task<JsonResult> SaveupdateREGO(RegoMaster model)
        {
            _logger.LogInformation("SaveupdateREGO");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.SaveupdateREGO(model);
            if (result.IsSuccess)
            {
                TempData["success"] = (model.RegoId > 0 ? "Updated successfully" : "Saved successfully");
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(result);
        }
        public async Task<IActionResult> BlockDeleteREGO(int Id, int Type, string ActionName = null)
        {
            _logger.LogInformation("BlockDeleteThreat");
            var Rego = new RegoMaster();
            Rego.RegoId = Id;
            Rego.Mode = Type.ToString();
            var result = await ApiClientFactory.Instance.BlockDeleteREGO(Rego);
            //if (result.IsSuccess)
            //{
            //    TempData["success"] = Type == 0 ? "Block successfully." : "Delete successfully.";
            //    return RedirectToAction("Index", "IntregratedControl", new { area = "Master" });
            //}
            //else
            //{
            //    TempData["error"] = "Something went wrong!";
            //}
            var regoMaster = new RegoMaster();
            regoMaster.Regolist = await ApiClientFactory.Instance.GetAllREGO();
            regoMaster.DdlCountry = new SelectList(await ApiClientFactory.Instance.GetAllCountry(), "Id", "CountryName");
            regoMaster.DdlDomain = new SelectList(await ApiClientFactory.Instance.GetAllDomain(), "DomainId", "SubDomain");
            regoMaster.DdlRegulation = new SelectList(await ApiClientFactory.Instance.GetAllLawRegulation(), "Id", "Title");
            //if (Type > 0)
            //{
            //    return Json(true);
            //}
            //else
            //{
            //    return Json(true);
            //}
            //return View("Index", regoMaster);
            return RedirectToAction("index");
        }
        //public IActionResult AddRego()
        //{
        //    return View();
        //}
    }
}
