﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using CyberaniumApi.Helpers;
using Cyberanium.Factory;
using CyberaniumModels;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Runtime.Versioning;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Data.OleDb;
using System.Data.Common;

namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class IntregratedControlController : Controller
    {
        private readonly ILogger<IntregratedControlController> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public IntregratedControlController(ILogger<IntregratedControlController> logger, IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
        }
        public async Task<IActionResult> Index(long Id = 0)
        {
            ICD model = new();

            if (Id > 0)
            {
                var ICDs = await ApiClientFactory.Instance.GetICD(Id);
                model = ICDs.Where(x => x.Id == Id).FirstOrDefault();
                var Artefactlist = await ApiClientFactory.Instance.GetArtefact();
                model.Artefactlist = Artefactlist.Where(x => x.ICDId == model.Id).ToList();
                model.RegulationMapList = await ApiClientFactory.Instance.GetRegulationMapByICDID(Id);
                ViewBag.StandardId = model.StandardIds;
                var standard = await ApiClientFactory.Instance.GetMappedStandardByICDId(Id);
                var standardIds = standard.Where(x => x.ICDId == Id).Select(s => s.Id).ToList();
                if (standardIds.Count > 1)
                {
                    var str1 = string.Join(",", standardIds);
                    ViewBag.StandardIds = str1;
                }
                else
                {
                    ViewBag.StandardIds = standardIds.FirstOrDefault();
                }
                var ThreatMap = await ApiClientFactory.Instance.GetICDThreatMap();
                var lst1 = ThreatMap.Where(x => x.ICDId == Id).Select(s => s.ThreatId).ToList();
                if (lst1.Count > 1)
                {
                    var str1 = string.Join(",", lst1);
                    ViewBag.Threat = str1;
                }
                else
                {
                    ViewBag.Threat = lst1.FirstOrDefault();
                }
                var catmap = await ApiClientFactory.Instance.GetICDCategoryMap();
                var catlist = catmap.Where(x => x.ICDId == Id).Select(s => s.CategoryId).ToList();
                if (catlist.Count > 1)
                {
                    var str1 = string.Join(",", catlist);
                    ViewBag.Category = str1;
                }
                else
                {
                    ViewBag.Category = catlist.FirstOrDefault();
                }
                ViewBag.country = model.CountryIds;
                ViewBag.SectorId = model.SectorIds;
            }
            model.DdlSection = new SelectList(await ApiClientFactory.Instance.GetEnumByType(5), "Value", "Name");
            model.Ddltype = new SelectList(await ApiClientFactory.Instance.GetEnumByType(7), "Value", "Name");
            model.DdlCountry = new SelectList(await ApiClientFactory.Instance.GetAllCountry(), "Id", "CountryName");
            model.DdlDomain = new SelectList(await ApiClientFactory.Instance.GetDomain(), "Id", "DomainName");
            model.DdlControlCategory = new SelectList(await ApiClientFactory.Instance.GetEnumByType(2), "Value", "Name");
            model.DdlControlType = new SelectList(await ApiClientFactory.Instance.GetEnumByType(9), "Value", "Name");
            model.DdlSector = new SelectList(await ApiClientFactory.Instance.GetEnumByType(1), "Value", "Name");
            model.DdlThreat = new SelectList((await ApiClientFactory.Instance.GetAllThreat()).Select(p => new { p.ThreatId, CodeName = p.ThreatCode + "-" + p.ThreatName }), "ThreatId", "CodeName");
            model.Domainlist = await ApiClientFactory.Instance.GetAllDomain();
            model.DomainlistNew = await ApiClientFactory.Instance.GetDomainList();
            model.DdlInternationalStandard = new SelectList(await ApiClientFactory.Instance.GetAllInternationalStandard(), "Id", "Title");
            model.MapLawRegulationsList = new();
            return View(model);
        }
        [HttpPost]
        public async Task<JsonResult> GetControlByDomain(int SubDomainId)
        {
            _logger.LogInformation("GetControlByDomain");
            var ICDs = await ApiClientFactory.Instance.GetICDBySubDomainID(SubDomainId);
            return Json(ICDs);
        }

        [HttpPost]
        public async Task<JsonResult> SaveupdateLawRegulation(int country, string ttle, string code, string XmlRegulation)
        {
            _logger.LogInformation("SaveupdateLawRegulationForPoPup");
            LawRegulation obj = new LawRegulation();
            obj.CreatedIP = HttpContext.Session.GetString("IP");
            obj.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            obj.CountryId = country;
            obj.Title = ttle;
            obj.Code = code;
            obj.XmlRegulation = XmlRegulation;
            var result = await ApiClientFactory.Instance.SaveupdateLawRegulation(obj);
            if (result.IsSuccess)
            {
                TempData["success"] = "Saved successfully";
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SubmitICD(ICD data)
        {
            _logger.LogInformation("SubmitICD");
            data.CreatedIP = HttpContext.Session.GetString("IP");
            data.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.SubmitICD(data);
            if (result.IsSuccess)
            {
                TempData["success"] = data.EventType == 0 ? "Saved successfully." : "Updated successfully.";
                return RedirectToAction("Index", "IntregratedControl", new { area = "Master" });
            }
            TempData["error"] = "Something went wrong!";
            ModelState.AddModelError("", "Invalid attempt");
            return View(data);
        }
        public async Task<JsonResult> BlockDeleteICD(long Id, int Type)
        {
            _logger.LogInformation("BlockDeleteICD");
            var model = new ICD();
            model.Id = Id;
            model.EventType = Type;
            var result = await ApiClientFactory.Instance.BlockDeleteICD(model);
            if (result.IsSuccess)
            {
                return Json(new { code = 200 });
            }
            return Json(new { code = 500 });
        }
        [HttpPost]
        public async Task<IActionResult> CodeById(int DomainId, string subDomain)
        {
            _logger.LogInformation("CodeById");
            var result = await ApiClientFactory.Instance.CodeById(DomainId, subDomain);
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> ControlById(int DomainId, int SubDomainId)
        {
            _logger.LogInformation("CodeById");
            var result = await ApiClientFactory.Instance.ControlById(DomainId, SubDomainId);
            return Json(result);
        }
        public async Task<IActionResult> GetDomain()
        {
            _logger.LogInformation("GetDomain");
            return Json(await ApiClientFactory.Instance.GetDomain());
        }

        public async Task<IActionResult> GetDomainID(long ID)
        {
            _logger.LogInformation("GetDomain");
            var result = await ApiClientFactory.Instance.GetDomain();
            return Json(result.Where(m => m.Id == ID));
        }

        public async Task<IActionResult> GetDomainListID(long ID)
        {
            _logger.LogInformation("GetDomainList");
            var result = await ApiClientFactory.Instance.GetDomainList();
            return Json(result.Where(m => m.Id == ID));
        }


        public async Task<JsonResult> SaveupdateDomain(Domain model)
        {
            _logger.LogInformation("Save Domain.");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.SaveDomain(model);
            if (result.IsSuccess)
            {
                TempData["success"] = (model.Id > 0 ? "Updated successfully" : "Saved successfully");
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(result);
        }
        public async Task<IActionResult> GetControlsByThreatId(long id)
        {
            _logger.LogInformation("GetControlsByThreatId");
            var result = await ApiClientFactory.Instance.GetControlsByThreatId(id);
            return Json(result);
        }

        [HttpPost]
        public async Task<IActionResult> DomainSave(Domain model)
        {
            _logger.LogInformation("Save Domain.");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            return Json(await ApiClientFactory.Instance.DomainSave(model));
        }
        [HttpPost]
        [SupportedOSPlatform("windows")]
        public JsonResult GetJsonFromExcelFile()
        {
            string jsonData;
            string webRootPath = _webHostEnvironment.WebRootPath;
            string contentRootPath = _webHostEnvironment.ContentRootPath;
            string pathToUpload = Path.Combine(webRootPath, "Uploads");
            if (!Directory.Exists(pathToUpload)) Directory.CreateDirectory(pathToUpload);
            if (Request.Form.Files.Count > 0)
            {
                var files = Request.Form.Files;
                var file = files[0];
                string fileExtension = Path.GetExtension(file.FileName);
                string fileName = Path.GetFileNameWithoutExtension(file.FileName);
                string fullFileName = string.Concat(fileName, DateTime.Now.ToFileTime(), fileExtension);
                string fullPath = Path.Combine(pathToUpload, fullFileName);
                var fileStream = System.IO.File.Create(fullPath);
                file.CopyTo(fileStream); fileStream.Flush(); fileStream.Close();
                var connectionString = $@"Provider=Microsoft.ACE.OLEDB.12.0; Data Source={fullPath}; Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1;""";
                var conn = new OleDbConnection(connectionString);
                try
                {
                    conn.Open();
                    var cmd = conn.CreateCommand();

                    cmd.CommandText = $@"SELECT * FROM [Sheet1$]";
                    var dr = cmd.ExecuteReader();
                    try
                    {
                        var query = (from DbDataRecord row in dr select row).Select(x =>
                        {
                            var data = new Dictionary<string, object>{
                            {"Domain",x[0]},
                            {"ControlType",x[1]},
                            {"ControlName",x[2]},
                            {"ControlDescription",x[3]},
                            };
                            return data;
                        });
                        jsonData = JsonConvert.SerializeObject(query);
                    }
                    catch (Exception ex)
                    {
                        return Json(new { Code = 422, Message = ex.Message, Data = new object() });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { Code = 505, Message = ex.Message, Data = new object() });
                }
                finally
                {
                    conn.Close(); conn.Dispose();
                    var fileInfo = new FileInfo(fullPath);
                    fileInfo.Delete();
                }
                var jsonList = new List<ICDJson>();
                var data = JsonConvert.DeserializeObject<List<ICDJson>>(jsonData);
                jsonList.AddRange(data.Where(x => x.Domain != "Select" && x.ControlType != "Select"));
                var newJsonList = new List<ICDJson>();
                foreach (var item in jsonList)
                {
                     item.DomainId = item.Domain == "Identify" ? 1 : item.Domain == "Protect" ? 2 : item.Domain == "Detect" ? 3 : item.Domain == "Respond" ? 4 : 5;
                     item.ControlTypeId = item.ControlType == "Preventive" ? 1 : item.ControlType == "Detective" ? 2 : 3;
                    newJsonList.Add(item);
                }
                return Json(new { Code = 200, Message = "File uploaded successfully.", Data = newJsonList });
            }
            return Json(new { Status = 404, Message = "File not found!", Data = new object() });
        }
        [HttpPost]
        public async Task<IActionResult> SubmitICDByExcel(string str = "")
        {
            var model = new ICDJson();
            model.ICDXML = str;
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitICDByExcel(model);
            return Json(res);
        }

    }
}
