﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class ThreatTypeController : Controller
    {
        private readonly ILogger<ThreatTypeController> _logger;
        public ThreatTypeController(ILogger<ThreatTypeController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var model = new ThreatType();
            var result = await ApiClientFactory.Instance.GetAllThreatType();
            model.ThreatTypes = result;
            ViewBag.message = TempData["save"];
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> SaveThreatType(ThreatType threatType)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("SaveThreatType");
                threatType.CreatedIP = HttpContext.Session.GetString("IP");
                threatType.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var result = await ApiClientFactory.Instance.SaveThreatType(threatType);
                if (!result.IsSuccess)
                {
                    if (threatType.TTypeId != 0)
                    {
                        TempData["save"] = "Update successfully!!";
                    }
                    else
                    {
                        TempData["save"] = "Save successfully!!";
                    }
                    return RedirectToAction("Index", "ThreatType", new { area = "Master" });
                }
            }
            ModelState.AddModelError("", "Invalid login attempt");
            return View(threatType);
        }
        //[HttpPost]
        //public async Task<IActionResult> UpdateThreatType(ThreatType threatType)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _logger.LogInformation("UpdateThreatType");
        //        var result = await ApiClientFactory.Instance.UpdateThreatType(threatType);
        //        if (!result.IsSuccess)
        //        {
        //            return RedirectToAction("Index", "ThreatType", new { area = "Master" });
        //        }
        //    }
        //    ModelState.AddModelError("", "Invalid login attempt");
        //    return View(threatType);
        //}
        public async Task<IActionResult> BlockDeleteThreatType(long TTypeId, int Type, string ActionName)
        {
            _logger.LogInformation("BlockDeleteThreatType");
            var threatType = new ThreatType();
            threatType.TTypeId = TTypeId;
            threatType.TTypeName = Type.ToString();
            var result = await ApiClientFactory.Instance.BlockDeleteThreatType(threatType);
            if (!result.IsSuccess)
            {
                if (Type > 0)
                {
                    return Json(result);
                }
                else
                {
                    TempData["save"] = ActionName + " " + "successfully!!";
                }
            }
            return RedirectToAction("Index", "ThreatType", new { area = "Master" });
        }
    }
}
