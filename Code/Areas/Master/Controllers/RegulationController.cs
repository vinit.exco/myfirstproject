﻿using Cyberanium.Factory;
using CyberaniumModels;
using CyberaniumModels.Enumeration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class RegulationController : Controller
    {
        private readonly ILogger<RegulationController> _logger;
        public RegulationController(ILogger<RegulationController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(int id = 0)
        {
            LawRegulation model = new LawRegulation();
            if (id > 0)
            {
                var result = await ApiClientFactory.Instance.GetAllLawRegulation();
                model = result.Where(m => m.Id == id).FirstOrDefault();
                //model.LawRegulations = result;
                model.MapLawRegulationsList = await ApiClientFactory.Instance.GetMapRegulationByID(id);
            }
            model.CountryList = await ApiClientFactory.Instance.LawRegulationCountry();
            model.DdlCountry = new SelectList(await ApiClientFactory.Instance.GetAllCountry(), "Id", "CountryName");
            model.DdlSector = new SelectList(await ApiClientFactory.Instance.GetEnumByType(1), "Value", "Name");
            model.DdlDomain = new SelectList(await ApiClientFactory.Instance.GetDomain(), "Id", "DomainName");
            model.Ddltype = new SelectList(await ApiClientFactory.Instance.GetEnumByType(7), "Value", "Name");
            model.DdlSection = new SelectList(await ApiClientFactory.Instance.GetEnumByType(5), "Value", "Name");
            //ViewBag.message = TempData["save"];
            return View(model);
        }
        [HttpPost]
        public async Task<JsonResult> GetRegulationByCountry(int CountryId)
        {
            _logger.LogInformation("GetRegulationByCountry");
            var LawRegulations = await ApiClientFactory.Instance.GetAllLawRegulation();
            return Json(LawRegulations.Where(m=> m.CountryId == CountryId));
        }

        [HttpPost]
        public async Task<IActionResult> SaveupdateLawRegulation(LawRegulation LawRegulation)
        {
                _logger.LogInformation("SaveLawRegulation");
                LawRegulation.CreatedIP = HttpContext.Session.GetString("IP");
                LawRegulation.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var result = await ApiClientFactory.Instance.SaveupdateLawRegulation(LawRegulation);
            //if (!result.IsSuccess)
            //{
            //    if (LawRegulation.Id > 0)
            //    {
            //        TempData["save"] = "Update successfully!!";
            //    }
            //    else
            //    {
            //        TempData["save"] = "Save successfully!!";
            //    }
            //}

            //return RedirectToAction("Index", "Regulation", new { area = "Master" });
            return Json(result);
        }

        public async Task<IActionResult> BlockDeleteLawRegulation(long RId, int Type, string ActionName)
        {
            _logger.LogInformation("BlockDeleteLawRegulation");
            var LawRegulation = new LawRegulation();
            LawRegulation.Id = RId;
            LawRegulation.Title = Type.ToString();
            _ = await ApiClientFactory.Instance.BlockDeleteLawRegulation(LawRegulation);
            var newdo = new LawRegulation();
            newdo.LawRegulations = await ApiClientFactory.Instance.GetAllLawRegulation();
            if (Type > 0)
            {
                return Json(true);
            }
            else
            {
                TempData["save"] = ActionName+" "+"successfully!!";
            }
            return RedirectToAction("Index", "Regulation", new { area = "Master" });
        }

        public async Task<JsonResult> GetSectorWiseRegulation(int SectorId, int CountryId)
        {

            var LawRegulation = await ApiClientFactory.Instance.GetAllLawRegulation();
            var returnList = LawRegulation.Where(m => m.SectorId.ToString() == SectorId.ToString() && m.CountryId == CountryId).ToList();
            return Json(returnList);
        }
        public async Task<JsonResult> GetCodeWiseRegulationID(int RegulationID)
        {
            var LawRegulation = await ApiClientFactory.Instance.GetAllLawRegulation();
            var returnList = LawRegulation.Where(m => m.Id == RegulationID).ToList();
            return Json(returnList);
        }

        public async Task<JsonResult> GetSectorCountryWiseRegulation(string SectorId, string CountryId)
        {
            var LawRegulation = await ApiClientFactory.Instance.GetAllRegulationbySectorAndCountryWise(SectorId, CountryId);
            return Json(LawRegulation);
        }
        [HttpPost]
        public async Task<IActionResult> BindRegulationDropdown(long ChildOrgId)
        {
            _logger.LogInformation("BindRegoDrop");

            var Domain = await ApiClientFactory.Instance.GetEnumByType(4);
            var Type = await ApiClientFactory.Instance.GetEnumByType(7);
            var Sector = await ApiClientFactory.Instance.GetEnumByType(1);
            var Section = await ApiClientFactory.Instance.GetEnumByType(5);
            return Json(new { Domain, Type, Sector, Section });
        }
    }
}
