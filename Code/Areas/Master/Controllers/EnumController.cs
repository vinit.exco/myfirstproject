﻿using Cyberanium.Factory;
using CyberaniumModels;
using CyberaniumModels.Enumeration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class EnumController : Controller
    {
        private readonly ILogger<EnumController> _logger;
        public EnumController(ILogger<EnumController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var Enum = new CyberaniumModels.Enum();
            var EnumType = await ApiClientFactory.Instance.GetAllEnumType();
            var result = await ApiClientFactory.Instance.GetAllEnum();
            Enum.Enums = result;
            Enum.EnumTypes = EnumType;
            ViewBag.message = TempData["Save"];
            ViewBag.error = TempData["error"];
            return View(Enum);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEnum(CyberaniumModels.Enum Enum)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("SaveEnum");
                Enum.CreatedIP = HttpContext.Session.GetString("IP");
                Enum.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var result = await ApiClientFactory.Instance.SaveEnum(Enum);
                if (!result.IsSuccess)
                {
                    if (Enum.Id == 0)
                    {
                        TempData["Save"] = "Save successfully!!";
                    }
                    return RedirectToAction("Index", "Enum", new { area = "Master" });
                }
            }
            ModelState.AddModelError("", "Invalid attempt");
            return View(Enum);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateEnum(CyberaniumModels.Enum Enum)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("UpdateEnum");
                Enum.CreatedIP = HttpContext.Session.GetString("IP");
                Enum.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var result = await ApiClientFactory.Instance.UpdateEnum(Enum);
                if (!result.IsSuccess)
                {
                    if (Enum.Id != 0)
                    {
                        TempData["Save"] = "Update successfully!!";
                    }
                }
                else
                {
                    TempData["error"] = "Update not succussfully!!";
                }
            }
            return RedirectToAction("Index", "Enum", new { area = "Master" });
        }
        public async Task<IActionResult> BlockDeleteEnum(long Id, int Type, string ActionName)
        {
            _logger.LogInformation("BlockDeleteEnum");
            var Enum = new CyberaniumModels.Enum();
            Enum.Id = Id;
            Enum.Title = Type.ToString();
            var result = await ApiClientFactory.Instance.BlockDeleteEnum(Enum);
            if (Type > 0)
            {
                return Json(result);
            }
            else
            {
                TempData["Save"] = ActionName + " " + "successfully!!";
            }
            return RedirectToAction("Index", "Enum", new { area = "Master" });
        }
        public async Task<long?> GetHighestValueForEnum(long id)
        {
            _logger.LogInformation("GetHighestValueForEnum");
            var Enum = new CyberaniumModels.Enum();
            Enum.Id = id;
            var value = await ApiClientFactory.Instance.GetHighestValueForEnum(Enum);
            return value?.Data?.Id;
        }
    }
}
