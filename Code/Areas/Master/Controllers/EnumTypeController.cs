﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class EnumTypeController : Controller
    {
        private readonly ILogger<EnumTypeController> _logger;
        public EnumTypeController(ILogger<EnumTypeController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var model = new EnumType();
            var result = await ApiClientFactory.Instance.GetAllEnumType();
            model.EnumTypes = result;
            ViewBag.message = TempData["update"];
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEnumType(EnumType EnumType)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("SaveEnumType");
                EnumType.CreatedIP = HttpContext.Session.GetString("IP");
                EnumType.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var result = await ApiClientFactory.Instance.SaveEnumType(EnumType);
                if (!result.IsSuccess)
                {
                    TempData["update"] = "Save successfully!!";
                    return RedirectToAction("Index", "EnumType", new { area = "Master" });
                }
            }
            ModelState.AddModelError("", "Invalid attempt");
            return View(EnumType);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateEnumType(EnumType EnumType)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("UpdateEnumType");
                EnumType.CreatedIP = HttpContext.Session.GetString("IP");
                EnumType.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var result = await ApiClientFactory.Instance.UpdateEnumType(EnumType);
                if (!result.IsSuccess)
                {
                    TempData["update"] = "Update successfully!!";
                    return RedirectToAction("Index", "EnumType", new { area = "Master" });
                }
            }
            ModelState.AddModelError("", "Invalid attempt");
            return View(EnumType);
        }
        public async Task<IActionResult> BlockDeleteEnumType(long Id, int Type, string ActionName)
        {
            try
            {
                _logger.LogInformation("BlockDeleteEnumType");
                var EnumType = new EnumType();
                EnumType.Id = Id;
                EnumType.Title = Type.ToString();
                var result = await ApiClientFactory.Instance.BlockDeleteEnumType(EnumType);
                if (result.IsSuccess)
                {
                    if (Type>0)
                    {
                        return Json(result);
                    }
                    else
                    {
                        TempData["update"] = ActionName + " " + "successfully!!";
                        return RedirectToAction("Index", "EnumType", new { area = "Master" });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "EnumType", new { area = "Master" });
        }
    }
}
