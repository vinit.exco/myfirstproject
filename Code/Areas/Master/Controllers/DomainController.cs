﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class DomainController : Controller
    {
        private readonly ILogger<DomainController> _logger;
        //private readonly Domain domain;
        public DomainController(ILogger<DomainController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index(int ID = 0)
        {
            var model = new Domain();
            var list = await ApiClientFactory.Instance.GetDomainList();
            if (ID > 0)
            {
                model = list.Where(m => m.Id == ID).FirstOrDefault();
            }
            model.Domainlist = await ApiClientFactory.Instance.GetAllDomain();
            model.DDlDomain = new SelectList(await ApiClientFactory.Instance.GetDomain(), "Id", "DomainName");
            ViewBag.message = TempData["success"];
            ViewBag.message = TempData["error"];
            return View(model);
        }
        public IActionResult TabMenu()
        {

            return View();
        }
        [HttpPost]
        public async Task<JsonResult> GetSubDomainByDomain(int DomainId)
        {
            _logger.LogInformation("GetSubDomainByDomain");
            var list = await ApiClientFactory.Instance.GetDomainList();
            return Json(list.Where(m => m.DomainId == DomainId));
        }

        public async Task<JsonResult> SaveupdateDomain(Domain model)
        {
            _logger.LogInformation("Save Domain.");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.SaveDomain(model);
            if (result.IsSuccess)
            {
                TempData["success"] = (model.Id > 0 ? "Updated successfully" : "Saved successfully");
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> BlockDeleteDomain(int Did, int Type, string ActionName = null)
        {
            _logger.LogInformation("BlockDeleteDomain");
            var Domain = new Domain();
            Domain.Id = Did;
            Domain.Eventtype = Type;
            var result = await ApiClientFactory.Instance.BlockDeleteDomain(Domain);
            if (Type > 0)
            {
                return Json(result);
            }
            else
            {
                TempData["Save"] = ActionName + " " + "successfully!!";
            }
            return RedirectToAction("Index", "Domain", new { area = "Master" });
        }
        public async Task<JsonResult> GetSubDomainByDomainId(long DomainId)
        {
            var obj = new Domain();
            obj.Domainlist = await ApiClientFactory.Instance.GetDomainList();
            return Json(obj.Domainlist.Where(x => x.DomainId == DomainId));
        }
        [HttpPost]
        public async Task<IActionResult> CheckSubDomain(string SubDomain)
        {
            _logger.LogInformation("CheckSubDomain");
            return Json(await ApiClientFactory.Instance.CheckSubDomain(SubDomain));
        }
        public async Task<IActionResult> CheckCode(string code)
        {
            _logger.LogInformation("CheckCode");
            return Json(await ApiClientFactory.Instance.CheckCode(code));
        }
        public async Task<IActionResult> GetDomain()
        {
            _logger.LogInformation("GetDomain");
            return Json(await ApiClientFactory.Instance.GetDomain());
        }
        public async Task<IActionResult> GetDomainID(long ID)
        {
            _logger.LogInformation("GetDomain");
            var result = await ApiClientFactory.Instance.GetDomain();
            return Json(result.Where(m=>m.Id== ID));
        }
        [HttpPost]
        public async Task<IActionResult> DomainSave(Domain model)
        {
            _logger.LogInformation("Save Domain.");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            return Json(await ApiClientFactory.Instance.DomainSave(model));
        }
        [HttpPost]
        public async Task<IActionResult> CheckDomain(string DomainName)
        {
            _logger.LogInformation("CheckDomain");
            return Json(await ApiClientFactory.Instance.CheckDomain(DomainName));
        }
        [HttpPost]
        public async Task<IActionResult> CheckDomainCode(string DomainCode)
        {
            _logger.LogInformation("CheckDomainCode");
            return Json(await ApiClientFactory.Instance.CheckDomainCode(DomainCode));
        }
    }
}