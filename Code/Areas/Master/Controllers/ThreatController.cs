﻿using Cyberanium.Factory;
using CyberaniumModels;
using CyberaniumModels.Enumeration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class ThreatController : Controller
    {
        private readonly ILogger<ThreatController> _logger;
        public ThreatController(ILogger<ThreatController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(int ThreatId = 0)
        {
            var model = new Threat();
            var threatTypes = await ApiClientFactory.Instance.GetAllThreatType();
            var threats = await ApiClientFactory.Instance.GetAllThreat();
            var ControlCategory = new SelectList(await ApiClientFactory.Instance.GetEnumByType(2), "Value", "Name");
            if (ThreatId > 0)
            {
                model = threats.Where(m => m.ThreatId == ThreatId).FirstOrDefault();
                model.Threats = threats;
                model.ThreatTypes = threatTypes;
                model.DdlControlCategory = ControlCategory;
            }
            else
            {
                model.ThreatTypes = threatTypes;
                model.Threats = threats;
                model.DdlControlCategory = ControlCategory;
            }
            ViewBag.message = TempData["save"];
            ViewBag.error = TempData["error"];
            return View(model);
        }
        [HttpPost]
        public async Task<JsonResult> GetThreatList(int Id)
        {
            _logger.LogInformation("GetThreatList");
            var threats = await ApiClientFactory.Instance.GetAllThreat();
            return Json(threats.Where(m => m.TTypeId == Id));
        }
        public async Task<JsonResult> GetLevelWiseThreat(int LevelId)
        {
            var model = new Threat();
            var threatTypes = await ApiClientFactory.Instance.GetAllThreatType();
            var sdsdd = System.Enum.GetName(typeof(ThreatLevel), LevelId);
            var returnList = threatTypes.Where(m => m.ThreatLevel.ToString() == System.Enum.GetName(typeof(ThreatLevel), LevelId).ToString()).ToList();
            return Json(returnList);
        }
        [HttpPost]
        public async Task<IActionResult> SaveupdateThreat(Threat Threat)
        {
            _logger.LogInformation("SaveupdateThreat");
            Threat.CreatedIP = HttpContext.Session.GetString("IP");
            Threat.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.SaveupdateThreat(Threat);
            if (!result.IsSuccess)
            {
                if (Threat.ThreatId > 0)
                {
                    TempData["save"] = "Update successfully!!";
                }
                else
                {
                    TempData["save"] = "Save successfully!!";
                }
            }
            return RedirectToAction("Index", "Threat", new { area = "Master" });
        }
        //[HttpPost]
        //public async Task<IActionResult> UpdateThreat(Threat Threat)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _logger.LogInformation("UpdateThreat");
        //        var result = await ApiClientFactory.Instance.UpdateThreat(Threat);
        //        if (!result.IsSuccess)
        //        {
        //            return RedirectToAction("Index", "Threat", new { area = "Master" });
        //        }
        //    }
        //    ModelState.AddModelError("", "Invalid login attempt");
        //    return View(Threat);
        //}
        public async Task<IActionResult> BlockDeleteThreat(long Id, int Type, string ActionName)
        {
            _logger.LogInformation("BlockDeleteThreat");
            var Threat = new Threat();
            Threat.ThreatId = Id;
            Threat.ThreatName = Type.ToString();
            var result = await ApiClientFactory.Instance.BlockDeleteThreat(Threat);

            var newdo = new Threat();
            newdo.ThreatTypes = await ApiClientFactory.Instance.GetAllThreatType();
            newdo.Threats = await ApiClientFactory.Instance.GetAllThreat();
            if (result.IsSuccess)
            {
                if (Type > 0)
                {
                    return Json(true);
                }
                else
                {
                    TempData["save"] = ActionName + " " + "successfully!!";
                }
            }
            else
            {
                TempData["error"] = result.ReturnMessage;
            }
            return RedirectToAction("Index", "Threat", new { area = "Master" });
        }
        public async Task<IActionResult> GetControlCategoriesByThreatId(long threatId)
        {
            _logger.LogInformation("GetControlCategoriesByThreatId");
            var result = await ApiClientFactory.Instance.GetControlCategoriesByThreatId(threatId);
            return Json(result.Select(x => x.ControlCategoryId).ToList());
        }
        public async Task<IActionResult> GetThreatsByControlCategoryId(int controlCategoryId)
        {
            _logger.LogInformation("GetThreatsByControlCategoryId");
            var result = await ApiClientFactory.Instance.GetThreatsByControlCategoryId(controlCategoryId);
            return Json(result);
        }
        public async Task<JsonResult> ValidateThreatName(int ThreatTypeId, string ThreatName)
        {
            _logger.LogInformation("ValidateThreatName");
            var res = await ApiClientFactory.Instance.ValidateThreatName(ThreatTypeId, ThreatName);
            if (res)
            {
                return Json(res);
            }
            else
            {
                var data = await ApiClientFactory.Instance.ValidateThreatName(ThreatTypeId, ThreatName.Trim());
                return Json(data);
            }
        }
    }
}
