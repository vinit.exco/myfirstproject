﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class InternationalStandardController : Controller
    {
        private readonly ILogger<InternationalStandardController> _logger;
        public InternationalStandardController(ILogger<InternationalStandardController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var model = new InternationalStandard();
            model.InternationalStandards = await ApiClientFactory.Instance.GetAllInternationalStandard();
            model.DDlServices = new SelectList(await ApiClientFactory.Instance.GetEnumByType(6), "Value", "Name");
            model.DDlServicesList = await ApiClientFactory.Instance.GetEnumByType(6);
            ViewBag.message = TempData["save"];
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> GetStandardsFrameworks(int Id)
        {
            _logger.LogInformation("GetStandardsFrameworks");
             var IntStandards = await ApiClientFactory.Instance.GetAllInternationalStandard();
            return Json(IntStandards.Where(m => m.TopicId == Id));
        }

        [HttpPost]
        public async Task<IActionResult> SaveInternationalStandard(InternationalStandard InternationalStandard)
        {
            _logger.LogInformation("SaveInternationalStandard");
            InternationalStandard.CreatedIP = HttpContext.Session.GetString("IP");
            InternationalStandard.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.SaveInternationalStandard(InternationalStandard);
            if (!result.IsSuccess)
            {
                if (InternationalStandard.Id > 0)
                {
                    TempData["save"] = "Update successfully!!";
                }
                else
                {
                    TempData["save"] = "Save successfully!!";
                }
                return RedirectToAction("Index", "InternationalStandard", new { area = "Master" });
            }
            ModelState.AddModelError("", "Invalid attempt");
            return View(InternationalStandard);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateInternationalStandard(InternationalStandard InternationalStandard)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("UpdateInternationalStandard");
                InternationalStandard.CreatedIP = HttpContext.Session.GetString("IP");
                InternationalStandard.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var result = await ApiClientFactory.Instance.UpdateInternationalStandard(InternationalStandard);
                if (!result.IsSuccess)
                {
                    TempData["save"] = "Update successfully!!";
                    return RedirectToAction("Index", "InternationalStandard", new { area = "Master" });
                }
            }
            ModelState.AddModelError("", "Invalid attempt");
            return View(InternationalStandard);
        }
        public async Task<IActionResult> BlockDeleteInternationalStandard(long Id, int Type, string ActionName)
        {
            _logger.LogInformation("BlockDeleteInternationalStandard");
            var InternationalStandard = new InternationalStandard();
            InternationalStandard.Id = Id;
            InternationalStandard.Title = Type.ToString();
            var result = await ApiClientFactory.Instance.BlockDeleteInternationalStandard(InternationalStandard);
            if (!result.IsSuccess)
            {
                if (Type > 0)
                {
                    return Json(result);
                }
                else
                {
                    TempData["save"] = ActionName + " " + "successfully!!";
                }
            }
            return RedirectToAction("Index", "InternationalStandard", new { area = "Master" });
        }
        public async Task<IActionResult> Edit(long id)
        {
            _logger.LogInformation("UpdateInternationalStandard");
            return Json(await ApiClientFactory.Instance.Edit(id));
        }
        public async Task<List<InternationalStandard>> GetVersionByStandardId(long standardId)
        {
            _logger.LogInformation("GetVersionByStandardId");
            var InternationalStandards = await ApiClientFactory.Instance.GetAllInternationalStandard();
            var Services = await ApiClientFactory.Instance.GetEnumByType(6);
            List<InternationalStandard> listModel = new();
            foreach (var item in InternationalStandards.Where(x => x.ParentISOId == 0 && x.ChildISOId == standardId))
            {
                item.Service = Services.Where(x => x.Value == item.TopicId).Select(x => x.Name).FirstOrDefault();
                listModel.Add(item);
            }
            return listModel;
        }
    }
}
