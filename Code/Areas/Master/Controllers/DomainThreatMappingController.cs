﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Cyberanium.Factory;
using CyberaniumModels;

namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class DomainThreatMappingController : Controller
    {
        private readonly ILogger<DomainThreatMappingController> _logger;
        public DomainThreatMappingController(ILogger<DomainThreatMappingController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var model = new DomainThreatMapping();
            model.ThreatList = await ApiClientFactory.Instance.GetAllThreat();
            model.DdlRiskDomain = new SelectList(await ApiClientFactory.Instance.GetEnumByType(3), "Value", "Name");
            return View(model);
        }
        public async Task<JsonResult> GetThreatByDomainId(DomainThreatMapping data)
        {
            var result = await ApiClientFactory.Instance.GetThreatByDomainId(data);
            return Json(result.Data);
        }
        public async Task<IActionResult> SubmitDomainThreatMapping(DomainThreatMapping data)
        {
            var model = new DomainThreatMapping();
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.SubmitDomainThreatMapping(data);
            return RedirectToAction("Index", model);
        }
        public async Task<JsonResult> GetDomainThreatMapList()
        {
            var result = await ApiClientFactory.Instance.GetDomainThreatMapList("");
            return Json(result);
        }
    }
}