﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.Master.Controllers
{
    [Area("Master")]
    public class CloudModelController : Controller
    {
        private readonly ILogger<CloudModelController> _logger;
        public CloudModelController(ILogger<CloudModelController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
