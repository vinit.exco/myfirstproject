﻿using ClosedXML.Excel;
using Cyberanium.Factory;
using CyberaniumModels;
using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]

    public class GenerateRCMExistingController : Controller
    {
        private readonly ILogger<GenerateRCMExistingController> _logger;
        public GenerateRCMExistingController(ILogger<GenerateRCMExistingController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            RiskAssessmentRCM obj = new RiskAssessmentRCM();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            obj.DdlOrganization = new SelectList(await ApiClientFactory.Instance.GetAssignedOrganizationByEmplyoyeeID(EmployeeId), "Id", "OrgName");
            return View(obj);
        }
        [HttpPost]
        public async Task<IActionResult> GetRCMListByOrgID(long OrgID)
        {
            _logger.LogInformation("GetRCMListByOrgID");
            var res = await ApiClientFactory.Instance.GetRCMListByOrgID(OrgID);
            return Json(res);
        }
        public async Task<IActionResult> Index2(long ID)
        {
            RiskAssessmentRCM obj = new RiskAssessmentRCM();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            obj.OrgID = ID;
            obj.DdlParentOrg = new SelectList(await ApiClientFactory.Instance.GetOrganizationByEmplyoyeeID(EmployeeId), "Id", "ParentOrgName");
            obj.RcmControlList = await ApiClientFactory.Instance.GetRCMControlListByOrg(ID);
            return View(obj);
        }

        public async Task<IActionResult> DrawingWinnersExcel(int Id)
        {
            try
            {
                RiskAssessmentRCM obj = new RiskAssessmentRCM();
                obj.RcmControlList = await ApiClientFactory.Instance.GetRCMControlListByOrg(Id);
                DataTable dt = new DataTable("GenerateRCM");
                dt.Columns.AddRange(new DataColumn[13] { new DataColumn("Domain"),
                new DataColumn("Sub-Domain"),
                new DataColumn("Control Name"),
                new DataColumn("Control Description"),
                new DataColumn("Control Id"),
                new DataColumn("Control Type"),
                new DataColumn("Governance"),
                new DataColumn("Application"),
                new DataColumn("Infrastructure"),
                new DataColumn("Cloud"),
                new DataColumn("Networks"),
                new DataColumn("People"),
                new DataColumn("Location")});
                foreach (var item in obj.RcmControlList)
                {
                    dt.Rows.Add(item.DomainName, item.SubDomainName, item.ControlName, item.ControlDescription, item.ControlId, item.ControlTypeName, item.Governance, item.Application, item.Infrastructure, item.Cloud, item.Networks, item.People, item.Location);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "GenerateRCM.xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HResult = 0; throw;
            }

        }
        public async Task<IActionResult> GenerateRCMVersion(long ID)
        {
            _logger.LogInformation("GenerateRCMVersion");
            RiskAssessmentRCM obj = new RiskAssessmentRCM();
            obj.ID = ID;
            obj.CreatedIP = HttpContext.Session.GetString("IP");
            obj.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.GenerateRCMVersion(obj);
            return Json(res);

        }

        public async Task<Message<RiskAssessmentRCM>> DeleteRCM(long ID) => await ApiClientFactory.Instance.DeleteRCM(ID);
        public async Task<IActionResult> Index3(long ID)
        {
            RiskAssessmentRCM obj = new RiskAssessmentRCM();

            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            if (ID > 0)
            {
                obj = await ApiClientFactory.Instance.GetRCMByID(ID);
                obj.ID = 0;
                obj.MapId = ID;
                var Artefactlist = await ApiClientFactory.Instance.GetArtefact();
                obj.Artefactlist = Artefactlist.Where(x => x.ICDId == ID).ToList();
            }
            obj.DdlParentOrg = new SelectList(await ApiClientFactory.Instance.GetOrganizationByEmplyoyeeID(EmployeeId), "Id", "ParentOrgName");
            obj.DomainList = await ApiClientFactory.Instance.GetAllDomain();
            obj.DdlDomain = new SelectList(await ApiClientFactory.Instance.GetDomain(), "Id", "DomainName");
            obj.DdlControlType = new SelectList(await ApiClientFactory.Instance.GetEnumByType(9), "Value", "Name");
            obj.DdlControlCategory = new SelectList(await ApiClientFactory.Instance.GetEnumByType(2), "Value", "Name");
            obj.DdlThreat = new SelectList((await ApiClientFactory.Instance.GetAllThreat()).Select(p => new { p.ThreatId, CodeName = p.ThreatCode + "-" + p.ThreatName }), "ThreatId", "CodeName");

            obj.SubDomainList = await ApiClientFactory.Instance.GetSubDomainRCMBID(ID);
            return View(obj);
        }
        [HttpPost]
        public async Task<IActionResult> GetRCMListBySubDomain(long ID, string SubDomainName)
        {
            _logger.LogInformation("GetRCMListBySubDomain");
            var res = await ApiClientFactory.Instance.GetRCMControlListByOrg(ID);
            var res2 = res.Where(m => m.SubDomainName.Contains(SubDomainName.Trim())).ToList();
            return Json(res2);
        }


        public async Task<Message<RiskAssessmentRCM>> DeleteControlByID(long ControlID) => await ApiClientFactory.Instance.DeleteControlByID(ControlID);
        [HttpPost]
        public async Task<JsonResult> SaveUpdateRCMICD(RiskAssessmentRCM model)
        {
            _logger.LogInformation("SaveUpdateRCMICD");
            model.Governance = model.ControlCategoryId.Contains("1");
            model.Application = model.ControlCategoryId.Contains("10");
            model.Infrastructure = model.ControlCategoryId.Contains("11");
            model.Cloud = model.ControlCategoryId.Contains("14");
            model.Networks = model.ControlCategoryId.Contains("15");
            model.People = model.ControlCategoryId.Contains("6");
            model.Location = model.ControlCategoryId.Contains("7");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveUpdateRCMICD(model);
            if (res.IsSuccess)
            {
                TempData["success"] = "Saved successfully";
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(res);
        }

        public async Task<JsonResult> GetRCMControlDetails(long ControlID)
        {
            RiskAssessmentRCM obj = new RiskAssessmentRCM();
            obj = await ApiClientFactory.Instance.GetRCMControlDetails(ControlID);
            var res = await ApiClientFactory.Instance.GetDomainList();
            string str = string.Empty;
            str = str + (obj.Governance ? "1," : "");
            str = str + (obj.Application ? "10," : "");
            str = str + (obj.Infrastructure ? "11," : "");
            str = str + (obj.Cloud ? "14," : "");
            str = str + (obj.Networks ? "15," : "");
            str = str + (obj.People ? "6," : "");
            str = str + (obj.Location ? "7," : "");
            obj.ControlCategoryId = str.TrimEnd(',');
            obj.DomainList = res.Where(m => m.DomainId == obj.DomainId).ToList();
            return Json(new { obj });
        }
    }
}
