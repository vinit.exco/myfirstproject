﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using CyberaniumModels;
using Cyberanium.Factory;
using Microsoft.AspNetCore.Http;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Office2010.Excel;
using System.Data;
using System.IO;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]

    public class ComplainceAssessmentRCMController : Controller
    {
        private readonly ILogger<ComplainceAssessmentRCMController> _logger;
        public ComplainceAssessmentRCMController(ILogger<ComplainceAssessmentRCMController> logger)
        {
            _logger = logger;
        }
        //public async Task<IActionResult> Index(int ID = 0)
        //{
        //    RiskAssessmentRCM obj = new RiskAssessmentRCM();
        //    if (ID>0)
        //    {
        //        obj.ID = ID;
        //    }

        //    obj.AssessMentList = await ApiClientFactory.Instance.GetAssignRiskAssessmentList(1);
        //    obj.ddlTestofDesign = new SelectList(await ApiClientFactory.Instance.GetEnumByType(12), "Value", "Name");
        //    obj.DdlSector = new SelectList(await ApiClientFactory.Instance.GetEnumByType(1), "Value", "Name");
        //    obj.DdlCountry = new SelectList(await ApiClientFactory.Instance.GetAllCountry(), "Id", "CountryName");
        //    obj.QuestionBanklist = await ApiClientFactory.Instance.GetQuestionBanklist();
        //    ViewBag.message = TempData["success"];
        //    ViewBag.message = TempData["error"];
        //    return View(obj);
        //}
        public async Task<IActionResult> Index2()
        {
            var model = new IRA();
            model.OrganizationList = await ApiClientFactory.Instance.GetAssessmentOrganizationList(2);
            return View(model);
        }
        public IActionResult TabMenu(long orgId = 0, string orgName = "")
        {
            var model = new IRA();
            model.RoleId = Convert.ToInt16(HttpContext.Session.GetInt32("Role"));
            model.OrgId = orgId;
            model.OrgName = orgName;
            return View(model);
        }
        public async Task<IActionResult> AssignTask(long orgId = 0, string orgName = "")
        {
            var model = new IRA();
            model.OrgId = orgId;
            var res = await ApiClientFactory.Instance.GetComplianceRCM(orgId);
            if (res.Count > 0)
            {
                model.ID = res[0].ID;
                model.StartDate = res[0].StartDate;
                model.EndDate = res[0].EndDate;
                model.AssessorID = res[0].AssessorID;
                model.CompletedDate = res[0].CompletedDate;
            }
            model.RCMListByOrgID = await ApiClientFactory.Instance.GetRCMListByOrgID(orgId);
            model.ddlAssessor = new SelectList(await ApiClientFactory.Instance.getClientadminFillData(orgId), "ClientId", "ClientName");
            model.OrgName = orgName;
            return View(model);
        }
        public async Task<IActionResult> PerformAssessment(long orgId = 0, string orgName = "", bool isTask = false)
        {
            var model = new IRA();
            string str = string.Empty;
            string thead = string.Empty;
            string tbody = string.Empty;
            model.IsTask = isTask;
            var res = await ApiClientFactory.Instance.GetComplianceRCM(orgId);
            model.SubDomainList = await ApiClientFactory.Instance.GetSubDomainRCMBID(res[0].RCMId);
            var res2 = await ApiClientFactory.Instance.GetRCMControlListByOrg(res[0].RCMId);
            var res3 = await ApiClientFactory.Instance.GetEnumByType(12);
            thead = "<thead class='txtCenter'><tr><th>Control Name</th><th>Control Description</th><th width='15%'>Test of design</th><th width='15%'>Test of effectiveness</th><th width='15%'>OverAll Effectiveness</th><th width='15%'>Remarks</th></tr></thead>";

            foreach (var item in model.SubDomainList)
            {
                str = str + "<div class='panel panel-default'><div class='panel-heading' role='tab' id='heading_" + item.SubDomainId + "'><div class='clsadjust'><div><h4 class='panel-title'>" +
                    "<a class='collapsed' role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse" + item.SubDomainId + "' aria-expanded='false' aria-controls='collapse" + item.SubDomainId + "'>" +
                    "" + item.SubDomainName + "</a></h4></div><div><label style='margin-right:10px'>Domain Compliance Score</label><input type='text' id=DomainComScore" + item.SubDomainId + " disabled='disabled' value='" + item.DomainCompilancescore + "' style='text-align: center;'/></div></div></div> <div id='collapse" + item.SubDomainId + "' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading" + item.SubDomainId + "'>" +
                    "<div class='panel-body'><div class='row'><div class='col-12'><div class='table_control'><table class='table' id=tbl_" + item.SubDomainId + ">" + thead + "<tbody>";
                foreach (var item2 in res2)
                {
                    if (item.SubDomainId == item2.SubDomainId)
                    {
                        string html = "<option value='0'>--Select--</option>";
                        string html2 = "<option value='0'>--Select--</option>";

                        foreach (var T in res3)
                        {
                            if (item2.Testofdesign == 1 && T.Value == 1)
                            {
                                html = html + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else if (item2.Testofdesign == 2 && T.Value == 2)
                            {
                                html = html + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else if (item2.Testofdesign == 3 && T.Value == 3)
                            {
                                html = html + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else
                            {
                                html = html + "<option value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            if (item2.Testofeffectiveness == 1 && T.Value == 1)
                            {
                                html2 = html2 + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else if (item2.Testofeffectiveness == 2 && T.Value == 2)
                            {
                                html2 = html2 + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else if (item2.Testofeffectiveness == 3 && T.Value == 3)
                            {
                                html2 = html2 + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else
                            {
                                html2 = html2 + "<option value=" + T.Value + ">" + T.Name + "</option>";
                            }
                        }
                        str = str + "<tr class='txtCenter'><td id='ControlName" + item2.ControlName + "'><input type='hidden' value=" + item2.ICDId + " />" + item2.ControlName + "</td><td id='ControlDescription" + item2.ControlDescription + "'><input type='hidden' value=" + item2.SubDomainId + " />" + item2.ControlDescription + "</td>" +
                            "<td id='TestOfDesign" + item2.SubDomainId + "'><select class='form-control m_no ClsTestDesign' onchange='fn_bindOverallEffectiveness(this);Calc(this," + item.SubDomainId + ");'>" + html + "</select></td><td id='TestOfEff" + item2.SubDomainId + "'><select class='form-control m_no ClsTestDesign' disabled='disabled' onchange='fn_bindOverallEffectiveness2(this);Calc(this," + item.SubDomainId + ");'>" + html2 + "</select></td><td id='OverAllEff" + item2.SubDomainId + "' ><input type='text' value='" + item2.overalleffectiveness + "' class='form-control' disabled='disabled'/></td><td id='Remarks" + item2.SubDomainId + "'><input type='text' value='" + item2.remarks + "' class='form-control'/></td></tr>";
                    }
                }
                str = str + "</tbody></table></div></div></div></div></div></div>";
            }

            model.HtmlCode = str;
            model.OrgId = orgId;
            model.OrgName = orgName;
            return View(model);
        }
        public async Task<IActionResult> DownloadPerformAssessmentReportToExcel(long orgId = 0)
        {
            try
            {
                var res = await ApiClientFactory.Instance.GetComplianceRCM(orgId);
                var listIRA = await ApiClientFactory.Instance.GetSubDomainRCMBID(res[0].RCMId);
                var controlList = await ApiClientFactory.Instance.GetRCMControlListByOrg(res[0].RCMId);
                var enumList = await ApiClientFactory.Instance.GetEnumByType(12);
                var wb = new XLWorkbook();
                var ws = wb.AddWorksheet("Perform Assessment Report");
                ws.Cell("A1").Value = "Sub-Domain";
                ws.Cell("B1").Value = "Control Name";
                ws.Cell("C1").Value = "Control Description";
                ws.Cell("D1").Value = "Test of design";
                ws.Cell("E1").Value = "Test of effectiveness";
                ws.Cell("F1").Value = "OverAll Effectiveness";
                ws.Cell("G1").Value = "Remarks";
                ws.Cell("H1").Value = "Domain Compliance Score";
                ws.Columns("1", "8").Width = 30;
                ws.Cells("A1:H1").Style.Font.FontSize = 14;
                ws.Cells("A1:H1").Style.Fill.BackgroundColor = XLColor.LightBlue;
                int sn = 1, i = 1;
                foreach (var item in listIRA)
                {
                    ++sn;
                    ws.Cell("A" + sn).Value = item.SubDomainName;
                    ws.Cell("H" + sn).Value = item.DomainCompilancescore;
                    int count = controlList.Where(x => x.SubDomainId == item.SubDomainId).Count();
                    var range1 = ws.Range("A" + sn + ":A" + (sn + count - 1));
                    range1.Merge();
                    range1.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    var range2 = ws.Range("H" + sn + ":H" + (sn + count - 1));
                    range2.Merge();
                    range2.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    foreach (var _item in controlList.Where(x => x.SubDomainId == item.SubDomainId))
                    {
                        i = sn;
                        var testOfDesign = enumList.Where(x => x.Value == _item.Testofdesign).Select(x => x.Name).FirstOrDefault();
                        var testOfEffectiveness = enumList.Where(x => x.Value == _item.Testofeffectiveness).Select(x => x.Name).FirstOrDefault();
                        ws.Cell("B" + sn).Value = _item.ControlName;
                        ws.Cell("C" + sn).Value = _item.ControlDescription;
                        //ws.Cell("C" + sn).Style.Alignment.WrapText = true;
                        ws.Cell("D" + sn).Value = testOfDesign;
                        ws.Cell("E" + sn).Value = testOfEffectiveness;
                        ws.Cell("F" + sn).Value = _item.overalleffectiveness;
                        ws.Cell("G" + sn).Value = _item.remarks;
                        sn++;
                    }
                    sn--;
                }
                using MemoryStream stream = new MemoryStream();
                wb.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "PerformAssessmentReport.xlsx");
            }
            catch (Exception ex)
            {
                ex.HResult = 0; throw;
            }
        }

        [HttpPost]
        public async Task<JsonResult> SaveComplianceRCM(IRA model)
        {
            _logger.LogInformation("SaveComplianceRCM");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveComplianceRCM(model);
            return Json(res);
        }

        [HttpPost]
        public async Task<JsonResult> SubmitComplainceAssessmentRCM(IRA model)
        {
            _logger.LogInformation("SubmitComplainceAssessmentRCM");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitComplainceAssessmentRCM(model);
            return Json(res);
        }

        //[HttpPost]
        //public async Task<IActionResult> BindQuestionStandardAndRegulation(string QuestionsID, string SectorId, string CountryId)
        //{
        //    _logger.LogInformation("BindQuestionStandardAndRegulation");
        //    var res = await ApiClientFactory.Instance.GetQuestionStandardWise(QuestionsID);
        //    var res2 = await ApiClientFactory.Instance.GetQuestionRegoWise(SectorId, CountryId);
        //    return Json(new { res, res2 });
        //}
        //[HttpPost]
        //public async Task<IActionResult> BindComplianceScore()
        //{
        //    _logger.LogInformation("BindComplianceScore");
        //    string html2 = string.Empty;
        //    var res = await ApiClientFactory.Instance.GetICD();
        //    var res2 = await ApiClientFactory.Instance.GetEnumByType(12);
        //    string html3 = string.Empty;
        //    string html4 = string.Empty;
        //    long SubDomainId = 0;
        //    string html = "<option value='0'>--Select--</option>";
        //    foreach (var item in res2)
        //    {
        //        html = html + "<option value="+item.Value+">"+ item.Name + "</option>";
        //    }
        //    foreach (var item in res)
        //    {
        //        if (SubDomainId!= item.SubDomainId)
        //        {
        //            html3 = "<tr style='border-top:solid #00a3a1' id='trid_"+item.SubDomainId+"'><td hidden>"+item.SubDomainId+"</td><td hidden>" + item.Id + "</td><td rowspan=" + item.Countee +" width='10%'>" + item.SubDomainName + "</td>";
        //            html4 = "<td class='clsScore text-center' rowspan=" + item.Countee + "></td>";
        //        }
        //        else
        //        {
        //            html3 = "<tr><td hidden>" + item.SubDomainId + "</td><td hidden>" + item.Id + "</td><td hidden rowspan=" + item.Countee + " width='10%'>" + item.SubDomainName + "</td>";
        //            html4 = "<td hidden rowspan=" + item.Countee + "></td>";
        //        }
        //        html2 = html2 + html3;
        //        html2 = html2 + "<td>" + item.ControlId + "</td>";
        //        html2 = html2 + "<td width='10%'>" + item.ControlName + "</td>";
        //        html2 = html2 + "<td width='20%'>" + item.ControlDescription + "</td>";
        //        html2 = html2 + "<td width='13%'><select class='form-control m_no ClsTestDesign' onchange='fn_bindOverallEffectiveness(this);Calc(this);'>" + html + "</select></td>";
        //        html2 = html2 + "<td width='13%'><select class='form-control m_no ClsTestDesign' onchange='fn_bindOverallEffectiveness2(this);Calc(this);'>" + html + "</select></td>";
        //        html2 = html2 + "<td width='13%'><input type='text' disabled class='form-control m_no'></td>";
        //        html2 = html2 + html4;
        //        html2 = html2 + "<td width='13%'><input type='text'  class='form-control m_no'></td>";
        //        html2 = html2 + "</tr>";
        //        SubDomainId = item.SubDomainId;
        //    }
        //    return Json(new { res, html2 });
        //}
    }
}
