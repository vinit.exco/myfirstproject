﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class AssesmentController : Controller
    {
        private readonly ILogger<AssesmentController> _logger;
        public AssesmentController(ILogger<AssesmentController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var obj = new Assesment();
            obj.DdlControlCategory = new SelectList(await ApiClientFactory.Instance.GetEnumByType(2), "Value", "Name");
            obj.DdlOrganization = new SelectList(await ApiClientFactory.Instance.GetOrganizationList(), "Id", "ParentOrgName");
            obj.Assesments = await ApiClientFactory.Instance.GetAssesment();
            obj.DDlchildOrgs = await ApiClientFactory.Instance.GetChildOrg();
            return View(obj);
        }

        [HttpPost]
        public async Task<IActionResult> SubmitAssesment(Assesment model)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("SubmitAssesment");
                model.CreatedIP = HttpContext.Session.GetString("IP");
                model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var res = await ApiClientFactory.Instance.SubmitAssesment(model);
                if (res.IsSuccess)
                {
                    TempData["success"] = model.EventType == 0 ? "Saved successfully." : "Updated successfully.";
                    return RedirectToAction("Index", "Assesment");
                }
            }
            TempData["error"] = "Something went wrong!";
            ModelState.AddModelError("", "Invalid attempt");
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> SaveAssesment(Assesment model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitAssesment(model);
            return Json(res);
        }
        public async Task<JsonResult> BlockDeleteAssesment(Assesment model)
        {
            _logger.LogInformation("BlockDeleteAssesment");
            var res = await ApiClientFactory.Instance.BlockDeleteAssesment(model);
            if (res.IsSuccess)
            {
                return Json(new { code = 200 });
            }
            return Json(new { code = 500 });
        }
    }
}
