﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class EnablerAssessmentController : Controller
    {
        private readonly ILogger<EnablerAssessmentController> _logger;
        public EnablerAssessmentController(ILogger<EnablerAssessmentController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(int OrganizationId = 0)
        {
            var obj = new ClientEnablers();
            var Organograms = await ApiClientFactory.Instance.GetOrganogram();
            obj.OrganizationsList = Organograms.Where(x => x.EmployeId == Convert.ToInt16(HttpContext.Session.GetInt32("UserId"))).ToList();
            obj.DdlOrganizations = new SelectList(await ApiClientFactory.Instance.GetChildCompanyListOnClientWise(OrganizationId), "Id", "ChildOrgName");
            obj.DdlControlCat = new SelectList(await ApiClientFactory.Instance.GetEnumByType(2), "Value", "Name");
            obj.ParentOrgId = OrganizationId;
            ViewBag.message = TempData["save"];
            ViewBag.error = TempData["error"];
            return View(obj);
        }

        public async Task<JsonResult> GetAssesmentClient(long parentOrgId, long childOrgId, int categoryId)
        {
            _logger.LogInformation("GetAssesmentClient");
            var GetEnablerQuestion = await ApiClientFactory.Instance.GetEnablerQuestionByClientCategory(parentOrgId, childOrgId, categoryId);

            var SelectGetQuestion = await ApiClientFactory.Instance.GetCompliance();
            var GetQuestion = SelectGetQuestion.Where(m => m.ControlCategory == categoryId).ToList();

            //List<GetEnablerQuestion> getEnablerQuestions = new List<GetEnablerQuestion>();
            //GetEnablerQuestion getEnablerQuestion = new GetEnablerQuestion();
            //EnablerAssessmentBind enablerAssessmentBind = new EnablerAssessmentBind();
            //long primaryid = 0;
            //foreach (var item in GetEnablerQuestion.Data)
            //{
            //       primaryid = item.QuestionnaireId;
            //    if (primaryid == item.QuestionnaireId)
            //    {
            //        enablerAssessmentBind = new EnablerAssessmentBind();
            //        getEnablerQuestion.EnablerAssessmentBinds = new List<EnablerAssessmentBind>();
            //        enablerAssessmentBind.Answer = item.Answer;
            //        enablerAssessmentBind.QuestionId = item.QuestionId;
            //        enablerAssessmentBind.QuestionnaireId = item.Id;
            //        getEnablerQuestion.EnablerAssessmentBinds.Add(enablerAssessmentBind);
            //    } 
            //}


            if (GetEnablerQuestion.IsSuccess)
            {
                return Json(new { GetEnablerQuestion = GetEnablerQuestion.Data, GetQuestion });
            }
            return Json(new { code = 500 });
        }
    }
}
