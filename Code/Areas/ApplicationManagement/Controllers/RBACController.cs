﻿using Cyberanium.Factory;
using CyberaniumApi.Helpers;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class RBACController : Controller
    {
        private readonly ILogger<RBACController> _logger;
        RBAC obj = new RBAC();
        public RBACController(ILogger<RBACController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            obj.Menues = await ApiClientFactory.Instance.GetMenu();
            obj.DdlRole = new SelectList(await ApiClientFactory.Instance.GetEnumByType(10), "Value", "Name");
            ViewBag.message = TempData["save"];
            ViewBag.error = TempData["error"];
            return View(obj);
        }
        [HttpPost]
        public async Task<IActionResult> Index(RBAC rBAC)
        {
            _logger.LogInformation("SubmitRole");
            rBAC.CreatedIP = HttpContext.Session.GetString("IP");
            rBAC.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitRole(rBAC);
            if (res.IsSuccess)
            {
                if (rBAC.Edit == "0") 
                {
                    TempData["save"] = "Save successfully!!";
                } else { TempData["save"] = "Update successfully!!"; }
                return RedirectToAction("Index", "RBAC");
            }
            TempData["error"] = "Somethig is worng??";
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> GetRoleById(long id)
        {
            var res = id > 0 ? await ApiClientFactory.Instance.GetRoleById(id) : new List<RBAC>();
            //obj.Menues = res;
            //return View();
            return Json(res);
        }

    }
}