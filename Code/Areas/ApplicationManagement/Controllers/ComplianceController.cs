﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class ComplianceController : Controller
    {
        private readonly ILogger<ComplianceController> _logger;
        public ComplianceController(ILogger<ComplianceController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var obj = new Compliance();
            obj.EnablerList = await ApiClientFactory.Instance.GetEnumByType(2);
            return View(obj);
        }
        public async Task<IActionResult> Questionnaire(int conCatId, bool next = false)
        {
            var obj = new Compliance();
            var res = await ApiClientFactory.Instance.GetCompliance();
            obj.ComplianceList = res.Where(x => x.ControlCategory == conCatId).ToList();
            obj.ControlCategory = conCatId;
            obj.Id = next ? obj.ComplianceList.Count > 0 ? 2 : 1 : 0;
            obj.DdlControlCat = new SelectList(await ApiClientFactory.Instance.GetEnumByType(2), "Value", "Name");
            return View(obj);
        }

        [HttpPost]
        public async Task<JsonResult> SubmitCompliance(Compliance model)
        {
            _logger.LogInformation("SubmitCompliance");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitCompliance(model);
            return Json(res);
        }
        public async Task<JsonResult> BlockDeleteCompliance(int id)
        {
            _logger.LogInformation("BlockDeleteCompliance");
            var res = await ApiClientFactory.Instance.BlockDeleteCompliance(id);
            return Json(res);
        }
        public async Task<JsonResult> SetIsPrimary(int id)
        {
            _logger.LogInformation("SetIsPrimary");
            var res = await ApiClientFactory.Instance.SetIsPrimary(id);
            return Json(res);
        }
        public async Task<List<Compliance>> GetQuestionnaireById(long CategoryId)
        {
            var listModel = new List<Compliance>();
            var Compliances = await ApiClientFactory.Instance.GetCompliance();
            return listModel = Compliances.Where(x => x.ControlCategory == CategoryId).ToList();
        }
        public async Task<List<Compliance>> GetQuestionnaireByIds(string CategoryIds)
        {
            var listModel = new List<Compliance>();
            var CategoryId = CategoryIds.Split(",");
            var Compliances = await ApiClientFactory.Instance.GetCompliance();
            foreach (var item in CategoryId)
            {
                listModel.AddRange(Compliances.Where(x => x.ControlCategory == Convert.ToInt32(item)).ToList());
            }
            return listModel;
        }
    }
}
