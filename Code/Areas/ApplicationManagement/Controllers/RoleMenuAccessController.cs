﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class RoleMenuAccessController : Controller
    {
        private readonly ILogger<RoleMenuAccessController> _logger;
        public RoleMenuAccessController(ILogger<RoleMenuAccessController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var obj = new RoleMenuAccess();
            var menues = await ApiClientFactory.Instance.GetMenu();
            obj.DDLRole = new SelectList(await ApiClientFactory.Instance.GetEnumByType(10), "Value", "Name");
            obj.Menues = menues;
            return View(obj);
        }

        [HttpPost]
        public async Task<IActionResult> SubmitRoleMenuAccess(Menu model)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("SubmitRoleMenuAccess");
                var res = await ApiClientFactory.Instance.SubmitMenu(model);
                if (res.IsSuccess)
                {
                    return RedirectToAction("Index", "RoleMenuAccess");
                }
            }
            ModelState.AddModelError("", "Invalid attempt");
            return View(model);
        }
    }
}
