﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class ProcessEnablerController : Controller
    {
        public async Task<IActionResult> Index(long parentOrgId = 0)
        {
            var model = new OrganoGrams();
            var Organograms = await ApiClientFactory.Instance.GetOrganogram();
            model.OrganizationsList = Organograms.Where(x => x.EmployeId == Convert.ToInt16(HttpContext.Session.GetInt32("UserId"))).ToList();
            model.DdlChildOrg = new SelectList(await ApiClientFactory.Instance.GetChildOrganizationByParentOrgID(parentOrgId), "Id", "ChildOrgName");
            model.Id = parentOrgId;
            return View(model);
        }
        public async Task<JsonResult> GetFunctionsByOrgId(long parentOrgId, long childOrgId)
        {
            var res = childOrgId > 0 ? await ApiClientFactory.Instance.GetFunctionByChildOrganization(Convert.ToInt32(childOrgId))
                : await ApiClientFactory.Instance.GetFunctionByOrganization(parentOrgId);
            return Json(res);
        }

        [HttpPost]
        public async Task<IActionResult> GetChildOrgControlCategory(int OrgID, int ChilOrgID)
        {
            var Values = await ApiClientFactory.Instance.GetAllEnumChildOrgCategory(OrgID, ChilOrgID);
            var Functions = await ApiClientFactory.Instance.GetFuncation(ChilOrgID);
            return Json(new { Values, Functions });
        }
        [HttpPost]
        public async Task<IActionResult> GetEnabler(long OrgId, long ChildOrgId, long CategoryId)
        {
            var Values = await ApiClientFactory.Instance.GetEnabler(OrgId, ChildOrgId, CategoryId);
            return Json(new { Values });
        }
        [HttpPost]
        public async Task<IActionResult> Get(long ChildId)
        {
            var values = await ApiClientFactory.Instance.GetAllEnumTypes(ChildId);
            //var function = await ApiClientFactory.Instance.GetFuncation(ChildId);
            var Enabler = await ApiClientFactory.Instance.GetEnabler(ChildId, 0, 0);
            return Json(new { values, Enabler });
        }
        [HttpPost]
        public async Task<IActionResult> GetProcess(long OrgId)
        {
            return Json(await ApiClientFactory.Instance.GetProcess(OrgId));
        }
        [HttpPost]
        public async Task<IActionResult> Index(OrganoGrams organoGrams)
        {
            //_logger.LogInformation("SaveProcessEnabler");
            organoGrams.CreatedIP = HttpContext.Session.GetString("IP");
            organoGrams.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.PostProcess(organoGrams);
            if (result.IsSuccess)
            {
                TempData["success"] = "Save successfully!!";
            }
            else
            {
                TempData["error"] = "Not save successfully!!";
            }
            return RedirectToAction("Index", "ProcessEnabler", new { area = "ApplicationManagement" });
        }
        [HttpPost]
        public async Task<IActionResult> GetCategoryDetails(long Id)
        {
            return Json(await ApiClientFactory.Instance.GetCategoryDetails(Id));
        }

        [HttpPost]
        public async Task<JsonResult> SubmitProcessEnabler(ProcessEnabler model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitProcessEnabler(model);
            return Json(res);
        }
    }
}
