﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class OrganizationController : Controller
    {
        private readonly ILogger<OrganizationController> _logger;
        public OrganizationController(ILogger<OrganizationController> logger)
        {
            _logger = logger;
        }
        #region New Organization
        public async Task<IActionResult> Index(int Id = 0, string name = "")
        {
            var obj = new Organization();
            obj.Id = Id;
            obj.Name = name;
            obj.OrganizationList = await ApiClientFactory.Instance.GetOrganizationList();
            return View(obj);
        }

        [HttpPost]
        public async Task<JsonResult> AddUpadeteOrganization(Organization model)
        {
            _logger.LogInformation("AddUpadeteOrganization");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.AddUpadeteOrganization(model);
            return Json(res);
        }
        public async Task<List<Organization>> GetOrganizationList() => await ApiClientFactory.Instance.GetOrganizationList();
        public JsonResult GetChildOrganizationList(long id = 0) => Json(GetOrganizationList().Result.Where(x => x.ChildOrgId == id));
        public JsonResult GetSubChildOrganizationList(long id = 0) => Json(GetOrganizationList().Result.Where(x => x.ChildOrgId == 0 && x.SubChildOrgId == id));
        public async Task<Message<Organization>> DeleteOrganizationById(long parentOrgId = 0) => await ApiClientFactory.Instance.DeleteOrganizationById(parentOrgId);
        public async Task<JsonResult> CheckOrgDuplicate(string OrgName)
        {
            _logger.LogInformation("CheckOrgDuplicate");
            var res = await ApiClientFactory.Instance.CheckOrgDuplicate(OrgName);
            return Json(res);
        }
        #endregion
        #region Old Organization

        [HttpPost]
        public async Task<JsonResult> SubmitOrganization(Organization model)
        {
            _logger.LogInformation("SubmitOrganization");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitOrganization(model);
            if (res.IsSuccess)
            {
                TempData["success"] = (model.Id > 0 ? "Updated successfully" : "Saved successfully");
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(res);
        }
        public async Task<IActionResult> BlockDeleteOrganization(int id, int Type, string ActionName = null)
        {
            _logger.LogInformation("BlockDeleteOrganization");
            var model = new Organization();
            model.Id = id;
            model.EventType = Type;
            var res = await ApiClientFactory.Instance.BlockDeleteOrganization(model);
            if (Type > 0)
            {
                return Json(res);
            }
            else
            {
                TempData["success"] = ActionName + " " + "successfully!!";
            }
            return RedirectToAction("Index", "Organization", new { area = "ApplicationManagement" });
        }
        public async Task<IActionResult> FunctionProcess()
        {
            var obj = new Organization();
            obj.DdlSector = new SelectList(await ApiClientFactory.Instance.GetEnumByType(1), "Value", "Name");
            obj.DdlCountry = new SelectList(await ApiClientFactory.Instance.GetAllCountry(), "Id", "CountryName");
            obj.DdlRiskDomain = new SelectList(await ApiClientFactory.Instance.GetEnumByType(3), "Value", "Name");
            obj.DdlInternational = new SelectList(await ApiClientFactory.Instance.GetAllInternationalStandard(), "Id", "Title");
            obj.DdlControlCat = new SelectList(await ApiClientFactory.Instance.GetEnumByType(2), "Value", "Name");
            //var enums = await ApiClientFactory.Instance.GetAllEnum();
            //var countries = await ApiClientFactory.Instance.GetAllCountry();
            obj.Compliances = await ApiClientFactory.Instance.GetCompliance();
            obj.OrganizationList = await ApiClientFactory.Instance.GetOrganizationList();
            obj.DdlOrganization = new SelectList(await ApiClientFactory.Instance.GetOrganizationList(), "Id", "ParentOrgName");
            obj.DdlRole = new SelectList(await ApiClientFactory.Instance.GetEnumByType(10), "Value", "Name");
            //obj.Enums = enums;
            //obj.Countries = countries;
            //obj.Compliances = compliances;
            //obj.Organizations = organizations;
            ViewBag.message = TempData["save"];
            return View(obj);
        }

        [HttpPost]
        public async Task<IActionResult> CheckParentOrgName(string ParentOrgName)
        {
            _logger.LogInformation("CheckParentOrgName");
            string msg = string.Empty;
            Organization obj = new Organization();
            obj.ParentOrgName = ParentOrgName;
            var res = await ApiClientFactory.Instance.CheckParentOrgName(obj);
            if (res.IsSuccess)
            {
                msg = "Already exists";
            }
            return Json(new { msg });
        }
        #endregion
    }
}
