﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]

    public class OrganogramController : Controller
    {
        private readonly ILogger<OrganogramController> _logger;
        public OrganogramController(ILogger<OrganogramController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> TabMenu()
        {
            var obj = new RBAC();
            obj.OrganizationList = await ApiClientFactory.Instance.GetOrganizationList();
            return View(obj);
        }
    }
}
