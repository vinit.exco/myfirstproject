﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class ProcessesAssignController : Controller
    {
        private readonly ILogger<ProcessesAssignController> _logger;
        public ProcessesAssignController(ILogger<ProcessesAssignController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(long parentOrgId = 0)
        {
            var model = new ProcessesAssign();
            var Organograms = await ApiClientFactory.Instance.GetOrganogram();
            model.OrganizationList = Organograms.Where(x => x.EmployeId == Convert.ToInt16(HttpContext.Session.GetInt32("UserId"))).ToList();
            model.DdlChildOrg = new SelectList(await ApiClientFactory.Instance.GetChildOrganizationByParentOrgID(parentOrgId), "Id", "ChildOrgName");
            model.Id = parentOrgId;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> EnablerScore(ProcessesAssign model)
        {
            var obj = new ProcessesAssign();
            obj.RiskDomainList = await ApiClientFactory.Instance.GetEnumByType(3);
            obj.ControlCategoryList = await ApiClientFactory.Instance.GetControlCatByProcessId(model);
            obj.DomainBaseRiskScoreList = await ApiClientFactory.Instance.GetDomainBaseRiskScore(model);
            return View(obj);
        }
        public async Task<JsonResult> GetControlCatByProcessId(ProcessesAssign model)
        {
            var res = await ApiClientFactory.Instance.GetControlCatByProcessId(model);
            return Json(res);
        }
        public async Task<JsonResult> GetDomainBaseRiskScore(ProcessesAssign model)
        {
            var res = await ApiClientFactory.Instance.GetDomainBaseRiskScore(model);
            return Json(res);
        }
        public async Task<JsonResult> GetProcessesByOrgId(long Id = 0)
        {
            var res = await ApiClientFactory.Instance.GetProcessesByOrgId(Id);
            return Json(res);
        }
        [HttpPost]
        public async Task<JsonResult> GetDomainCriticalityScoreByProcessId(ProcessesAssign model)
        {
            var res = await ApiClientFactory.Instance.GetDomainCriticalityScoreByProcessId(model);
            return Json(res);
        }
        [HttpPost]
        public async Task<bool> SubmitDCEnablerFinalScore(ProcessesAssign model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitDCEnablerFinalScore(model);
            return res.IsSuccess;
        }
    }
}
