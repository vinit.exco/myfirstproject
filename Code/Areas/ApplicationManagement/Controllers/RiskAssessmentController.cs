﻿using ClosedXML.Excel;
using Cyberanium.Factory;
using CyberaniumModels;
using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]

    public class RiskAssessmentController : Controller
    {
        private readonly ILogger<RiskAssessmentController> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public RiskAssessmentController(ILogger<RiskAssessmentController> logger, IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
        }
        public IActionResult Index(long orgId = 0, long CategoryId = 0, long QuestionnaireID = 0, string EnablerName = "", string orgName = "")
        {
            var obj = new RiskAssessment();
            obj.OrgId = orgId;
            obj.OrgName = orgName;
            obj.CategoryId = CategoryId;
            obj.QuestionnaireID = QuestionnaireID;
            obj.CategoryName = EnablerName;
            return View(obj);
        }
        public async Task<IActionResult> PerformCtrlAssessment(long orgId = 0, long CategoryId = 0, long QuestionnaireID = 0, string EnablerName = "", bool isTask = false)
        {
            RiskAssessment obj = new RiskAssessment();
            obj.RCMControlByOrgLatestList = await ApiClientFactory.Instance.GetRCMControlByOrgLatest(orgId, QuestionnaireID);
            obj.ControlAssessmentList = await ApiClientFactory.Instance.GetControlAssessmentListByOrg(orgId, QuestionnaireID);
            var res = await ApiClientFactory.Instance.GetApprovalFor(orgId, QuestionnaireID, CategoryId);
            obj.ApprovalFor = res.Mode;
            obj.IsTask = isTask;
            var ddlRole = await ApiClientFactory.Instance.GetUsers();
            obj.DdlRole = new SelectList(ddlRole.Where(x => x.RoleId == 4 || x.RoleId == 2).Select(s => new { s.Id, s.UserName }).ToList(), "Id", "UserName");
            if (obj.ControlAssessmentList.Count > 0)
            {
                obj.Status = obj.ControlAssessmentList[0].Status;
            }
            else
            {
                obj.Status = 0;
            }
            obj.OrgId = orgId;
            obj.CategoryId = CategoryId;
            obj.CategoryName = EnablerName;
            obj.QuestionnaireID = QuestionnaireID;
            var CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            obj.RAList = await ApiClientFactory.Instance.GetAllComments(orgId, CategoryId, QuestionnaireID, CreatedBy);
            return View(obj);
        }
        public async Task<IActionResult> Artifacts(long orgId = 0, long CategoryId = 0, long QuestionnaireID = 0, string EnablerName = "")
        {
            RiskAssessment obj = new RiskAssessment();
            obj.RCMControlByOrgLatestList = await ApiClientFactory.Instance.GetRCMControlByOrgLatest(orgId, QuestionnaireID);
            obj.OrgId = orgId;
            obj.CategoryId = CategoryId;
            obj.CategoryName = EnablerName;
            obj.QuestionnaireID = QuestionnaireID;
            var CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            obj.ClientUserList = await ApiClientFactory.Instance.GetClientUser();
            obj.AssScopeEndDate = await ApiClientFactory.Instance.GetEndDateOfAS(orgId);
            return View(obj);
        }
        public async Task<JsonResult> SaveApprovalfor(RiskAssessment model)
        {
            _logger.LogInformation("SaveApprovalfor");
            var res = await ApiClientFactory.Instance.SaveApprovalfor(model);
            return Json(res);
        }
        public async Task<IActionResult> RemoveTask(long OrgId, long CategoryId, long QuestionnaireID)
        {
            _logger.LogInformation("RemoveTask");
            var res = await ApiClientFactory.Instance.RemoveTask(OrgId, CategoryId, QuestionnaireID);
            return Json(res);
        }
        public async Task<IActionResult> AssessmentReport(long orgId = 0, long CategoryId = 0, long QuestionnaireID = 0, string EnablerName = "")
        {
            RiskAssessment obj = new RiskAssessment();
            obj.ControlAssessmentList = await ApiClientFactory.Instance.GetControlAssessmentListByOrg(orgId, QuestionnaireID);
            obj.OrgId = orgId;
            obj.CategoryId = CategoryId;
            obj.CategoryName = EnablerName;
            obj.QuestionnaireID = QuestionnaireID;
            return View(obj);
        }
        [HttpPost]
        public async Task<IActionResult> SaveUpdateControlAssessment(RiskAssessment model)
        {
            _logger.LogInformation("SaveUpdateControlAssessment");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveUpdateControlAssessment(model);
            return Json(res);
        }
        [HttpPost]
        public async Task<IActionResult> SaveComment(RiskAssessment model)
        {
            _logger.LogInformation("SaveComment");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveComment(model);
            return Json(res);
        }
        public async Task<IActionResult> FinalReport(long orgId = 0, int CategoryId = 0, long QuestionnaireId = 0, string EnablerName = "")
        {
            var obj = new RiskAssessment();
            obj.OrgId = orgId;
            obj.CategoryId = CategoryId;
            obj.CategoryName = EnablerName;
            obj.QuestionnaireID = QuestionnaireId;
            obj.ThreatList = await ApiClientFactory.Instance.GetThreatListById(orgId, CategoryId, QuestionnaireId);
            var res = await ApiClientFactory.Instance.getAssessmentScope(orgId);
            obj.DomainThreatMapList = await ApiClientFactory.Instance.GetDomainThreatMapList(res[0].DomainIds);
            obj.RiskDomainList = await ApiClientFactory.Instance.GetRiskDomainListByOrgId(orgId);
            return View(obj);
        }
        public async Task<IActionResult> RiskTreatmentPlan(long orgId = 0, int CategoryId = 0, long QuestionnaireId = 0, string EnablerName = "")
        {
            var obj = new RiskAssessment();
            obj.OrgId = orgId;
            obj.CategoryId = CategoryId;
            obj.CategoryName = EnablerName;
            obj.ddlStatus = new SelectList(await ApiClientFactory.Instance.GetEnumByType(16), "Value", "Name");
            obj.ddlDecision = new SelectList(await ApiClientFactory.Instance.GetEnumByType(17), "Value", "Name");
            obj.ControlAssesmentControlList = await ApiClientFactory.Instance.GetControlAssessmentControl(orgId, QuestionnaireId);
            obj.QuestionnaireID = QuestionnaireId;
            return View(obj);
        }
        public async Task<IActionResult> DownloadRiskTreatmentPlanExcel(long orgId = 0, int categoryId = 0, long questionnaireId = 0, string enablerName = "")
        {

            try
            {
                var obj = new RiskAssessment();
                obj.OrgId = orgId;
                obj.CategoryId = categoryId;
                obj.CategoryName = enablerName;
                obj.StatusList = await ApiClientFactory.Instance.GetEnumByType(16);
                obj.ManagenmentDecisionList = await ApiClientFactory.Instance.GetEnumByType(17);
                obj.ControlAssesmentControlList = await ApiClientFactory.Instance.GetControlAssessmentControl(orgId, questionnaireId);
                obj.QuestionnaireID = questionnaireId;
                DataTable dt = new DataTable("AssessmentReport");
                dt.Columns.AddRange(new DataColumn[12] { new DataColumn("Control Name"),
                new DataColumn("Control ID"),
                new DataColumn("Control Rating"),
                new DataColumn("Observation"),
                new DataColumn("Recommendation"),
                new DataColumn("Risk Owner"),
                new DataColumn("Management Decision"),
                new DataColumn("Justification"),
                new DataColumn("Action Plan"),
                new DataColumn("Responsibility"),
                new DataColumn("Target closure Date"),
                new DataColumn("Status")});
                foreach (var item in obj.ControlAssesmentControlList)
                {
                    var decesion = obj.ManagenmentDecisionList.Where(x => x.Value == item.Decision).Select(y => y.Name).FirstOrDefault();
                    var status = obj.StatusList.Where(x => x.Value == item.Status).Select(y => y.Name).FirstOrDefault();
                    dt.Rows.Add(item.ControlName, item.ControlId, item.ControlName, item.Observation, item.Recommendation, item.RiskOwner, decesion, item.Justification, item.ActionPlan, item.Responsibility, item.TargetclosureDate, status);
                }
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "AssessmentReport.xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HResult = 0;
                throw;
            }
        }
        public async Task<IActionResult> DownloadFinalReportExcel(long orgId = 0, int categoryId = 0, long questionnaireId = 0)
        {
            try
            {
                var threatList = await ApiClientFactory.Instance.GetThreatListById(orgId, categoryId, questionnaireId);
                var res = await ApiClientFactory.Instance.getAssessmentScope(orgId);
                var domainThreatMapList = await ApiClientFactory.Instance.GetDomainThreatMapList(res[0].DomainIds);
                var riskDomainList = await ApiClientFactory.Instance.GetRiskDomainListByOrgId(orgId);
                var controlList = await ApiClientFactory.Instance.GetControlListByIds(orgId, categoryId, questionnaireId);

                var pRatingList = controlList.Where(x => x.ControlTypeID == 3 && x.Rating > 0).Select(x => x.Rating).ToList();
                var dcRatingList = controlList.Where(x => x.ControlTypeID != 3 && x.Rating > 0).Select(x => x.Rating).ToList();
                var pAvg = pRatingList.Count() > 0 ? Math.Round(pRatingList.Average(), 2) : 0;
                var dcAvg = dcRatingList.Count() > 0 ? Math.Round(dcRatingList.Average(), 2) : 0;
                double brs1 = 0, brs2 = 0, brs3 = 0, brs4 = 0, brs5 = 0;
                double s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0;
                int l1 = 0, l2 = 0, l3 = 0, l4 = 0, l5 = 0;
                foreach (var threat in threatList)
                {
                    int p = threat.Score, _i = 1;
                    var rp = pAvg <= 1 ? p * 1 : pAvg <= 2 ? p * 0.8 : pAvg <= 3 ? p * 0.5 : pAvg <= 4 ? p * 0.3 : p * 0.1;
                    var ri = dcAvg <= 1 ? _i * 1 : dcAvg <= 2 ? _i * 0.8 : dcAvg <= 3 ? _i * 0.5 : dcAvg <= 4 ? _i * 0.3 : _i * 0.1;
                    var trr = Math.Round(1 + ((rp * ri) - 0.01) * ((5 - 1) / (5 - 0.01)), 2);
                    var b1 = domainThreatMapList.Where(x => x.DomainId == 1 && x.ThreatId == threat.ThreatId).Any(x => x.Status);
                    var b2 = domainThreatMapList.Where(x => x.DomainId == 2 && x.ThreatId == threat.ThreatId).Any(x => x.Status);
                    var b3 = domainThreatMapList.Where(x => x.DomainId == 3 && x.ThreatId == threat.ThreatId).Any(x => x.Status);
                    var b4 = domainThreatMapList.Where(x => x.DomainId == 4 && x.ThreatId == threat.ThreatId).Any(x => x.Status);
                    var b5 = domainThreatMapList.Where(x => x.DomainId == 5 && x.ThreatId == threat.ThreatId).Any(x => x.Status);
                    s1 += b1 ? trr : 0; s2 += b2 ? trr : 0; s3 += b3 ? trr : 0; s4 += b4 ? trr : 0; s5 += b5 ? trr : 0;
                    _ = b1 ? l1++ : l1; _ = b2 ? l2++ : l2; _ = b3 ? l3++ : l3; _ = b4 ? l4++ : l4; _ = b5 ? l5++ : l5;
                }
                brs1 = Math.Round(s1 > 0 ? (s1 / l1) : 0, 2);
                brs2 = Math.Round(s2 > 0 ? (s2 / l2) : 0, 2);
                brs3 = Math.Round(s3 > 0 ? (s3 / l3) : 0, 2);
                brs4 = Math.Round(s4 > 0 ? (s4 / l4) : 0, 2);
                brs5 = Math.Round(s5 > 0 ? (s5 / l5) : 0, 2);
                var fbrs = Math.Round((brs1 + brs2 + brs3 + brs4 + brs5) / riskDomainList.Count, 2);
                string riskDomain = "Confidentiality,Communication Security,Integrity,Availablity,Privacy";
                var wb = new XLWorkbook();
                var ws = wb.AddWorksheet("Final Report");
                ws.Range("B2").Value = riskDomain.Split(",");
                ws.Range("C2").Value = new double[] { brs1, brs2, brs3, brs4, brs5 };
                ws.Ranges("B2:B6,C2:C6").Style.Fill.BackgroundColor = XLColor.LightBlue;
                ws.Ranges("B2:B6,C2:C6").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("C2:C6").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                ws.Range("E2").Value = "Over All Risk Score";
                ws.Range("E2").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("E3").Value = fbrs;
                ws.Range("E3").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("E2:E3").Style.Fill.BackgroundColor = XLColor.LightBlue;
                ws.Range("E2:E3").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                var colums = "Sr.No.,Threat Name,Theat Code,Probability,Impact,Sr.No.,Control Id,Control Name,Control Description,Rating,Average P,Average DC,Revised Probability,Revised Impact,Threat Residual Risk".Split(",");
                int i = 1, n = 1;
                foreach (var item in colums)
                {
                    ws.Cell(8, i++).Value = item;
                }
                ws.Range("A8:O8").Style.Font.Bold = true;
                ws.Range("A8:O8").Style.Font.FontSize = 12;
                ws.Range("A8:O8").Style.Fill.BackgroundColor = XLColor.LightBlue;
                ws.Range("A8:O8").Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("A8:O8").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Columns().AdjustToContents();
                int row = 9;
                foreach (var t in threatList)
                {
                    int p = t.Score, _i = 1;
                    var rp = pAvg <= 1 ? p * 1 : pAvg <= 2 ? p * 0.8 : pAvg <= 3 ? p * 0.5 : pAvg <= 4 ? p * 0.3 : p * 0.1;
                    var ri = dcAvg <= 1 ? _i * 1 : dcAvg <= 2 ? _i * 0.8 : dcAvg <= 3 ? _i * 0.5 : dcAvg <= 4 ? _i * 0.3 : _i * 0.1;
                    var trr = Math.Round(1 + ((rp * ri) - 0.01) * ((5 - 1) / (5 - 0.01)), 2);
                    ws.Cell(row, 1).Value = n++;
                    ws.Cell(row, 2).Value = t.ThreatName;
                    ws.Cell(row, 3).Value = t.ThreatCode;
                    ws.Cell(row, 4).Value = p;
                    ws.Cell(row, 5).Value = 1;
                    ws.Cell(row, 11).Value = pAvg > 0 ? pAvg : 1;
                    ws.Cell(row, 12).Value = dcAvg > 0 ? dcAvg : 1;
                    ws.Cell(row, 13).Value = rp;
                    ws.Cell(row, 14).Value = ri;
                    ws.Cell(row, 15).Value = trr;
                    ws.Range(row, 1, (row + controlList.Count - 1), 1).Merge();
                    ws.Range(row, 2, (row + controlList.Count - 1), 2).Merge();
                    ws.Range(row, 3, (row + controlList.Count - 1), 3).Merge();
                    ws.Range(row, 4, (row + controlList.Count - 1), 4).Merge();
                    ws.Range(row, 5, (row + controlList.Count - 1), 5).Merge();
                    ws.Range(row, 11, (row + controlList.Count - 1), 11).Merge();
                    ws.Range(row, 12, (row + controlList.Count - 1), 12).Merge();
                    ws.Range(row, 13, (row + controlList.Count - 1), 13).Merge();
                    ws.Range(row, 14, (row + controlList.Count - 1), 14).Merge();
                    ws.Range(row, 15, (row + controlList.Count - 1), 15).Merge();
                    int sn = 1;
                    foreach (var c in controlList)
                    {
                        ws.Cell(row, 6).Value = sn++;
                        ws.Cell(row, 7).Value = c.ControlId;
                        ws.Cell(row, 8).Value = c.ControlName;
                        ws.Cell(row, 9).Value = c.ControlDescription;
                        ws.Cell(row, 10).Value = c.Rating;
                        row++;
                    }
                }
                int l = 8 + (controlList.Count * threatList.Count);
                string ranges = $"A9:A{l},B9:B{l},C9:C{l},D9:D{l},E9:E{l},K9:K{l},L9:L{l},M9:M{l},N9:N{l},O9:O{l},";
                ws.Ranges(ranges).Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                ws.Ranges(ranges).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                ws.Columns("A,F,J").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Columns("H,I").AdjustToContents();
                ws.Range("B8:I8").SetAutoFilter();
                ws.SheetView.FreezeColumns(6);
                ws.SheetView.FreezeRows(8);
                var memoryStream = new MemoryStream();
                wb.SaveAs(memoryStream);
                return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "FinalReport.xlsx");
            }
            catch (Exception ex)
            {
                ex.HResult = 0; throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveUpdateRiskTreatmentPlan(RiskAssessment model)
        {
            _logger.LogInformation("SaveUpdateRiskTreatmentPlan");
            var res = await ApiClientFactory.Instance.SaveUpdateRiskTreatmentPlan(model);
            return Json(res);
        }
        public async Task<Message<int>> SubmitDomainBaseRiskScore(DomainBaseRiskScore model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitDomainBaseRiskScore(model);
            return res;
        }
        public async Task<JsonResult> GetControlListByIds(long orgId = 0, int conCatId = 0, long questionnaireId = 0) => Json(await ApiClientFactory.Instance.GetControlListByIds(orgId, conCatId, questionnaireId));

        #region Threat Probability Mapping
        public async Task<IActionResult> ThreatProbabilityScore(long orgId = 0, int conCatId = 0, long questionnaireId = 0, string enablerName = "")
        {
            string table = "<table class='table' id='tblThreatProbability'>";
            string thead = "<thead><tr><th>Threat Code</th><th>Threat Name</th>";
            var res = await ApiClientFactory.Instance.GetFirstQuesAnswersByConCatId(conCatId, orgId);
            var threatList = await ApiClientFactory.Instance.GetAllThreadsByEnablerCategory(conCatId.ToString());
            var threatEnablerList = await ApiClientFactory.Instance.GetThreatEnablerListByOrgId(orgId, conCatId, questionnaireId);
            foreach (var item in res.Where(x => x.Value == questionnaireId))
            {
                thead += "<th>" + item.Answer + "</th>";
            }
            thead += "</tr></thead>";
            string tbody = "<tbody>";
            if (threatEnablerList.Count > 0)
            {
                var list = threatEnablerList.GroupBy(x => x.ThreatId).Select(x => x.Key).ToList();
                list.AddRange(threatList.Select(x => (int)x.ThreatId).Except(list).ToList());
                foreach (var threatId in list)
                {
                    var item = threatList.Where(x => x.ThreatId == threatId).FirstOrDefault();
                    var genScore = threatEnablerList.Where(x => x.ThreatId == threatId && x.ConCatId == 0 && x.QuestionnaireId == 0).Select(x => x.Score).FirstOrDefault();
                    tbody += "<tr class='trThreats'><td hidden>" + item.ThreatId + "</td><td>" + item.ThreatName + "</td><td>" + item.ThreatCode + "</td>";
                    var enablerList = threatEnablerList.Where(x => x.ThreatId == threatId && x.ConCatId > 0 && x.QuestionnaireId > 0).ToList();
                    if (enablerList.Count > 0)
                    {
                        foreach (var ele in enablerList)
                        {
                            tbody += "<td class='tdThreats threats_" + ele.ConCatId + "_" + ele.QuestionnaireId + "'><input type='number' class='form-control' value='" + ele.Score + "' min='1' max='5' onchange='Validate(this)'/></td>";
                        }
                    }
                    else
                    {
                        foreach (var conCat in res.Where(x => x.Value == questionnaireId))
                        {
                            tbody += "<td class='tdThreats threats_" + conCat.CategoryId + "_" + conCat.Value + "'><input type='number' class='form-control' min='1' max='5' onchange='Validate(this)'/></td>";
                        }
                    }
                    tbody += "</tr>";
                }
            }
            else
            {
                foreach (var item in threatList)
                {
                    tbody += "<tr class='trThreats'><td hidden>" + item.ThreatId + "</td><td>" + item.ThreatCode + "</td><td>" + item.ThreatName + "</td>";
                    foreach (var conCat in res.Where(x => x.Value == questionnaireId))
                    {
                        tbody += "<td class='tdThreats threats_" + conCat.CategoryId + "_" + conCat.Value + "'><input type='number' class='form-control' min='1' max='5' onchange='Validate(this)'/></td>";
                    }
                    tbody += "</tr>";
                }
            }
            tbody = tbody + "</tbody>";
            table += thead + tbody + "</table>";
            var obj = new IRA();
            obj.OrgId = orgId;
            obj.htmlTable = table;
            obj.CategoryId = conCatId;
            obj.CategoryName = enablerName;
            obj.QuestionnaireID = questionnaireId;
            return View(obj);
        }
        public async Task<JsonResult> SubmitThreatProbabilityScore(long OrgId, string strJson)
        {
            var model = new Base();
            _logger.LogInformation("SubmitThreatProbabilityScore");
            model.UpdatedBy = OrgId;
            model.UpdatedIP = strJson;
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitThreatProbabilityScore(model);
            return Json(res);
        }
        #endregion

        [HttpPost]
        public JsonResult UploadFiles()
        {
            var strList = new List<string>();
            string webRootPath = _webHostEnvironment.WebRootPath;
            //string contentRootPath = _webHostEnvironment.ContentRootPath;
            string pathToUpload = Path.Combine(webRootPath, "ArtefactUploads");
            if (!Directory.Exists(pathToUpload)) Directory.CreateDirectory(pathToUpload);
            if (Request.Form.Files.Count > 0)
            {
                var files = Request.Form.Files;
                foreach (var file in files)
                {
                    string fileExtension = Path.GetExtension(file.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    string fullFileName = string.Concat(fileName, DateTime.Now.ToFileTime(), fileExtension);
                    string fullPath = Path.Combine(pathToUpload, fullFileName);
                    var fileStream = System.IO.File.Create(fullPath);
                    file.CopyTo(fileStream); fileStream.Flush(); fileStream.Close();
                    strList.Add(fullFileName);
                }
                return Json(strList);
            }
            return Json(strList);
        }

        [HttpPost]
        public async Task<JsonResult> SaveUpdateArtefacts(RiskAssessment model)
        {
            _logger.LogInformation("SaveUpdateArtefacts");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveUpdateArtefacts(model);
            return Json(res);
        }

        public async Task<IActionResult> SendReminder(long OrgId = 0, long CategoryId = 0, long QuestionnaireId = 0, string EnablerName = "")
        {
            RiskAssessment obj = new RiskAssessment();
            obj.RCMControlByOrgLatestList = await ApiClientFactory.Instance.GetRCMControlByOrgLatest(OrgId, QuestionnaireId);
            obj.OrgId = OrgId;
            obj.CategoryId = CategoryId;
            obj.CategoryName = EnablerName;
            obj.QuestionnaireID = QuestionnaireId;
            var CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            obj.ClientUserList = await ApiClientFactory.Instance.GetClientUser();
            obj.AssScopeEndDate = await ApiClientFactory.Instance.GetEndDateOfAS(OrgId);
            return View(obj);
        }
    }
}
