﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class OrganizationUserController : Controller
    {
        private readonly ILogger<OrganizationUserController> _logger;

        public OrganizationUserController(ILogger<OrganizationUserController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            UserLogin obj = new UserLogin();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            obj.DdlParentOrg = new SelectList(await ApiClientFactory.Instance.GetOrganizationByEmplyoyeeID(EmployeeId), "Id", "ParentOrgName");
            obj.DdlOrgUserAccess = new SelectList(await ApiClientFactory.Instance.GetEnumByType(14), "Value", "Name");
            List<SelectListItem> List = new List<SelectListItem>();
            var data = new[]{
                 new SelectListItem{ Value="1",Text="Viewer"},
                 new SelectListItem{ Value="2",Text="Approver"},
             };
            List = data.ToList();
            obj.DdlAccessRole = List;
            ViewBag.message = TempData["success"];
            ViewBag.error = TempData["error"];
            return View(obj);
        }
        public async Task<IActionResult> Index2()
        {
            UserLogin obj = new UserLogin();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            obj.DdlOrganization = new SelectList(await ApiClientFactory.Instance.GetAssignedOrganizationByEmplyoyeeID(EmployeeId), "Id", "OrgName");
            var userlist = await ApiClientFactory.Instance.GetUsers();
            var str = await ApiClientFactory.Instance.GetEnumByType(10);
            var res = await ApiClientFactory.Instance.GetUsers();
            obj.UserSelectList = new SelectList(res.Where(m=>m.RoleId==4 || m.RoleId==2), "Id", "UserName");
            return View(obj);
        }
        public async Task<JsonResult> GetClientList(string TeamId)
        {
            var teamIdval = TeamId;
            dynamic stuff = JsonConvert.DeserializeObject(teamIdval);
            var str1 = string.Empty;
            var str2 = string.Empty;
            var str4 = "";
            var items = "";
            var SrNo = 0;
            foreach (var a in stuff)
            {
                SrNo++;
                int _TermId = a.Id;
                var userlist = await ApiClientFactory.Instance.GetUsers();
                var term = userlist.Where(x => x.Id == _TermId);
                foreach (var item in term)
                {
                    var b = item.RoleId;
                    var str = await ApiClientFactory.Instance.GetEnumByType(10);
                    var str3 = str.Where(x => x.Value == b);
                    foreach (var item2 in str3)
                    {
                        items = "<option value=\"" + item2.Value + "\">" + item2.Name + "</option>";
                        str2 += "<tr><td>" + SrNo + "</td><td class='col-md-6 col-sm-6'><input type='hidden' class='form-control m_no txtClientId' value=" + a.Id + " placeholder='Enter User name'/><input class='form-control m_no txtClientName' value='" + a.UserName + "' placeholder='Enter User name'/><td><td class='col-md-6 col-sm-6'><select id='DropDownListClient_" + SrNo + "' value="+b+" class='DropDownListClient form-control m_no'>"+ items + "</select><td>" +
                   "</tr>";
                    }
                    str4 = str2;
                }
            }
            return Json(str4);
        }
        public async Task<Message<ClientAdmin>> DeleteClientAdmin(int Id) => await ApiClientFactory.Instance.DeleteClientAdmin(Id);
        public async Task<JsonResult> getClientadmin(int OrgId = 0)
        {
            var SrNo = 0;
            var str1 = string.Empty;
            var str2 = string.Empty;
            var strr = string.Empty;
            var items = "";
            var res = await ApiClientFactory.Instance.getClientadmin();
            if (OrgId != 0)
            {
                try
                {
                    var filldata = await ApiClientFactory.Instance.getClientadminFillData(OrgId);
                    if (filldata.Count > 0)
                    {
                        int[] cat = new int[filldata.Count];
                        for (int i = 0; i < filldata.Count; i++)
                        {
                            cat[i] = filldata[i].ClientId;
                        }
                        strr = String.Join(",", cat);
                    }
                    else
                    {
                        strr = "";
                    }

                }
                catch (Exception ex)
                {
                    ex.HResult = 0;
                    //throw ex;
                }

            }
            else
            {
                strr = "";
            }
            if (OrgId > 0)
            {
                var res2 = res.Where(x => x.OrganizationId == OrgId);
                foreach (var item in res2)
                {
                    SrNo++;
                    var b = item.AssignRoleClientId;
                    var c = item.AssignRoleClient;
                    items = "<option value=\"" + b + "\">" + c + "</option>";
                    str1 += "<tr><td>" + SrNo + "</td><td>" + item.Organization + "</td><td>" + item.ClientName + "</td><td>" + item.AssignRoleClient + "</td><td><a class='delete_btn' onclick='Confirmation(" + item.Id + ",`" + item.ClientName + "`)'></a></td></tr>";
                    str2 += "<tr><td>" + SrNo + "</td><td class='col-md-6 col-sm-6'><input type='hidden' class='form-control m_no txtClientId' value=" + item.Id + " placeholder='Enter User name'/><input class='form-control m_no txtClientName' value=" + item.ClientName + " placeholder='Enter User name'/><td><td class='col-md-6 col-sm-6'><select id='DropDownListClient_" + SrNo + "' value=" + b + " class='DropDownListClient form-control m_no'>" + items + "</select><td>" +
                       "</tr>";
                }
            }
            else
            {
                foreach (var item in res)
                {
                    SrNo++;
                    str1 += "<tr><td>" + SrNo + "</td><td>" + item.Organization + "</td><td>" + item.ClientName + "</td><td>" + item.AssignRoleClient + "</td><td><a class='delete_btn' onclick='Confirmation(" + item.Id + ",`" + item.ClientName + "`)'></a></td></tr>";
                }
            }
            return Json(new { str1, str2, strr });
        }

        [HttpPost]
        public async Task<JsonResult> SaveOrganizationUser(UserLogin model)
        {
            _logger.LogInformation("SaveOrganizationUser");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveOrganizationUser(model);
            if (res.IsSuccess)
            {
                TempData["success"] = (model.Id > 0 ? "Updated successfully" : "Saved successfully");
            }
            else
            {
                TempData["error"] = "Error during save functions";
            }
            return Json(res);
        }

        [HttpPost]
        public async Task<JsonResult> SubmitClientAdmin(UserLogin model)
        {

            _logger.LogInformation("SubmitClientAdmin");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitClientAdmin(model);
            if (res.IsSuccess)
            {
                TempData["success"] = "Saved successfully";
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(res);
        }
        public async Task<IActionResult> getOrganizationUsers(int OrgID)
        {
            _logger.LogInformation("getOrganizationUsers");
            var res = await ApiClientFactory.Instance.getOrganizationUsers(OrgID);
            return Json(res);
        }
        public async Task<IActionResult> EditOrganizationUsers(int ID)
        {
            _logger.LogInformation("EditOrganizationUsers");
            var res = await ApiClientFactory.Instance.EditOrganizationUsers(ID);
            return Json(res);
        }

        //public async Task<IActionResult> GetOrganizationUsersPassword(int ID)
        //{
        //    _logger.LogInformation("GetOrganizationUsersPassword");
        //    var res2 = await ApiClientFactory.Instance.EditOrganizationUsers(ID);
        //    var res = await ApiClientFactory.Instance.OrganizationUsersPassword(ID);
        //    return Json(new { res, res2 });
        //}
        public async Task<IActionResult> DeleteOrganizationUsers(int ID)
        {
            _logger.LogInformation("DeleteOrganizationUsers");
            var res = await ApiClientFactory.Instance.DeleteOrganizationUsers(ID);
            return Json(res);
        }
        public async Task<IActionResult> DisableOrganizationUsers(int ID)
        {
            _logger.LogInformation("DisableOrganizationUsers");
            var res = await ApiClientFactory.Instance.DisableOrganizationUsers(ID);
            return Json(res);
        }
        public async Task<IActionResult> PasswordResetOrganizationUsers(int ID, string Password)
        {
            _logger.LogInformation("PasswordResetOrganizationUsers");
            int MOdifiedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.PasswordResetOrganizationUsers(ID, Password, MOdifiedBy);
            return Json(res);
        }
        public async Task<object> IsValidPassword(int userId, string password)
        {
            var msg = string.Empty;
            var numeric = new Regex(@"[0-9]+");
            var specialCharacter = new Regex(@"[!#$%&'()*+,-.:;<=>?@[\\\]{}^_`|~]");
            if (password.Length < 8) { msg = "Use 8 characters or more for your password!"; return new { IsValid = false, Msg = msg }; }
            if (password.Length > 16) { msg = "Use 16 characters or fewer for your password!"; return new { IsValid = false, Msg = msg }; }
            //if (password.Contains(" ")) { msg = "Do not use any white space in your password!"; return new { IsValid = false, Msg = msg }; }
            if (!password.Any(char.IsUpper)) { msg = "Use atleast one uppercase character for your password!"; return new { IsValid = false, Msg = msg }; }
            if (!password.Any(char.IsLower)) { msg = "Use atleast one lowercase character for your password!"; return new { IsValid = false, Msg = msg }; }
            if (!numeric.IsMatch(password)) { msg = "Use atleast one numeric value for your password!"; return new { IsValid = false, Msg = msg }; }
            if (!specialCharacter.IsMatch(password)) { msg = "Use atleast one special character for your password!"; return new { IsValid = false, Msg = msg }; }
            if (await IsOldPassword(userId, password)) { msg = "your current password should not match with new password!"; return new { IsValid = false, Msg = msg }; }
            else { return new { IsValid = true, Msg = msg }; }
        }
        public static async Task<bool> IsOldPassword(int userId, string password)
        {
            var res = await ApiClientFactory.Instance.OrganizationUsersPassword(userId);
            var isExists = res.Any(x => x.Password == password);
            return isExists;
        }

        public async Task<object> OrganizationUsersEmail(string Email)
        {
            var msg = string.Empty;
            if (await IsOldEmail(Email)) { msg = "your email should not match with existing email!"; return new { IsValid = false, Msg = msg }; }
            else { return new { IsValid = true, Msg = msg }; }
        }

        public static async Task<bool> IsOldEmail(string Email)
            {
            try
            {
                var res = await ApiClientFactory.Instance.OrganizationUsersEmail(Email);
                var isExists = res.Any(x => x.Email == Email);
                return isExists;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
