﻿using Cyberanium.Factory;
using Cyberanium.Models;
using Cyberanium.Utility;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;


namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;
        private readonly IOptions<MySettingsModel> appSettings;

        public LoginController(IOptions<MySettingsModel> app, ILogger<LoginController> logger)
        {
            appSettings = app;
            ApplicationSettings.WebApiUrl = appSettings.Value.WebApiBaseUrl;
            _logger = logger;
        }
        public IActionResult Index()
        {
            ViewBag.error = TempData["error"];
            return View();
        }
        [HttpGet]
        public IActionResult Login(string returnUrl = "")
        {
            var model = new UserLogin();
            ViewBag.error = TempData["error"];
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserLogin model)
        {
            if (ModelState.IsValid)
            {
                if (NetworkInterface.GetIsNetworkAvailable() && new Ping().Send(new IPAddress(new byte[] { 8, 8, 8, 8 }), 2000).Status == IPStatus.Success)
                {
                    _logger.LogInformation("User log In.");
                    var result = await ApiClientFactory.Instance.GetUserLogin(model);
                    if (result.IsSuccess)
                    {
                        HttpContext.Session.SetString("UserName", result?.Data?.UserName ?? String.Empty);
                        HttpContext.Session.SetInt32("UserId", Convert.ToInt32(result?.Data?.Id ?? 0));
                        HttpContext.Session.SetInt32("Role", result?.Data?.RoleId ?? 0);

                        var remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
                        string result1 = "";
                        if (remoteIpAddress != null)
                        {
                            // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
                            // This usually only happens when the browser is on the same machine as the server.
                            if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                            {
                                remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                        .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                            }
                            result1 = remoteIpAddress.ToString();
                        }
                        HttpContext.Session.SetString("IP", result1);
                        await GetRoleMenuByRole();
                        var RoleId = HttpContext.Session.GetInt32("Role");
                        if (RoleId == 1)
                        {
                            return RedirectToAction("TabMenu", "Organogram", new { area = "ApplicationManagement" });
                        }
                        else
                        {
                            return RedirectToAction("Index", "Landing", new { area = "Dashboard" });
                        }
                    }
                    else
                    {
                        TempData["error"] = "Your Enter the Wrong Credential, Please Check Your Credential";
                        return RedirectToAction("Login");
                    }
                }
                //is online
                else
                {
                    ViewBag.error = "Please check your internet connection.Try again later.";
                    return View(model);
                }
                //is offline

            }
            ModelState.AddModelError("", "Invalid login attempt");
            return View(model);
        }

        public IActionResult Logout()
        {
            _logger.LogInformation($"User Logout at {DateTime.Now}");
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "Login", new { area = "ApplicationManagement" });
        }
        public async Task<IActionResult> GetRoleMenuByRole()
        {
            Menu obj = new Menu();
            var RoleId = HttpContext.Session.GetInt32("Role");
            var list = await ApiClientFactory.Instance.GetRoleMenuById((int)RoleId);
            HttpContext.Session.SetObject("RoleList", list);
            return View("_Layout");
        }
        //   public bool checkInterNetConnection()
        //   {
        //       if (NetworkInterface.GetIsNetworkAvailable() &&
        //new Ping().Send(new IPAddress(new byte[] { 8, 8, 8, 8 }), 2000).Status == IPStatus.Success)
        //       {
        //           return true;
        //       }
        //       else
        //       {
        //           return false;
        //       }
        //   }
    }
}
