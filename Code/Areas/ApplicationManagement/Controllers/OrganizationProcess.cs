﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class OrganizationProcess : Controller
    {
        private readonly ILogger<OrganizationProcess> _logger;
        public OrganizationProcess(ILogger<OrganizationProcess> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(int ID = 0)
        {
            OrganizationProcesss obj = new OrganizationProcesss();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            if (ID > 0)
            {
                obj = await ApiClientFactory.Instance.GetFunctionProcessByID(ID);
                obj.DdlChildOrg = new SelectList(await ApiClientFactory.Instance.GetChildOrganizationByParentOrgID(obj.ParentOrgId), "Id", "ChildOrgName");
                if (obj.IsType == 1)
                {
                    obj.DdlFunction = new SelectList(await ApiClientFactory.Instance.GetFunctionByOrganization(obj.ParentOrgId), "Id", "FunctionName");
                    obj.DdlProcessOwner = new SelectList(await ApiClientFactory.Instance.GetOrganizationAudityListByID(obj.ParentOrgId), "Id", "UName");
                }
                else
                {
                    obj.DdlFunction = new SelectList(await ApiClientFactory.Instance.GetFunctionByChildOrganization(Convert.ToInt32(obj.ChildOrgId)), "Id", "FunctionName");
                    obj.DdlProcessOwner = new SelectList(await ApiClientFactory.Instance.GetChildOrganizationAudityListByID(Convert.ToInt32(obj.ChildOrgId)), "Id", "UName");
                }
            }
            obj.FunctionProcessList = await ApiClientFactory.Instance.GetProcesses(EmployeeId);
            obj.DdlParentOrg = new SelectList(await ApiClientFactory.Instance.GetOrganizationByEmplyoyeeID(EmployeeId), "Id", "ParentOrgName");
            obj.ConsultantList = await ApiClientFactory.Instance.GetAssignedConsultantsByProcessId(ID);
            ViewBag.message = TempData["success"];
            ViewBag.message = TempData["error"];
            return View(obj);
        }
        public async Task<JsonResult> GetAssignedConsultantsByFunctionMapId(long functionId = 0)
        {
            var consultantList = await ApiClientFactory.Instance.GetAssignedConsultantsByFunctionMapId(functionId);
            return Json(consultantList);
        }

        [HttpPost]
        public async Task<JsonResult> SubmitFunctionProcess(OrganizationProcesss model)
        {
            _logger.LogInformation("SubmitFunctionProcess");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            if (model.ChildOrgId == null)
            {
                model.ChildOrgId = 0;
            }
            var res = await ApiClientFactory.Instance.SubmitFunctionProcess(model);
            if (res.IsSuccess)
            {
                TempData["success"] = (model.Id > 0 ? "Updated successfully" : "Saved successfully");
            }
            else
            {
                TempData["error"] = "Error during save function process";
            }
            return Json(res);
        }

        [HttpPost]
        public async Task<IActionResult> BindOrganizationParentFunction(long ParentOrgId)
        {
            _logger.LogInformation("BindOrganizationParentFunction");
            var res = await ApiClientFactory.Instance.GetFunctionByOrganization(ParentOrgId);
            var res2 = await ApiClientFactory.Instance.GetOrganizationAudityListByID(ParentOrgId);
            return Json(new { res, res2 });
        }

        [HttpPost]
        public async Task<IActionResult> BindOrganizationChildFunction(long ChildOrgId)
        {
            _logger.LogInformation("BindOrganizationChildFunction");
            var res = await ApiClientFactory.Instance.GetFunctionByChildOrganization(ChildOrgId);
            var res2 = await ApiClientFactory.Instance.GetChildOrganizationAudityListByID(ChildOrgId);
            return Json(new { res, res2 });
        }
    }
}
