﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class FunctionDashboardController : Controller
    {
        private readonly ILogger<FunctionDashboardController> _logger;
        public FunctionDashboardController(ILogger<FunctionDashboardController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(long parentOrgId = 0)
        {
            var model = new FunctionDashboard();
            var Organograms = await ApiClientFactory.Instance.GetOrganogram();
            model.OrganizationList = Organograms.Where(x => x.EmployeId == Convert.ToInt16(HttpContext.Session.GetInt32("UserId"))).ToList();
            model.DdlChildOrg = new SelectList(await ApiClientFactory.Instance.GetChildOrganizationByParentOrgID(parentOrgId), "Id", "ChildOrgName");
            model.Id = parentOrgId;
            return View(model);
        }
        //public async Task<IActionResult> Index(int OrganizationId = 0)
        //{
        //    var model = new FunctionDashboard();
        //    var Organograms = await ApiClientFactory.Instance.GetOrganogram();
        //    if (OrganizationId > 0)
        //    {
        //        model.Id = OrganizationId;
        //    }
        //    //model.EnumTypeList = await ApiClientFactory.Instance.GetEnumByType(3);
        //    model.OrganizationsList = Organograms.Where(x => x.EmployeId == Convert.ToInt16(HttpContext.Session.GetInt32("UserId"))).ToList();
        //    model.DdlChildList = new SelectList(await ApiClientFactory.Instance.GetChildCompanyListOnClientWise(OrganizationId), "Id", "ChildOrgName");
        //    return View(model);
        //}
        public async Task<JsonResult> GetChildOrganizations(int parentOrgId)
        {
            var res = await ApiClientFactory.Instance.GetChildCompanyListOnClientWise(parentOrgId);
            return Json(res);
        }
        public async Task<JsonResult> GetFuncations(long childOrgId)
        {
            var res = await ApiClientFactory.Instance.GetFuncation(childOrgId);
            return Json(res);
        }
        public async Task<JsonResult> GetProcesses(long functionId)
        {
            var res = await ApiClientFactory.Instance.GetProcess(functionId);
            return Json(res);
        }
        public async Task<IActionResult> GetRiskDomainsByOrgId(RiskDomain model)
        {
            ProcessesAssign obj = new ProcessesAssign();
            string html = string.Empty;
            string thead = "<thead><tr><th style='text-align:center'>Parameters</th>";
            string tbody = "<tbody>";
            List<RiskDomain> parameters = new List<RiskDomain>
            {
                new RiskDomain { Id=1, RiskDomainName="Financial"},
                new RiskDomain { Id=2, RiskDomainName="Operational"},
                new RiskDomain { Id=3, RiskDomainName="Reputational"},
                new RiskDomain { Id=4, RiskDomainName="Regulatory"},
                new RiskDomain { Id=5, RiskDomainName="Domain Criticality Score"},
            };
            var res = await ApiClientFactory.Instance.GetRiskDomainsByOrgId(model);
            obj.ParentOrgId = model.ParentOrgId;
            obj.ChildOrgId = model.ChildOrgId;
            obj.FunctionId = model.FunctionId;
            obj.ProcessId = model.ProcessId;
            var res2 = await ApiClientFactory.Instance.GetDomainCritiScoreByfunIDAndProcessID(obj);
            foreach (var item in res)
            {
                thead += "<th style='text-align:center;width:200px' id="+ item.RiskDomainId +"> "+ item.RiskDomainName + "</th>";
            }
            thead += "</tr></thead>";
            if (res2.Count>0)
            {
                foreach (var vp in parameters)
                {
                    var res3 = res2.Where(m=>m.ParameterId== vp.Id);
                    tbody += "<tr><td class='parameter' id=" + vp.Id + ">" + vp.RiskDomainName + "</td>";
                    foreach (var v in res)
                    {
                        var res4 = res3.Where(m => m.RiskDomainId==v.RiskDomainId).FirstOrDefault();
                        tbody += "<td class=parameter" + vp.Id + "><input type='number' value="+ res4.Score + " class='form-control RD_" + v.RiskDomainId + "' min='1' max='5' onchange='calculateAverage(this)' /></td>";
                    }
                    tbody += "</tr>";
                }
            }
            else
            {
                foreach (var vp in parameters)
                {
                    tbody += "<tr><td class='parameter' id=" + vp.Id + ">" + vp.RiskDomainName + "</td>";
                    foreach (var v in res)
                    {
                        tbody += "<td class=parameter" + vp.Id + "><input type='number' class='form-control RD_" + v.RiskDomainId + "' min='1' max='5' onchange='calculateAverage(this)' /></td>";
                    }
                    tbody += "</tr>";
                }
            }
            
            tbody += "</tbody>";
            html += thead;
            html += tbody;
            return Json(new { html});
        }
        public async Task<bool> SubmitDomainCriticalityScore(RiskDomain model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitDomainCriticalityScore(model);
            return res.IsSuccess;
        }
        public async Task<IActionResult> Get(long ChildId)
        {
            var values = await ApiClientFactory.Instance.GetAllEnumTypes(ChildId);
            var functions = await ApiClientFactory.Instance.GetFuncation(ChildId);
            return Json(new { values, functions });
        }
        [HttpPost]
        public async Task<IActionResult> Getfunction(long ChildId)
        {
            var values = await ApiClientFactory.Instance.GetAllEnumTypes(ChildId);
            var function = await ApiClientFactory.Instance.GetFuncation(ChildId);
            return Json(new { values, function });
        }
        [HttpPost]
        public async Task<IActionResult> Index(OrganoGrams organoGrams)
        {
            //_logger.LogInformation("SaveProcessEnabler");
            organoGrams.CreatedIP = HttpContext.Session.GetString("IP");
            organoGrams.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var result = await ApiClientFactory.Instance.PostProcess(organoGrams);
            if (result.IsSuccess)
            {
                TempData["success"] = "Save successfully!!";
            }
            else
            {
                TempData["error"] = "Not save successfully!!";
            }
            return RedirectToAction("Index", "ProcessEnabler", new { area = "ApplicationManagement" });
        }
        [HttpPost]
        public async Task<IActionResult> GetCategoryDetails(long Id)
        {
            return Json(await ApiClientFactory.Instance.GetCategoryDetails(Id));
        }
    }
}
