﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;


namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class ChildOrganization : Controller
    {
        private readonly ILogger<ChildOrganization> _logger;
        public ChildOrganization(ILogger<ChildOrganization> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var obj = new OrganizationChild();
            obj.OrganizationList = await ApiClientFactory.Instance.GetOrganizationList();
            return View(obj);
        }

        public async Task<JsonResult> SubmitChildOrganization(OrganizationChild model)
        {
            _logger.LogInformation("SubmitChildOrganization");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitChildOrganization(model);
            if (res.IsSuccess)
            {
                TempData["success"] = (model.Id > 0 ? "Updated successfully" : "Saved successfully");
            }
            else
            {
                TempData["error"] = "Error during save child organization";
            }
            return Json(res);
        }

        public async Task<IActionResult> SaveCountry(string CountryName)
        {
            _logger.LogInformation("SaveCountry");
            string msg = string.Empty;
            Country model = new Country();
            model.CountryName = CountryName;
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveCountry(model);
            var country = await ApiClientFactory.Instance.GetAllCountry();
            if (res.IsSuccess)
            {
                msg = "Country save successfully!";
            }
            else
            {
                msg = "Error during save country";
            }
            return Json(new { msg, country });
        }

        public async Task<IActionResult> SaveState(int countryId, string stateName)
        {
            _logger.LogInformation("SaveState");
            string msg = string.Empty;
            State model = new State();
            model.CID = countryId;
            model.StateName = stateName;
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveState(model);
            var States = await ApiClientFactory.Instance.GetAllState(countryId);
            if (res.IsSuccess)
            {
                msg = "State save successfully!";
            }
            else
            {
                msg = "Error during save state";
            }
            return Json(new { msg, States });
        }

        public async Task<IActionResult> SaveCity(int countryId, int StateId, string CityName)
        {
            _logger.LogInformation("SaveCity");
            string msg = string.Empty;
            City model = new City();
            model.CID = countryId;
            model.SID = StateId;
            model.CityName = CityName;
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveCity(model);
            var Citys = await ApiClientFactory.Instance.GetAllCity(countryId, StateId);
            if (res.IsSuccess)
            {
                msg = "City save successfully!";
            }
            else
            {
                msg = "Error during save city";
            }
            return Json(new { msg, Citys });
        }

        [HttpPost]
        public async Task<IActionResult> BindStateById(int CountryId)
        {
            _logger.LogInformation("BindStateByID");
            var res = await ApiClientFactory.Instance.GetAllState(CountryId);
            return Json(res);
        }

        [HttpPost]
        public async Task<IActionResult> BindCityById(int CountryId, int StateId)
        {
            _logger.LogInformation("BindCityById");
            var res = await ApiClientFactory.Instance.GetAllCity(CountryId, StateId);
            return Json(res);
        }
        public async Task<JsonResult> updateOrgSignStaus(int ID)
        {
            _logger.LogInformation("updateOrgSignStaus");
            OrganizationChild model = new OrganizationChild();
            model.Id = ID;
            var res = await ApiClientFactory.Instance.updateOrgSignStaus(model);
            return Json(res);
        }
    }
}
