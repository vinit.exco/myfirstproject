﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]

    public class OrganizationFunction : Controller
    {
        private readonly ILogger<OrganizationFunction> _logger;
        public OrganizationFunction(ILogger<OrganizationFunction> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            OrganizationFunctions obj = new OrganizationFunctions();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            obj.DdlOrganization = new SelectList(await ApiClientFactory.Instance.GetAssignedOrganizationByEmplyoyeeID(EmployeeId), "Id", "OrgName");
            ViewBag.message = TempData["success"];
            ViewBag.message = TempData["error"];
            return View(obj);
        }
        //public async Task<JsonResult> GetAssignedConsultantsByOrgId(long parentOrgId = 0, long childOrgId = 0)
        //{
        //    var consultantList = await ApiClientFactory.Instance.GetAssignedConsultantsByOrgId(parentOrgId, childOrgId);
        //    return Json(consultantList);
        //}

        //[HttpPost]
        //public async Task<IActionResult> BindChildOrganization(int ParentOrgId)
        //{
        //    _logger.LogInformation("BindChildOrganization");
        //    var res = await ApiClientFactory.Instance.GetChildOrganizationByParentOrgID(ParentOrgId);
        //    return Json(res);
        //}
        public async Task<JsonResult> SaveOrganizationFunction(OrganizationFunctions model)
        {
            _logger.LogInformation("SaveOrganizationFunction");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveOrganizationFunction(model);
            if (res.IsSuccess)
            {
                TempData["success"] = (model.Id > 0 ? "Updated successfully" : "Saved successfully");
            }
            else
            {
                TempData["error"] = "Error during save functions";
            }
            return Json(res);
        }
        [HttpPost]
        public async Task<IActionResult> GetFunctionByOrganization(long OrgID)
        {
            _logger.LogInformation("GetFunctionByOrganization");
            string str = string.Empty;
            var res = await ApiClientFactory.Instance.GetFunctionByOrganization(OrgID);
            var res2 = await ApiClientFactory.Instance.GetProcessesByOrgId(OrgID);
            long Id = 0;
            if (res.Count > 0 || res2.Count > 0)
            {
                str = GetExistingString(res, res2);
                Id = 1;
            }
            else
            {
                str = GetString();
            }
            return Json(new { Id = Id, Str = str });
        }
        public string GetString()
        {
            string str = string.Empty;
            str = str + "<div class='row functions'>";
            str = str + "<div class='col-12'>";
            str = str + "<div class='row'>";
            str = str + "<div class='collapsebtn' onclick='toggleClass(this)'></div>";
            str = str + "<div class='col-3'>";
            str = str + "<label class='required'>Name</label>";
            str = str + "<div class='select_box m_bottom'>";
            str = str + "<input type='hidden' class='functionId' value='0' />";
            str = str + "<input type='text' class='form-control Functionname' required placeholder='Function Name' onchange='fn_CheckDuplicateFun(this)' />";
            str = str + "</div>";
            str = str + "</div>";
            str = str + "<div class='col-3'>";
            str = str + "<label>Owner</label>";
            str = str + "<div class='select_box m_bottom'>";
            str = str + "<input type='text' class='form-control FunctionOwner' />";
            str = str + "</div>";
            str = str + "</div>";
            str = str + "<div class='col-4'>";
            str = str + "<label>Description</label>";
            str = str + "<div class='select_box m_bottom'>";
            str = str + "<input type='text' class='form-control FunctionDescription' />";
            str = str + "</div>";
            str = str + "</div>";
            str = str + "<div class='col-1'>";
            str = str + "<label class='addbtn' onclick='fn_AddProcess(this)' title='Add Process' style='cursor: pointer'></label>";
            str = str + "<label class='remvbtn htn' onclick='fn_DeleteFunction(this)' title='Delete Function' style='cursor: pointer'></label>";
            str = str + "</div>";
            str = str + "</div>";
            str = str + "</div>";
            str = str + "</div>";
            return str;
        }
        public async Task<JsonResult> CheckFunProDuplicate(long orgId, string FunProName)
        {
            _logger.LogInformation("CheckFunProDuplicate");
            var res = await ApiClientFactory.Instance.CheckFunProDuplicate(orgId,FunProName);
            return Json(res);
        }
        public string GetExistingString(List<OrganizationFunctions> res, List<FunctionProcess> res2)
        {
            string str = string.Empty;
            foreach (var item in res)
            {
                str = str + "<div class='row functions'>";
                str = str + "<div class='col-12'>";
                str = str + "<div class='row'>";
                str = str + "<div class='collapsebtn' onclick='toggleClass(this)'></div>";
                str = str + "<div class='col-3'>";
                str = str + "<label class='required'>Name</label>";
                str = str + "<div class='select_box m_bottom'>";
                str = str + "<input type='hidden' class='functionId' value='" + item.Id + "' />";
                str = str + "<input type='text' class='form-control Functionname' value='" + item.FunctionName + "' required placeholder='Function Name' onchange='fn_CheckDuplicateFun(this)' />";
                str = str + "</div>";
                str = str + "</div>";
                str = str + "<div class='col-3'>";
                str = str + "<label>Owner</label>";
                str = str + "<div class='select_box m_bottom'>";
                str = str + "<input type='text' class='form-control FunctionOwner' value='" + item.FunctionOwner + "' />";
                str = str + "</div>";
                str = str + "</div>";
                str = str + "<div class='col-4'>";
                str = str + "<label>Description</label>";
                str = str + "<div class='select_box m_bottom'>";
                str = str + "<input type='text' class='form-control FunctionDescription' value='" + item.FunctionDescription + "' />";
                str = str + "</div>";
                str = str + "</div>";
                str = str + "<div class='col-1'>";
                str = str + "<label class='addbtn' onclick='fn_AddProcess(this)' title='Add Process' style='cursor: pointer'></label>";
                str = str + "<label class='remvbtn htn' onclick='fn_DeleteFunction(this)' title='Delete Function' style='cursor: pointer'></label>";
                str = str + "</div>";
                str = str + "</div>";
                foreach (var item2 in res2)
                {
                    if (item2.FunctionID == item.Id)
                    {
                        str = str + "<div class='col-12 divProcess'>";
                        str = str + "<div class='adddivProcess'>";
                        str = str + "<div class='row'>";
                        str = str + "<div class='col-1'>";
                        str = str + "</div>";
                        str = str + "<div class='col-3'>";
                        str = str + "<label class='required'>Name</label>";
                        str = str + "<div class='select_box m_bottom'>";
                        str = str + "<input type='hidden' class='processId' value='" + item2.Id + "' />";
                        str = str + "<input type='text' class='form-control m_no' name='ProcessName' value='" + item2.ProcessName + "' required placeholder='Process Name' onchange='fn_CheckDuplicateFun(this)'>";
                        str = str + "</div>";
                        str = str + "</div>";
                        str = str + "<div class='col-3'>";
                        str = str + "<label>Owner</label>";
                        str = str + "<div class='select_box m_bottom'>";
                        str = str + "<input type='text' class='form-control m_no' name='ProcessOwner' value='" + item2.ProcessOwner + "'>";
                        str = str + "</div>";
                        str = str + "</div>";
                        str = str + "<div class='col-4'>";
                        str = str + "<label>Description</label>";
                        str = str + "<input type='text' class='form-control m_no' name='ProcessDescription' value='" + item2.ProcessDescription + "'>";
                        str = str + "</div>";
                        str = str + "<div class='col-1'>";
                        str = str + "<label class='remvbtn' onclick='fn_DeleteProcess(this)' title='Delete Process' style='cursor: pointer'></label>";
                        str = str + "</div>";
                        str = str + "</div>";
                        str = str + "</div>";
                        str = str + "</div>";
                    }
                }
                str = str + "</div>";
                str = str + "</div>";
            }
            return str;
        }
    }
}
