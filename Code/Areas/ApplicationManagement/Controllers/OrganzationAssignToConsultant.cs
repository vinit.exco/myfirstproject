﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class OrganzationAssignToConsultant : Controller
    {
        private readonly ILogger<OrganzationAssignToConsultant> _logger;
        public OrganzationAssignToConsultant(ILogger<OrganzationAssignToConsultant> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            Organization obj = new Organization();
            var ddlRole = await ApiClientFactory.Instance.GetUsers();   
            obj.DdlRole = new SelectList(ddlRole.Where(x => x.RoleId == 4).Select(s => new { s.Id, s.UserName }).ToList(), "Id", "UserName");
            obj.OrganizationList = await ApiClientFactory.Instance.AssignedOrgList(); ;
            return View(obj);
        }
        public async Task<JsonResult> SaveAssignedOrganizationUser(Organization model)
        {
            _logger.LogInformation("SaveAssignedOrganizationUser");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveAssignedOrganizationUser(model);
            return Json(res);
        }
        public async Task<JsonResult> GetOrganizationNotAssigned()
        {
            _logger.LogInformation("GetOrganizationNotAssigned");
            var res = await ApiClientFactory.Instance.GetOrganizationNotAssigned();
            return Json(res);
        }
        [HttpPost]
        public async Task<JsonResult> UpdateAssigntoConsultant(Organization model)
        {
            _logger.LogInformation("UpdateAssigntoConsultant");
            var res = await ApiClientFactory.Instance.UpdateAssigntoConsultant(model);
            return Json(res);
        }
        [HttpPost]
        public async Task<JsonResult> EditAssignedOrg(long OrgID)
        {
            _logger.LogInformation("EditAssignedOrg");
            var res = await ApiClientFactory.Instance.EditAssignedOrg(OrgID);
            var res2 = await ApiClientFactory.Instance.GetOrganizationByEmplyoyeeID(0);
            return Json(new { res, res2 });
        }
    }
}
