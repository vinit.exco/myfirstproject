﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(int Id=0)
        {
            UserLogin userLogin = new UserLogin();
            var UserList = await ApiClientFactory.Instance.GetUsers();
            if (Id>0)
            {
                userLogin = UserList.Where(m=>m.Id== Id).FirstOrDefault();
            }
            userLogin.Userlist=await ApiClientFactory.Instance.GetUsers();
            userLogin.DdlRole= new SelectList(await ApiClientFactory.Instance.GetEnumByType(10), "Value", "Name");
            return View(userLogin);
        }
        public IActionResult TabMenu()
        {
            
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> SaveupdateUser(UserLogin userLogin)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("SaveupdateUser");
                userLogin.CreatedIP = HttpContext.Session.GetString("IP");
                userLogin.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var result = await ApiClientFactory.Instance.SaveupdateUser(userLogin);
                if (result.IsSuccess)
                {
                    TempData["success"] = userLogin.Id == 0 ? "Saved successfully." : "Updated successfully.";
                    return RedirectToAction("Index", "User", new { area = "ApplicationManagement" });
                }
            }
            TempData["error"] = "Something went wrong!";
            ModelState.AddModelError("", "Invalid login attempt");
            return View(userLogin);
        }
  
        //public static async Task<bool> IsOldUserName(string UserName)
        //{
        //    try
        //    {
        //        var res = await ApiClientFactory.Instance.SaveUsername(UserName);
        //        var isExists = res.Any(x => x.UserName == UserName);
        //        return isExists;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        public async Task<IActionResult> DeleteActiveUser(int id, int Type)
        {
            _logger.LogInformation("BlockDeleteMenu");
            var res = await ApiClientFactory.Instance.DeleteActiveUser(id,Type);
            if (Type > 0)
            {
                return RedirectToAction(actionName: "Index", controllerName: "User");
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "User");
            }
            //return RedirectToAction("Index", "Menu", new { area = "ApplicationManagement" });
        }
        public async Task<JsonResult> CheckDuplicateuser(string UserName)
        {
            var res = await ApiClientFactory.Instance.SaveUsername(UserName);
            return Json(res);
        }
    }
}
