﻿using Cyberanium.Factory;
using CyberaniumModels;
using CyberaniumModels.Comman;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class IRAController : Controller
    {
        private readonly ILogger<IRAController> _logger;
        public IRAController(ILogger<IRAController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var model = new IRA();
            model.OrganizationList = await ApiClientFactory.Instance.GetAssessmentOrganizationList(1);
            return View(model);
        }
        public IActionResult TabMenu(long orgId = 0, string orgName = "")
        {
            var model = new IRA();
            model.OrgId = orgId;
            model.OrgName = orgName;
            model.RoleId = Convert.ToInt16(HttpContext.Session.GetInt32("Role"));
            return View(model);
        }
        public async Task<IActionResult> AssScope(long orgId = 0, string orgName = "")
        {
            var model = new IRA();
            var res = await ApiClientFactory.Instance.getAssessmentScope(orgId);
            if (res.Count > 0)
            {
                model.ID = res[0].ID;
                model.StartDate = res[0].StartDate;
                model.EndDate = res[0].EndDate;
                model.DomainIds = res[0].DomainIds;
            }
            model.OrgId = orgId;
            model.ListDomains = await ApiClientFactory.Instance.GetEnumByType(3);
            model.OrgName = orgName;
            return View(model);
        }
        #region Assessment Process
        public async Task<IActionResult> AssProcess(long orgId = 0, string orgName = "")
        {
            var model = new IRA();
            model.OrgId = orgId;
            model.OrgName = orgName;
            model.RoleId = Convert.ToInt16(HttpContext.Session.GetInt32("Role"));
            model.FunctionList = await ApiClientFactory.Instance.GetFunctionByOrganization(orgId);
            model.ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(orgId);
            model.UserList = await ApiClientFactory.Instance.getClientadminFillData(orgId);
            return View(model);  
        }
        public async Task<JsonResult> GetProcessesByFunctionId(long orgId = 0, long functionId = 0)
        {
            var res = await ApiClientFactory.Instance.GetProcessesByOrgId(orgId);
            var data = res.Where(x => x.FunctionID == functionId).ToList();
            return Json(data);
        }

        [HttpPost]
        public async Task<JsonResult> SaveAssignedProcess(IRA model)
        {
            _logger.LogInformation("SaveAssignedProcess");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveAssignedProcess(model);
            return Json(res);
        }

        [HttpPost]
        public async Task<JsonResult> SaveAssessmentScope(IRA model)
        {
            _logger.LogInformation("SaveAssessmentScope");
            model.EndDate = Convert.ToDateTime(model._EndDate);
            model.StartDate = Convert.ToDateTime(model._StartDate);
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveAssessmentScope(model);
            return Json(res);
        }
        public async Task<JsonResult> GetAssignedUserByProcessesId(long processId = 0) => Json(await ApiClientFactory.Instance.GetAssignedUserByProcessesId(processId));

        public async Task<IActionResult> GetDomainCriticalityScoreByProcessesId(long orgId, long functionId, long processId)
        {
            string html = string.Empty;
            string thead = "<thead><tr><th style='text-align:center;width:120px;'>Parameters</th>";
            string tbody = "<tbody>";
            var parameterList = new List<Parameter>
            {
                new Parameter { Id=1, Name="Financial"},
                new Parameter { Id=2, Name="Operational"},
                new Parameter { Id=3, Name="Reputational"},
                new Parameter { Id=4, Name="Regulatory"},
                new Parameter { Id=5, Name="Domain Criticality Score"},
            };
            var riskDomainList = await ApiClientFactory.Instance.GetEnumsByEnumTypeId(3);
            var res = await ApiClientFactory.Instance.GetDomainCriticalityScoreByProcessesId(orgId, functionId, processId);
            if (res.Count > 0)
            {
                var riskDomains = res.Where(x => x.ParameterId == 1).Select(x => x.RiskDomainId).ToList();
                foreach (var item in riskDomains)
                {
                    var _item = riskDomainList.Where(x => x.Value == item).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                    thead += "<th style='text-align:center;width:200px' id=" + _item.Value + "> " + _item.Name + "</th>";
                }
                thead += "</tr></thead>";
                foreach (var parameter in parameterList)
                {
                    var readOnly = parameter.Id == 5 ? "readonly" : "";
                    tbody += "<tr><td class='parameter' id=" + parameter.Id + ">" + parameter.Name + "</td>";
                    var dcList = res.Where(m => m.ParameterId == parameter.Id).ToList();
                    foreach (var item in dcList)
                    {
                        var _item = riskDomainList.Where(x => x.Value == item.RiskDomainId).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                        var dc = dcList.Where(x => x.RiskDomainId == _item.Value).FirstOrDefault();
                        tbody += "<td class=parameter" + parameter.Id + "><input type='number' value=" + dc.Score + " class='form-control RD_" + _item.Value + "' min='1' max='5' onchange='CalculateAverage(this)'" + readOnly + " /></td>";
                    }
                    tbody += "</tr>";
                }
            }
            else
            {
                var scopeList = await ApiClientFactory.Instance.getAssessmentScope(orgId);
                var riskDomains = string.IsNullOrEmpty(scopeList[0].DomainIds) ? default : scopeList[0].DomainIds.Split(",");
                foreach (var rd in Array.ConvertAll(riskDomains, x => Convert.ToInt32(x)))
                {
                    var _rd = riskDomainList.Where(x => x.Value == rd).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                    thead += "<th style='text-align:center;width:200px' id=" + _rd.Value + "> " + _rd.Name + "</th>";
                }
                thead += "</tr></thead>";
                foreach (var parameter in parameterList)
                {
                    var readOnly = parameter.Id == 5 ? "readonly" : "";
                    tbody += "<tr><td class='parameter' id=" + parameter.Id + ">" + parameter.Name + "</td>";
                    foreach (var rd in Array.ConvertAll(riskDomains, x => Convert.ToInt32(x)))
                    {
                        var _rd = riskDomainList.Where(x => x.Value == rd).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                        tbody += "<td class=parameter" + parameter.Id + "><input type='number' class='form-control RD_" + _rd.Value + "' min='1' max='5' onchange='CalculateAverage(this)'" + readOnly + " /></td>";
                    }
                    tbody += "</tr>";
                }
            }

            tbody += "</tbody>";
            html += thead;
            html += tbody;
            return Json(new { html = html, isEdit = res.Count > 0 ? true : false });
        }

        public async Task<JsonResult> SubmitDomainCriticalityScore(DomainCriticalityScore model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitDomainCriticalityScore(model);
            return Json(res);
        }
        public async Task<JsonResult> SubmitProcessEnablerMap(string strJson)
        {
            var model = new Base();
            model.Mode = strJson;
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitProcessEnablerMap(model);
            return Json(res);
        }
        public async Task<JsonResult> GetQuestionnaireIdByProcessId(long orgId = 0, long functionId = 0, long processId = 0, int conCatId = 0)
        {
            var res = await ApiClientFactory.Instance.GetQuestionnaireIdByProcessId(orgId, functionId, processId, conCatId);
            return Json(res);
        }
        public async Task<JsonResult> GetFirstQuesAnswersByConCatId(int conCatId = 0, long orgId = 0)
        {
            var res = await ApiClientFactory.Instance.GetFirstQuesAnswersByConCatId(conCatId, orgId);
            return Json(res);
        }
        public async Task<JsonResult> UpadateProcess(FunctionProcess model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.UpadateProcess(model);
            return Json(res);
        }
        #endregion

        #region Enabler Assessment
        public async Task<IActionResult> AssEnabler(int orgId = 0, string orgName = "", bool isTask = false)
        {
            var model = new IRA();
            var res = await ApiClientFactory.Instance.getAssessmentScope(orgId);
            model.OrgId = orgId;
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            model.RoleId = Convert.ToInt16(HttpContext.Session.GetInt32("Role"));
            model.AssignedEnablerCategoryList = await ApiClientFactory.Instance.GetAssignedEnablerCategory(EmployeeId, orgId);
            model.AssignedEnablerCategoryTypeList = await ApiClientFactory.Instance.GetAssignedEnalbersCategoryType(EmployeeId, orgId);
            model.ddlAssessor = new SelectList(await ApiClientFactory.Instance.getClientadminFillData(orgId), "ClientId", "ClientName");
            model.CategoryList = await ApiClientFactory.Instance.GetEnablerCategoryByOrgID(orgId);
            model.OrgName = orgName;
            model.IsTask = isTask;
            model.StartDate = res?.FirstOrDefault()?.StartDate ?? default;
            model.EndDate = res?.FirstOrDefault()?.EndDate ?? default;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> GetTypeOfCategory(long OrgId, int CategoryID)
        {
            _logger.LogInformation("GetTypeOfCategory");
            string str = string.Empty;

            var res = await ApiClientFactory.Instance.GetFirstQuesAnswersByConCatId(CategoryID, OrgId);
            var res2 = await ApiClientFactory.Instance.GetAssignedEnablerByOrgAndCategory(OrgId, CategoryID);
            foreach (var item in res)
            {
                string html = "";
                string html2 = "";
                if (res2.Exists(m => m.QuestionnaireID == item.Value))
                {
                    int index = res2.FindIndex(m => m.QuestionnaireID == item.Value);
                    html = "<span class='icon' title='Assigned to " + res2[index].UserName + "'><img src='/Image/CreateOrganogram/AccountMgtClient.png' height=42 /></span>";
                    html2 = "<input checked type='checkbox' id=chk_" + item.Value + " class='customcheckbtn'/>";
                }
                else
                {
                    html = "<span class='icon'><img src='/Image/CreateOrganogram/Trangle_Warning.png' /></span>";
                    html2 = "<input type='checkbox' id=chk_" + item.Value + " class='customcheckbtn'/>";
                }
                str = str + "<div class='row'><div class='col-12 p-2'>" + html2 + "<label for=chk_" + item.Value + ">" + item.Answer +
                    "</label>" + html +
                    "<div class='addbtn' style='float:right;width:auto'><div class='add_litigation'><input type='button' onclick='fn_openPopup(" + item.CategoryId + "," + item.QuestionId + "," + item.Value + ")' value='Assign / Re Assign'></div></div></div></div>";
            }
            return Json(str);
        }

        [HttpPost]
        public async Task<JsonResult> SaveEnablerAssessment(IRA model)
        {
            _logger.LogInformation("SaveEnablerAssessment");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            model.CompletedDate = Convert.ToDateTime(model.strCompletedDate);
            var res = await ApiClientFactory.Instance.SaveEnablerAssessment(model);
            return Json(res);
        }

        [HttpPost]
        public async Task<IActionResult> GetAssignedEnalbersCategory(long QuestionnaireID)
        {
            _logger.LogInformation("GetAssignedEnalbersCategory");
            var res = await ApiClientFactory.Instance.GetAssignedEnalbersDetails(QuestionnaireID);
            string str;
            if (res.Count > 0)
            {
                str = string.Concat(res[0].CompletedDate.Day, '/', res[0].CompletedDate.Month, '/', res[0].CompletedDate.Year);
                res[0].strCompletedDate = str;
            }
            return Json(res);
        }

        public async Task<JsonResult> GetControlCategoryListByOrgId(long orgId) => Json(await ApiClientFactory.Instance.GetEnablerCategoryByOrgID(orgId));
        #endregion



    }
}
