﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Cyberanium.Factory;
using CyberaniumModels;
using System;
using System.Linq;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class ScopeAssessmentController : Controller
    {
        private readonly ILogger<ScopeAssessmentController> _logger;
        public ScopeAssessmentController(ILogger<ScopeAssessmentController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(int OrganizationId = 0)
        {
            var obj = new ScopeAssessments();
            var Organograms = await ApiClientFactory.Instance.GetOrganogram();
            obj.OrganizationsList = Organograms.Where(x => x.EmployeId == Convert.ToInt16(HttpContext.Session.GetInt32("UserId"))).ToList();
            obj.DdlSector = new SelectList(await ApiClientFactory.Instance.GetEnumByType(1), "Value", "Name");
            obj.DdlCountry = new SelectList(await ApiClientFactory.Instance.GetAllCountry(), "Id", "CountryName");
            obj.DdlRiskDomain = new SelectList(await ApiClientFactory.Instance.GetEnumByType(3), "Value", "Name");
            obj.DdlInternational = new SelectList(await ApiClientFactory.Instance.GetAllInternationalStandard(), "Id", "Title");
            obj.DDlOrganizationChild = new SelectList(await ApiClientFactory.Instance.GetChildCompanyListOnClientWise(OrganizationId), "Id", "ChildOrgName");
            obj.ParentOrgId = OrganizationId;
            return View(obj);
        }

        [HttpPost]
        public async Task<IActionResult> SubmitScopeAssessment(ScopeAssessments model)
        {
            _logger.LogInformation("SubmitAssesment");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitScopeAssessment(model);
            if (res.IsSuccess)
            {
                TempData["success"] = model.EventType == 0 ? "Saved successfully." : "Updated successfully.";
                return RedirectToAction("Index", "ScopeAssessment");
            }
            TempData["error"] = "Something went wrong!";
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> SaveAssesment(Assesment model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitAssesment(model);
            return Json(res);
        }
        public async Task<JsonResult> BlockDeleteAssesment(Assesment model)
        {
            _logger.LogInformation("BlockDeleteAssesment");
            var res = await ApiClientFactory.Instance.BlockDeleteAssesment(model);
            if (res.IsSuccess)
            {
                return Json(new { code = 200 });
            }
            return Json(new { code = 500 });
        }
    }
}