﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class SFDCController : Controller
    {
        private readonly ILogger<SFDCController> _logger;
        public SFDCController(ILogger<SFDCController> logger)
        {
            _logger = logger;
        }
        public static long ParentOrgId;
        public static long ChildOrgId;
        public static long CategoryId;
        public static long AnswerId;
        public async Task<IActionResult> Index(DomainBaseRiskScore model)
        {
            var obj = new ICD();
            ParentOrgId = model.ParentOrgId;
            ChildOrgId = model.ChildOrgId;
            CategoryId = model.CategoryId;
            AnswerId = model.AnswerId;
            var res = await ApiClientFactory.Instance.GetThreatsByClientId(model);
            obj.DomainThreatMapList = await ApiClientFactory.Instance.GetDomainThreatMapList("");
            obj.ThreatList = res.Data;
            obj.ParentOrgId = model.ParentOrgId;
            obj.ChildOrgId = model.ChildOrgId;
            obj.CategoryId = model.CategoryId;
            obj.AnswerId = model.AnswerId;
            return View(obj);
        }
        public async Task<JsonResult> GetProbabilityScore(DomainBaseRiskScore model)
        {
            model.ParentOrgId = ParentOrgId;
            model.ChildOrgId = ChildOrgId;
            model.CategoryId = CategoryId;
            model.AnswerId = AnswerId;
            var probability = await ApiClientFactory.Instance.GetProbabilityScore(model);
            return Json(probability);
        }
        public async Task<Message<int>> SubmitDataByAnswer(DomainBaseRiskScore model)
        {
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitDataByAnswer(model);
            return res;
        }
    }
}
