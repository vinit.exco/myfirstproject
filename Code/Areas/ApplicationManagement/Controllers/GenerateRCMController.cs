﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]

    public class GenerateRCMController : Controller
    {
        private readonly ILogger<GenerateRCMController> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public GenerateRCMController(ILogger<GenerateRCMController> logger, IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
        }
        public async Task<IActionResult> Index()
        {
            RiskAssessmentRCM obj = new RiskAssessmentRCM();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            obj.DdlOrganization = new SelectList(await ApiClientFactory.Instance.GetAssignedOrganizationByEmplyoyeeID(EmployeeId), "Id", "OrgName");
            obj.DdlSector = new SelectList(await ApiClientFactory.Instance.GetEnumByType(1), "Value", "Name");
            obj.DdlCountry = new SelectList(await ApiClientFactory.Instance.GetAllCountry(), "Id", "CountryName");
            return View(obj);
        }
        [HttpPost]
        public async Task<IActionResult> BindQuestionStandardAndRegulation(string QuestionsID, string SectorId, string CountryId, bool IsAadhar, bool PCIDss, bool Hipaa,bool PersonalInfo)
        {
            _logger.LogInformation("BindQuestionStandardAndRegulation");
            string str = string.Empty;
            string str2 = string.Empty;
            var res = await ApiClientFactory.Instance.GetAllInternationalStandard();
            var res2 = await ApiClientFactory.Instance.GetAllRegulationbySectorAndCountryWise(SectorId, CountryId);
            foreach (var item in res)
            {
                if ((item.TopicId == 5 && PCIDss == true) || (item.TopicId == 6 && Hipaa == true) || item.TopicId == 3 && PersonalInfo==true)
                {
                    str2 = str2 + "<tr><td><input type='checkbox' checked value=" + item.Id + "></td><td>" + item.Service + "</td><td>" + item.Title + "</td></tr>";
                }
                else
                {
                    str2 = str2 + "<tr><td><input type='checkbox' value=" + item.Id + "></td><td>" + item.Service + "</td><td>" + item.Title + "</td></tr>";
                }
            }
            if (!IsAadhar)
            {
                res2.RemoveAll(m => m.Requirement_ID.StartsWith("Aadhaar"));
            }
            foreach (var item in res2)
            {
                if (item.Requirement_ID.StartsWith("Aadhaar") || (item.Section == 2 && PersonalInfo == true))
                {
                    str = str + "<tr><td><input type='checkbox' checked value=" + item.RegoId + "></td><td>" + item.CountryName + "</td><td>" + item.SectorName + "</td><td>" + item.RegulationName + "</td><td title='" + item.Text + "'>" + item.Requirement_ID + "</td></tr>";
                }
                else
                {
                    str = str + "<tr><td><input type='checkbox' value=" + item.RegoId + "></td><td>" + item.CountryName + "</td><td>" + item.SectorName + "</td><td>" + item.RegulationName + "</td><td title='" + item.Text + "'>" + item.Requirement_ID + "</td></tr>";
                }
            }
            return Json(new { str2, str });
        }
        [HttpPost]
        public async Task<IActionResult> BindQuestionBanklist(string CountryId)
        {
            _logger.LogInformation("BindQuestionBanklist");
            string str = string.Empty;
            var res = await ApiClientFactory.Instance.GetQuestionBanklist();
            if (CountryId.Contains('3'))
            {
                foreach (var item in res)
                {
                    if (item.ID == 2)
                    {
                        str = str + "<tr><td><div class='tabsection'>" + item.Question + "</div></td>";
                        str = str + "<td><label class='radio-inline' style='width:80px; padding-left: 50px'><input type='radio' data-toggle='modal' data-target='#CloudModal' name='chkYes_" + item.ID + "' id='chkYes' value=" + item.ID + ">Yes</label>";
                        str = str + "<label class='radio-inline' style='width: 80px; padding-left:30px'><input type='radio' name='chkYes_" + item.ID + "' id='chkNo' value=" + item.ID + ">No</label></td></tr>";
                    }
                    else
                    {
                        str = str + "<tr><td><div class='tabsection'>" + item.Question + "</div></td>";
                        str = str + "<td><label class='radio-inline' style='width:80px; padding-left: 50px'><input type='radio' name='chkYes_" + item.ID + "' id='chkYes' value=" + item.ID + ">Yes</label>";
                        str = str + "<label class='radio-inline' style='width: 80px; padding-left:30px'><input type='radio' name='chkYes_" + item.ID + "' id='chkNo' value=" + item.ID + ">No</label></td></tr>";
                    }
                }
            }
            else
            {
                foreach (var item in res)
                {
                    if (item.ID == 3)
                    {
                        str = str + "<tr hidden><td><div class='tabsection'>" + item.Question + "</div></td>";
                        str = str + "<td><label class='radio-inline' style='width:80px; padding-left: 50px'><input type='radio' name='chkYes_" + item.ID + "' id='chkYes' value=" + item.ID + ">Yes</label>";
                        str = str + "<label class='radio-inline' style='width: 80px; padding-left:30px'><input type='radio' name='chkYes_" + item.ID + "' id='chkNo' value=" + item.ID + ">No</label></td></tr>";
                    }
                    else
                    {
                        str = str + "<tr><td><div class='tabsection'>" + item.Question + "</div></td>";
                        str = str + "<td><label class='radio-inline' style='width:80px; padding-left: 50px'><input type='radio' name='chkYes_" + item.ID + "' id='chkYes' value=" + item.ID + ">Yes</label>";
                        str = str + "<label class='radio-inline' style='width: 80px; padding-left:30px'><input type='radio' name='chkYes_" + item.ID + "' id='chkNo' value=" + item.ID + ">No</label></td></tr>";
                    }
                }
            }
            return Json(str);
        }

        [HttpPost]
        public async Task<IActionResult> BindComplianceScore(RiskAssessmentRCM model)
        {
            _logger.LogInformation("BindComplianceScore");
            string html2 = string.Empty;
            var res = await ApiClientFactory.Instance.GetControlsByVersion(model.StandardIds, model.RegulationIds);
            var res2 = await ApiClientFactory.Instance.GetEnumByType(12);
            string html3 = string.Empty;
            long SubDomainId = 0;
            if (!model.IsThirdParty)
            {
                res.RemoveAll(m => m.SubDomainId.Equals(4));
            }
            if (!model.IsCloud)
            {
                res.RemoveAll(m => m.SubDomainId.Equals(17));
            }
            else
            {
                if (!model.IsIaas)
                {
                    res.RemoveAll(m => m.IaaS.Contains("0"));
                }
                if (!model.IsPaaS)
                {
                    res.RemoveAll(m => m.PaaS.Contains("0"));
                }
                if (!model.IsSaaS)
                {
                    res.RemoveAll(m => m.SaaS.Contains("0"));
                }
            }
            if (!model.PersonalInfo)
            {
                res.RemoveAll(m => m.SubDomainId.Equals(12));
            }

            //string [] str;
            //str = StandardIds.Split(',');
            //foreach (var item in str)
            //{
            //    res.Exists(m => m.StandardIds.Contains(StandardIds.Split(',')));
            //}
            foreach (var item in res)
            {
                long SubDomainID = item.SubDomainId;
                long cc = res.Where(m => m.SubDomainId == SubDomainID).Count();
                //if (SubDomainId != item.SubDomainId)
                //{
                //    html3 = "<tr style='border-top:solid #00a3a1' id='trid_" + item.SubDomainId + "'><td hidden>" + item.SubDomainId + "</td><td hidden>" + item.Id + "</td><td rowspan=" + cc + " width='10%'>" + item.SubDomainName + "</td>";
                //}
                //else
                //{
                //    html3 = "<tr><td hidden>" + item.SubDomainId + "</td><td hidden>" + item.Id + "</td><td hidden rowspan=" + cc + " width='10%'>" + item.SubDomainName + "</td>";
                //}
                html3 = "<tr><td><input type='hidden' value=" + item.DomainId + ">" + item.DomainName + "</td>";
                html3 = html3 + "<td><input type='hidden' value=" + item.SubDomainId + ">" + item.SubDomainName + "</td>";
                html3 = html3 + "<td><input type='hidden' value=" + item.Id + ">" + item.ControlName + "</td>";
                html3 = html3 + "<td>" + item.ControlDescription + "</td>";
                html3 = html3 + "<td>" + item.ControlId + "</td>";
                html3 = html3 + "<td><input type='hidden' value=" + item.ControlTypeIDNew + ">" + item.ControlTypeName + "</td>";
                html3 = html3 + "<td>" + (item.EnablerCategoryIds.Contains("1") ? "Yes" : "No") + "</td>";
                html3 = html3 + "<td>" + (item.EnablerCategoryIds.Contains("10") ? "Yes" : "No") + "</td>";
                html3 = html3 + "<td>" + (item.EnablerCategoryIds.Contains("11") ? "Yes" : "No") + "</td>";
                html3 = html3 + "<td>" + (item.EnablerCategoryIds.Contains("14") ? "Yes" : "No") + "</td>";
                html3 = html3 + "<td>" + (item.EnablerCategoryIds.Contains("15") ? "Yes" : "No") + "</td>";
                html3 = html3 + "<td>" + (item.EnablerCategoryIds.Contains("6") ? "Yes" : "No") + "</td>";
                html3 = html3 + "<td>" + (item.EnablerCategoryIds.Contains("7") ? "Yes" : "No") + "</td>";
                html2 = html2 + html3;
                html2 = html2 + "</tr>";
                SubDomainId = item.SubDomainId;
            }
            return Json(new { res, html2 });
        }

        [HttpPost]
        public async Task<JsonResult> SubmitRCM(RiskAssessmentRCM model)
        {
            _logger.LogInformation("SubmitRCM");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitRCM(model);
            if (res.IsSuccess)
            {
                TempData["success"] = model.ID == 0 ? "Saved successfully." : "Updated successfully.";
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(res);
        }

        [HttpPost]
        [SupportedOSPlatform("windows")]
        public JsonResult GetJsonFromExcelFile()
        {
            string jsonData;
            string webRootPath = _webHostEnvironment.WebRootPath;
            string contentRootPath = _webHostEnvironment.ContentRootPath;
            string pathToUpload = Path.Combine(webRootPath, "Uploads");
            if (!Directory.Exists(pathToUpload)) Directory.CreateDirectory(pathToUpload);
            if (Request.Form.Files.Count > 0)
            {
                var files = Request.Form.Files;
                var file = files[0];
                string fileExtension = Path.GetExtension(file.FileName);
                string fileName = Path.GetFileNameWithoutExtension(file.FileName);
                string fullFileName = string.Concat(fileName, DateTime.Now.ToFileTime(), fileExtension);
                string fullPath = Path.Combine(pathToUpload, fullFileName);
                var fileStream = System.IO.File.Create(fullPath);
                file.CopyTo(fileStream); fileStream.Flush(); fileStream.Close();
                //"HDR=Yes;" indicates that the first row contains column names, not data.
                var connectionString = $@"Provider=Microsoft.ACE.OLEDB.12.0; Data Source={fullPath}; Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1;""";
                var conn = new OleDbConnection(connectionString);
                try
                {
                    conn.Open();
                    var cmd = conn.CreateCommand();
                    //cmd.CommandText = $@"SELECT * FROM [{fileName}$]";
                    cmd.CommandText = $@"SELECT * FROM [Sheet1$]";
                    var dr = cmd.ExecuteReader();
                    try
                    {
                        var query = (from DbDataRecord row in dr select row).Select(x =>
                        {
                            //var isNumeric = decimal.TryParse(x[3].ToString(), out _);
                            //var value = isNumeric ? x[3] : 0.00;
                            var data = new Dictionary<string, object>{
                                        {"DomainName",x[0]},  //Use dr.GetName(0) for access excel column name.
                                        {"SubDomainName",x[1]},
                                        {"ControlName",x[2]},
                                        {"ControlDescription",x[3]},
                                        {"ControlID",x[4]},
                                        {"ControlTypeName",x[5]},
                                        {"Governance",x[6].ToString()=="Yes"?true:false},
                                        {"Application",x[7].ToString()=="Yes"?true:false},
                                        {"Infrastructure",x[8].ToString()=="Yes"?true:false},
                                        {"Cloud",x[9].ToString()=="Yes"?true:false},
                                        {"Networks",x[10].ToString()=="Yes"?true:false},
                                        {"People",x[11].ToString()=="Yes"?true:false},
                                        {"Location",x[12].ToString()=="Yes"?true:false}};
                            return data;
                        });
                        jsonData = JsonConvert.SerializeObject(query);
                    }
                    catch (Exception ex)
                    {
                        return Json(new { Code = 422, Message = ex.Message, Data = new object() });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { Code = 505, Message = ex.Message, Data = new object() });
                }
                finally
                {
                    conn.Close(); conn.Dispose();
                    var fileInfo = new FileInfo(fullPath);
                    fileInfo.Delete();
                }
                var jsonList = new List<RCMJson>();
                var data = JsonConvert.DeserializeObject<List<RCMJson>>(jsonData);
                jsonList.AddRange(data.Where(x => x.DomainName != "Select" && x.DomainName != "NIST Domain"));
                var sn = 0;
                foreach (var item in jsonList)
                {
                    var controlId = string.Concat(new string(item.DomainName.Take(3).ToArray()).ToUpper(), "-", (++sn).ToString());
                    item.ControlID = string.IsNullOrEmpty(item.ControlID) ? controlId : item.ControlID;
                }
                return Json(new { Code = 200, Message = "File uploaded successfully.", Data = jsonList });
            }
            return Json(new { Status = 404, Message = "File not found!", Data = new object() });
        }

        [HttpPost]
        public async Task<IActionResult> SubmitRCMByExcel(long orgId = 0, string str = "")
        {
            _logger.LogInformation("SaveRCMByExcel");
            var model = new RCM();
            model.RCMXML = str;
            model.OrgID = orgId;
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitRCMByExcel(model);
            return Json(res);
        }
    }
}
