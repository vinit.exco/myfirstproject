﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class ClientEnablerController : Controller
    {
        private readonly ILogger<ClientEnablerController> _logger;
        public ClientEnablerController(ILogger<ClientEnablerController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index2()
        {
            var obj = new ClientEnablers();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            obj.OrganizationList = await ApiClientFactory.Instance.GetAssignedOrganizationByEmplyoyeeID(EmployeeId);
            obj.EnumList = await ApiClientFactory.Instance.GetOrganizationMainOrder();
            obj.EnumList2 = await ApiClientFactory.Instance.GetOrganizationOrder();
            return View(obj);
        }

        public async Task<IActionResult> EditOrganizaton(int Id)
        {
            var obj = new ClientEnablers();
            string res = string.Empty;
            var res1 = await ApiClientFactory.Instance.GetOrganizationMainOrder();
            var res2 = await ApiClientFactory.Instance.GetOrganizationOrder();
            var res3 = await ApiClientFactory.Instance.GetOrganizationId(Id);
            res = Getdata(res1, res2, res3);
            return Json(res);
        }

        private string GetExistingData(List<CyberaniumModels.Enum> res1, List<CyberaniumModels.Enum> res2, List<ClientEnablers> res3)
        {
            throw new NotImplementedException();
        }

        public string Getdata(List<CyberaniumModels.Enum> res1, List<CyberaniumModels.Enum> res2, List<ClientEnablers> res3)
        {
            string str = string.Empty;
            string str1 = string.Empty;
            string str2 = string.Empty;
            string str3 = string.Empty;
            string str4 = string.Empty;
            str1 += "<ul>";
            str2 += "<ul>";
            str3 += "<li><ul>";
            if (res3.Count > 0)
            {
                foreach (var item2 in res2)
                {
                    if (item2.Name != "People" && item2.Name != "Governance")
                    {
                        bool t = res3.Exists(m => m.CategoryId == item2.Value);
                        if (t)
                        {
                            str3 += "<li><span class='li'><input type='checkbox' checked='checked' name='name' value=" + item2.Value + " />" + item2.Name + "<span class='clickIcon' value=" + item2.Value + " onclick='fn_Organization(" + item2.Value + ", this,`" + item2.Name + "`)'></span></span></li>";
                        }
                        else
                        {
                            str3 += "<li><span class='li'><input type='checkbox'  name='name' value=" + item2.Value + " />" + item2.Name + "<span class='clickIcon' value=" + item2.Value + " onclick='fn_Organization(" + item2.Value + ", this,`" + item2.Name + "`)'></span></span></li>";
                        }
                    }
                    else
                    {
                        if (item2.Flag == 1)
                        {
                            bool t = res3.Exists(m => m.CategoryId == item2.Value);
                            if (t)
                            {
                                str3 += "<li><span class='li'><input type='hidden'   value=" + item2.Flag + " /><input type='checkbox' checked='checked'  class='GovernanceClass' name='name' value=" + item2.Value + " />" + item2.Name + "</span></li>";
                            }
                            else
                            {
                                str3 += "<li><span class='li'><input type='hidden'  value=" + item2.Flag + " /><input type='checkbox'  class='GovernanceClass'  name='name' value=" + item2.Value + " />" + item2.Name + "</span></li>";
                            }
                        }
                        else
                        {
                            bool t = res3.Exists(m => m.CategoryId == item2.Value);
                            if (t)
                            {
                                str3 += "<li><span class='li'><input type='hidden'  value=" + item2.Flag + " /><input type='checkbox' checked='checked'  class='GovernanceClass' name='name' value=" + item2.Value + " />" + item2.Name + "<span class='clickIcon' value=" + item2.Value + " onclick='fn_Organization(" + item2.Value + ", this,`" + item2.Name + "`)'></span></span></li>";
                            }
                            else
                            {
                                str3 += "<li><span class='li'><input type='hidden' value=" + item2.Flag + " /><input type='checkbox' checked='checked' class='GovernanceClass'  name='name' value=" + item2.Value + " />" + item2.Name + "<span class='clickIcon' value=" + item2.Value + " onclick='fn_Organization(" + item2.Value + ", this,`" + item2.Name + "`)'></span></span></li>";
                            }
                        }

                    }
                }
                str2 += "<li><span class='li'><input type='checkbox' checked='checked' name='name' value=16 />General</span>";
                foreach (var item in res1)
                {
                    bool t = res3.Exists(m => m.CategoryId == item.Value);
                    if (t)
                    {
                        str1 += "<li><span class='li'><input type='checkbox' checked='checked' name='name' value=" + item.Value + " />" + item.Name + "<span class='clickIcon' value=" + item.Value + " onclick='fn_Organization(" + item.Value + ", this,`" + item.Name + "`)'></span></span></li>";
                    }
                    else
                    {
                        str1 += "<li><span class='li'><input type='checkbox'  name='name' value=" + item.Value + " />" + item.Name + "<span class='clickIcon' value=" + item.Value + " onclick='fn_Organization(" + item.Value + ", this,`" + item.Name + "`)'></span></span></li>";
                    }
                }
                str1 += "</ul>";
                str3 += "</ul></li></li></ul>";
                str = str2 + str3 + str1;
            }
            else
            {
                foreach (var item2 in res2)
                {
                    if (item2.Name != "People" && item2.Name != "Governance")
                    {
                        str3 += "<li><span class='li'><input type='checkbox' checked='checked'  name='name' value=" + item2.Value + " />" + item2.Name + "<span class='clickIcon' value=" + item2.Value + " onclick='fn_Organization(" + item2.Value + ", this,`" + item2.Name + "`)'></span></span></li>";
                    }
                    else
                    {
                        if (item2.Flag == 1)
                        {
                            str3 += "<li><span class='li'><input type='hidden'   value=" + item2.Flag + " /><input type='checkbox' checked='checked' class='GovernanceClass'  name='name' value=" + item2.Value + " />" + item2.Name + "</span></li>";
                        }
                        else
                        {
                            str3 += "<li><span class='li'><input type='hidden'  value=" + item2.Flag + " /><input type='checkbox' checked='checked' class='GovernanceClass'  name='name' value=" + item2.Value + " />" + item2.Name + "<span class='clickIcon' value=" + item2.Value + " onclick='fn_Organization(" + item2.Value + ", this,`" + item2.Name + "`)'></span></span></li>";
                        }
                    }
                }
                str2 += "<li><span class='li'><input type='checkbox' checked='checked' name='name' value=16 />General</span>";
                foreach (var item in res1)
                {
                    str1 += "<li><span class='li'><input type='checkbox' name='name' value=" + item.Value + " />" + item.Name + "<span class='clickIcon' value=" + item.Value + " onclick='fn_Organization(" + item.Value + ", this,`" + item.Name + "`)'></span></span></li>";

                }
                str1 += "</ul>";
                str3 += "</ul></li></li></ul>";
                str = str2 + str3 + str1;
            }
            return str;
        }

        public async Task<IActionResult> Index(bool isParent = true, long parentOrgId = 0, long childOrgId = 0)
        {
            var obj = new ClientEnablers();
            int empID = (int)HttpContext.Session.GetInt32("UserId");
            if (isParent)
            {
                var parentOrgList = await ApiClientFactory.Instance.GetParentOrgListByEmployeeId((int)HttpContext.Session.GetInt32("UserId"));
                if (parentOrgId > 0)
                {
                    obj.OrganizationsList = parentOrgList.Where(x => x.Id == parentOrgId).ToList();
                    obj.ParentOrgId = parentOrgId;
                    obj.ChildOrgId = 0;
                }
                else
                {
                    obj.OrganizationsList = parentOrgList;
                }
            }
            else
            {
                var childOrgList = await ApiClientFactory.Instance.GetChildOrgListByEmployeeId((int)HttpContext.Session.GetInt32("UserId"));
                if (childOrgId > 0)
                {
                    obj.OrganizationsList = childOrgList.Where(x => x.OrganizationId == parentOrgId && x.Id == childOrgId).ToList();
                    obj.ParentOrgId = parentOrgId;
                    obj.ChildOrgId = childOrgId;
                }
                else
                {
                    obj.OrganizationsList = childOrgList;
                }
            }
            obj.DdlOrganizations = new SelectList(await ApiClientFactory.Instance.GetChildCompanyListOnClientWise((int)parentOrgId), "Id", "ChildOrgName");
            obj.DdlControlCat = new SelectList(await ApiClientFactory.Instance.GetEnumByType(2), "Value", "Name");
            ViewBag.message = TempData["success"];
            ViewBag.message = TempData["error"];
            return View(obj);
        }

        [HttpPost]
        public async Task<JsonResult> SubmitClientEnabler(ClientEnablers model)
        {

            _logger.LogInformation("SubmitAssesment");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitEnablerAssesments(model);
            if (res.IsSuccess)
            {
                TempData["success"] = "Saved successfully";
            }
            else
            {
                TempData["error"] = "Something went wrong!";
            }
            return Json(res);
        }


        public async Task<IActionResult> GetAddPartiipate(int id, int OrganizationId)
        {
            _logger.LogInformation("GetAddPartiipate");
            string str = string.Empty;
            ClientEnablers model = new ClientEnablers();
            var res = await ApiClientFactory.Instance.GetAddparticipant(id);
            var res2 = await ApiClientFactory.Instance.GetQuestionnaire(id, OrganizationId);
            if (res2.Count > 0)
            {
                str = GetExistingString(res, res2);
            }
            else
            {
                str = GetString(res);
            }
            return Json(str);
        }

        public string GetString(List<Compliance> res)
        {
            string str = string.Empty;
            string str1 = string.Empty;
            string str2 = string.Empty;
            str1 += "<thead><tr><th>SrNo.</th>";
            str2 += "<tbody><tr><td style='color:black'>1</td>";
            foreach (var item in res.OrderBy(x => x.OrderNo).ToList())
            {
                if (item.ControlCategory != 11)
                {
                    str1 += "<th><input type='hidden'  class='form-control hidden' value=" + item.Id + " />" + item.Query + "</th>";
                    str2 += "<td><input type='text' required name='username' class='form-control questionId" + item.Id + "' /><input type='hidden' class='form-control QuestionnaireId' value=0 /></td>";
                }
                else
                {
                    str1 += "<th id='txtInfra'><input type='hidden' class='form-control hidden' value=" + item.Id + " />" + item.Query + "</th>";
                    str2 += "<td><input type='text' required name='username' class='form-control questionId" + item.Id + "' /></td>";
                }
            }
            str1 += "<th>Action</th></tr></thead>";
            str2 += "<td><span class='clickIcon' onclick='AddMoreAnswers(this)'></span'></td></tr></tbody>";
            str = str1 + str2;
            return str;
        }
        public string GetExistingString(List<Compliance> res, List<ClientEnablers> res2)
        {
            ClientEnablers model = new ClientEnablers();
            string str = string.Empty;
            string str1 = string.Empty;
            string str2 = string.Empty;
            int Count = 1;
            str1 += "<thead><tr><th>SrNo.</th>";
            str2 += "<tbody>";
            var srNo = 0;
            var Exitdata = res2.GroupBy(x => x.RowNo);
            foreach (var item in res.OrderBy(x => x.OrderNo).ToList())
            {
                if (item.ControlCategory != 11)
                {
                    str1 += "<th><input type='hidden' class='form-control hidden' value=" + item.Id + " />" + item.Query + "</th>";
                }
                else
                {
                    str1 += "<th id='txtInfra'><input type='hidden' class='form-control hidden' value=" + item.Id + " />" + item.Query + "</th>";
                }
            }
            foreach (var item2 in Exitdata)
            {
                srNo++;
                str2 += "<tr><td style='color:black'>" + srNo + "</td>";
                foreach (var item in res.OrderBy(x => x.OrderNo).ToList())
                {
                    var _item = item2.Where(x => x.QuestionId == item.Id).FirstOrDefault();
                    if (_item.CategoryId != 11)
                    {
                        str2 += "<td><input type='text' required name='username' class='form-control questionId" + _item.QuestionId + "' value='" + _item.Answer + "'><input type='hidden' class='form-control QuestionnaireId' value=" + _item.Id + " /></td>";
                    }
                    else
                    {
                        str2 += "<td><input type='text' required name='username' class='form-control questionId" + _item.QuestionId + "' value='" + _item.Answer + "'><input type='hidden' class='form-control QuestionnaireId' value=" + _item.Id + " /></td>";
                    }
                }
                //foreach (var item3 in item2)
                //{
                //    str2 += "<td><input type='text' required name='username' class='form-control questionId" + item3.QuestionId + "' value='" + item3.Answer + "'><input type='hidden' class='form-control QuestionnaireId' value=" + item3.Id + " /></td>";
                //}
                if (Count == 1)
                {
                    str2 += "<td><span class='clickIcon' onclick='AddMoreAnswers(this)'></span></td></tr>";
                }
                else
                {
                    str2 += "<td><span class='clickIcon remove' value='-'></span></td></tr>";
                }
                Count += 1;
            }
            str1 += "<th>Action</th></tr></thead>";
            str2 += "</tbody>";
            str = str1 + str2;
            return str;
        }
    }

}
