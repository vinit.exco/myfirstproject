﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class MenuController : Controller
    {
        private readonly ILogger<MenuController> _logger;
        public MenuController(ILogger<MenuController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var obj = new Menu();
            var menues = await ApiClientFactory.Instance.GetMenu();
            obj.Menues = menues;
            ViewBag.message = TempData["save"];
            return View(obj);
        }

        [HttpPost]
        public async Task<IActionResult> SubmitMenu(Menu model)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("SubmitMenu");
                model.Action = "Index";
                model.CreatedIP = HttpContext.Session.GetString("IP");
                model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var res = await ApiClientFactory.Instance.SubmitMenu(model);
                if (res.IsSuccess)
                {
                    TempData["success"] = model.EventType == 0 ? "Saved successfully." : "Updated successfully.";
                    return RedirectToAction("Index", "Menu");
                }
            }
            TempData["error"] = "Something went wrong!";
            ModelState.AddModelError("", "Invalid attempt");
            return View(model);
        }
        public async Task<IActionResult> BlockDeleteMenu(int menuId, int Type, string ActionName)
        {
            _logger.LogInformation("BlockDeleteMenu");
            var model = new Menu();
            model.MenuId = menuId;
            model.EventType = Type;
            var res = await ApiClientFactory.Instance.BlockDeleteMenu(model);
            if (Type > 0)
            {
                return Json(res);
            }
            else
            {
                return Json(true);
            }
            //return RedirectToAction("Index", "Menu", new { area = "ApplicationManagement" });
        }
        public async Task<JsonResult> GetChildMenuByParentId(long parentId)
        {
            var obj = new Menu();
            var menues = await ApiClientFactory.Instance.GetMenu();
            obj.Menues = menues;
            return Json(obj.Menues.Where(x => x.ParentMenuId == parentId && x.ChildMenuId > 0 && x.SubMenuId == 0));
        }
    }
}
