﻿using ClosedXML.Excel;
using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class ComplainceCustomRCMController : Controller
    {
        private readonly ILogger<ComplainceCustomRCMController> _logger;
        public ComplainceCustomRCMController(ILogger<ComplainceCustomRCMController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            var model = new IRA();
            model.OrganizationList = await ApiClientFactory.Instance.GetAssessmentOrganizationList(4);
            return View(model);
            
        }
        public IActionResult TabMenu(long orgId = 0, string orgName = "")
        {
            var model = new IRA();
            model.OrgId = orgId;
            model.OrgName = orgName;
            return View(model);
        }
        public async Task<IActionResult> AssignTask(long orgId = 0, string orgName = "")
        {
            var model = new IRA();
            model.OrgId = orgId;
            var res = await ApiClientFactory.Instance.GetCustomRCM(orgId);
            if (res.Count > 0)
            {
                model.ID = res[0].ID;
                model.StartDate = res[0].StartDate;
                model.EndDate = res[0].EndDate;
                model.AssessorID = res[0].AssessorID;
                model.CompletedDate = res[0].CompletedDate;
            }
            model.ddlAssessor = new SelectList(await ApiClientFactory.Instance.getClientadminFillData(orgId), "ClientId", "ClientName");
            model.RCMListByOrgID = await ApiClientFactory.Instance.GetRCMListByOrgID(orgId);
            model.OrgName = orgName;
            return View(model);
        }
        public async Task<IActionResult> PerformAssessment(long orgId = 0, string orgName = "")
        {
            var model = new IRA();

            string str = string.Empty;
            string thead = string.Empty;
            string tbody = string.Empty;
            var res = await ApiClientFactory.Instance.GetCustomRCM(orgId);
            model.SubDomainList = await ApiClientFactory.Instance.GetSubDomainCustomRCM(res[0].RCMId);
            var res2 = await ApiClientFactory.Instance.GetControlLisCustomRCM(res[0].RCMId);
            var res3 = await ApiClientFactory.Instance.GetEnumByType(12);
            thead = "<thead class='txtCenter'><tr><th>Control Name</th><th>Control Description</th><th width='15%'>Test of design</th><th width='15%'>Test of effectiveness</th><th width='15%'>OverAll Effectiveness</th><th width='15%'>Remarks</th></tr></thead>";

            foreach (var item in model.SubDomainList)
            {
                str = str + "<div class='panel panel-default'><div class='panel-heading' role='tab' id='heading_" + item.SubDomainId + "'><div class='clsadjust'><div><h4 class='panel-title'>" +
                    "<a class='collapsed' role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse" + item.SubDomainId + "' aria-expanded='false' aria-controls='collapse" + item.SubDomainId + "'>" +
                    "" + item.SubDomainName + "</a></h4></div><div><label style='margin-right:10px'>Domain Compliance Score</label><input type='text' id=DomainComScore" + item.SubDomainId + " disabled='disabled' value='" + item.DomainCompilancescore + "' style='text-align: center;'/></div></div></div> <div id='collapse" + item.SubDomainId + "' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading" + item.SubDomainId + "'>" +
                    "<div class='panel-body'><div class='row'><div class='col-12'><div class='table_control'><table class='table' id=tbl_" + item.SubDomainId + ">" + thead + "<tbody>";
                foreach (var item2 in res2)
                {
                    if (item.SubDomainId == item2.SubDomainId)
                    {
                        string html = "<option value='0'>--Select--</option>";
                        string html2 = "<option value='0'>--Select--</option>";

                        foreach (var T in res3)
                        {
                            if (item2.Testofdesign == 1 && T.Value == 1)
                            {
                                html = html + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else if (item2.Testofdesign == 2 && T.Value == 2)
                            {
                                html = html + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else if (item2.Testofdesign == 3 && T.Value == 3)
                            {
                                html = html + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else
                            {
                                html = html + "<option value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            if (item2.Testofeffectiveness == 1 && T.Value == 1)
                            {
                                html2 = html2 + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else if (item2.Testofeffectiveness == 2 && T.Value == 2)
                            {
                                html2 = html2 + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else if (item2.Testofeffectiveness == 3 && T.Value == 3)
                            {
                                html2 = html2 + "<option selected value=" + T.Value + ">" + T.Name + "</option>";
                            }
                            else
                            {
                                html2 = html2 + "<option value=" + T.Value + ">" + T.Name + "</option>";
                            }
                        }
                        str = str + "<tr class='txtCenter'><td id='ControlName" + item2.ControlName + "'><input type='hidden' value=" + item2.ICDId + " />" + item2.ControlName + "</td><td id='ControlDescription" + item2.ControlDescription + "'><input type='hidden' value=" + item2.SubDomainId + " />" + item2.ControlDescription + "</td>" +
                            "<td id='TestOfDesign" + item2.SubDomainId + "'><select class='form-control m_no ClsTestDesign' onchange='fn_bindOverallEffectiveness(this);Calc(this," + item.SubDomainId + ");'>" + html + "</select></td><td id='TestOfEff" + item2.SubDomainId + "'><select class='form-control m_no ClsTestDesign' disabled='disabled' onchange='fn_bindOverallEffectiveness2(this);Calc(this," + item.SubDomainId + ");'>" + html2 + "</select></td><td id='OverAllEff" + item2.SubDomainId + "' ><input type='text' value='" + item2.overalleffectiveness + "' class='form-control' disabled='disabled'/></td><td id='Remarks" + item2.SubDomainId + "'><input type='text' value='" + item2.remarks + "' class='form-control'/></td></tr>";
                    }
                }
                str = str + "</tbody></table></div></div></div></div></div></div>";
            }

            model.HtmlCode = str;
            model.OrgId = orgId;
            model.OrgName = orgName;
            return View(model);
        }
        [HttpPost]
        public async Task<JsonResult> SaveCustomRCM(IRA model)
        {
            _logger.LogInformation("SaveCustomRCM");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SaveCustomRCM(model);
            return Json(res);
        }
        [HttpPost]
        public async Task<JsonResult> SubmitCustomRCM(IRA model)
        {
            _logger.LogInformation("SubmitCustomRCM");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitCustomRCM(model);
            return Json(res);
        }
        public async Task<IActionResult> DownloadPerformAssessmentReportToExcel(long orgId = 0)
        {
            try
            {
                var res = await ApiClientFactory.Instance.GetCustomRCM(orgId);
                var listIRA = await ApiClientFactory.Instance.GetSubDomainCustomRCM(res[0].RCMId);
                var controlList = await ApiClientFactory.Instance.GetControlLisCustomRCM(res[0].RCMId);
                var enumList = await ApiClientFactory.Instance.GetEnumByType(12);
                var wb = new XLWorkbook();
                var ws = wb.AddWorksheet("Perform Assessment Report");
                ws.Cell("A1").Value = "Sub-Domain";
                ws.Cell("B1").Value = "Control Name";
                ws.Cell("C1").Value = "Control Description";
                ws.Cell("D1").Value = "Test of design";
                ws.Cell("E1").Value = "Test of effectiveness";
                ws.Cell("F1").Value = "OverAll Effectiveness";
                ws.Cell("G1").Value = "Remarks";
                ws.Cell("H1").Value = "Domain Compliance Score";
                ws.Columns("1", "8").Width = 30;
                ws.Cells("A1:H1").Style.Font.FontSize = 14;
                ws.Cells("A1:H1").Style.Fill.BackgroundColor = XLColor.LightBlue;
                int sn = 1, i = 1;
                foreach (var item in listIRA)
                {
                    ++sn;
                    ws.Cell("A" + sn).Value = item.SubDomainName;
                    ws.Cell("H" + sn).Value = item.DomainCompilancescore;
                    int count = controlList.Where(x => x.SubDomainId == item.SubDomainId).Count();
                    var range1 = ws.Range("A" + sn + ":A" + (sn + count - 1));
                    range1.Merge();
                    range1.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    var range2 = ws.Range("H" + sn + ":H" + (sn + count - 1));
                    range2.Merge();
                    range2.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    foreach (var _item in controlList.Where(x => x.SubDomainId == item.SubDomainId))
                    {
                        i = sn;
                        var testOfDesign = enumList.Where(x => x.Value == _item.Testofdesign).Select(x => x.Name).FirstOrDefault();
                        var testOfEffectiveness = enumList.Where(x => x.Value == _item.Testofeffectiveness).Select(x => x.Name).FirstOrDefault();
                        ws.Cell("B" + sn).Value = _item.ControlName;
                        ws.Cell("C" + sn).Value = _item.ControlDescription;
                        //ws.Cell("C" + sn).Style.Alignment.WrapText = true;
                        ws.Cell("D" + sn).Value = testOfDesign;
                        ws.Cell("E" + sn).Value = testOfEffectiveness;
                        ws.Cell("F" + sn).Value = _item.overalleffectiveness;
                        ws.Cell("G" + sn).Value = _item.remarks;
                        sn++;
                    }
                    sn--;
                }
                using MemoryStream stream = new MemoryStream();
                wb.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "PerformAssessmentReport.xlsx");
            }
            catch (Exception ex)
            {
                ex.HResult = 0; throw;
            }
        }

    }
}
