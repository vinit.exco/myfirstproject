﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class AssignAssessmentController : Controller
    {
        public async Task<IActionResult> Index()
        {
            var obj = new OrganizationChild();
            obj.OrganizationList = await ApiClientFactory.Instance.GetOrganizationList();
            obj.OrgAssignedAssessmentList = await ApiClientFactory.Instance.AssessmentListByOrgId(0);
            return View(obj);
        }
        public async Task<IActionResult> AssessmentList(long orgId, string orgName)
        {
            var obj = new OrgAssignedAssessment();
            obj.OrgId = orgId;
            obj.OrgName = orgName;
            obj.EnumList = await ApiClientFactory.Instance.GetEnumByType(11);
            obj.OrgAssignedAssessmentList = await ApiClientFactory.Instance.AssessmentListByOrgId(orgId);
            return View(obj);
        }

        [HttpPost]
        public async Task<JsonResult> SubmitAssignedAssessment(OrgAssignedAssessment model)
        {
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitAssignedAssessment(model);
            return Json(res);
        }
    }
}
