﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Linq;
using System;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Cyberanium.Areas.ApplicationManagement.Controllers
{
    [Area("ApplicationManagement")]
    public class ThreatEnablerController : Controller
    {
        private readonly ILogger<ThreatEnablerController> _logger;
        public ThreatEnablerController(ILogger<ThreatEnablerController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index(string ParentOrgName, int Id = 0)
        {
            var model = new ThreatEnabler();
            var Organograms = await ApiClientFactory.Instance.GetOrganogram();
            int UserId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            model.ThreatEnablerList= await ApiClientFactory.Instance.GetThreatEnablerByParentIDAndChildId(Id,0);
            model.ThreatEnablerMapList = await ApiClientFactory.Instance.GetThreatEnablerMapByParentIDAndChildId(Id, 0);
            model.OrganizationsList = Organograms.Where(x => x.ClientAdminIds.Contains(UserId.ToString())).ToList();
            model.ParentOrgDDL = new SelectList(await ApiClientFactory.Instance.GetChildCompanyListOnClientWise(Id), "Id", "ChildOrgName");
            if (Id > 0)
            {
                model.Id = Id;
                model.Name = ParentOrgName;
                foreach (var item in model.ThreatEnablerList)
                {
                    string thead = "<th>Threats</th>";
                    var res = await ApiClientFactory.Instance.GetFirstQuesAnswersByConCatId(item.ConCatId, Id);
                    foreach (var tt in res)
                    {
                        thead += "<th>" + tt.Answer + "</th>";
                    }
                    model.FormHtml = model.FormHtml + "<div class='card'><div class='card-header' id=ConCat_" + item.ConCatId + ">"+
                        "<h2 class='mb-0'>"+
                        "<button class='btn btn-link' type='button' data-toggle='collapse' data-target=#collapse_"+ item.ConCatId + " aria-expanded='true' aria-controls=collapse_" + item.ConCatId + ">" + item.CategoryName + "</button>"+
                        "</h2></div>"+
                        "<div id=collapse_" + item.ConCatId + " class='undefined collapse show' aria-labelledby=ConCat_" + item.ConCatId + " data-parent='#accordionExample' style=''>"+
                        "<div class='card-body'>"+
                        "<table class='table table-striped tblControlCategory' id=conCatTable_"+ item.ConCatId + ">"+
                        "<thead class='bg-primary'><tr>" + thead + "</tr></thead>"+
                        "<tbody></tbody>"+
                        "</table>"+
                        "</div></div></div>";
                }
            }
            ViewBag.message = TempData["success"];
            ViewBag.error = TempData["error"];
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ThreadList()
        {
            var res = await ApiClientFactory.Instance.GetAllThreats();
            return Json(res);
        }
        [HttpPost]
        public async Task<IActionResult> GetChildOrgControlCategory(int ChilOrgID, int OrgID)
        {
            List<ThreatEnabler> parts = new List<ThreatEnabler>();
            CyberaniumModels.Enum obj = new CyberaniumModels.Enum();
            obj.Enums = await ApiClientFactory.Instance.GetAllEnumChildOrgCategory(OrgID, ChilOrgID);
            foreach (var item in obj.Enums)
            {
                var res2 = await ApiClientFactory.Instance.GetTypeOfCategory(OrgID, ChilOrgID, item.Value);
                string str = string.Empty;
                foreach (var item2 in res2)
                {
                    str = str + "<th id=" + item2.Id + ">" + item2.Answer + "</th>";
                }
                parts.Add(new ThreatEnabler() { Title = str, Value = Convert.ToInt32(item.Value) });
            }
            obj.ArrList = parts;
            return Json(obj);
        }
        public async Task<JsonResult> GetControlCategoryByOrgId(long parentOrgId, long childOrgId = 0)
        {
            var res = await ApiClientFactory.Instance.GetControlCategoryByOrgId(parentOrgId, childOrgId);
            return Json(res);
        }
        public async Task<IActionResult> GetFirstQuesAnswersByConCatId(int controlCategoryId , long ChildOrgId ,long OrganizationId)
        {
            _logger.LogInformation("GetFirstQuesAnswersByConCatId");
            var result = await ApiClientFactory.Instance.GetFirstQuesAnswersByConCatId(controlCategoryId, OrganizationId);
            return Json(result);
        }

        [HttpPost]
        public async Task<IActionResult> SubmitThreatsEnabler(ThreatEnabler model)
        {
            if (ModelState.IsValid)
            {
                _logger.LogInformation("SubmitAssesment");
                model.CreatedIP = HttpContext.Session.GetString("IP");
                model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
                var res = await ApiClientFactory.Instance.SubmitThreatsEnabler(model);
                if (res.IsSuccess)
                {
                    TempData["success"] = model.EventType == 0 ? "Saved successfully." : "Updated successfully.";
                }
                else
                {
                    TempData["error"] = "Something went wrong!";
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult index(ThreatEnabler enabler)
        {
            _logger.LogInformation("postThreatenabler");
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> SubmitThreatEnablerScore(ThreatEnablers model)
        {
            _logger.LogInformation("SubmitThreatEnablerScore");
            model.CreatedIP = HttpContext.Session.GetString("IP");
            model.CreatedBy = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.SubmitThreatEnablerScore(model);
            return Json(res);
        }
    }
}
