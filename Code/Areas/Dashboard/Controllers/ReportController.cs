﻿using Cyberanium.Factory;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.Dashboard.Controllers
{
    [Area("Dashboard")]
    public class ReportController : Controller
    {
        private readonly ILogger<ReportController> _logger;

        public ReportController(ILogger<ReportController> logger)
        {
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetMisReports()
        {
            _logger.LogInformation("GetMisReports");
            RBAC model = new RBAC();
            model.UserID = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.GetMISReportId(model);
            var res2 = await ApiClientFactory.Instance.GetProcesssCount(model);
            return Json(new { res, res2 });
        }

        [HttpPost]
        public async Task<IActionResult> GetBaseRiskScore()
        {
            _logger.LogInformation("GetBaseRiskScore");
            RBAC model = new RBAC();
            model.UserID = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.GetMISReportId(model);
            var res2 = await ApiClientFactory.Instance.GetProcesssCount(model);
            return Json(new { res, res2 });
        }
    }
}
