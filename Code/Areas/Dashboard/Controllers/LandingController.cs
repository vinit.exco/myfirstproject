﻿using Cyberanium.Factory;
using Cyberanium.Models;
using Cyberanium.Utility;
using CyberaniumModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberanium.Areas.Dashboard.Controllers
{
    [Area("Dashboard")]
    public class LandingController : Controller
    {

        private readonly ILogger<LandingController> _logger;
        private readonly IOptions<MySettingsModel> appSettings;
        public LandingController(IOptions<MySettingsModel> app, ILogger<LandingController> logger)
        {
            appSettings = app;
            ApplicationSettings.WebApiUrl = appSettings.Value.WebApiBaseUrl;
            _logger = logger;
        }
        public async Task<IActionResult> Index()
        {
            RBAC model = new RBAC();
            RBAC obj = new RBAC();
            int EmpId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            obj.ApprovalList = await ApiClientFactory.Instance.GetTaskCountByEmpId(EmpId);
            obj.EnablerList = await ApiClientFactory.Instance.GetEnablerAssessmentByEmpId(EmpId);
            obj.ComplianceList = await ApiClientFactory.Instance.GetComplianceRCMByEmpId(EmpId);
            var OrgList = obj.EnablerList.Select(x => x.OrgId).Distinct().ToList();
            foreach (var item in OrgList)
            {
                bool IsCompleted = obj.EnablerList.Where(x => x.OrgId == item).All(x => x.Status == 1);
                if (!IsCompleted)
                {
                    obj.Counts++;
                }
            }
            foreach (var item in obj.ComplianceList)
            {
                if (item.Status != 1)
                {
                    obj.Counts++;
                }


            }
            foreach (var item in obj.ApprovalList)
            {
                if (item.Status2 == 1)
                {
                    obj.Counts++;
                }


            }

            var RoleId = HttpContext.Session.GetInt32("Role");
            if (RoleId == 0 || RoleId == null)
            {
                TempData["error"] = "Session time out. Please try again.";
                return RedirectToAction("Login", "Login", new { area = "ApplicationManagement" });
            }
            var usr = User.Identity.Name;
            return View(obj);
        }

        public async Task<JsonResult> ShowType3(int Type)
        {
            var model = new RBAC();
            string str = string.Empty;
            int EmpId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            model.ApprovalList = await ApiClientFactory.Instance.GetTaskCountByEmpId(EmpId);
            model.EnablerList = await ApiClientFactory.Instance.GetEnablerAssessmentByEmpId(EmpId);
            model.ComplianceList = await ApiClientFactory.Instance.GetComplianceRCMByEmpId(EmpId);
            if (Type == 1)
            {
                var OrgList = model.EnablerList.Select(x => x.OrgId).Distinct().ToList();
                foreach (var item in OrgList)
                {
                    bool IsCompleted = model.EnablerList.Where(x => x.OrgId == item).All(x => x.Status == 1);
                    if (!IsCompleted)
                    {
                        var _item = model.EnablerList.Where(x => x.OrgId == item).FirstOrDefault();
                        str = str + "<div><h2 class='h2class'>Details</h2><div class='row'><div class='col-12 p-2'><label>Assigned by :</label>" +
                                                      "<label> " + _item.UserName + " </label><br/><label>Time Stamp :</label><label>" + _item.CreatedOn + "</label><br/><label>Organization :</label>" +
                                                      "<label>" + _item.OrgName + "</label><br/><button type ='button' class='btn btn-primary'  onclick='fn_GotoEnablerAssessment(" + _item.OrgId + ",`" + _item.OrgName + "`);' >Take to the Task</button></div></div></div>";
                    }
                }
            }
            else if (Type == 2)
            {
                foreach (var item in model.ComplianceList)
                {
                    if (item.Status != 1)
                    {
                        str = str + "<div><h2 class='h2class'>Details</h2><div class='row'><div class='col-12 p-2'><label>Assigned by :</label>" +
                                  "<label>" + item.UserName + "</label><br/><label>Time Stamp :</label><label>" + item.CreatedOn + "</label><br/><label>Organization :</label>" +
                                  "<label>" + item.OrgName + "</label><br/><button type ='button' class='btn btn-primary' onclick='fn_GoToComplianceAssessment(" + item.OrgId + ",`" + item.OrgName + "`)'>Take to the Task</button></div></div></div>";
                    }


                }
            }
            else
            {
                foreach (var item in model.ApprovalList)
                {
                    if (item.Status2 == 1)
                    {
                        str = str + "<div><h2 class='h2class'>Details</h2><div class='row'><div class='col-12 p-2'><label>Assigned by :</label>" +
                                   "<label> " + @item.UserName + " </label><br/><label>Time Stamp :</label><label>" + @item.CreatedOn + "</label><br/><label>Organization :</label>" +
                                   "<label>" + @item.OrgName + "</label><br/><label>Task :</label><label>" + @item.EnablerName + "</label><br/><button type ='button' class='btn btn-primary' onclick='fn_GotoAssignedEnabler(" + item.OrgId + "," + item.CategoryId + "," + item.QuestionnaireID + ",`" + item.EnablerName + "`);'>Take to the Task</button></div></div></div>";
                    }


                }
            }

            return Json(str);
        }

        public async Task<IActionResult> ProcessView(int orgId = 0, int functionId = 0)
        {
            var model = new IRA();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            model.DdlParentOrg = new SelectList(await ApiClientFactory.Instance.GetAssignedOrganizationByEmplyoyeeID(EmployeeId), "Id", "OrgName");
            model.DdlProcess = new SelectList(await ApiClientFactory.Instance.BindFunctionProcess(orgId, functionId), "Id", "FunctionName");
            model.OrgId = orgId;
            model.FunctionList = await ApiClientFactory.Instance.GetFunctionByOrganization(orgId);
            model.FunctionList = model.FunctionList.Where(x => x.Id == functionId).ToList();
            model.ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(orgId);
            model.ProcessList = model.ProcessList.Where(x => x.FunctionID == functionId).ToList();
            model.UserList = await ApiClientFactory.Instance.getClientadminFillData(orgId);
            return View(model);
        }

        public async Task<IActionResult> _ProcessTree(int orgId = 0, int functionId = 0)
        {
            string res = string.Empty;
            var model = new IRA();
            model.OrgId = orgId;
            model.FunctionList = await ApiClientFactory.Instance.GetFunctionByOrganization(orgId);
            model.FunctionList = model.FunctionList.Where(x => x.Id == functionId).ToList();
            model.ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(orgId);
            model.ProcessList = model.ProcessList.Where(x => x.FunctionID == functionId).ToList();
            foreach (var item in model.ProcessList)
            {
                var Processid = item.Id;
                res = OrgFunctionList(orgId, functionId, Processid).ToString();
            }
            return View(model);
        }

        public async Task<IActionResult> ProcessTree(int orgId = 0, int functionId = 0)
        {
            string res = string.Empty;
            string FinalScore = string.Empty;
            var model = new IRA();
            model.OrgId = orgId;
            model.FunctionList = await ApiClientFactory.Instance.GetFunctionByOrganization(orgId);
            var FunctionList = model.FunctionList.Where(x => x.Id == functionId).ToList();
            model.ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(orgId);
            var ProcessList = model.ProcessList.Where(x => x.FunctionID == functionId && x.AssignToUserId == Convert.ToInt64(HttpContext.Session.GetInt32("UserId"))).ToList();
            FinalScore = await OrgFunctionList(orgId, functionId, 0);
            return Json(new { FunctionList, ProcessList, FinalScore });
        }
        public async Task<string> OrgFunctionList(int orgId = 0, int functionId = 0, int processId = 0)
        {
            long userId = Convert.ToInt64(HttpContext.Session.GetInt32("UserId"));
            var ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(orgId);
            var _ProcessList = ProcessList.Where(x => x.FunctionID == functionId && x.AssignToUserId == userId).ToList();
            decimal fscore1 = 0, fscore2 = 0, fscore3 = 0, fscore4 = 0, fscore5 = 0;
            decimal frd1 = 0, frd2 = 0, frd3 = 0, frd4 = 0, frd5 = 0;
            decimal dc1 = 0, dc2 = 0, dc3 = 0, dc4 = 0, dc5 = 0;
            foreach (var item in _ProcessList)
            {
                var parameterList = await ApiClientFactory.Instance.GetConCatListByIds(orgId, functionId, item.Id);
                var str = parameterList.GroupBy(x => new { x.CategoryId, x.CategoryName }).ToList();
                var res = await ApiClientFactory.Instance.GetDomainCriticalityScoreByProcessesId(orgId, functionId, item.Id);
                var res5 = res.Where(x => x.ParameterId == 5).ToList();
                var rowdata = new List<ClientEnablers>();
                decimal rd1 = 0, rd2 = 0, rd3 = 0, rd4 = 0, rd5 = 0;
                int L1 = 0, L2 = 0, L3 = 0, L4 = 0, L5 = 0;
                var _scopeList = await ApiClientFactory.Instance.getAssessmentScope(orgId);
                var _riskDomains = string.IsNullOrEmpty(_scopeList[0].DomainIds) ? default : _scopeList[0].DomainIds.Split(",");
                var riskDomainIds = Array.ConvertAll(_riskDomains, x => Convert.ToInt32(x));
                foreach (var parameter in str)
                {
                    rowdata = await ApiClientFactory.Instance.GetDomainBaseRiskScore(orgId, functionId, item.Id, Convert.ToInt32(parameter.Key.CategoryId), 0);
                    foreach (var rd in rowdata)
                    {
                        var i = rd.RiskDomainId;
                        _ = i == 1 ? (rd1 += rd.Score) : i == 2 ? (rd2 += rd.Score) : i == 3 ? (rd3 += rd.Score) : i == 4 ? (rd4 += rd.Score) : (rd5 += rd.Score);
                        _ = i == 1 ? (rd.Score > 0 ? L1++ : L1) : i == 2 ? (rd.Score > 0 ? L2++ : L2) : i == 3 ? (rd.Score > 0 ? L3++ : L3) : i == 4 ? (rd.Score > 0 ? L4++ : L4) : (rd.Score > 0 ? L5++ : L5);
                    }
                }

                var length = str.Count;
                foreach (var rd in riskDomainIds)
                {
                    _ = rd == 1 ? (frd1 = rd1 > 0 ? Math.Round(rd1 / L1, 2) : 0) : rd == 2 ? (frd2 = rd2 > 0 ? Math.Round(rd2 / L2, 2) : 0) : rd == 3 ? (frd3 = rd3 > 0 ? Math.Round(rd3 / L3, 2) : 0) : rd == 4 ? (frd4 = rd4 > 0 ? Math.Round(rd4 / L4, 2) : 0) : (frd5 = rd5 > 0 ? Math.Round(rd5 / L5, 2) : 0);
                }
                foreach (var rd in riskDomainIds)
                {
                    try
                    {
                        if (length > 0)
                        {
                            var score = res5.Where(x => x.RiskDomainId == rd).Select(x => x.Score).FirstOrDefault();
                            _ = rd == 1 ? (dc1 = res5.Count > 0 ? Math.Round(score, 2) : 0) : rd == 2 ? (dc2 = res5.Count > 0 ? Math.Round(score, 2) : 0) : rd == 3 ? (dc3 = res5.Count > 0 ? Math.Round(score, 2) : 0) : rd == 4 ? (dc4 = res5.Count > 0 ? Math.Round(score, 2) : 0) : (dc5 = res5.Count > 0 ? Math.Round(score, 2) : 0);
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.HResult = 0;
                    }
                }
                foreach (var rd in riskDomainIds)
                {
                    _ = rd == 1 ? fscore1 += Math.Round(frd1 * dc1, 2) : rd == 2 ? fscore2 += Math.Round(frd2 * dc2, 2) : rd == 3 ? fscore3 += Math.Round(frd3 * dc3, 2) : rd == 4 ? fscore4 += Math.Round(frd4 * dc4, 2) : fscore5 += Math.Round(frd5 * dc5, 2);
                }
            }

            string html = string.Empty;
            string thead = "<thead><tr><th style='text-align:center;width:120px'>Parameters</th>";
            string tbody = "<tbody><tr><td style='width:120px'> Final Risk Score</td>";
            var riskDomainList = await ApiClientFactory.Instance.GetEnumsByEnumTypeId(3);

            var scopeList = await ApiClientFactory.Instance.getAssessmentScope(orgId);
            var riskDomains = string.IsNullOrEmpty(scopeList[0].DomainIds) ? default : scopeList[0].DomainIds.Split(",");
            foreach (var rd in Array.ConvertAll(riskDomains, x => Convert.ToInt32(x)))
            {
                var _rd = riskDomainList.Where(x => x.Value == rd).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                var s = _rd.Value;
                thead += "<th style='text-align:center;width:200px' id=" + _rd.Value + "> " + _rd.Name + "</th>";
                tbody += "<td class='Scoreclass" + s + "'><input type='number' class='form-control finalscoregrafclass rd" + s + "'  value='" + (s == 1 ? fscore1 : s == 2 ? fscore2 : s == 3 ? fscore3 : s == 4 ? fscore4 : fscore5) + "'/></td>";
            }
            thead += "</tr></thead>";
            tbody += "</tr></tbody>";
            html += thead;
            html += tbody;
            return html;
        }

        public async Task<IActionResult> OrgCategory(int orgId = 0, int functionId = 0, int processId = 0)
        {
            string html = string.Empty;
            string thead = "<thead><tr><th style='text-align:center;width:120px'>Parameters</th>";
            string tbody = "<tbody>";
            var parameterList = await ApiClientFactory.Instance.GetConCatListByIds(orgId, functionId, processId);
            var str = parameterList.GroupBy(x => new { x.CategoryId, x.CategoryName }).ToList();

            var riskDomainList = await ApiClientFactory.Instance.GetEnumsByEnumTypeId(3);
            var res = await ApiClientFactory.Instance.GetDomainCriticalityScoreByProcessesId(orgId, functionId, processId);
            if (res.Count > 0)
            {
                var riskDomains = res.Where(x => x.ParameterId == 1).Select(x => x.RiskDomainId).ToList();
                foreach (var item in riskDomains)
                {
                    var _item = riskDomainList.Where(x => x.Value == item).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                    thead += "<th class='rdth' style='text-align:center;width:200px' id=" + _item.Value + "> " + _item.Name + "</th>";
                }
                thead += "</tr></thead>";
                var rowdata = new List<ClientEnablers>();
                var _scopeList = await ApiClientFactory.Instance.getAssessmentScope(orgId);
                var _riskDomains = string.IsNullOrEmpty(_scopeList[0].DomainIds) ? default : _scopeList[0].DomainIds.Split(",");
                var riskDomainIds = Array.ConvertAll(_riskDomains, x => Convert.ToInt32(x));
                foreach (var parameter in str)
                {
                    string ttt = string.Empty;
                    rowdata = await ApiClientFactory.Instance.GetDomainBaseRiskScore(orgId, functionId, processId, Convert.ToInt32(parameter.Key.CategoryId), 0);
                    var Tdno = 0;
                    if (rowdata.Count > 0)
                    {
                        foreach (var item3 in riskDomainIds)
                        {
                            var sum = rowdata.Where(x => x.RiskDomainId == item3).Select(x => x.Score).Sum();
                            Tdno++;
                            ttt = ttt + "<td class='Scoreclass" + item3 + "'><input type='number' value=" + sum + " class='form-control  min='1' max='5' onchange='CalculateAverage(this)' /></td>";

                        }
                    }
                    else
                    {
                        foreach (var item3 in riskDomains)
                        {
                            Tdno++;
                            ttt = ttt + "<td class='Scoreclass" + item3 + "'><input type='number' value=0 class='form-control  min='1' max='5' onchange='CalculateAverage(this)' /></td>";

                        }
                    }

                    tbody += "<tr><td class='parameter' id=" + parameter.Key.CategoryId + ">" + parameter.Key.CategoryName + "</td>" + ttt;
                }
                string ttt2 = "<td>Domain Score</td>";
                if (rowdata.Count > 0)
                {
                    foreach (var item3 in riskDomainIds)
                    {
                        ttt2 += "<td><input class='form-control' id='rd" + item3 + "' readonly /></td>";
                    }
                }
                else
                {
                    foreach (var item3 in riskDomains)
                    {
                        ttt2 += "<td><input class='form-control' id='rd" + item3 + "' readonly /></td>";
                    }
                }
                tbody += "<tr>" + ttt2 + "</tr></tr>";
            }
            else
            {
                var scopeList = await ApiClientFactory.Instance.getAssessmentScope(orgId);
                var riskDomains = string.IsNullOrEmpty(scopeList[0].DomainIds) ? default : scopeList[0].DomainIds.Split(",");
                foreach (var rd in Array.ConvertAll(riskDomains, x => Convert.ToInt32(x)))
                {
                    var _rd = riskDomainList.Where(x => x.Value == rd).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                    thead += "<th style='text-align:center;width:200px' id=" + _rd.Value + "> " + _rd.Name + "</th>";
                }
                thead += "</tr></thead>";
                foreach (var parameter in parameterList)
                {
                    tbody += "<tr><td class='parameter' id=" + parameter.Id + ">" + parameter.CategoryName + "</td>";
                    foreach (var rd in Array.ConvertAll(riskDomains, x => Convert.ToInt32(x)))
                    {
                        var _rd = riskDomainList.Where(x => x.Value == rd).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                        tbody += "<td class=parameter" + parameter.Id + "><input type='number' class='form-control RD_" + _rd.Value + "' min='1' max='5' onchange='CalculateAverage(this)' /></td>";
                    }
                    tbody += "</tr>";
                }
            }

            tbody += "</tbody>";
            html += thead;
            html += tbody;
            return Json(new { html = html, isEdit = res.Count > 0 ? true : false });
        }


        [HttpPost]
        public async Task<IActionResult> BindFunctionProcess(int OrgID, string Orgname, int FunId)
        {
            _logger.LogInformation("BindFunctionProcess");
            string html = string.Empty;
            string res = string.Empty;
            string resfun = string.Empty;
            string resfunChild = string.Empty;
            string resfunSubChild = string.Empty;
            var riskDomainIds = await GetHeaderFunction(OrgID);
            var res1 = await ApiClientFactory.Instance.BindFunctionProcess(OrgID, FunId);
            resfun = await GetFunctionIdData(OrgID, res1, Orgname, riskDomainIds);
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            var ParentOrg = await ApiClientFactory.Instance.GetOrganizationByEmplyoyeeID(EmployeeId);
            var parent = ParentOrg.Where(x => x.Id == OrgID && x.ChildOrgId == 0 && x.SubChildOrgId == 0).ToList();
            var child = ParentOrg.Where(x => x.Id == OrgID && x.ChildOrgId > 0 && x.SubChildOrgId == 0).ToList();
            if (parent.Count > 0)
            {
                foreach (var itemChild in ParentOrg.Where(x => x.ChildOrgId == OrgID && x.SubChildOrgId == 0))
                {
                    var res2 = await ApiClientFactory.Instance.BindFunctionProcess(itemChild.Id, FunId);
                    if (res2.Count > 0)
                    {
                        resfunChild += await GetFunctionIdData(itemChild.Id, res2, itemChild.ParentOrgName, riskDomainIds);

                        var subchild = ParentOrg.Where(x => x.SubChildOrgId == itemChild.Id && x.ChildOrgId == 0);
                        foreach (var itemsubchild in subchild)
                        {
                            var res3 = await ApiClientFactory.Instance.BindFunctionProcess(itemsubchild.Id, FunId);
                            if (res3.Count > 0)
                            {
                                resfunSubChild += await GetFunctionIdData(itemsubchild.Id, res3, itemsubchild.ParentOrgName, riskDomainIds);
                            }
                        }
                    }

                }
            }
            else if (child.Count > 0)
            {
                var subchild = ParentOrg.Where(x => x.SubChildOrgId == OrgID);
                foreach (var itemsubchild in subchild)
                {
                    var res3 = await ApiClientFactory.Instance.BindFunctionProcess(itemsubchild.Id, FunId);
                    if (res3.Count > 0)
                    {
                        resfunSubChild += await GetFunctionIdData(itemsubchild.Id, res3, itemsubchild.ParentOrgName, riskDomainIds);
                    }
                }
            }
            res = Getdata(res1);

            string thead = "<thead><tr><th style='text-align:center;width:120px'>Parameters</th>";
            string tbody = "<tbody>";
            var riskDomainList = await ApiClientFactory.Instance.GetEnumsByEnumTypeId(3);
            foreach (var s in riskDomainIds)
            {
                var _rd = riskDomainList.Where(x => x.Value == s).Select(x => new { x.Value, x.Name }).FirstOrDefault();
                thead += "<th class='rdth' style='text-align:center;width:200px' id=" + _rd.Value + "> " + _rd.Name + "</th>";
            }
            thead += "</tr></thead>";
            tbody += resfun + resfunChild + resfunSubChild + "</tbody>";
            html += thead;
            html += tbody;

            return Json(new { res, res1, html });
        }

        private async Task<List<int>> GetHeaderFunction(long OrgID)
        {
            List<int> Ids = new List<int>();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            var ParentOrg = await ApiClientFactory.Instance.GetOrganizationByEmplyoyeeID(EmployeeId);
            var parent = ParentOrg.Where(x => x.Id == OrgID && x.ChildOrgId == 0 && x.SubChildOrgId == 0).ToList();
            var child = ParentOrg.Where(x => x.Id == OrgID && x.ChildOrgId > 0 && x.SubChildOrgId == 0).ToList();

            var scopeList1 = await ApiClientFactory.Instance.getAssessmentScope(OrgID);
            var riskDomains1 = string.IsNullOrEmpty(scopeList1[0].DomainIds) ? default : scopeList1[0].DomainIds.Split(",");
            Ids.AddRange(Array.ConvertAll(riskDomains1, x => Convert.ToInt32(x)));
            return Ids.Distinct().OrderBy(x => x).ToList();
        }
        private async Task<string> GetFunctionIdData(long OrgID, List<OrganizationProcesss> res1, string ParentOrgName, List<int> riskDomainIds)
        {
            string html = string.Empty;
            dynamic FS;

            var ProcessListParent = await ApiClientFactory.Instance.GetProcessesByOrgId(OrgID);
            decimal fscore1 = 0, fscore2 = 0, fscore3 = 0, fscore4 = 0, fscore5 = 0;
            decimal fs1 = 0, fs2 = 0, fs3 = 0, fs4 = 0, fs5 = 0;
            int L1 = 0, L2 = 0, L3 = 0, L4 = 0, L5 = 0;
            foreach (var item in res1)
            {
                FS = await OrgFunctionProcessList(OrgID, item.Id, 0);
                fs1 += FS.fscore1;
                fs2 += FS.fscore2;
                fs3 += FS.fscore3;
                fs4 += FS.fscore4;
                fs5 += FS.fscore5;
                _ = FS.fscore1 > 0 ? L1++ : L1;
                _ = FS.fscore2 > 0 ? L2++ : L2;
                _ = FS.fscore3 > 0 ? L3++ : L3;
                _ = FS.fscore4 > 0 ? L4++ : L4;
                _ = FS.fscore5 > 0 ? L5++ : L5;
            }
            fscore1 = Math.Round(fs1 > 0 ? fs1 / L1 : 0, 2);
            fscore2 = Math.Round(fs2 > 0 ? fs2 / L2 : 0, 2);
            fscore3 = Math.Round(fs3 > 0 ? fs3 / L3 : 0, 2);
            fscore4 = Math.Round(fs4 > 0 ? fs4 / L4 : 0, 2);
            fscore5 = Math.Round(fs5 > 0 ? fs5 / L5 : 0, 2);

            string tbody = "<tr><td style='width:120px'>Final Risk Score of '" + ParentOrgName + "'</td>";

            foreach (var s in riskDomainIds)
            {
                tbody += "<td class='Scoreclass" + s + "'><input type='number' class='form-control finalscoregrafclass rd" + s + "'  value='" + (s == 1 ? fscore1 : s == 2 ? fscore2 : s == 3 ? fscore3 : s == 4 ? fscore4 : fscore5) + "'/></td>";
            }
            //var scopeList = await ApiClientFactory.Instance.getAssessmentScope(OrgID);
            //if(scopeList.Count > 0)
            //{
            //    var riskDomains = string.IsNullOrEmpty(scopeList[0].DomainIds) ? default : scopeList[0].DomainIds.Split(",");
            //    foreach (var s in riskDomainIds)
            //    {
            //        tbody += "<td class='Scoreclass" + s + "'><input type='number' class='form-control finalscoregrafclass rd" + s + "'  value='" + (s == 1 ? fscore1 : s == 2 ? fscore2 : s == 3 ? fscore3 : s == 4 ? fscore4 : fscore5) + "'/></td>";
            //    }
            //}
            tbody += "</tr>";
            html += tbody;
            return html;
        }


        public async Task<dynamic> OrgFunctionProcessList(long orgId = 0, int functionId = 0, int processId = 0)
        {
            long userId = Convert.ToInt64(HttpContext.Session.GetInt32("UserId"));
            var ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(orgId);
            var _ProcessList = ProcessList.Where(x => x.FunctionID == functionId && x.AssignToUserId == userId).ToList();
            decimal fscore1 = 0, fscore2 = 0, fscore3 = 0, fscore4 = 0, fscore5 = 0;
            decimal frd1 = 0, frd2 = 0, frd3 = 0, frd4 = 0, frd5 = 0;
            decimal dc1 = 0, dc2 = 0, dc3 = 0, dc4 = 0, dc5 = 0;
            foreach (var item in _ProcessList)
            {
                var parameterList = await ApiClientFactory.Instance.GetConCatListByIds(orgId, functionId, item.Id);
                var str = parameterList.GroupBy(x => new { x.CategoryId, x.CategoryName }).ToList();
                var res = await ApiClientFactory.Instance.GetDomainCriticalityScoreByProcessesId(orgId, functionId, item.Id);
                var res5 = res.Where(x => x.ParameterId == 5).ToList();
                var rowdata = new List<ClientEnablers>();
                decimal rd1 = 0, rd2 = 0, rd3 = 0, rd4 = 0, rd5 = 0;
                int L1 = 0, L2 = 0, L3 = 0, L4 = 0, L5 = 0;
                var _scopeList = await ApiClientFactory.Instance.getAssessmentScope(orgId);
                var _riskDomains = string.IsNullOrEmpty(_scopeList[0].DomainIds) ? default : _scopeList[0].DomainIds.Split(",");
                var riskDomainIds = Array.ConvertAll(_riskDomains, x => Convert.ToInt32(x));
                foreach (var parameter in str)
                {
                    rowdata = await ApiClientFactory.Instance.GetDomainBaseRiskScore(orgId, functionId, item.Id, Convert.ToInt32(parameter.Key.CategoryId), 0);
                    foreach (var rd in rowdata)
                    {
                        var i = rd.RiskDomainId;
                        _ = i == 1 ? (rd1 += rd.Score) : i == 2 ? (rd2 += rd.Score) : i == 3 ? (rd3 += rd.Score) : i == 4 ? (rd4 += rd.Score) : (rd5 += rd.Score);
                        _ = i == 1 ? (rd.Score > 0 ? L1++ : L1) : i == 2 ? (rd.Score > 0 ? L2++ : L2) : i == 3 ? (rd.Score > 0 ? L3++ : L3) : i == 4 ? (rd.Score > 0 ? L4++ : L4) : (rd.Score > 0 ? L5++ : L5);
                    }
                }

                var length = str.Count;
                foreach (var rd in riskDomainIds)
                {
                    _ = rd == 1 ? (frd1 = rd1 > 0 ? Math.Round(rd1 / L1, 2) : 0) : rd == 2 ? (frd2 = rd2 > 0 ? Math.Round(rd2 / L2, 2) : 0) : rd == 3 ? (frd3 = rd3 > 0 ? Math.Round(rd3 / L3, 2) : 0) : rd == 4 ? (frd4 = rd4 > 0 ? Math.Round(rd4 / L4, 2) : 0) : (frd5 = rd5 > 0 ? Math.Round(rd5 / L5, 2) : 0);
                }
                foreach (var rd in riskDomainIds)
                {
                    try
                    {
                        if (length > 0)
                        {
                            var score = res5.Where(x => x.RiskDomainId == rd).Select(x => x.Score).FirstOrDefault();
                            _ = rd == 1 ? (dc1 = res5.Count > 0 ? Math.Round(score, 2) : 0) : rd == 2 ? (dc2 = res5.Count > 0 ? Math.Round(score, 2) : 0) : rd == 3 ? (dc3 = res5.Count > 0 ? Math.Round(score, 2) : 0) : rd == 4 ? (dc4 = res5.Count > 0 ? Math.Round(score, 2) : 0) : (dc5 = res5.Count > 0 ? Math.Round(score, 2) : 0);
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.HResult = 0;
                    }
                }
                foreach (var rd in riskDomainIds)
                {
                    _ = rd == 1 ? fscore1 += Math.Round(frd1 * dc1, 2) : rd == 2 ? fscore2 += Math.Round(frd2 * dc2, 2) : rd == 3 ? fscore3 += Math.Round(frd3 * dc3, 2) : rd == 4 ? fscore4 += Math.Round(frd4 * dc4, 2) : fscore5 += Math.Round(frd5 * dc5, 2);
                }
            }
            return new { fscore1, fscore2, fscore3, fscore4, fscore5 };
        }

        public string Getdata(List<OrganizationProcesss> res1)
        {
            string str = string.Empty;
            string str1 = string.Empty;
            str1 += "<ul>";
            if (res1.Count > 0)
            {
                foreach (var item in res1)
                {
                    str1 += "<li><span class='li'>" + item.FunctionName + "</span></li>";
                }
                str1 += "</ul>";
                str = str1;
            }
            return str;
        }
        public async Task<IActionResult> MisReport()
        {
            var model = new IRA();
            int EmployeeId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            model.DdlParentOrg = new SelectList(await ApiClientFactory.Instance.GetAssignedOrganizationByEmplyoyeeID(EmployeeId), "Id", "OrgName");
            return View(model);
        }

        public async Task<IActionResult> FunctionDetails(long OrgID = 0)
        {
            var res = "";
            var model = new IRA();
            model.OrgId = OrgID;
            var CategoryList = await ApiClientFactory.Instance.GetEnablerCategoryByOrgID(OrgID);
            model.RoleId = Convert.ToInt16(HttpContext.Session.GetInt32("Role"));
            model.FunctionList = await ApiClientFactory.Instance.GetFunctionByOrganization(OrgID);
            model.ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(OrgID);
            model.UserList = await ApiClientFactory.Instance.getClientadminFillData(OrgID);
            if(model.FunctionList != null)
            {
                if(model.RoleId == 4)
                {
                    foreach (var item in model.FunctionList)
                    {
                        var processList = model.ProcessList.Where(x => x.FunctionID == item.Id);
                        res += "<li class='first inactive'><div><span class='text' onclick='fn_GetFuncton(" + OrgID + ", " + item.Id + "); '>" + item.FunctionName + "</span></div><ul>";
                        foreach (var item2 in processList)
                        {
                            //res += "<li><div><span class='text'>"+item2.ProcessName + "</span><a href ='#' onclick='GetDomainCriticalityScore("+ OrgID + ","+item.Id+ ", " + item2.Id + ", "+item2.ProcessName + ", "+item2.ProcessOwner + ", "+item2.ProcessDescription + ")'><span class='icon'><i class='fa fa-caret-right fa-3x' style='padding-top:7px;'></i></span></a></div></li>";
                            res += "<li><div><span class='text'>" + item2.ProcessName + "</span><a href='#' class='procesClass' onclick='fn_ShowGetOrgCategory(" + OrgID + ", " + item.Id + ", " + item2.Id + "); '><span class='icon'><i class='fa fa-caret-right fa-3x' style='padding-top:7px;'></i></span></a></div></li>";

                        }
                        res += "</ul></li>";
                    }
                }
              
            }
            RBAC model1 = new RBAC();
            model1.UserID = (int)HttpContext.Session.GetInt32("UserId");
            var _res = await ApiClientFactory.Instance.GetMISReportId(model1);
            var _res2 = await ApiClientFactory.Instance.GetProcesssCount(model1);
            var ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(OrgID);
            return Json(new { res , _res, _res2, ProcessList , CategoryList });
        }

        public async Task<IActionResult> GetShowGetOrgCategory(long OrgID = 0,int FunctionId = 0,int ProcessId = 0)
        {
            RBAC model1 = new RBAC();
            model1.UserID = (int)HttpContext.Session.GetInt32("UserId");
            var _res = await ApiClientFactory.Instance.GetMISReportId(model1);
            var _res2 = await ApiClientFactory.Instance.GetProcesssCount(model1);
            var ProcessList = await ApiClientFactory.Instance.GetProcessesForMis(OrgID, FunctionId, ProcessId);
            //var FunctionList = await ApiClientFactory.Instance.GetFunctionForMis(OrgID);
            return Json(new { _res, _res2, ProcessList });
        }

        public async Task<IActionResult> GetFunction(long OrgID = 0, int FunctionId = 0)
        {
            var result = "";
            var _result = "";
            var finalResult = "";
            var proces = "'Year'" + ",";
            var model = new IRA();

            var FunctionListCount = await ApiClientFactory.Instance.GetOraganizationForMis(OrgID);
            var FunctionCount = FunctionListCount.GroupBy(x => new { x.FunctionID }).ToList();


            var FunctionList = await ApiClientFactory.Instance.GetFunctionForMis(OrgID, FunctionId);
            model.ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(OrgID);
            var Process = FunctionList.GroupBy(x => new { x.ProcessId }).ToList();
            foreach (var _item in Process)
            {
                var proId = _item.Key.ProcessId;
                var ProcessList = model.ProcessList.Where(x => x.Id == proId && x.AssignToUserId == Convert.ToInt64(HttpContext.Session.GetInt32("UserId"))).ToList();
                foreach (var Processname in ProcessList)
                {
                    proces += "'" + Processname.ProcessName + "'" + ",";
                }
            }
            var funct = FunctionList.GroupBy(x => x.FinYear);
            var funct1 = FunctionListCount.GroupBy(x => x.FinYear);
            //double result1 = 0.00;
            var result1 = new List<double>();
            foreach (var item in funct1)
            {
                var AvgScore1 = 0.0;
                var fnScore = item.GroupBy(x => new { x.FinYear, x.Score, x.ProcessId, x.FunctionID }).ToList();
                foreach (var item1 in fnScore)
                {
                    AvgScore1 += Convert.ToDouble(item1.Key.Score);
                }
                //result1 += AvgScore1 / FunctionCount.Count;
                result1.Add(AvgScore1 / FunctionCount.Count);
            }
            int sn = 0;
            foreach (var item in funct)
            {
                var str = "";
                var str2 = "";
                var AvgScore = 0.0;
                var fnScore = item.GroupBy(x => new { x.FinYear, x.Score, x.ProcessId, x.FunctionID }).ToList();
                foreach (var item1 in fnScore)
                {
                    //var functionScore = FunctionList.Where(x => x.FunctionID == item1.Key.FunctionID && x.FinYear == item1.Key.FinYear).ToList();
                    str = "'" + item1.Key.FinYear + "'";
                    str2 += item1.Key.Score + ",";
                    AvgScore += Convert.ToDouble(item1.Key.Score);
                }
                result += "[" + str + "," + str2.TrimEnd(',') + ", " + AvgScore / fnScore.Count + " , " + result1[sn].ToString("F2") + "],";
                _result = result.TrimEnd(',');
                sn++;
            }
            proces += "'Function'" + "," + "'Organization'";
            finalResult += "[" + "[" + proces.TrimEnd(',') + "]," + _result + "]";
            var Processcount = Process.Count;

            return Json(new { finalResult, Processcount });
        }

        public async Task<IActionResult> GetOrgMis(long OrgID = 0)
        {  
            var result = "";
            var _result = "";
            var finalResult = "";
            var fn = "'Year'" + ",";
            var model = new IRA();
         
            var FunctionList = await ApiClientFactory.Instance.GetOraganizationForMis(OrgID);
            var FunctionwithYear = FunctionList.GroupBy(x => new {x.FinYear}).ToList();
            var Function = FunctionList.GroupBy(x => new { x.FunctionID }).ToList();

            model.FunctionList = await ApiClientFactory.Instance.GetFunctionByOrganization(OrgID);
            var Process = FunctionList.GroupBy(x => new { x.FunctionID }).ToList();
            foreach (var _item in Process)
            {
                var FunId = _item.Key.FunctionID;
                var _FunctionList = model.FunctionList.Where(x => x.Id == FunId).ToList();
                foreach (var functionName in _FunctionList)
                {
                    fn += "'" + functionName.FunctionName + "'" + ",";
                }
            }
            var funct = FunctionList.GroupBy(x => x.FinYear);
            foreach (var item in funct)
            {
                var str = "";
                var str2 = "";
                var AvgScore = 0.0;
                var AvgFnScoreCount = 0;

                var fnScore = item.GroupBy(x => new { x.FinYear, x.Score, x.ProcessId, x.FunctionID }).ToList();
                foreach (var item1 in fnScore)
                {
                    str = "'" + item1.Key.FinYear + "'";
                    str2 += item1.Key.Score + ",";
                    AvgScore += Convert.ToDouble(item1.Key.Score);
                    AvgFnScoreCount = 1;
                }
                //result += "[" + str + "," + str2.TrimEnd(',') + ", " + AvgScore / Function.Count + " , " + AvgScore / AvgFnScoreCount + "],";
                result += "[" + str + "," + str2.TrimEnd(',') + ", " + AvgScore / Function.Count + "],";
                _result = result.TrimEnd(',');
            }
            fn += "'Organization'";
            finalResult += "[" + "[" + fn.TrimEnd(',') + "]," + _result + "]";
            var Processcount = Function.Count;
            return Json(new { finalResult, Processcount });
        }
        public async Task<IActionResult> GovernanceReports()
        {
            var model = new RBAC();
            int EmpId = Convert.ToInt16(HttpContext.Session.GetInt32("UserId"));
            var roleId = HttpContext.Session.GetInt32("Role");
            model.RoleId = roleId ?? 0;
            model.ApprovalList = await ApiClientFactory.Instance.GetTaskCountByEmpId(EmpId);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Reports(long OrgID = 0)
        {
            _logger.LogInformation("Reports");
            RBAC model = new RBAC();
            model.UserID = (int)HttpContext.Session.GetInt32("UserId");
            var res = await ApiClientFactory.Instance.GetMISReportId(model);
            var res2 = await ApiClientFactory.Instance.GetProcesssCount(model);
            var ProcessList = await ApiClientFactory.Instance.GetProcessesByOrgId(OrgID);
            return Json(new { res, res2, ProcessList });
        }

        public async Task<IActionResult> ArtefactTasks(int type)
        {
            var model = new RequestArtefacts();
            long userId = (long)HttpContext.Session.GetInt32("UserId");
            model.ListRequestArtefacts = await ApiClientFactory.Instance.GetArtefactTasks(userId);
            return View(model);
        }
    }
}
