#pragma checksum "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "caa616de09aa7fdb52fc46d7bc1ce8cb476562f2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_ApplicationManagement_Views_ProcessesAssign_EnablerScore), @"mvc.1.0.view", @"/Areas/ApplicationManagement/Views/ProcessesAssign/EnablerScore.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"caa616de09aa7fdb52fc46d7bc1ce8cb476562f2", @"/Areas/ApplicationManagement/Views/ProcessesAssign/EnablerScore.cshtml")]
    public class Areas_ApplicationManagement_Views_ProcessesAssign_EnablerScore : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CyberaniumModels.ProcessesAssign>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<table id=\"Enabler\" class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th width=\"200px\">Enablers</th>\r\n");
#nullable restore
#line 7 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
             foreach (var item in Model.RiskDomainList)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <th>");
#nullable restore
#line 9 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
                Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</th>\r\n");
#nullable restore
#line 10 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
#nullable restore
#line 14 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
         foreach (var category in Model.ControlCategoryList)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>");
#nullable restore
#line 17 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
               Write(category.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(" Enabler Value</td>\r\n");
#nullable restore
#line 18 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
                  
                    var list = Model.DomainBaseRiskScoreList.Where(x => x.CategoryId == category.Id).ToList();
                    decimal score = 0.0M;
                    foreach (var domain in Model.RiskDomainList)
                    {
                        score = list.Where(x => x.DomainId == domain.Value).Select(x => x.Score).Sum();

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <td>");
#nullable restore
#line 24 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
                        Write(score);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n");
#nullable restore
#line 25 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
                    }
                

#line default
#line hidden
#nullable disable
            WriteLiteral("            </tr>\r\n");
#nullable restore
#line 28 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td style=\"background-color:yellow\">Base Residual Risk Score</td>\r\n");
#nullable restore
#line 31 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
             foreach (var domain in Model.RiskDomainList)
            {
                var list = Model.DomainBaseRiskScoreList.Where(x => x.DomainId == domain.Value).ToList();
                var scoreList = list.All(x => x.CategoryId.ToString().Contains(Model.ControlCategoryList.Select(x => x.Id).ToString()));
                var count = list.Count();
                var score = list.Select(x => x.Score).Sum();

#line default
#line hidden
#nullable disable
            WriteLiteral("                <td>");
#nullable restore
#line 37 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
                Write(score);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n");
#nullable restore
#line 38 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\ProcessesAssign\EnablerScore.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tr>\r\n    </tbody>\r\n</table>\r\n<script>\r\n    $(document).ready(function () {\r\n\r\n    })\r\n</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CyberaniumModels.ProcessesAssign> Html { get; private set; }
    }
}
#pragma warning restore 1591
