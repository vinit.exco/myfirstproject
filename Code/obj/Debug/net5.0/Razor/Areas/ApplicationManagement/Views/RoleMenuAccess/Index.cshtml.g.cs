#pragma checksum "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b06f09ac89f05b035a82c5843db93ae959e9677c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_ApplicationManagement_Views_RoleMenuAccess_Index), @"mvc.1.0.view", @"/Areas/ApplicationManagement/Views/RoleMenuAccess/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 2 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
using DataTables.AspNetCore.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b06f09ac89f05b035a82c5843db93ae959e9677c", @"/Areas/ApplicationManagement/Views/RoleMenuAccess/Index.cshtml")]
    public class Areas_ApplicationManagement_Views_RoleMenuAccess_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CyberaniumModels.RoleMenuAccess>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-formhelper", new global::Microsoft.AspNetCore.Html.HtmlString("true"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "SaveupdateUser", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "User", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-area", "ApplicationManagement", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-ajax", new global::Microsoft.AspNetCore.Html.HtmlString("true"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-ajax-method", new global::Microsoft.AspNetCore.Html.HtmlString("POST"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-ajax-mode", new global::Microsoft.AspNetCore.Html.HtmlString("replace"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-loading-text", new global::Microsoft.AspNetCore.Html.HtmlString("loading......"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-ajax-update", new global::Microsoft.AspNetCore.Html.HtmlString("#ajaxresult"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-ajax-success", new global::Microsoft.AspNetCore.Html.HtmlString("fn_Onsuccess(data,\'ApplicationManagement/User/Index\');"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_10 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-ajax-failure", new global::Microsoft.AspNetCore.Html.HtmlString("Errmsg(data);"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_11 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("Threatform"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_12 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
  
    ViewData["Title"] = "Index";


#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<section>

    <div class=""right_section"">
        <div class=""titile_heading"" id=""title_List"">
            <h1>User List</h1>
            <div class=""titile_line""></div>
        </div>
        <div class=""col-lg-12 col-md-12 col-sm-12 col-xs-12"">
            <div class=""right_panel"">
                <div class=""right_content"">
                    <div class=""service_line"">
                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b06f09ac89f05b035a82c5843db93ae959e9677c8185", async() => {
                WriteLiteral(@"
                            <div class=""row"">
                                <div class=""col-md-4 col-sm-6 col-xs-12"">
                                    <label>Role</label>
                                    <div class=""select_box m_bottom"">
                                        ");
#nullable restore
#line 32 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
                                   Write(Html.DropDownListFor(m => m.RoleId, Model.DDLRole, "Select", new { @class = "form-control m_no" }));

#line default
#line hidden
#nullable disable
                WriteLiteral(@"


                                    </div>
                                </div>
                            </div>
                            <div class=""row"">
                                <div class=""col-md-4 col-sm-6 col-xs-12"">
                                    <div class=""submitbtn"">
                                        <input type=""submit"" id=""btnsubmit"" name=""Submit"" value=""Submit"" onclick=""CheckValidation()"">
                                    </div>
                                    <div class=""back_btn"" id=""viewbutton"">
                                        <div class=""back_btn_in"" data-title=""Back"">
                                            <input type=""button"" name=""Back"" onclick=""BackButton()"" value=""Back"">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Area = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_10);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_11);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_12.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_12);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                    </div>
                    <div class=""assign_panel assign_section"">
                        <div class=""addbtn"">
                            <div class=""add_litigation"">
                                <span>+</span>
                                <input type=""button"" onclick=""AddButton()"" value=""Add User"">
                            </div>
                        </div><!--edn of addbtn-->
");
            WriteLiteral("                        <div class=\"container\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-sm-11 col-md-11\">\r\n                                    <div class=\"panel-group\" id=\"accordion\">\r\n");
#nullable restore
#line 64 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
                                          int p = 0;
                                            foreach (var parent in Model.Menues.Where(x => x.ParentMenuId > 0 && x.ChildMenuId == 0 && x.SubMenuId == 0))
                                            {
                                                p++; int c = 0;
                                                foreach (var child in Model.Menues.Where(x => x.ParentMenuId == parent.ParentMenuId && x.ParentMenuId > 0 && x.ChildMenuId > 0 && x.SubMenuId == 0))
                                                {
                                                    c++;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                    <div class=""panel"" style=""box-shadow:0px 0px 10px 2px;background-color:skyblue;"">
                                                        <div class=""panel-heading pb-0"">
                                                            <h4 class=""panel-title"">
                                                                <input type=""checkbox"" class=""form-check-input pb-3""");
            BeginWriteAttribute("value", " value=\"", 4335, "\"", 4343, 0);
            EndWriteAttribute();
            BeginWriteAttribute("id", " id=\"", 4344, "\"", 4370, 2);
            WriteAttributeValue("", 4349, "ChildMenuId_", 4349, 12, true);
#nullable restore
#line 74 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
WriteAttributeValue("", 4361, p+""+c, 4361, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" onclick=\"CheckAllSubMenu(this)\">\r\n                                                                <a data-toggle=\"collapse\" data-parent=\"#accordion\"");
            BeginWriteAttribute("href", " href=\"", 4520, "\"", 4546, 2);
            WriteAttributeValue("", 4527, "#collapse_", 4527, 10, true);
#nullable restore
#line 75 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
WriteAttributeValue("", 4537, p+""+c, 4537, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                                                    <label class=\"form-check-label pb-3 pl-3\"");
            BeginWriteAttribute("for", " for=\"", 4659, "\"", 4686, 2);
            WriteAttributeValue("", 4665, "ChildMenuId_", 4665, 12, true);
#nullable restore
#line 76 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
WriteAttributeValue("", 4677, p+""+c, 4677, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 76 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
                                                                                                                                     Write(child.MenuName);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</label><br />
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div");
            BeginWriteAttribute("id", " id=\"", 4980, "\"", 5003, 2);
            WriteAttributeValue("", 4985, "collapse_", 4985, 9, true);
#nullable restore
#line 80 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
WriteAttributeValue("", 4994, p+""+c, 4994, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"panel-collapse collapse\">\r\n                                                            <div class=\"panel-body py-0\">\r\n                                                                <table class=\"table mb-0\">\r\n");
#nullable restore
#line 83 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
                                                                      int s = 0;
                                                                        foreach (var sub in Model.Menues.Where(x => x.ParentMenuId == child.ParentMenuId && x.ChildMenuId == child.ChildMenuId && x.ParentMenuId > 0 && x.ChildMenuId > 0 && x.SubMenuId > 0))
                                                                        {
                                                                            s++;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                                            <tr>
                                                                                <td class=""pb-0"">
                                                                                    <input type=""checkbox""");
            BeginWriteAttribute("class", " class=\"", 6004, "\"", 6055, 4);
            WriteAttributeValue("", 6012, "form-check-input", 6012, 16, true);
            WriteAttributeValue(" ", 6028, "pb-3", 6029, 5, true);
            WriteAttributeValue(" ", 6033, "subCheckbox_", 6034, 13, true);
#nullable restore
#line 89 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
WriteAttributeValue("", 6046, p+""+c, 6046, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 6056, "\"", 6064, 0);
            EndWriteAttribute();
            BeginWriteAttribute("id", " id=\"", 6065, "\"", 6094, 2);
            WriteAttributeValue("", 6070, "SubMenuId_", 6070, 10, true);
#nullable restore
#line 89 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
WriteAttributeValue("", 6080, p+""+c+""+s, 6080, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                                                                    <label class=\"form-check-label pb-3 pl-3\"");
            BeginWriteAttribute("for", " for=\"", 6223, "\"", 6253, 2);
            WriteAttributeValue("", 6229, "SubMenuId_", 6229, 10, true);
#nullable restore
#line 90 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
WriteAttributeValue("", 6239, p+""+c+""+s, 6239, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 90 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
                                                                                                                                                        Write(sub.MenuName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</label><br />\r\n                                                                                </td>\r\n                                                                            </tr>\r\n");
#nullable restore
#line 93 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
                                                                        }
                                                                    

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
");
#nullable restore
#line 99 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\RoleMenuAccess\Index.cshtml"
                                                }
                                            }
                                        

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type=""text/javascript"">
    function CheckAllSubMenu(childCheck) {
        var index = parseInt($(childCheck).attr('id').split('_')[1]);
        alert(index);
        $('.subCheckbox_' + index).each(function (i, v) {
            $('#SubMenuId' + index + '' + i).prop('checked', 'checked');
        });
    }



    $(document).ready(function () {
        $(""#btnsubmit"").show();
        if ($(""#ThreatId"").val() > 0) {
            debugger;
            $("".titile_heading h1"").html(""Edit User"");
            $("".service_line"").addClass(""active"");
            $("".assign_panel"").hide();
            $("".titile_heading"").show();
        } else {
            $("".titile_heading h1"").html(""User list"");
            $("".service_line"")");
            WriteLiteral(@".removeClass(""active"");
            $("".assign_panel"").show();
            $("".titile_heading"").show();
        }
    });
    function AddButton() {
        $("".titile_heading h1"").html(""Add User"");
        $("".service_line"").addClass(""active"");
        $("".assign_panel"").hide();
        //$("".titile_heading"").show();}
    }
    function BackButton() {
        $("".titile_heading h1"").html(""User List"");
        $("".service_line"").removeClass(""active"");
        $("".assign_panel"").show();
        //$("".titile_heading"").show();
        //$(""#viewbutton"").show();
    }
    //function fnHideShowForm() {
    //    $('#dvForm').toggleClass('service_line');
    //}
    //function fnUpdate(ThreatId, TTypeId, ThreatCode, ThreatName) {
    //    fnHideShowForm()
    //    $('#hiddenThreatId').val(ThreatId);
    //    $('#ddlTTypeId').val(TTypeId);
    //    $('#txtThreatCode').val(ThreatCode);
    //    $('#txtThreatName').val(ThreatName);
    //    $('#form').removeAttr('action').attr('action'");
            WriteLiteral(@", '/Master/Threat/UpdateThreat');
    //}
    function fnBlockDelete(ThreatId, Action, Type) {
        if (confirm('Are you sure you want to' + Action + ' this?')) {
            $.getJSON('/Master/Threat/BlockDeleteThreat', { ThreatId: ThreatId, Type: Type }, function (data) {
                debugger;
                if (data > 0) {
                }
            });
            setTimeout(function () { window.location.reload(); }, 1000);
        }
    }
</script>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CyberaniumModels.RoleMenuAccess> Html { get; private set; }
    }
}
#pragma warning restore 1591
