#pragma checksum "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3c6206f5e2d2b5057a8b510b65c7ebebbe66c586"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_ApplicationManagement_Views_Organogram_TabMenu), @"mvc.1.0.view", @"/Areas/ApplicationManagement/Views/Organogram/TabMenu.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3c6206f5e2d2b5057a8b510b65c7ebebbe66c586", @"/Areas/ApplicationManagement/Views/Organogram/TabMenu.cshtml")]
    public class Areas_ApplicationManagement_Views_Organogram_TabMenu : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CyberaniumModels.RBAC>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery/dist/jquery.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Image/CreateOrganogram/Organization.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Image/CreateOrganogram/assignTask.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Image/CreateOrganogram/Trangle_Warning.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Image/CreateOrganogram/Circle_Information.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Image/CreateOrganogram/Triangle2.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Image/CreateOrganogram/Signature.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
  
    ViewData["Title"] = "KPMG";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<style>\r\n    .centerbox .cardbtn {\r\n        width: 330px;\r\n        margin-left: 10px;\r\n    }\r\n\r\n    a:hover {\r\n        color: white;\r\n        cursor: pointer;\r\n    }\r\n</style>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c5866010", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

<section>
    <div class=""right_section"">
        <div class=""titile_heading"" id=""title_List"">
            <h1>Create Organogram</h1>
            <div class=""titile_line""></div>
        </div>
        <div class=""col-lg-8 col-md-8 col-sm-8 col-xs-8"">
            <div class=""right_panel"">
                <div class=""right_content"">
                    <div class=""centerbox"">
                        <div class=""cardbtn"">
                            <a href=""#"" onclick=""Active(this); $.get('/ApplicationManagement/Organization/Index', function (res) { $('#ajaxresult').html(res) });"">
                                <div class=""cardbtnIn"">
                                    <div class=""cardbtnimg"">
                                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c5867834", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                                    </div>
                                    <div class=""cardbtntext"">Create Organogram</div>
                                </div>
                            </a>
                        </div>
                        <div class=""cardbtn"">
                            <a href=""#"" onclick=""Active(this); $.get('/ApplicationManagement/OrganzationAssignToConsultant/Index', function (res) { $('#ajaxresult').html(res) });"">
                            <div class=""cardbtnIn"">
                                <div class=""cardbtnimg"">
                                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c5869487", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                                </div>
                                <div class=""cardbtntext"">Assign Organization</div>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=""col-lg-4 col-md-4 col-sm-4 col-xs-4"">
            <div class=""right_panel"">
                <div class=""right_content"">
                    <h3>Organization Map</h3>
                    <ul class=""orgmap"" style=""height: calc(100vh - 308px);overflow:auto;padding:10px"">
");
#nullable restore
#line 60 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                         foreach (var item in Model.OrganizationList.Where(x => x.ChildOrgId == 0 && x.SubChildOrgId == 0))
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                            <li class=\"first inactive assessment\">\r\n                                <div>\r\n                                    <span class=\"text\">");
#nullable restore
#line 64 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                  Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n                                    <a href=\"#\" class=\'edit_btn\' title=\"Edit Organization\" style=\"margin-top:19px;\"");
            BeginWriteAttribute("onclick", " onclick=\"", 2924, "\"", 3066, 11);
            WriteAttributeValue("", 2934, "$.get(\'/ApplicationManagement/Organization/Index\',", 2934, 50, true);
            WriteAttributeValue(" ", 2984, "{id:\'", 2985, 6, true);
#nullable restore
#line 65 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
WriteAttributeValue("", 2990, item.Id, 2990, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2998, "\',name:\'", 2998, 8, true);
#nullable restore
#line 65 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
WriteAttributeValue("", 3006, item.Name, 3006, 10, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3016, "\'},", 3016, 3, true);
            WriteAttributeValue(" ", 3019, "function", 3020, 9, true);
            WriteAttributeValue(" ", 3028, "(res)", 3029, 6, true);
            WriteAttributeValue(" ", 3034, "{", 3035, 2, true);
            WriteAttributeValue(" ", 3036, "$(\'#ajaxresult\').html(res)", 3037, 27, true);
            WriteAttributeValue(" ", 3063, "})", 3064, 3, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                    </a>\r\n                                    <a href=\"#\" class=\'delete_btn\' title=\"Delete Organization\" style=\"margin-top:19px;\"");
            BeginWriteAttribute("onclick", " onclick=\"", 3231, "\"", 3289, 6);
            WriteAttributeValue("", 3241, "DeleteOrganizationById(\'", 3241, 24, true);
#nullable restore
#line 67 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
WriteAttributeValue("", 3265, item.Id, 3265, 8, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3273, "\',", 3273, 2, true);
            WriteAttributeValue(" ", 3275, "\'", 3276, 2, true);
#nullable restore
#line 67 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
WriteAttributeValue("", 3277, item.Name, 3277, 10, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3287, "\')", 3287, 2, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                    </a>\r\n");
#nullable restore
#line 69 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                     if (item.Status == 0)
                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58614806", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 72 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                    }
                                    else if (item.Status == 1)
                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58616244", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 76 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                                                                                                                                                                                                                 
                                    }
                                    else
                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58617903", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 81 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                </div>\r\n                                <ul>\r\n");
#nullable restore
#line 84 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                     foreach (var _item in Model.OrganizationList.Where(x => x.ChildOrgId == item.Id && x.SubChildOrgId == 0))
                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        <li>\r\n                                            <div>\r\n                                                <span class=\"text\">");
#nullable restore
#line 88 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                              Write(_item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n");
#nullable restore
#line 89 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                 if (_item.Status == 0)
                                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                    <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58620493", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 92 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                }
                                                else if (_item.Status == 1)
                                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                    <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58621980", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 96 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                                                                                                                                                                                                                              
                                                }
                                                else
                                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                    <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58623700", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 101 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            </div>\r\n                                            <ul>\r\n");
#nullable restore
#line 104 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                 foreach (var __item in Model.OrganizationList.Where(x => x.ChildOrgId == 0 && x.SubChildOrgId == _item.Id))
                                                {
                                                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 106 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                     if (!String.IsNullOrEmpty(__item.Name))
                                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                        <li>\r\n                                                            <div>\r\n                                                                <span class=\"text\">");
#nullable restore
#line 110 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                                              Write(__item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n");
#nullable restore
#line 111 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                                 if (__item.Status == 0)
                                                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                                    <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58626840", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 114 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                                }
                                                                else if (__item.Status == 1)
                                                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                                    <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58628393", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 118 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                                                                                                                                                                                                                                               
                                                                }
                                                                else
                                                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                                    <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58630195", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n");
#nullable restore
#line 123 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                            </div>\r\n                                                        </li>\r\n");
#nullable restore
#line 126 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 126 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                                     
                                                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            </ul>\r\n                                        </li>\r\n");
#nullable restore
#line 130 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                </ul>\r\n                            </li>\r\n");
#nullable restore
#line 133 "D:\Binit\New folder\Code\Areas\ApplicationManagement\Views\Organogram\TabMenu.cshtml"
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                    </ul>\r\n                    <div class=\"clsHints\">\r\n                        <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58632923", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("Clients are not ready for Assessment</span>\r\n                        <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58634042", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("Client ready for Assessment, Click on ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58635107", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(" to activate</span>\r\n                        <span class=\"icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3c6206f5e2d2b5057a8b510b65c7ebebbe66c58636202", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"Assessment Activated</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function Active(anchor) {
        $('.cardbtnIn').removeClass('active');
        $(anchor).children('div').addClass('active');
    }
    $("".orgmap .text"").click(function () {
        $(this).parent().parent().toggleClass(""inactive"");
    })
    function DeleteOrganizationById(parentOrgId, parentOrgName) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You are about to delete all functions and process mapped to ' + parentOrgName,
            showCancelButton: true,
            confirmButtonText: 'Delete',
        }).then((result) => {
            if (result.isConfirmed) {
                $('#preloader').show();
                $.get('/ApplicationManagement/Organization/DeleteOrganizationById', { parentOrgId: parentOrgId }, function (res) {
                    if (res.isSuccess) {
                        $('#pr");
            WriteLiteral(@"eloader').hide();
                        toastr.success(parentOrgName + ' deleted successfully');
                        $('a[href=""/ApplicationManagement/ChildOrganization/Index""]').click();
                    } else {
                        toastr.warning('something went wrong!');
                    }
                })
            }
        })
    }
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CyberaniumModels.RBAC> Html { get; private set; }
    }
}
#pragma warning restore 1591
