﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="InternationalStandardController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class InternationalStandardController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="InternationalStandardController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public InternationalStandardController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The GetAllInternationalStandard.
        /// </summary>
        /// <returns>The <see cref="Task{List{InternationalStandard}}"/>.</returns>
        [HttpGet(nameof(GetAllInternationalStandard))]
        public async Task<List<InternationalStandard>> GetAllInternationalStandard()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<InternationalStandard>("[dbo].[SP_GetAllInternationalStandard]", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(Edit))]
        public async Task<InternationalStandard> Edit(long id)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("id", id, DbType.Int32);
            return await Task.FromResult(_dapper.Get<InternationalStandard>("SP_EditInternationalStandard", dbparams, CommandType.StoredProcedure));
        }
        /// <summary>
        /// The SaveInternationalStandard.
        /// </summary>
        /// <param name="InternationalStandard">The InternationalStandard<see cref="InternationalStandard"/>.</param>
        /// <returns>The <see cref="Task{InternationalStandard}"/>.</returns>
        [HttpPost(nameof(SaveInternationalStandard))]
        public async Task<InternationalStandard> SaveInternationalStandard(InternationalStandard InternationalStandard)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("Id", InternationalStandard.Id, DbType.Int32);
                dbparams.Add("TopicId", InternationalStandard.TopicId, DbType.Int16);
                dbparams.Add("Title", InternationalStandard.Title, DbType.String);
                dbparams.Add("IP", InternationalStandard.CreatedIP, DbType.String);
                dbparams.Add("UserID", InternationalStandard.CreatedBy, DbType.Int32);
                var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_InternationalStandard]", dbparams, CommandType.StoredProcedure));
                return new InternationalStandard() { Id = result };
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// The UpdateInternationalStandard.
        /// </summary>
        /// <param name="InternationalStandard">The InternationalStandard<see cref="InternationalStandard"/>.</param>
        /// <returns>The <see cref="Task{InternationalStandard}"/>.</returns>
        [HttpPost(nameof(UpdateInternationalStandard))]
        public async Task<InternationalStandard> UpdateInternationalStandard(InternationalStandard InternationalStandard)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", InternationalStandard.Id, DbType.Int32);
            dbparams.Add("TopicId", InternationalStandard.TopicId, DbType.Int16);
            dbparams.Add("Title", InternationalStandard.Title, DbType.String);
            dbparams.Add("IP", InternationalStandard.CreatedIP, DbType.String);
            dbparams.Add("UserID", InternationalStandard.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_Update_InternationalStandard]", dbparams, CommandType.StoredProcedure));
            return new InternationalStandard() { Id = result };
        }

        /// <summary>
        /// The BlockDeleteInternationalStandard.
        /// </summary>
        /// <param name="InternationalStandard">The InternationalStandard<see cref="InternationalStandard"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPost(nameof(BlockDeleteInternationalStandard))]
        public async Task<InternationalStandard> BlockDeleteInternationalStandard(InternationalStandard InternationalStandard)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", InternationalStandard.Id, DbType.Int32);
            dbparams.Add("Title", InternationalStandard.Title, DbType.String);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteInternationalStandard]", dbparams, CommandType.StoredProcedure));
            return new InternationalStandard() { Id = result };
        }

        [HttpGet(nameof(GetInternationalStandard))]
        public async Task<List<InternationalStandard>> GetInternationalStandard()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<InternationalStandard>("proc_GetInternationalStandard", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetInternationalStandardVersion))]
        public async Task<List<InternationalStandard>> GetInternationalStandardVersion(string StandardID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("StandardID", StandardID, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<InternationalStandard>("proc_GetInternationalStandardVersion", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetMappedStandardByICDId))]
        public async Task<List<InternationalStandard>> GetMappedStandardByICDId(long ICDId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ICDId", ICDId, DbType.Int64);
            var result = await Task.FromResult(_dapper.GetAll<InternationalStandard>("proc_GetMappedStandardByICDId", dbparams, CommandType.StoredProcedure));
            return result;
        }
    }
}
