﻿using CyberaniumApi.Helpers;
using CyberaniumApi.IServices;
using CyberaniumModels;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CyberaniumApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SFDCController : Controller
    {
        private readonly IDapper _dapper;
        public SFDCController(IDapper dapper)
        {
            _dapper = dapper;
        }
        [HttpPost(nameof(GetDomainBaseRiskScore))]
        public async Task<Message<List<DomainBaseRiskScore>>> GetDomainBaseRiskScore(ProcessesAssign model)
        {
            var obj = new Message<List<DomainBaseRiskScore>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("ConCatId", model.ConCatId, DbType.Int32);
            dbparams.Add("AnswerId", model.AnswerId, DbType.Int64);
            var result = await Task.FromResult(_dapper.GetAll<DomainBaseRiskScore>("[dbo].[SP_GetDomainBaseRiskScore]", dbparams, CommandType.StoredProcedure));
            obj.Data = result;
            return obj;
        }
        [HttpPost(nameof(SubmitDataByAnswer))]
        public async Task<Message<int>> SubmitDataByAnswer(DomainBaseRiskScore model)
        {
            var message = new Message<int>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", model.Id, DbType.Int32);
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("CategoryId", model.CategoryId, DbType.Int64);
            dbparams.Add("AnswerId", model.AnswerId, DbType.Int64);
            dbparams.Add("DomainBaseRiskScoreXML", obj.CreateDomainBaseRiskScoreXML(model.DomainBaseRiskScoreXML), DbType.String);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            dbparams.Add("UserID", model.CreatedBy, DbType.Int64);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitDataByAnswer]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = 200;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = 505;
                message.IsSuccess = false;
                message.ReturnMessage = "Internal server error";
            }
            return message;
        }

        [HttpPost(nameof(GetDomainCritiScoreByfunIDAndProcessID))]
        public async Task<Message<List<DomainBaseRiskScore>>> GetDomainCritiScoreByfunIDAndProcessID(ProcessesAssign model)
        {
            var obj = new Message<List<DomainBaseRiskScore>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("FunctionId", model.FunctionId, DbType.Int32);
            dbparams.Add("ProcessId", model.ProcessId, DbType.Int64);
            var result = await Task.FromResult(_dapper.GetAll<DomainBaseRiskScore>("proc_GetDmCritiScoreByfunIDAndProcessID", dbparams, CommandType.StoredProcedure));
            obj.Data = result;
            return obj;
        }
    }
}
