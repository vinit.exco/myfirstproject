﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="EnumTypeController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EnumTypeController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumTypeController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public EnumTypeController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The GetAllEnumType.
        /// </summary>
        /// <returns>The <see cref="Task{List{EnumType}}"/>.</returns>
        [HttpGet(nameof(GetAllEnumType))]
        public async Task<List<EnumType>> GetAllEnumType()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<EnumType>("[dbo].[SP_GetAllEnumType]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The SaveEnumType.
        /// </summary>
        /// <param name="EnumType">The EnumType<see cref="EnumType"/>.</param>
        /// <returns>The <see cref="Task{EnumType}"/>.</returns>
        [HttpPost(nameof(SaveEnumType))]
        public async Task<EnumType> SaveEnumType(EnumType EnumType)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Name", EnumType.Name, DbType.String);
            dbparams.Add("Title", EnumType.Title, DbType.String);
            dbparams.Add("IP", EnumType.CreatedIP, DbType.String);
            dbparams.Add("UserID", EnumType.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_EnumType]", dbparams, CommandType.StoredProcedure));
            return new EnumType() { Id = result };
        }

        /// <summary>
        /// The UpdateEnumType.
        /// </summary>
        /// <param name="EnumType">The EnumType<see cref="EnumType"/>.</param>
        /// <returns>The <see cref="Task{EnumType}"/>.</returns>
        [HttpPost(nameof(UpdateEnumType))]
        public async Task<EnumType> UpdateEnumType(EnumType EnumType)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", EnumType.Id, DbType.Int32);
            dbparams.Add("Name", EnumType.Name, DbType.String);
            dbparams.Add("Title", EnumType.Title, DbType.String);
            dbparams.Add("IP", EnumType.CreatedIP, DbType.String);
            dbparams.Add("UserID", EnumType.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_Update_EnumType]", dbparams, CommandType.StoredProcedure));
            return new EnumType() { Id = result };
        }

        /// <summary>
        /// The BlockDeleteEnumType.
        /// </summary>
        /// <param name="EnumType">The EnumType<see cref="EnumType"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPost(nameof(BlockDeleteEnumType))]
        public async Task<Message<EnumType>> BlockDeleteEnumType(EnumType EnumType)
        {
            Message<EnumType> message = new Message<EnumType>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", EnumType.Id, DbType.Int32);
            dbparams.Add("Title", EnumType.Title, DbType.String);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteEnumType]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = new EnumType();
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new EnumType();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
    }
}
