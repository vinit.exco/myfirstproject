﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="RoleMenuAccessController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class RoleMenuAccessController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleMenuAccessController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public RoleMenuAccessController(IDapper dapper)
        {
            _dapper = dapper;
        }

        [HttpPost(nameof(SubmitRoleMenuAccess))]
        public async Task<Message<RoleMenuAccess>> SubmitRoleMenuAccess(RoleMenuAccess data)
        {
            Message<RoleMenuAccess> message = new Message<RoleMenuAccess>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("RoleId", data.RoleId, DbType.Int16);
            dbparams.Add("MenuId", data.MenuId, DbType.Int32);
            dbparams.Add("ParentMenuId", data.ParentMenuId, DbType.Int32);
            dbparams.Add("ChildMenuId", data.ChildMenuId, DbType.Int32);
            dbparams.Add("SubMenuId", data.SubMenuId, DbType.Int32);
            dbparams.Add("CanViewAll", data.CanViewAll, DbType.Boolean);
            dbparams.Add("CanCreate", data.CanCreate, DbType.Boolean);
            dbparams.Add("CanView", data.CanView, DbType.Boolean);
            dbparams.Add("CanEdit", data.CanEdit, DbType.Boolean);
            dbparams.Add("CanUpdate", data.CanUpdate, DbType.Boolean);
            dbparams.Add("CanDeleted", data.CanDeleted, DbType.Boolean);
            dbparams.Add("CanApproveReject", data.CanApproveReject, DbType.Boolean);
            dbparams.Add("CanBlockUnblock", data.CanBlockUnblock, DbType.Boolean);
            dbparams.Add("CanPrint", data.CanPrint, DbType.Boolean);
            dbparams.Add("CanDownload", data.CanDownload, DbType.Boolean);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitRoleMenuAccess]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetRoleMenuAccess))]
        public async Task<List<RoleMenuAccess>> GetRoleMenuAccess()
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", 0, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<RoleMenuAccess>("[dbo].[SP_GetRoleMenuAccess]", dbparams, CommandType.StoredProcedure));
            return result;
        }

    }
}
