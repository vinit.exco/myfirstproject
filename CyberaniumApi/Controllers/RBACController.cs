﻿using Microsoft.AspNetCore.Http;

namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    

    [Route("api/[controller]")]
    [ApiController]
    /// <summary>
    /// Defines the <see cref="RBACController" />.
    /// </summary>
    public class RBACController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;
        /// <summary>
        /// Initializes a new instance of the <see cref="RBACController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public RBACController(IDapper dapper)
        {
            _dapper = dapper;
        }
        [HttpPost(nameof(SubmitRole))]
        public async Task<Message<RBAC>> SubmitRole(RBAC rBAC)
        {
            Message<RBAC> message = new Message<RBAC>();
            var XML = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentMenuId", XML.CreateRBACXML(rBAC.RBACXML), DbType.Xml);
            dbparams.Add("Role", rBAC.RoleId, DbType.Int32);
            dbparams.Add("IP", rBAC.CreatedIP, DbType.String);
            dbparams.Add("UserID", rBAC.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SubmitRole", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = rBAC;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = rBAC;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpGet(nameof(GetRoleById))]
        public async Task<List<RBAC>> GetRoleById(int id)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("RoleId", id, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<RBAC>("SP_GetRoleById", dbparams, CommandType.StoredProcedure));
            return result;
        }
        
    }
}