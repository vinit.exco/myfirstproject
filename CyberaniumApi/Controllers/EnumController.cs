﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="EnumController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EnumController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public EnumController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The GetAllEnum.
        /// </summary>
        /// <returns>The <see cref="Task{List{Enum}}"/>.</returns>
        [HttpGet(nameof(GetAllEnum))]
        public async Task<List<Enum>> GetAllEnum()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<Enum>("[dbo].[SP_GetAllEnum]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The SaveEnum.
        /// </summary>
        /// <param name="Enum">The Enum<see cref="Enum"/>.</param>
        /// <returns>The <see cref="Task{Enum}"/>.</returns>
        [HttpPost(nameof(SaveEnum))]
        public async Task<Enum> SaveEnum(Enum Enum)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("EnumTypeId", Enum.EnumTypeId, DbType.Int32);
            dbparams.Add("Value", Enum.Value, DbType.Int32);
            dbparams.Add("Name", Enum.Name, DbType.String);
            dbparams.Add("Title", Enum.Title, DbType.String);
            dbparams.Add("IP", Enum.CreatedIP, DbType.String);
            dbparams.Add("UserID", Enum.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_Enum]", dbparams, CommandType.StoredProcedure));
            return new Enum() { Id = result };
        }

        /// <summary>
        /// The UpdateEnum.
        /// </summary>
        /// <param name="Enum">The Enum<see cref="Enum"/>.</param>
        /// <returns>The <see cref="Task{Enum}"/>.</returns>
        [HttpPost(nameof(UpdateEnum))]
        public async Task<Enum> UpdateEnum(Enum Enum)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", Enum.Id, DbType.Int32);
            dbparams.Add("EnumTypeId", Enum.EnumTypeId, DbType.Int32);
            dbparams.Add("Value", Enum.Value, DbType.Int32);
            dbparams.Add("Name", Enum.Name, DbType.String);
            dbparams.Add("Title", Enum.Title, DbType.String);
            dbparams.Add("IP", Enum.CreatedIP, DbType.String);
            dbparams.Add("UserID", Enum.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_Update_Enum]", dbparams, CommandType.StoredProcedure));
            return new Enum() { Id = result };
        }

        /// <summary>
        /// The BlockDeleteEnum.
        /// </summary>
        /// <param name="Enum">The Enum<see cref="Enum"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPost(nameof(BlockDeleteEnum))]
        public async Task<Message<Enum>> BlockDeleteEnum(Enum Enum)
        {
            Message<Enum> message = new Message<Enum>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", Enum.Id, DbType.Int32);
            dbparams.Add("Title", Enum.Title, DbType.String);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteEnum]", dbparams, CommandType.StoredProcedure));
            message.Data = new Enum() { Id = result };
            message.IsSuccess = true;
            return message;
        }

        /// <summary>
        /// The GetHighestValueForEnum.
        /// </summary>
        /// <param name="Enum">The Enum<see cref="Enum"/>.</param>
        /// <returns>The <see cref="Task{Message{Enum}}"/>.</returns>
        [HttpPost(nameof(GetHighestValueForEnum))]
        public async Task<Message<Enum>> GetHighestValueForEnum(Enum Enum)
        {
            var message = new Message<Enum>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", Enum.Id, DbType.Int32);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_HighestValueForEnum]", dbparams, CommandType.StoredProcedure));
            message.Data = new Enum() { Id = result };
            message.IsSuccess = true;
            return message;
        }

        /// <summary>
        /// The GetEnumByType.
        /// </summary>
        /// <param name="EnumType">The EnumType<see cref="int?"/>.</param>
        /// <returns>The <see cref="Task{List{Enum}}"/>.</returns>
        [HttpGet(nameof(GetEnumByType))]
        public async Task<List<Enum>> GetEnumByType(int? EnumType)
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<Enum>($"Select * From Enum where EnumTypeId ={EnumType} and IsDeleted=0", dbparams, CommandType.Text));
            return result;
        }

        [HttpPost(nameof(GetControlCatByProcessId))]
        public async Task<Message<List<Enum>>> GetControlCatByProcessId(ProcessesAssign model)
        {
            var message = new Message<List<Enum>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("FunctionId", model.FunctionId, DbType.Int64);
            dbparams.Add("ProcessId", model.ProcessId, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<Enum>("[dbo].[SP_GetControlCatByProcessId]", dbparams, CommandType.StoredProcedure));
            message.Data = res;
            message.IsSuccess = true;
            return message;
        }

        [HttpPost(nameof(GetMISReportId))]
        public async Task<Message<List<RBAC>>> GetMISReportId(RBAC model)
        {
            var message = new Message<List<RBAC>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("UserID", model.UserID, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<RBAC>("[dbo].[Sp_MisReport]", dbparams, CommandType.StoredProcedure));
            message.Data = res;
            message.IsSuccess = true;
            return message;
        }

        [HttpGet(nameof(GetAddparticipant))]
        public async Task<List<Compliance>> GetAddparticipant(int id)
        {
            var message = new List<Compliance>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ConCatId", id, DbType.Int32);
            var res = await Task.FromResult(_dapper.GetAll<Compliance>("[dbo].[GetEnablerQuestionByClientCategory]", dbparams, CommandType.StoredProcedure));
           
            return res;
        }

        [HttpGet(nameof(GetQuestionnaire))]
        public async Task<List<ClientEnablers>> GetQuestionnaire(int id,int OrganizationId)
        {
            var message = new List<ClientEnablers>();
            var dbparams = new DynamicParameters();
            dbparams.Add("CategoryId", id, DbType.Int32);
            dbparams.Add("OrganizationId", OrganizationId, DbType.Int32);
            var res = await Task.FromResult(_dapper.GetAll<ClientEnablers>("[dbo].[sp_GetQuestionnaire]", dbparams, CommandType.StoredProcedure));

            return res;
        }

        [HttpGet(nameof(GetOrganizationId))]
        public async Task<List<ClientEnablers>> GetOrganizationId(int id)
        {
            var message = new List<ClientEnablers>();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrganizationId", id, DbType.Int32);
            var res = await Task.FromResult(_dapper.GetAll<ClientEnablers>("[dbo].[sp_GetOrganizationId]", dbparams, CommandType.StoredProcedure));

            return res;
        }

        [HttpGet(nameof(GetOrganizationOrder))]
        public async Task<List<Enum>> GetOrganizationOrder()
        {
            var message = new List<Enum>();
            var dbparams = new DynamicParameters();
            var res = await Task.FromResult(_dapper.GetAll<Enum>("[dbo].[Sp_OrganizationOrder]", dbparams, CommandType.StoredProcedure));
            return res;
        }

        [HttpGet(nameof(GetOrganizationMainOrder))]
        public async Task<List<Enum>> GetOrganizationMainOrder()
        {
            var message = new List<Enum>();
            var dbparams = new DynamicParameters();
            var res = await Task.FromResult(_dapper.GetAll<Enum>("[dbo].[Sp_OrganizationMainOrder]", dbparams, CommandType.StoredProcedure));
            return res;
        }

        [HttpPost(nameof(GetProcesssCount))]
        public async Task<Message<List<RBAC>>> GetProcesssCount(RBAC model)
        {
            var message = new Message<List<RBAC>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("UserID", model.UserID, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<RBAC>("proc_GetProcessCount", dbparams, CommandType.StoredProcedure));
            message.Data = res;
            message.IsSuccess = true;
            return message;
        }

        [HttpPost(nameof(GetBaseRiskScore))]
        public async Task<Message<List<RBAC>>> GetBaseRiskScore(RBAC model)
        {
            var message = new Message<List<RBAC>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("UserID", model.UserID, DbType.Int64);
            dbparams.Add("CategoryId", model.CategoryId, DbType.Int32);
            var res = await Task.FromResult(_dapper.GetAll<RBAC>("[dbo].[Sp_GetBaseRiskScore]", dbparams, CommandType.StoredProcedure));
            message.Data = res;
            message.IsSuccess = true;
            return message;
        }
    }
}
