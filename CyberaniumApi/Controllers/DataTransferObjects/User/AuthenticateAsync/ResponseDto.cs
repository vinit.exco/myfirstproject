namespace CyberaniumApi.Controllers.DataTransferObjects.User.AuthenticateAsync
{
    using System;

    public class ResponseDto
    {
        public Guid Id { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }
    }
}
