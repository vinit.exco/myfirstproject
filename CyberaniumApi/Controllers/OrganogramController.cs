﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using CyberaniumModels.Comman;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Data.SqlClient;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    [ApiController]
    public class OrganogramController : Controller
    {
        private readonly IDapper _dapper;
        private readonly IConfiguration _config;
        private string Connectionstring = "DevConnection";
        SqlConnection con;
        public OrganogramController(IDapper dapper, IConfiguration _config)
        {
            _dapper = dapper;
            con = new SqlConnection(_config.GetConnectionString(Connectionstring));
        }

        [HttpPost(nameof(UpdateAssigntoConsultant))]
        public async Task<Message<Organization>> UpdateAssigntoConsultant(Organization data)
        {
            Message<Organization> message = new Message<Organization>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("ConsultantIds", data.ConsultantIds, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_UpdateAssigntoConsultant", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        //[HttpPost(nameof(SaveAssignedOrganizationUser))]
        //public async Task<Message<Organization>> SaveAssignedOrganizationUser(Organization data)
        //{
        //    Message<Organization> message = new Message<Organization>();
            
        //    var dbparams = new DynamicParameters();
        //    dbparams.Add("OrgId", data.OrgID, DbType.Int32);
        //    dbparams.Add("Id", data.AssgnedIDs, DbType.String);
        //    var result = await Task.FromResult(_dapper.Insert<int>("proc_SaveAssignedOrganizationUser", dbparams, CommandType.StoredProcedure));
        //    if (result == 0)
        //    {
        //        message.Data = data;
        //        message.IsSuccess = true;
        //        message.ReturnMessage = "Success";
        //    }
        //    else
        //    {
        //        message.Data = data;
        //        message.IsSuccess = false;
        //        message.ReturnMessage = "Success";
        //    }
        //    return message;
        //}

        [HttpPost(nameof(SaveAssignedOrganizationUser))]
        public async Task<Message<Organization>> SaveAssignedOrganizationUser(Organization data)
        {
            var obj = new XMLCreater();
            Message<Organization> message = new Message<Organization>();
            var dbparams = new DynamicParameters();
            try
            {
                dbparams.Add("OrgId", data.OrgID, DbType.Int32);
                dbparams.Add("EmpIds", data.EmployeId, DbType.Int64);
                dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int64);
                dbparams.Add("OrgAssigneToConsultantMapXML", obj.OrgAssigneToConsultantMap(data.OrganizationXML), DbType.Xml);
                var result = await Task.FromResult(_dapper.Insert<int>("Sp_OrgAssigneToConsultantMap", dbparams, CommandType.StoredProcedure));
                if (result == 0)
                {
                    message.Data = data;
                    message.IsSuccess = true;
                    message.ReturnMessage = "Success";
                }
                else
                {
                    message.Data = data;
                    message.IsSuccess = false;
                    message.ReturnMessage = "Success";
                }
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        #region Child Organization
        [HttpPost(nameof(SubmitChildOrganization))]
        public async Task<Message<int>> SubmitChildOrganization(OrganizationChild data)
        {
            Message<int> message = new Message<int>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("OrganizationId", data.ParentOrgId, DbType.Int32);
            dbparams.Add("ChildOrgName", data.ChildOrgName, DbType.String);
            dbparams.Add("ChildOrgLocation", data.ChildOrgLocation, DbType.String);
            dbparams.Add("CountryId", data.CountryID, DbType.Int32);
            dbparams.Add("StateId", data.StateId, DbType.Int32);
            dbparams.Add("CityId", data.CityId, DbType.Int32);
            dbparams.Add("ConsultantIds", data.ConsultantIds, DbType.String);
            dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", data.CreatedIP, DbType.String);
            dbparams.Add("Xml", obj.xmlOrganizationChildAudity(data.xmlAudity), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitChildOrganization]", dbparams, CommandType.StoredProcedure));
            if (result > 0)
            {
                message.Data = result;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = result;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(GetChildCompanyListOnClientWise))]
        public async Task<Message<List<OrganizationChild>>> GetChildCompanyListOnClientWise(OrganizationChild model)
        {
            var message = new Message<List<OrganizationChild>>();
            var item = await Task.FromResult(_dapper.GetAll<OrganizationChild>($"Select * from [OrganizationChildMap] where OrganizationId = {model.Id}", null, commandType: CommandType.Text));
            message.Data = item;
            return message;
        }

        [HttpGet(nameof(GetChildOrganizationList))]
        public async Task<List<OrganizationChild>> GetChildOrganizationList()
        {
            var result = await Task.FromResult(_dapper.GetAll<OrganizationChild>("proc_GetChildOrganizationByCliendId", null, commandType: CommandType.Text));
            return result;
        }

        [HttpGet(nameof(GetChildOrganizationByParentOrgID))]
        public async Task<List<Organization>> GetChildOrganizationByParentOrgID(long ParentOrgID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgID", ParentOrgID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("Proc_GetChildOrganizationByParentOrgID", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetChildOrganizationAudityListByID))]
        public async Task<List<ClientAudity>> GetChildOrganizationAudityListByID(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<ClientAudity>("proc_GetChildOrganizationAudityListByID", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpPost(nameof(updateOrgSignStaus))]
        public async Task<Message<int>> updateOrgSignStaus(OrganizationChild data)
        {
            Message<int> message = new Message<int>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", data.Id, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[proc_updateOrgSignStaus]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = result;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = result;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        #endregion

        #region Organization Function
        [HttpPost(nameof(SaveOrganizationFunction))]
        public async Task<Message<OrganizationFunctions>> SaveOrganizationFunction(OrganizationFunctions data)
        {
            var message = new Message<OrganizationFunctions>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int64);
            dbparams.Add("OrgID", data.OrgID, DbType.Int64);
            dbparams.Add("Xml", obj.CreateFunctionProcessXML(data.xmlFunction), DbType.Xml);
            dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", data.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("Proc_SaveUpdateOrganizationFunction", dbparams, CommandType.StoredProcedure));
            data.xmlFunction = "";
            if (result > 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetOrganizationFunctionList))]
        public async Task<List<OrganizationFunctions>> GetOrganizationFunctionList(int EmployeeId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("EmployeeId", EmployeeId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<OrganizationFunctions>("Proc_GetOrganizationFunctionList", dbparams, commandType: CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetOrganizationFunctionByID))]
        public async Task<OrganizationFunctions> GetOrganizationFunctionByID(int ID)
        {
            OrganizationFunctions obj = new OrganizationFunctions();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            obj = await Task.FromResult(_dapper.Get<OrganizationFunctions>("proc_GetOrganizationFunctionByID", dbparams, commandType: CommandType.StoredProcedure));
            obj.OrgFunctionListMap = await Task.FromResult(_dapper.GetAll<OrganizationFunctions>("Proc_GetOrganizationFunctionMapList", dbparams, commandType: CommandType.StoredProcedure));
            return obj;
        }

        [HttpGet(nameof(GetFunctionByOrganization))]
        public async Task<List<OrganizationFunctions>> GetFunctionByOrganization(long OrgID)
        {
            OrganizationFunctions obj = new OrganizationFunctions();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<OrganizationFunctions>("proc_GetFunctionByOrganization", dbparams, commandType: CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetFunctionByChildOrganization))]
        public async Task<List<OrganizationFunctions>> GetFunctionByChildOrganization(long ChildId)
        {
            OrganizationFunctions obj = new OrganizationFunctions();
            var dbparams = new DynamicParameters();
            dbparams.Add("ChildId", ChildId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<OrganizationFunctions>("proc_GetFunctionByChildOrganization", dbparams, commandType: CommandType.StoredProcedure));
            return result;
        }
        #endregion

        #region Function Process
        [HttpPost(nameof(SubmitFunctionProcess))]
        public async Task<Message<OrganizationProcesss>> SubmitFunctionProcess(OrganizationProcesss data)
        {
            Message<OrganizationProcesss> message = new Message<OrganizationProcesss>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("OrganizationParentID", data.ParentOrgId, DbType.Int32);
            dbparams.Add("OrganizationChildID", data.ChildOrgId, DbType.Int32);
            dbparams.Add("FunctionID", data.FunctionId, DbType.Int32);
            dbparams.Add("IsType", data.IsType, DbType.Int32);
            dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", data.CreatedIP, DbType.String);
            //dbparams.Add("XmlProcess", obj.CreateFunctionProcessXml(data.xmlProcess), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[proc_SubmitFunctionProcess]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetFunctionProcessList))]
        public async Task<List<OrganizationProcesss>> GetFunctionProcessList()
        {
            var result = await Task.FromResult(_dapper.GetAll<OrganizationProcesss>("proc_GetFunctionProcessList", null, commandType: CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetFunctionProcessByID))]
        public async Task<OrganizationProcesss> GetFunctionProcessByID(int ID)
        {
            OrganizationProcesss obj = new OrganizationProcesss();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            obj = await Task.FromResult(_dapper.Get<OrganizationProcesss>("proc_GetFunctionProcessByID", dbparams, commandType: CommandType.StoredProcedure));
            obj.FunctionProcessMapList = await Task.FromResult(_dapper.GetAll<OrganizationProcesss>("proc_GetFunctionProcessMapByID", dbparams, commandType: CommandType.StoredProcedure));
            return obj;
        }
        [HttpGet(nameof(CheckFunProDuplicate))]
        public async Task<FunctionProcess> CheckFunProDuplicate(long orgId, string FunProName)
        {
            FunctionProcess obj = new FunctionProcess();
            var dbparams = new DynamicParameters();
            dbparams.Add("orgId", orgId, DbType.String);
            dbparams.Add("FunProName", FunProName, DbType.String);
            obj.CountFunction = await Task.FromResult(_dapper.Get<int>("sp_CheckDuplicateFunction", dbparams, commandType: CommandType.StoredProcedure));
            obj.CountProcess = await Task.FromResult(_dapper.Get<int>("sp_CheckDuplicateProcess", dbparams, commandType: CommandType.StoredProcedure));
            return obj;
        }
        [HttpGet(nameof(GetTaskCountByEmpId))]
        public async Task<List<RiskAssessment>> GetTaskCountByEmpId(int EmpId)
        {
          
            var dbparams = new DynamicParameters();
            dbparams.Add("EmpId", EmpId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<RiskAssessment>("proc_GetTaskCountByEmpId", dbparams, commandType: CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetProcessesByOrgId))]
        public async Task<List<FunctionProcess>> GetProcessesByOrgId(long orgId)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("OrgId", orgId, DbType.Int64);
                return await Task.FromResult(_dapper.GetAll<FunctionProcess>("SP_GetProcessesByOrgId", dbparams, CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
        }

        [HttpGet(nameof(GetProcessesForMis))]
        public async Task<List<FunctionProcess>> GetProcessesForMis(long orgId, int FunctionId, int ProcessId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", orgId, DbType.Int64);
            dbparams.Add("FunctionId", FunctionId, DbType.Int32);
            dbparams.Add("ProcessId", ProcessId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<FunctionProcess>("Sp_FinalScoreByPprocess", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetFunctionForMis))]
        public async Task<List<FunctionProcess>> GetFunctionForMis(long orgId,long FunctionId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", orgId, DbType.Int64);
            dbparams.Add("FunctionId", FunctionId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<FunctionProcess>("Sp_FinalScoreByFunction", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetOraganizationForMis))]
        public async Task<List<FunctionProcess>> GetOraganizationForMis(long orgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", orgId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<FunctionProcess>("Sp_FinalScoreByOrg", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetProcesses))]
        public async Task<List<OrganizationProcesss>> GetProcesses(int employeeId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("EmployeeId", employeeId, DbType.Int64);
            var result = await Task.FromResult(_dapper.GetAll<OrganizationProcesss>("proc_GetProcesses", dbparams, commandType: CommandType.StoredProcedure));
            return result;
        }
        #endregion

        #region Compliance RCM
        [HttpGet(nameof(GetQuestionBanklist))]
        public async Task<List<RiskAssessmentRCM>> GetQuestionBanklist(long orgId)
        {
            return await Task.FromResult(_dapper.GetAll<RiskAssessmentRCM>("proc_GetQuestionBanklist", null, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetQuestionStandardWise))]
        public async Task<List<RiskAssessmentRCM>> GetQuestionStandardWise(string QuestionsID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("QuestionID", QuestionsID, DbType.String);
            return await Task.FromResult(_dapper.GetAll<RiskAssessmentRCM>("proc_GetQuestionStandardWise", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetQuestionRegoWise))]
        public async Task<List<RiskAssessmentRCM>> GetQuestionRegoWise(string SectorId, string CountryId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("SectorId", SectorId, DbType.String);
            dbparams.Add("CountryId", CountryId, DbType.String);
            return await Task.FromResult(_dapper.GetAll<RiskAssessmentRCM>("proc_GetQuestionRegoWise", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetRegulationBySectorAndCountryWise))]
        public async Task<List<RiskAssessmentRCM>> GetRegulationBySectorAndCountryWise(string SectorId, string CountryId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("SectorId", SectorId, DbType.String);
            dbparams.Add("CountryId", CountryId, DbType.String);
            return await Task.FromResult(_dapper.GetAll<RiskAssessmentRCM>("proc_GetRegulationBySectorAndCountryWise", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(SubmitStandardRegulationRCM))]
        public async Task<Message<RiskAssessmentRCM>> SubmitStandardRegulationRCM(RiskAssessmentRCM data)
        {
            Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.ID, DbType.Int32);
            dbparams.Add("FromDate", data.FromDate, DbType.DateTime);
            dbparams.Add("ToDate", data.ToDate, DbType.DateTime);
            dbparams.Add("SectorID", data.SectorIDs, DbType.String);
            dbparams.Add("CountryID", data.CountryIDs, DbType.String);
            dbparams.Add("StandardID", data.StandardIds, DbType.String);
            dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", data.CreatedIP, DbType.String);
            dbparams.Add("Xml", obj.xmlStandardRegulationScore(data.xmlStandardRegulation), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("Proc_SaveStandardRegulationRCM", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }


        //[HttpPost(nameof(SaveAssignRiskAssessment))]
        //public async Task<Message<RiskAssessmentRCM>> SaveAssignRiskAssessment(RiskAssessmentRCM data)
        //{
        //    Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
        //    var dbparams = new DynamicParameters();
        //    dbparams.Add("Id", data.ID, DbType.Int32);
        //    dbparams.Add("ParentOrgId", data.ParentOrgId, DbType.Int32);
        //    dbparams.Add("ChildOrgId", data.ChildOrgId, DbType.Int32);
        //    dbparams.Add("AssessmentTypeID", data.AssessmentTypeID, DbType.Int32);
        //    dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
        //    dbparams.Add("CreatedIP", data.CreatedIP, DbType.String);
        //    var result = await Task.FromResult(_dapper.Insert<int>("proc_SaveAssignRiskAssessment", dbparams, CommandType.StoredProcedure));
        //    if (result == 0)
        //    {
        //        message.Data = data;
        //        message.IsSuccess = true;
        //        message.ReturnMessage = "Success";
        //    }
        //    else
        //    {
        //        message.Data = data;
        //        message.IsSuccess = false;
        //        message.ReturnMessage = "Success";
        //    }
        //    return message;
        //}

        [HttpGet(nameof(GetAssignRiskAssessmentList))]
        public async Task<List<RiskAssessmentRCM>> GetAssignRiskAssessmentList(int TypeID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("TypeID", TypeID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<RiskAssessmentRCM>("Proc_GetAssignRiskAssessment", dbparams, commandType: CommandType.StoredProcedure));
            return result;
        }


        [HttpPost(nameof(SubmitRCM))]
        public async Task<Message<RiskAssessmentRCM>> SubmitRCM(RiskAssessmentRCM data)
        {
            Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", data.OrgID, DbType.Int32);
            dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", data.CreatedIP, DbType.String);
            dbparams.Add("xml", obj.xmlGenerateRCM(data.xmlRCM), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("Proc_SaveRCM", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SaveUpdateRCMICD))]
        public async Task<Message<RiskAssessmentRCM>> SaveUpdateRCMICD(RiskAssessmentRCM data)
        {
            try
            {
                Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
                var obj = new XMLCreater();
                var dbparams = new DynamicParameters();
                dbparams.Add("ID", data.ID, DbType.Int32);
                dbparams.Add("MapId", data.MapId, DbType.Int32);
                dbparams.Add("OrgID", data.OrgID, DbType.Int32);
                dbparams.Add("DomainID", data.DomainId, DbType.Int32);
                dbparams.Add("DomainName", data.DomainName, DbType.String);
                dbparams.Add("SubDomainId", data.SubDomain, DbType.Int32);
                dbparams.Add("SubDomainName", data.SubDomainName, DbType.String);
                dbparams.Add("ControlId", data.ControlId, DbType.String);
                dbparams.Add("ControlName", data.ControlName, DbType.String);
                dbparams.Add("ControlDescription", data.ControlDescription, DbType.String);
                dbparams.Add("ControlTypeId", data.ControlTypeId, DbType.Int32);
                dbparams.Add("ControlTypeName", data.ControlTypeName, DbType.String);
                dbparams.Add("Governance", data.Governance, DbType.Boolean);
                dbparams.Add("Application", data.Application, DbType.Boolean);
                dbparams.Add("Infrastructure", data.Infrastructure, DbType.Boolean);
                dbparams.Add("Cloud", data.Cloud, DbType.Boolean);
                dbparams.Add("Networks", data.Networks, DbType.Boolean);
                dbparams.Add("People", data.People, DbType.Boolean);
                dbparams.Add("Location", data.Location, DbType.Boolean);
                dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
                dbparams.Add("CreatedIP", data.CreatedIP, DbType.String);
                dbparams.Add("RCMThreatMapXML", obj.CreateRCMThreatMapXML(data.RCMThreatMapXML), DbType.Xml);
                dbparams.Add("RCMArtefactMapXML", obj.CreateRCMArtefactMapXML(data.RCMArtefactMapXML), DbType.Xml);
                //dbparams.Add("xml", obj.xmlGenerateRCM(data.xmlRCM), DbType.Xml);
                var result = await Task.FromResult(_dapper.Insert<int>("proc_SaveUpdateRCMICD", dbparams, CommandType.StoredProcedure));
                if (result == 0)
                {
                    message.Data = data;
                    message.IsSuccess = true;
                    message.ReturnMessage = "Success";
                }
                else
                {
                    message.Data = data;
                    message.IsSuccess = false;
                    message.ReturnMessage = "Success";
                }
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }


        [HttpGet(nameof(GetRCMListByOrgID))]
        public async Task<List<RiskAssessmentRCM>> GetRCMListByOrgID(long OrgID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<RiskAssessmentRCM>("proc_GetRCMListByOrgID", dbparams, CommandType.StoredProcedure));
        }
        [HttpPost(nameof(GenerateRCMVersion))]
        public async Task<Message<RiskAssessmentRCM>> GenerateRCMVersion(RiskAssessmentRCM model)
        {
            var dbparams = new DynamicParameters();
            Message<RiskAssessmentRCM> message = new Message<RiskAssessmentRCM>();
            dbparams.Add("Id", model.ID, DbType.Int32);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_GenerateRCMVersion", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetRCMControlListByOrg))]
        public async Task<List<ICD>> GetRCMControlListByOrg(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ICD>("proc_GetRCMControlListByOrg", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetControlLisCustomRCM))]
        public async Task<List<ICD>> GetControlLisCustomRCM(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ICD>("proc_GetControlLisCustomRCM", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetRCMControlListByStdRegOrg))]
        public async Task<List<ICD>> GetRCMControlListByStdRegOrg(long ID, string StandardIDs)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            dbparams.Add("StandardIDs", StandardIDs, DbType.String);
            return await Task.FromResult(_dapper.GetAll<ICD>("proc_GetRCMControlListByStdRegOrg", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(DeleteRCM))]
        public async Task<Message<RiskAssessmentRCM>> DeleteRCM(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int64);
            var res = await Task.FromResult(_dapper.Get<long>("proc_DeleteRCM", dbparams, CommandType.StoredProcedure));
            return new Message<RiskAssessmentRCM> { IsSuccess = res == 0 ? true : false };
        }

        [HttpGet(nameof(GetSubDomainRCMBID))]
        public async Task<List<ICD>> GetSubDomainRCMBID(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ICD>("Proc_GetSubDomainRCMBID", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetSubDomainCustomRCM))]
        public async Task<List<ICD>> GetSubDomainCustomRCM(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ICD>("proc_GetSubDomainCustomRCM", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetSubDomainStdRegRCMBID))]
        public async Task<List<ICD>> GetSubDomainStdRegRCMBID(long ID,string StandardIDs)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            dbparams.Add("StandardIDs", StandardIDs, DbType.String);
            return await Task.FromResult(_dapper.GetAll<ICD>("Proc_GetSubDomainStdRegRCMBID", dbparams, CommandType.StoredProcedure));
        }
        [HttpPost(nameof(SubmitRCMByExcel))]
        public async Task<Message<RCM>> SubmitRCMByExcel(RCM model)
        {
            var message = new Message<RCM>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", model.OrgID, DbType.Int64);
            dbparams.Add("UserId", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            dbparams.Add("RCMXML", obj.CreateRCMXML(model.RCMXML), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SubmitRCMByExcel", dbparams, CommandType.StoredProcedure));
            if (result > 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "UnSuccess";
            }
            return message;
        }

        [HttpGet(nameof(GetRCMByID))]
        public async Task<RiskAssessmentRCM> GetRCMByID(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            return await Task.FromResult(_dapper.Get<RiskAssessmentRCM>("proc_GetRCMByID", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(DeleteControlByID))]
        public async Task<Message<RiskAssessmentRCM>> DeleteControlByID(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int64);
            var res = await Task.FromResult(_dapper.Get<long>("proc_DeleteControlByID", dbparams, CommandType.StoredProcedure));
            return new Message<RiskAssessmentRCM> { IsSuccess = res == 0 ? true : false };
        }

        [HttpGet(nameof(GetRCMControlDetails))]
        public async Task<RiskAssessmentRCM> GetRCMControlDetails(long ControlID)
        {
            var dbparams = new DynamicParameters();
            RiskAssessmentRCM obj = new RiskAssessmentRCM();
            dbparams.Add("ControlID", ControlID, DbType.Int64);
            obj = await Task.FromResult(_dapper.Get<RiskAssessmentRCM>("proc_GetRCMControlDetails", dbparams, CommandType.StoredProcedure));
            obj.Artefactlist = await Task.FromResult(_dapper.GetAll<Artefact>("proc_GetRCMControlArtefact", dbparams, CommandType.StoredProcedure));
            return obj;
        }

 
        #endregion
    }
}
