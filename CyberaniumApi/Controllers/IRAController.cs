﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using CyberaniumModels.Comman;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Enum = CyberaniumModels.Enum;

    [Route("api/[controller]")]
    [ApiController]
    public class IRAController : ControllerBase
    {

        private readonly IDapper _dapper;
        public IRAController(IDapper dapper)
        {
            _dapper = dapper;
        }

        [HttpPost(nameof(SaveAssignedProcess))]
        public async Task<Message<IRA>> SaveAssignedProcess(IRA model)
        {
            var message = new Message<IRA>();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("FunctionId", model.FunctionId, DbType.Int64);
            dbparams.Add("ProcessId", model.ProcessId, DbType.Int64);
            dbparams.Add("AssignToUserId", model.AssignToUserId, DbType.Int64);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            dbparams.Add("UserId", model.CreatedBy, DbType.Int64);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SaveAssignedProcess", dbparams, CommandType.StoredProcedure));
            if (result > 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(SaveComplianceRCM))]
        public async Task<Message<IRA>> SaveComplianceRCM(IRA model)
        {
            var message = new Message<IRA>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", model.ID, DbType.Int32);
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("AssessmentStartDate", model.StartDate, DbType.DateTime);
            dbparams.Add("AssessmentEndDate", model.EndDate, DbType.DateTime);
            dbparams.Add("AssignTo", model.AssessorID, DbType.Int64);
            dbparams.Add("AssessmentCompleteDate", model.CompletedDate, DbType.DateTime);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            dbparams.Add("RCMId", model.RCMId, DbType.Int64);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_SaveComplianceRCM", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SaveCustomRCM))]
        public async Task<Message<IRA>> SaveCustomRCM(IRA model)
        {
            var message = new Message<IRA>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", model.ID, DbType.Int32);
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("AssessmentStartDate", model.StartDate, DbType.DateTime);
            dbparams.Add("AssessmentEndDate", model.EndDate, DbType.DateTime);
            dbparams.Add("AssignTo", model.AssessorID, DbType.Int64);
            dbparams.Add("AssessmentCompleteDate", model.CompletedDate, DbType.DateTime);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            dbparams.Add("RCMId", model.RCMId, DbType.Int64);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_SaveCustomRCM", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SaveStandardAndRegulationRCM))]
        public async Task<Message<IRA>> SaveStandardAndRegulationRCM(IRA model)
        {
            var message = new Message<IRA>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", model.ID, DbType.Int32);
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("Standard", model.StandardIDs, DbType.String);
            dbparams.Add("Regulation", model.RegulationIDs, DbType.String);
            dbparams.Add("AssessmentStartDate", model.StartDate, DbType.DateTime);
            dbparams.Add("AssessmentEndDate", model.EndDate, DbType.DateTime);
            dbparams.Add("AssignTo", model.AssessorID, DbType.Int64);
            dbparams.Add("AssessmentCompleteDate", model.CompletedDate, DbType.DateTime);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            dbparams.Add("RCMId", model.RCMId, DbType.Int64);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_SaveStandardAndRegulationRCM", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(SaveAssessmentScope))]
        public async Task<Message<IRA>> SaveAssessmentScope(IRA model)
        {
            var message = new Message<IRA>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", model.ID, DbType.Int32);
            dbparams.Add("OrgId", model.OrgId, DbType.Int32);
            dbparams.Add("StartDate", model.StartDate, DbType.DateTime);
            dbparams.Add("EndDate", model.EndDate, DbType.DateTime);
            dbparams.Add("DomainIds", model.DomainIds, DbType.String);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_SaveAssessmentScope", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(getAssessmentScope))]
        public async Task<List<IRA>> getAssessmentScope(int OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", OrgId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<IRA>("proc_getAssessmentScope", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetEnablerCategoryByOrgID))]
        public async Task<List<ClientEnablers>> GetEnablerCategoryByOrgID(long OrgID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ClientEnablers>("Proc_GetEnablerCategoryByOrgID", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetConCatListByIds))]
        public async Task<List<ClientEnablers>> GetConCatListByIds(long orgId, int functionId, int processId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", orgId, DbType.Int64);
            dbparams.Add("FunctionId", functionId, DbType.Int32);
            dbparams.Add("ProcessId", processId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ClientEnablers>("SP_GetConCatListByIds", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetDomainBaseRiskScore))]
        public async Task<List<ClientEnablers>> GetDomainBaseRiskScore(long orgId, int functionId, int processId, int ConCatId, int QuestionnaireId)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("OrgId", orgId, DbType.Int64);
                dbparams.Add("FunctionId", functionId, DbType.Int32);
                dbparams.Add("ProcessId", processId, DbType.Int32);
                dbparams.Add("QuestionnaireId", 0, DbType.Int32);
                dbparams.Add("ConCatId", ConCatId, DbType.Int32);
                return await Task.FromResult(_dapper.GetAll<ClientEnablers>("SP_GetDomainBaseRiskScore", dbparams, CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpGet(nameof(GetComplianceRCM))]
        public async Task<List<IRA>> GetComplianceRCM(long OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<IRA>("proc_GetComplianceRCM", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetCustomRCM))]
        public async Task<List<IRA>> GetCustomRCM(long OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<IRA>("proc_GetCustomRCM", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetStandardAndRegulationRCM))]
        public async Task<List<IRA>> GetStandardAndRegulationRCM(long OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<IRA>("proc_GetStandardAndRegulationRCM", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpPost(nameof(SaveEnablerAssessment))]
        public async Task<Message<IRA>> SaveEnablerAssessment(IRA model)
        {
            var message = new Message<IRA>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", model.ID, DbType.Int32);
            dbparams.Add("OrgId", model.OrgId, DbType.Int32);
            dbparams.Add("CategoryId", model.CategoryId, DbType.Int32);
            dbparams.Add("QuestionId", model.QuestionId, DbType.Int32);
            dbparams.Add("AssessorID", model.AssessorID, DbType.Int32);
            dbparams.Add("CompletedDate", model.CompletedDate, DbType.DateTime);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int32);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            dbparams.Add("QuestionnaireID", model.QuestionnaireID, DbType.Int32);

            var result = await Task.FromResult(_dapper.Insert<int>("Proc_SaveUpdateEnalberAssessment", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpGet(nameof(GetEnablerAssessmentByEmpId))]
        public async Task<List<IRA>> GetEnablerAssessmentByEmpId(int EmpId)
        {

            var dbparams = new DynamicParameters();
            dbparams.Add("EmpId", EmpId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<IRA>("proc_GetEnablerAssessmentByEmpId", dbparams, commandType: CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetComplianceRCMByEmpId))]
        public async Task<List<IRA>> GetComplianceRCMByEmpId(int EmpId)
        {

            var dbparams = new DynamicParameters();
            dbparams.Add("EmpId", EmpId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<IRA>("proc_GetComplianceRCMByEmpId", dbparams, commandType: CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetAssignedEnalbersDetails))]
        public async Task<List<IRA>> GetAssignedEnalbersDetails(long QuestionnaireID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("QuestionnaireID", QuestionnaireID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<IRA>("proc_GetAssignedEnalbersDetails", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetAssignedEnablerCategory))]
        public async Task<List<IRA>> GetAssignedEnablerCategory(long UserID, long orgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("UserID", UserID, DbType.Int32);
            dbparams.Add("OrgID", orgId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<IRA>("proc_GetAssignedEnablerCategory", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetAssignedEnalbersCategoryType))]
        public async Task<List<IRA>> GetAssignedEnalbersCategoryType(long UserID, long orgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("UserID", UserID, DbType.Int32);
            dbparams.Add("OrgID", orgId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<IRA>("proc_GetAssignedEnalbersCategoryType", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetClientUser))]
        public async Task<List<UserLogin>> GetClientUser() => await Task.FromResult(_dapper.GetAll<UserLogin>("proc_ClientUser", default, CommandType.StoredProcedure));

        [HttpGet(nameof(GetEndDateOfAS))]
        public async Task<DateTime> GetEndDateOfAS(long OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", OrgId, DbType.Int32);
            return await Task.FromResult(_dapper.Get<DateTime>("proc_GetEndDateOfAS", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetAssignedEnablerByOrgAndCategory))]
        public async Task<List<IRA>> GetAssignedEnablerByOrgAndCategory(long OrgId, long CategoryId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", OrgId, DbType.Int32);
            dbparams.Add("CategoryId", CategoryId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<IRA>("proc_GetAssignedEnablerByOrgAndCategory", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetAssignedUserByProcessesId))]
        public async Task<IRA> GetAssignedUserByProcessesId(long processId = 0)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("processId", processId, DbType.Int64);
            var res = await Task.FromResult(_dapper.Get<long>("SP_GetAssignedUserByProcessesId", dbparams, CommandType.StoredProcedure));
            return new IRA { AssignToUserId = res };
        }

        [HttpGet(nameof(GetDomainCriticalityScoreByProcessesId))]
        public async Task<List<DomainCriticalityScore>> GetDomainCriticalityScoreByProcessesId(long orgId, long functionId, long processId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("orgId", orgId, DbType.Int64);
            dbparams.Add("functionId", functionId, DbType.Int64);
            dbparams.Add("processId", processId, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<DomainCriticalityScore>("SP_GetDomainCriticalityScoreByProcessesId", dbparams, CommandType.StoredProcedure));
            return res;
        }

        [HttpGet(nameof(GetEnumsByEnumTypeId))]
        public async Task<List<CyberaniumModels.Enum>> GetEnumsByEnumTypeId(long enumTypeId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("enumTypeId", enumTypeId, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<CyberaniumModels.Enum>("SP_GetEnumsByEnumTypeId", dbparams, CommandType.StoredProcedure));
            return res;
        }

        [HttpPost(nameof(SubmitDomainCriticalityScore))]
        public async Task<Message<int>> SubmitDomainCriticalityScore(DomainCriticalityScore model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("Id", model.Id, DbType.Int64);
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("FunctionId", model.FunctionId, DbType.Int64);
            dbparams.Add("ProcessId", model.ProcessId, DbType.Int64);
            dbparams.Add("DomainCriticalityScoreXML", CreateXML.CreateDomainCriticalityScoreXML(model.DomainCriticalityScoreXML), DbType.String);
            dbparams.Add("UserID", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SubmitDomainCriticalityScore", dbparams, CommandType.StoredProcedure));
            if (result > 0)
            {
                message.Data = result;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = result;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(SubmitProcessEnablerMap))]
        public async Task<Message<int>> SubmitProcessEnablerMap(Base model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("UserId", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            dbparams.Add("ProcessEnablerMapXML", CreateXML.CreateProcessEnablerMapXML(model.Mode), DbType.Xml);
            message.Data = await Task.FromResult(_dapper.Insert<int>("SP_SubmitProcessEnablerMap", dbparams, CommandType.StoredProcedure));
            if (message.Data > 0)
            {
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetQuestionnaireIdByProcessId))]
        public async Task<List<long>> GetQuestionnaireIdByProcessId(long orgId = 0, long functionId = 0, long processId = 0, long conCatId = 0)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("orgId", orgId, DbType.Int64);
            dbparams.Add("functionId", functionId, DbType.Int64);
            dbparams.Add("processId", processId, DbType.Int64);
            dbparams.Add("conCatId", conCatId, DbType.Int32);
            var res = await Task.FromResult(_dapper.GetAll<long>("SP_GetQuestionnaireIdByProcessId", dbparams, CommandType.StoredProcedure));
            return res;
        }

        [HttpGet(nameof(GetAllThreadsByEnablerCategory))]
        public async Task<List<IRA>> GetAllThreadsByEnablerCategory(string EnablerCategory)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("EnablerCategory", EnablerCategory, DbType.String);
            return await Task.FromResult(_dapper.GetAll<IRA>("proc_GetAllThreadsByEnablerCategory", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(UpadateProcess))]
        public async Task<Message<int>> UpadateProcess(FunctionProcess model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", model.Id, DbType.Int64);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            dbparams.Add("UserId", model.CreatedBy, DbType.Int32);
            dbparams.Add("ProcessName", model.ProcessName, DbType.String);
            dbparams.Add("ProcessOwner", model.ProcessOwner, DbType.String);
            dbparams.Add("ProcessDescription", model.ProcessDescription, DbType.String);
            message.Data = await Task.FromResult(_dapper.Insert<int>("SP_UpadateProcess", dbparams, CommandType.StoredProcedure));
            if (message.Data > 0)
            {
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetRCMControlByOrgLatest))]
        public async Task<List<RiskAssessment>> GetRCMControlByOrgLatest(long OrgID, long QuestionnaireID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            dbparams.Add("QuestionnaireID", QuestionnaireID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<RiskAssessment>("proc_GetRCMControlByOrgLatest", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(SubmitThreatProbabilityScore))]
        public async Task<Message<int>> SubmitThreatProbabilityScore(Base model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            var obj = new XMLCreater();
            dbparams.Add("OrgId", model.UpdatedBy, DbType.Int64);
            dbparams.Add("ThreatProbabilityScoreXML", obj.CreateThreatProbabilityScoreXML(model.UpdatedIP), DbType.Xml);
            dbparams.Add("UserId", model.CreatedBy, DbType.Int64);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            message.Data = await Task.FromResult(_dapper.Insert<int>("SP_SubmitThreatProbabilityScore", dbparams, CommandType.StoredProcedure));
            if (message.Data > 0)
            {
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetThreatEnablerListByOrgId))]
        public async Task<List<ThreatEnabler>> GetThreatEnablerListByOrgId(long orgId, int conCatId, long questionnaireId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("orgId", orgId, DbType.Int64);
            dbparams.Add("conCatId", conCatId, DbType.Int32);
            dbparams.Add("questionnaireId", questionnaireId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<ThreatEnabler>("SP_GetThreatEnablerListByOrgId", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(SaveUpdateControlAssessment))]
        public async Task<Message<int>> SaveUpdateControlAssessment(RiskAssessment model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("Status", model.Status, DbType.Int64);
            dbparams.Add("CategoryId", model.CategoryId, DbType.Int64);
            dbparams.Add("QuestionnaireID", model.QuestionnaireID, DbType.Int64);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int64);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            dbparams.Add("xml", CreateXML.CreateControlAssessmentXML(model.xmlControlAssessment), DbType.Xml);
            message.Data = await Task.FromResult(_dapper.Insert<int>("proc_SaveUpdateControlAssessment", dbparams, CommandType.StoredProcedure));
            if (message.Data == 0)
            {
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(SaveUpdateArtefacts))]
        public async Task<Message<int>> SaveUpdateArtefacts(RiskAssessment model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("CategoryId", model.CategoryId, DbType.Int64);
            dbparams.Add("QuestionnaireID", model.QuestionnaireID, DbType.Int64);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int64);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            dbparams.Add("ArtefactsXML", CreateXML.CreateArtefactsXML(model.ArtefactsXML), DbType.Xml);
            dbparams.Add("ImagesXML", CreateXML.CreateImagesXML(model.ImagesXML), DbType.Xml);
            message.Data = await Task.FromResult(_dapper.Insert<int>("proc_SaveUpdateArtefacts", dbparams, CommandType.StoredProcedure));
            message.IsSuccess = message.Data > 0 ? true : false;
            return message;
        }

        [HttpPost(nameof(SaveComment))]
        public async Task<Message<int>> SaveComment(RiskAssessment model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("Status", model.Status, DbType.Int64);
            dbparams.Add("CategoryID", model.CategoryId, DbType.Int64);
            dbparams.Add("QuestionaireID", model.QuestionnaireID, DbType.Int64);
            dbparams.Add("CreatedBy", model.CreatedBy, DbType.Int64);
            dbparams.Add("CreatedIP", model.CreatedIP, DbType.String);
            dbparams.Add("xml", CreateXML.CreateCommentXML(model.xmlControlAssessment), DbType.Xml);
            message.Data = await Task.FromResult(_dapper.Insert<int>("proc_SaveComment", dbparams, CommandType.StoredProcedure));
            if (message.Data > 0)
            {
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(SaveApprovalfor))]
        public async Task<Message<RiskAssessment>> SaveApprovalfor(RiskAssessment data)
        {
            var message = new Message<RiskAssessment>();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", data.OrgId, DbType.Int64);
            dbparams.Add("ApprovalFor", data.ApprovalFor, DbType.String);
            dbparams.Add("CategoryID", data.CategoryId, DbType.Int64);
            dbparams.Add("QuestionnaireID", data.QuestionnaireID, DbType.Int64);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_SaveApprovalfor", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpGet(nameof(RemoveTask))]
        public async Task<int> RemoveTask(long OrgId, long CategoryId, long QuestionnaireID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", OrgId, DbType.Int32);
            dbparams.Add("CategoryId", CategoryId, DbType.Int32);
            dbparams.Add("QuestionnaireID", QuestionnaireID, DbType.Int32);
            return await Task.FromResult(_dapper.Get<int>("Proc_RemoveTask", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetAllComments))]
        public async Task<List<RiskAssessment>> GetAllComments(long OrgId, long CategoryId, long QuestionnaireID, long CreatedBy)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", OrgId, DbType.Int64);
            dbparams.Add("CategoryId", CategoryId, DbType.Int64);
            dbparams.Add("QuestionaireID", QuestionnaireID, DbType.Int64);
            dbparams.Add("CreatedBy", CreatedBy, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<RiskAssessment>("proc_GetAllComments", dbparams, CommandType.StoredProcedure));
            return res;
        }
        [HttpGet(nameof(GetControlAssessmentListByOrg))]
        public async Task<List<RiskAssessment>> GetControlAssessmentListByOrg(long OrgID, long QuestionnaireID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            dbparams.Add("QuestionnaireID", QuestionnaireID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<RiskAssessment>("Proc_GetControlAssessmentListByOrg", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetApprovalFor))]
        public async Task<Base> GetApprovalFor(long OrgID, long QuestionnaireID, long CategoryId)
        {
            var dbparams = new DynamicParameters();
            Base obj = new();
            dbparams.Add("OrgID", OrgID, DbType.Int64);
            dbparams.Add("QuestionnaireID", QuestionnaireID, DbType.Int64);
            dbparams.Add("CategoryID", CategoryId, DbType.Int64);
            obj.Mode = await Task.FromResult(_dapper.Get<string>("proc_GetApprovalFor", dbparams, CommandType.StoredProcedure));
            return obj;
        }
        [HttpGet(nameof(GetThreatListById))]
        public async Task<List<Threat>> GetThreatListById(long orgId, int categoryId, long questionnaireId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("orgId", orgId, DbType.Int64);
            dbparams.Add("categoryId", categoryId, DbType.Int32);
            dbparams.Add("questionnaireId", questionnaireId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<Threat>("SP_GetThreatListById", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetControlListByIds))]
        public async Task<List<RiskAssessment>> GetControlListByIds(long orgId = 0, int conCatId = 0, long questionnaireId = 0)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("orgId", orgId, DbType.Int64);
            dbparams.Add("conCatId", conCatId, DbType.Int32);
            dbparams.Add("questionnaireId", questionnaireId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<RiskAssessment>("SP_GetControlListByIds", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(SubmitComplainceAssessmentRCM))]
        public async Task<Message<IRA>> SubmitComplainceAssessmentRCM(IRA data)
        {
            Message<IRA> message = new Message<IRA>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", data.OrgId, DbType.Int32);
            dbparams.Add("xml", obj.CreateComplainceAssessmentXML(data.xmlPerformAssessment), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("Proc_ComplainceAssessmentRCM", dbparams, CommandType.StoredProcedure));
            if (result > 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SubmitCustomRCM))]
        public async Task<Message<IRA>> SubmitCustomRCM(IRA data)
        {
            Message<IRA> message = new Message<IRA>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", data.OrgId, DbType.Int32);
            dbparams.Add("xml", obj.CreateCustomRCMXML(data.xmlPerformAssessment), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_SubmitCustomRCM", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SubmitStandardAndRegulationAssessmentRCM))]
        public async Task<Message<IRA>> SubmitStandardAndRegulationAssessmentRCM(IRA data)
        {
            Message<IRA> message = new Message<IRA>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", data.OrgId, DbType.Int32);
            dbparams.Add("xml", obj.CreateStdAndRegAssessmentXML(data.xmlPerformAssessment), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_StandardAndRegulationAssessmentRCM", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetControlAssessmentControl))]
        public async Task<List<RiskAssessment>> GetControlAssessmentControl(long OrgID, long QuestionnaireID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            dbparams.Add("QuestionnaireID", QuestionnaireID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<RiskAssessment>("proc_GetControlAssessmentControl", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(SaveUpdateRiskTreatmentPlan))]
        public async Task<Message<int>> SaveUpdateRiskTreatmentPlan(RiskAssessment model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("QuestionaireID", model.QuestionnaireID, DbType.Int64);
            dbparams.Add("xml", CreateXML.CreateRiskTreatmentPlanXML(model.xmlRiskTreatMent), DbType.Xml);
            message.Data = await Task.FromResult(_dapper.Insert<int>("proc_SaveUpdateRiskTreatmentPlan", dbparams, CommandType.StoredProcedure));
            if (message.Data == 0)
            {
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetRiskDomainListByOrgId))]
        public async Task<List<Enum>> GetRiskDomainListByOrgId(long orgId = 0)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("orgId", orgId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<Enum>("SP_GetRiskDomainListByOrgId", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(SubmitDomainBaseRiskScore))]
        public async Task<Message<int>> SubmitDomainBaseRiskScore(DomainBaseRiskScore model)
        {
            var obj = new XMLCreater();
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", model.OrgId, DbType.Int64);
            dbparams.Add("ConCatId", model.ConCatId, DbType.Int32);
            dbparams.Add("QuestionnaireId", model.QuestionnaireId, DbType.Int64);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            dbparams.Add("UserId", model.CreatedBy, DbType.Int32);
            dbparams.Add("DomainBaseRiskScoreXML", obj.CreateDomainBaseRiskScoreXML(model.DomainBaseRiskScoreXML), DbType.Xml);
            message.Data = await Task.FromResult(_dapper.Insert<int>("SP_SubmitDomainBaseRiskScore", dbparams, CommandType.StoredProcedure));
            if (message.Data > 0)
            {
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpGet(nameof(GetArtefactTasks))]
        public async Task<List<RequestArtefacts>> GetArtefactTasks(long userId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("userId", userId, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<RequestArtefacts>("proc_GetArtefactsByUserId", dbparams, CommandType.StoredProcedure));
            return res;
        }
    }
}
