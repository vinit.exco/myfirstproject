﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="MenuController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public MenuController(IDapper dapper)
        {
            _dapper = dapper;
        }

        [HttpPost(nameof(SubmitMenu))]
        public async Task<Message<Menu>> SubmitMenu(Menu data)
        {
            Message<Menu> message = new Message<Menu>();
            var dbparams = new DynamicParameters();
            dbparams.Add("MenuId", data.MenuId, DbType.Int32);
            dbparams.Add("MenuType", data.MenuType, DbType.Int16);
            dbparams.Add("ParentMenuId", data.ParentMenuId, DbType.Int32);
            dbparams.Add("ChildMenuId", data.ChildMenuId, DbType.Int32);
            dbparams.Add("SubMenuId", data.SubMenuId, DbType.Int32);
            dbparams.Add("OrderNo", data.OrderNo, DbType.Int16);
            dbparams.Add("MenuName", data.MenuName, DbType.String);
            dbparams.Add("MenuTitle", data.MenuTitle, DbType.String);
            dbparams.Add("Area", data.Area, DbType.String);
            dbparams.Add("Controller", data.Controller, DbType.String);
            dbparams.Add("Action", data.Action, DbType.String);
            dbparams.Add("Icon", data.Icon, DbType.String);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            dbparams.Add("UserID", data.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitMenu]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetMenu))]
        public async Task<List<Menu>> GetMenu()
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("MenuId", 0, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Menu>("[dbo].[SP_GetMenu]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpPost(nameof(BlockDeleteMenu))]
        public async Task<Message<Menu>> BlockDeleteMenu(Menu data)
        {
            Message<Menu> message = new Message<Menu>();
            var dbparams = new DynamicParameters();
            dbparams.Add("MenuId", data.MenuId, DbType.Int32);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteMenu]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new Menu();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

    }
}
