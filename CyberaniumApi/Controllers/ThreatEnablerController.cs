﻿using CyberaniumApi.Helpers;
using CyberaniumApi.IServices;
using CyberaniumModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CyberaniumApi.Controllers
{
    /// <summary>
    /// Defines the <see cref="ThreatEnablerController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ThreatEnablerController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;
        /// <summary>
        /// Initializes a new instance of the <see cref="ThreatEnablerController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public ThreatEnablerController(IDapper dapper)
        {
            _dapper = dapper;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ThreatEnablerController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        [HttpGet(nameof(GetAllThreats))]
        public async Task<List<ThreatEnabler>> GetAllThreats()
        {
            var result = await Task.FromResult(_dapper.GetAll<ThreatEnabler>("SP_GetAllThreats", null, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetAllEnumTypes))]
        public async Task<List<ThreatEnabler>> GetAllEnumTypes(long Id)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentId", Id, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ThreatEnabler>("SP_GetAllEnumTypes", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetAllChildOrgCategory))]
        public async Task<List<ThreatEnabler>> GetAllChildOrgCategory(int OrgID, int ChilOrgID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            dbparams.Add("ChilOrgID", ChilOrgID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ThreatEnabler>("SP_GetAllChildOrgCategory", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetTypeOfCategory))]
        public async Task<List<ThreatEnabler>> GetTypeOfCategory(int OrgID, int ChilOrgID, long CatID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            dbparams.Add("ChilOrgID", ChilOrgID, DbType.Int32);
            dbparams.Add("CatID", CatID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ThreatEnabler>("SP_GetTypeOfCategory", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetControlCategoryByOrgId))]
        public async Task<List<ThreatEnabler>> GetControlCategoryByOrgId(long parentOrgId, long childOrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", parentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", childOrgId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<ThreatEnabler>("SP_GetControlCategoryByOrgId", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(SubmitThreatsEnabler))]
        public async Task<Message<ThreatEnabler>> SubmitThreatsEnabler(ThreatEnabler data)
        {
            Message<ThreatEnabler> message = new Message<ThreatEnabler>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("OrganizationId", data.Id, DbType.Int32);
            dbparams.Add("XML", obj.CreateThreadScoreXML(data.QuestionnaireXML), DbType.Xml);
            dbparams.Add("ChildOrgId", data.ChildOrg, DbType.Int32);
            dbparams.Add("userID", data.CreatedBy, DbType.Int32);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitThreadsEnablerScore]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Error";
            }
            return message;
        }

        [HttpPost(nameof(SubmitThreatEnablerScore))]
        public async Task<Message<ThreatEnablers>> SubmitThreatEnablerScore(ThreatEnablers model)
        {
            var message = new Message<ThreatEnablers>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("EnablerXML", obj.CreateEnablerXML(model.EnablerXML), DbType.Xml);
            dbparams.Add("ThreatScoreXML", obj.CreateThreatScoreXML(model.ThreatScoreXML), DbType.Xml);
            dbparams.Add("userID", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitThreatEnablerScore]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Error";
            }
            return message;
        }

        [HttpGet(nameof(GetThreatEnablerByParentIDAndChildId))]
        public async Task<List<ThreatEnabler>> GetThreatEnablerByParentIDAndChildId(long ParentOrgId, long ChildOrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", ParentOrgId, DbType.Int32);
            dbparams.Add("ChildOrgId", ChildOrgId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ThreatEnabler>("procc_GetThreatEnablerByParentIDAndChildId", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(GetThreatEnablerMapByParentIDAndChildId))]
        public async Task<List<ThreatEnabler>> GetThreatEnablerMapByParentIDAndChildId(long ParentOrgId, long ChildOrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", ParentOrgId, DbType.Int32);
            dbparams.Add("ChildOrgId", ChildOrgId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ThreatEnabler>("procc_GetThreatEnablerMapByParentIDAndChildId", dbparams, CommandType.StoredProcedure));
        }
    }
}
