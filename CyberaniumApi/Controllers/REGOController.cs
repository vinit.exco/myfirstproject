﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using CyberaniumModels.Procedures;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="REGOController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class REGOController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="REGOController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public REGOController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The SaveupdateREGO.
        /// </summary>
        /// <param name="LawRegulation">The LawRegulation<see cref="RegoMaster"/>.</param>
        /// <returns>The <see cref="Task{RegoMaster}"/>.</returns>
        [HttpPost(nameof(SaveupdateREGO))]
        public async Task<Message<int>> SaveupdateREGO(RegoMaster LawRegulation)
        {
            var dbparams = new DynamicParameters();
            Message<int> message = new Message<int>();
            dbparams.Add("RegoId", value: LawRegulation.RegoId, DbType.Int32);
            dbparams.Add("CountryId", value: LawRegulation.CountryId, DbType.Int32);
            dbparams.Add("TypeId", value: LawRegulation.TypeId, dbType: DbType.Int32);
            dbparams.Add("Regulation", value: LawRegulation.Regulation, dbType: DbType.String);
            dbparams.Add("YearPublish", LawRegulation.YearPublish, DbType.Int16);
            dbparams.Add("Revision", LawRegulation.Revision, DbType.Int16);
            dbparams.Add("Sector", value: LawRegulation.Sector, dbType: DbType.Int32);
            dbparams.Add("Section", value: LawRegulation.Section, dbType: DbType.Int32);
            dbparams.Add("Domain", value: LawRegulation.Domain, dbType: DbType.Int32);
            dbparams.Add("Text", LawRegulation.Text, DbType.String);
            dbparams.Add("Requirement_ID", LawRegulation.Requirement_ID, DbType.String);
            dbparams.Add("IP", LawRegulation.CreatedIP, DbType.String);
            dbparams.Add("UserID", LawRegulation.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[sp_SaveupdateREGO]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = result;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = result;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        /// <summary>
        /// The GetAllREGOData.
        /// </summary>
        /// <returns>The <see cref="Task{List{RegoMaster}}"/>.</returns>
        [HttpGet(nameof(GetAllREGOData))]
        public async Task<List<RegoMaster>> GetAllREGOData()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<RegoMaster>("Select * From REGODB where isdeleted=0 ", dbparams, CommandType.Text));
            return result;
        }
         [HttpGet(nameof(GetAllRegulationbySectorAndCountryWise))]
        public async Task<List<RegulationbySectorAndCountry>> GetAllRegulationbySectorAndCountryWise(string SectorId, string CountryId)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("CountryId", CountryId, DbType.String);
                dbparams.Add("Sector", SectorId, DbType.String);
                var result = await Task.FromResult(_dapper.GetAll<RegulationbySectorAndCountry>("sp_GetRegulationbySectorAndCountry", dbparams, CommandType.StoredProcedure));
                return result;
            }
            catch (System.Exception)
            {
                throw;
            }
          
        }

        /// <summary>
        /// The BlockDeleteREGO.
        /// </summary>
        /// <param name="regoMaster">The regoMaster<see cref="RegoMaster"/>.</param>
        /// <returns>The <see cref="Task{Message{RegoMaster}}"/>.</returns>
        [HttpPost(nameof(BlockDeleteREGO))]
        public async Task<Message<RegoMaster>> BlockDeleteREGO(RegoMaster regoMaster)
        {
            Message<RegoMaster> message = new Message<RegoMaster>();
            var dbparams = new DynamicParameters();
            dbparams.Add("RegoId", regoMaster.RegoId, DbType.Int32);
            dbparams.Add("Mode", regoMaster.Mode, DbType.String);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteRego]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = new RegoMaster();
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new RegoMaster();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
    }
}
