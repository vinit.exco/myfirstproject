﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="CountryController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]

    public class CountryController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public CountryController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The GetAllCountry.
        /// </summary>
        /// <returns>The <see cref="Task{List{Country}}"/>.</returns>
        [HttpGet(nameof(GetAllCountry))]
        public async Task<List<Country>> GetAllCountry()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<Country>("proc_GetAllCountry", null, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(LawRegulationCountry))]
        public async Task<List<Country>> LawRegulationCountry()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<Country>("proc_LawRegulationCountry", null, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetAllStates))]
        public async Task<List<State>> GetAllStates(long CID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("CID", CID,DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<State>("proc_GetAllStates", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetAllCity))]
        public async Task<List<City>> GetAllCity(long CID , long SID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("CID", CID, DbType.Int32);
            dbparams.Add("SID", SID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<City>("proc_GetAllCity", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpPost(nameof(SaveCountry))]
        public async Task<Message<Country>> SaveCountry(Country Model)
        {
            Message<Country> message = new Message<Country>();
            var dbparams = new DynamicParameters();
            dbparams.Add("IP", Model.CreatedIP);
            dbparams.Add("UserID", Model.CreatedBy);
            dbparams.Add("CountryName", Model.CountryName);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SaveCountry", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = Model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = Model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SaveState))]
        public async Task<Message<State>> SaveState(State model)
        {
            Message<State> message = new Message<State>();
            var dbparams = new DynamicParameters();
            dbparams.Add("IP", model.CreatedIP);
            dbparams.Add("UserID", model.CreatedBy);
            dbparams.Add("CountryId", model.CID);
            dbparams.Add("StateName", model.StateName);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SaveState", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SaveCity))]
        public async Task<Message<City>> SaveCity(City Model)
        {
            Message<City> message = new Message<City>();
            var dbparams = new DynamicParameters();
            dbparams.Add("IP", Model.CreatedIP, DbType.String);
            dbparams.Add("UserID", Model.CreatedBy, DbType.Int32);
            dbparams.Add("CountryId", Model.CID, DbType.Int32);
            dbparams.Add("StateId", Model.SID, DbType.Int32);
            dbparams.Add("CityName", Model.CityName, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SaveCity", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = Model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = Model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(CheckCountry))]
        public async Task<Message<Country>> CheckCountry(Country Model)
        {
            Message<Country> message = new Message<Country>();

            var dbparams = new DynamicParameters();
            dbparams.Add("CountryName", Model.CountryName, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("SP_GetcountryByName", dbparams, CommandType.StoredProcedure));
            if (result.Count > 0)
            {
                message.Data = Model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = Model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(CheckState))]
        public async Task<Message<State>> CheckState(State Model)
        {
            Message<State> message = new Message<State>();

            var dbparams = new DynamicParameters();
            dbparams.Add("CountryId", Model.CID, DbType.Int32);
            dbparams.Add("stateName", Model.StateName, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("SP_GetStateByName", dbparams, CommandType.StoredProcedure));
            if (result.Count > 0)
            {
                message.Data = Model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = Model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(CheckCity))]
        public async Task<Message<City>> CheckCity(City Model)
        {
            Message<City> message = new Message<City>();
            var dbparams = new DynamicParameters();
            dbparams.Add("CountryId", Model.CID, DbType.Int32);
            dbparams.Add("StateId", Model.SID, DbType.Int32);
            dbparams.Add("City", Model.CityName, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("SP_GetCityByName", dbparams, CommandType.StoredProcedure));
            if (result.Count > 0)
            {
                message.Data = Model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = Model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpGet(nameof(BindStateById))]
        public async Task<List<State>> BindStateById(long countryId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", countryId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<State>("SP_GetStateById", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(FindCountryByName))]
        public async Task<List<Country>> FindCountryByName(string countryName)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Country", countryName, DbType.String);
            return await Task.FromResult(_dapper.GetAll<Country>("SP_getcountrysbyname", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(FindStateByName))]
        public async Task<List<State>> FindStateByName(string StateName)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("state", StateName, DbType.String);
            return await Task.FromResult(_dapper.GetAll<State>("SP_GetStatesByName", dbparams, CommandType.StoredProcedure));
        }
        [HttpGet(nameof(FindCityByName))]
        public async Task<List<City>> FindCityByName(string cityName)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("city", cityName, DbType.String);
            return await Task.FromResult(_dapper.GetAll<City>("SP_GetCitysByName", dbparams, CommandType.StoredProcedure));
        }
    }
}
