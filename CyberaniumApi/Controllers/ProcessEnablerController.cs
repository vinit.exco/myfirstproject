﻿using CyberaniumApi.Helpers;
using CyberaniumApi.IServices;
using CyberaniumModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CyberaniumApi.Controllers
{
    /// <summary>
    /// Defines the <see cref="AssesmentController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessEnablerController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AssesmentController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public ProcessEnablerController(IDapper dapper)
        {
            _dapper = dapper;
        }
        [HttpGet(nameof(GetFuncation))]
        public async Task<List<OrganizationFunctions>> GetFuncation(long OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", OrgId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<OrganizationFunctions>("SP_GetOrganizationFunctionById", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetProcess))]
        public async Task<List<OrganizationProcesss>> GetProcess(long OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", OrgId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<OrganizationProcesss>("SP_GetProcessById", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetEnabler))]
        public async Task<List<OrganoGrams>> GetEnabler(long OrgId, long ChildOrgId, long CategoryId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgId", OrgId, DbType.Int32);
            dbparams.Add("ChildOrgId", ChildOrgId, DbType.Int32);
            dbparams.Add("CategoryId", CategoryId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<OrganoGrams>("SP_GetEnablerById", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpPost(nameof(PostProcess))]
        public async Task<Message<OrganoGrams>> PostProcess(OrganoGrams organo)
        {
            Message<OrganoGrams> message = new Message<OrganoGrams>();
            try
            {
                var obj = new XMLCreater();
                var dbparams = new DynamicParameters();
                dbparams.Add("OrganizationId", organo.Id, DbType.Int32);
                dbparams.Add("ChildOrgId", organo.ChildOrgId, DbType.Int32);
                dbparams.Add("xml", obj.xmlProcessEnablerMapping(organo.ChildOrgXML), DbType.Xml);
                dbparams.Add("IP", organo.CreatedIP, DbType.String);
                dbparams.Add("UserId", organo.CreatedBy, DbType.Int32);
                var result = await Task.FromResult(_dapper.Insert<int>("SP_PostProcessEnablerMap", dbparams, CommandType.StoredProcedure));
                if (result == 0)
                {
                    message.IsSuccess = true;
                    message.Data = organo;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return message;
        }
        [HttpGet(nameof(GetCategoryDetails))]
        public async Task<List<OrganoGrams>> GetCategoryDetails(long id)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", id, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<OrganoGrams>("SP_GetCategoryDetailsById", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetEnablerList))]
        public async Task<List<OrganoGrams>> GetEnablerList(long OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Orgid", OrgId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<OrganoGrams>("proc_GetProcessEnalberByOrg", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpPost(nameof(SubmitProcessEnabler))]
        public async Task<Message<ProcessEnabler>> SubmitProcessEnabler(ProcessEnabler model)
        {
            var message = new Message<ProcessEnabler>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("FunctionId", model.FunctionId, DbType.Int64);
            dbparams.Add("ProcessId", model.ProcessId, DbType.Int64);
            dbparams.Add("ProcessEnablerXML", obj.CreateProcessEnablerXML(model.ProcessEnablerXML), DbType.Xml);
            dbparams.Add("userID", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitProcessEnabler]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Error";
            }
            return message;
        }

        [HttpPost(nameof(GetDomainCriticalityScoreByProcessId))]
        public async Task<Message<List<DomainCriticalityScore>>> GetDomainCriticalityScoreByProcessId(ProcessesAssign model)
        {
            var message = new Message<List<DomainCriticalityScore>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("FunctionId", model.FunctionId, DbType.Int64);
            dbparams.Add("ProcessId", model.ProcessId, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<DomainCriticalityScore>("[dbo].[SP_GetDomainCriticalityScoreByProcessId]", dbparams, CommandType.StoredProcedure));
            message.Data = res;
            message.IsSuccess = true;
            return message;
        }

        [HttpPost(nameof(SubmitDCEnablerFinalScore))]
        public async Task<Message<int>> SubmitDCEnablerFinalScore(ProcessesAssign model)
        {
            Message<int> message = new Message<int>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("Id", model.Id, DbType.Int64);
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("FunctionId", model.FunctionId, DbType.Int64);
            dbparams.Add("ProcessId", model.ProcessId, DbType.Int64);
            dbparams.Add("ClientProcessDCScoreXML", CreateXML.CreateDomainCriticalityScoreXML(model.ClientProcessDCScoreXML), DbType.String);
            dbparams.Add("ClientProcessEnablerScoreXML", CreateXML.CreateClientProcessEnablerScoreXML(model.ClientProcessEnablerScoreXML), DbType.String);
            dbparams.Add("ClientProcessFinalScoreXML", CreateXML.CreateClientProcessFinalScoreXML(model.ClientProcessFinalScoreXML), DbType.String);
            dbparams.Add("UserID", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SubmitDCEnablerFinalScore", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = 200;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = 500;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
    }
}
