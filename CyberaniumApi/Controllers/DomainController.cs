﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="DomainController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DomainController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public DomainController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The CreateDomain.
        /// </summary>
        /// <param name="data">The data<see cref="Domain"/>.</param>
        /// <returns>The <see cref="Task{Message{Domain}}"/>.</returns>
        [HttpPost(nameof(CreateDomain))]
        public async Task<Message<Domain>> CreateDomain(Domain data)
        {
            Message<Domain> message = new Message<Domain>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", data.Id, DbType.Int32);
            dbparams.Add("DomainId", data.DomainId, DbType.Int32);
            dbparams.Add("code", data.Code, DbType.String);
            dbparams.Add("SubDomain", data.SubDomain.Replace("&", "and"), DbType.String);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            dbparams.Add("UserID", data.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_SubDomain]", dbparams, commandType: CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        /// <summary>
        /// The GetAllDomain.
        /// </summary>
        /// <returns>The <see cref="Task{List{Domain}}"/>.</returns>
        [HttpGet(nameof(GetAllDomain))]
        public async Task<List<Domain>> GetAllDomain()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<Domain>("[dbo].[SP_GetAllDomain]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The BlockDeleteDomain.
        /// </summary>
        /// <param name="domain">The domain<see cref="Domain"/>.</param>
        /// <returns>The <see cref="Task{Message{Domain}}"/>.</returns>
        [HttpPost(nameof(BlockDeleteDomain))]
        public async Task<Message<Domain>> BlockDeleteDomain(Domain domain)
        {
            Message<Domain> message = new Message<Domain>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", domain.Id, DbType.Int32);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteDomain]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = new Domain();
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new Domain();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(CheckSubDomain))]
        public async Task<Message<Domain>> CheckSubDomain(Domain domain)
        {
            Message<Domain> message = new Message<Domain>();
            var dbparams = new DynamicParameters();
            dbparams.Add("SubDomain", domain.SubDomain, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<Domain>("SP_CheckSubDomain", dbparams, CommandType.StoredProcedure));
            if (result.Count > 0)
            {
                message.IsSuccess = false;
            }
            else
            {
                message.IsSuccess = true;
            }
            return message;
        }
        [HttpPost(nameof(CheckCode))]
        public async Task<Message<Domain>> CheckCode(Domain domain)
        {
            Message<Domain> message = new Message<Domain>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Code", domain.Code, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<Domain>("SP_CheckCode", dbparams, CommandType.StoredProcedure));
            if (result.Count > 0)
            {
                message.IsSuccess = false;
            }
            else
            {
                message.IsSuccess = true;
            }
            return message;
        }
        [HttpGet(nameof(GetDomain))]
        public async Task<List<Domain>> GetDomain()
        {
            var dbparams = new DynamicParameters();
            return await Task.FromResult(_dapper.GetAll<Domain>("GetDomain", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetDomainList))]
        public async Task<List<Domain>> GetDomainList()
        {
            var dbparams = new DynamicParameters();
            return await Task.FromResult(_dapper.GetAll<Domain>("GetDomainList", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(DomainSave))]
        public async Task<Message<Domain>> DomainSave(Domain data)
        {
            Message<Domain> message = new Message<Domain>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", data.Id, DbType.Int32);
            dbparams.Add("DomainName", data.DomainName, DbType.String);
            dbparams.Add("DomainCode", data.DomainCode.ToUpper(), DbType.String);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            dbparams.Add("UserID", data.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SaveDomainMaster", dbparams, commandType: CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(CheckDomain))]
        public async Task<Message<Domain>> CheckDomain(Domain domain)
        {
            Message<Domain> message = new Message<Domain>();
            var dbparams = new DynamicParameters();
            dbparams.Add("DomainName", domain.DomainName, DbType.String);
            var list = await Task.FromResult(_dapper.GetAll<Domain>("SP_GetDomainNameByName"
                , dbparams,
                CommandType.StoredProcedure));
            if (list.Count > 0)
            {
                message.IsSuccess = true;
            }
            else
            {
                message.IsSuccess = false;
            }
            return message;
        }
        [HttpPost(nameof(CheckDomainCode))]
        public async Task<Message<Domain>> CheckDomainCode(Domain domain)
        {
            Message<Domain> message = new Message<Domain>();
            var dbparams = new DynamicParameters();
            dbparams.Add("DomainCode", domain.DomainCode, DbType.String);
            var list = await Task.FromResult(_dapper.GetAll<Domain>("SP_GetDomainNameByCode"
                , dbparams,
                CommandType.StoredProcedure));
            if (list.Count > 0)
            {
                message.IsSuccess = true;
            }
            else
            {
                message.IsSuccess = false;
            }
            return message;
        }
    }
}
