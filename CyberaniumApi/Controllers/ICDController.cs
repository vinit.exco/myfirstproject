﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ICDController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ICDController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ICDController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public ICDController(IDapper dapper)
        {
            _dapper = dapper;
        }
        [HttpPost(nameof(SubmitICD))]
        public async Task<Message<ICD>> SubmitICD(ICD data)
        {
            var message = new Message<ICD>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("ControlId", data.ControlId, DbType.String);
            dbparams.Add("ControlName", data.ControlName, DbType.String);
            dbparams.Add("ControlDescription", data.ControlDescription, DbType.String);
            dbparams.Add("DomainId", data.DomainId, DbType.Int32);
            dbparams.Add("SubDomainId", data.SubDomainId, DbType.Int32);
            dbparams.Add("ControlTypeId", data.ControlTypeId, DbType.Int32);
            dbparams.Add("IaaS", data.IaaS, DbType.String);
            dbparams.Add("PaaS", data.PaaS, DbType.String);
            dbparams.Add("SaaS", data.SaaS, DbType.String);
            dbparams.Add("CountryIds", data.CountryIds, DbType.String);
            dbparams.Add("SectorIds", data.SectorIds, DbType.String);
            dbparams.Add("ICDCategoryMapXML", obj.CreateICDCategoryMapXML(data.ICDCategoryMapXML), DbType.Xml);
            dbparams.Add("ICDStandardMapXML", obj.CreateICDStandardMapXML(data.ICDStandardMapXML), DbType.Xml);
            dbparams.Add("ICDArtefactMapXML", obj.CreateICDArtefactMapXML(data.ICDArtefactMapXML), DbType.Xml);
            dbparams.Add("ICDThreatMapXML", obj.CreateICDThreatMapXML(data.ICDThreatMapXML), DbType.Xml);
            dbparams.Add("ICDRegulationMapXML", obj.CreateICDRegulationMapXML(data.ICDRegulationMapXML), DbType.Xml);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            dbparams.Add("UserID", data.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitICD]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SubmitICDByExcel))]
        public async Task<Message<ICDJson>> SubmitICDByExcel(ICDJson model)
        {
            var message = new Message<ICDJson>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            
            dbparams.Add("UserId", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            dbparams.Add("ICDXML", obj.CreateICDXML(model.ICDXML), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_SubmitICDByExcel", dbparams, CommandType.StoredProcedure));
            if (result > 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "UnSuccess";
            }
            return message;
        }
        [HttpGet(nameof(GetICD))]
        public async Task<List<ICD>> GetICD(long ICDID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", ICDID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<ICD>("[dbo].[SP_GetICD]", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetICDBySubDomainID))]
        public async Task<List<ICD>> GetICDBySubDomainID(long SubDomainID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("SubDomainID", SubDomainID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<ICD>("[dbo].[proc_GetICDBySubDomainID]", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpPost(nameof(CodeById))]
        public async Task<Message<ICD>> CodeById(ICD cD)
        {
            Message<ICD> message = new Message<ICD>();
            var dbparams = new DynamicParameters();
            dbparams.Add("DomainId", cD.DomainId, DbType.Int32);
            dbparams.Add("SubDomain", cD.ControlName, DbType.String);
            var result = await Task.FromResult(_dapper.Get<ICD>("SP_GetCodeById", dbparams, CommandType.StoredProcedure));
            message.ReturnMessage = result.DomainCode + " - " + result.Code;
            return message;
        }
        [HttpPost(nameof(ControlById))]
        public async Task<Message<ICD>> ControlById(ICD iCD)
        {
            Message<ICD> message = new Message<ICD>();
            var dbparams = new DynamicParameters();
            dbparams.Add("DomainId", iCD.DomainId, DbType.Int32);
            dbparams.Add("SubDomainId", iCD.SubDomainId, DbType.Int32);
            var result = await Task.FromResult(_dapper.Get<ICD>("SP_GetControlById", dbparams, CommandType.StoredProcedure));
            message.ReturnMessage = result.ControlId;
            return message;
        }

        [HttpPost(nameof(GetControlsByThreatId))]
        public async Task<Message<List<ICD>>> GetControlsByThreatId(ICD model)
        {
            var message = new Message<List<ICD>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", model.Id, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<ICD>("SP_GetControlsByThreatId", dbparams, CommandType.StoredProcedure));
            message.Data = result;
            return message;
        }
        [HttpPost(nameof(BlockDeleteICD))]
        public async Task<Message<ICD>> BlockDeleteICD(ICD data)
        {
            Message<ICD> message = new Message<ICD>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteICD]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new ICD();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpGet(nameof(GetICDThreatMap))]
        public async Task<List<ICD>> GetICDThreatMap()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<ICD>("SP_GetICDThreatMapById", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetICDRiskDomainMap))]
        public async Task<List<ICD>> GetICDRiskDomainMap()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<ICD>("SP_GetICDRiskDomainMapById", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetICDCategoryMap))]
        public async Task<List<ICD>> GetICDCategoryMap()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<ICD>("SP_ICDCategotyMap", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetICDControlTypeMap))]
        public async Task<List<ICD>> GetICDControlTypeMap()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<ICD>("SP_GetICDControlTypeMap", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetControlsByVersion))]
        public async Task<List<ICD>> GetControlsByVersion(string StandardIds, string RegulationIds)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("StandardIds", StandardIds, DbType.String);
            dbparams.Add("RegulationId", RegulationIds, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<ICD>("proc_GetControlsByVersion", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetRegulationMapByICDID))]
        public async Task<List<ICD>> GetRegulationMapByICDID(long ICDId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ICDId", ICDId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<ICD>("proc_GetRegulationMapByICDID", dbparams, CommandType.StoredProcedure));
            return result;
        }
    }
}
