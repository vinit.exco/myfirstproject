﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ThreatTypeController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ThreatTypeController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreatTypeController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public ThreatTypeController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The GetAllThreatType.
        /// </summary>
        /// <returns>The <see cref="Task{List{ThreatType}}"/>.</returns>
        [HttpGet(nameof(GetAllThreatType))]
        public async Task<List<ThreatType>> GetAllThreatType()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<ThreatType>("[dbo].[SP_GetAllThreatType]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The SaveThreatType.
        /// </summary>
        /// <param name="threatType">The threatType<see cref="ThreatType"/>.</param>
        /// <returns>The <see cref="Task{ThreatType}"/>.</returns>
        [HttpPost(nameof(SaveThreatType))]
        public async Task<ThreatType> SaveThreatType(ThreatType threatType)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", threatType.TTypeId, DbType.Int32);
            dbparams.Add("ThreatLevel", 1, DbType.Int16);
            dbparams.Add("TTypeName", threatType.TTypeName, DbType.String);
            dbparams.Add("IP", threatType.CreatedIP, DbType.String);
            dbparams.Add("UserID", threatType.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_ThreatType]", dbparams, CommandType.StoredProcedure));
            return new ThreatType() { TTypeId = result };
        }

        /// <summary>
        /// The UpdateThreatType.
        /// </summary>
        /// <param name="threatType">The threatType<see cref="ThreatType"/>.</param>
        /// <returns>The <see cref="Task{ThreatType}"/>.</returns>
        //[HttpPost(nameof(UpdateThreatType))]
        //public async Task<ThreatType> UpdateThreatType(ThreatType threatType)
        //{
        //    var dbparams = new DynamicParameters();
        //    dbparams.Add("TTypeId", threatType.TTypeId, DbType.Int32);
        //    dbparams.Add("ThreatLevel", threatType.ThreatLevel, DbType.Int16);
        //    dbparams.Add("TTypeName", threatType.TTypeName, DbType.String);
        //    var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_Update_ThreatType]", dbparams, CommandType.StoredProcedure));
        //    return new ThreatType() { TTypeId = result };
        //}

        /// <summary>
        /// The BlockDeleteThreatType.
        /// </summary>
        /// <param name="threatType">The threatType<see cref="ThreatType"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPost(nameof(BlockDeleteThreatType))]
        public async Task<ThreatType> BlockDeleteThreatType(ThreatType threatType)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("TTypeId", threatType.TTypeId, DbType.Int32);
            dbparams.Add("TTypeName", threatType.TTypeName, DbType.String);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteThreatType]", dbparams, CommandType.StoredProcedure));
            return new ThreatType() { TTypeId = result };
        }
    }
}
