﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="HomeController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public HomeController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The Create.
        /// </summary>
        /// <param name="data">The data<see cref="UserLogin"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPost(nameof(Create))]
        public async Task<int> Create(UserLogin data)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_Article]"
                , dbparams,
                commandType: CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The GetById.
        /// </summary>
        /// <param name="Id">The Id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{UserLogin}"/>.</returns>
        [HttpGet(nameof(GetById))]
        public async Task<UserLogin> GetById(int Id)
        {
            var result = await Task.FromResult(_dapper.Get<UserLogin>($"Select * from [UserLogin] where Id = {Id}", null, commandType: CommandType.Text));
            return result;
        }

        /// <summary>
        /// The GetByIds.
        /// </summary>
        /// <param name="Id">The Id<see cref="UserLogin"/>.</param>
        /// <returns>The <see cref="Task{Message{UserLogin}}"/>.</returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        [ActionName(nameof(GetByIds))]
        public async Task<Message<UserLogin>> GetByIds(UserLogin Id)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var result = await Task.FromResult(_dapper.Get<UserLogin>($"Select * from [UserLogin] where UserName = '{Id.UserName}'", null, commandType: CommandType.Text));
            if (result != null)
            {
                message.Data = result;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = result;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        /// <summary>
        /// The Delete.
        /// </summary>
        /// <param name="Id">The Id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpDelete(nameof(Delete))]
        public async Task<int> Delete(int Id)
        {
            var result = await Task.FromResult(_dapper.Execute($"Delete [Dummy] Where Id = {Id}", null, commandType: CommandType.Text));
            return result;
        }

        /// <summary>
        /// The Count.
        /// </summary>
        /// <param name="num">The num<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpGet(nameof(Count))]
        public Task<int> Count(int num)
        {
            var totalcount = Task.FromResult(_dapper.Get<int>($"select COUNT(*) from [Dummy] WHERE Age like '%{num}%'", null,
                    commandType: CommandType.Text));
            return totalcount;
        }

        /// <summary>
        /// The Update.
        /// </summary>
        /// <param name="data">The data<see cref="UserLogin"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPatch(nameof(Update))]
        public Task<int> Update(UserLogin data)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("Id", data.Id);
            dbPara.Add("Name", data.UserName, DbType.String);

            var updateArticle = Task.FromResult(_dapper.Update<int>("[dbo].[SP_Update_Article]",
                            dbPara,
                            commandType: CommandType.StoredProcedure));
            return updateArticle;
        }
    }
}
