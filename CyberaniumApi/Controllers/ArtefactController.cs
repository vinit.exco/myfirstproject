﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ArtefactController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ArtefactController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArtefactController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public ArtefactController(IDapper dapper)
        {
            _dapper = dapper;
        }

        [HttpPost(nameof(SubmitArtefact))]
        public async Task<Message<Artefact>> SubmitArtefact(Artefact data)
        {
            Message<Artefact> message = new Message<Artefact>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("ICDId", data.ICDId, DbType.Int32);
            dbparams.Add("Name", data.Name, DbType.String);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitArtefact]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetArtefact))]
        public async Task<List<Artefact>> GetArtefact()
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", 0, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Artefact>("[dbo].[SP_GetArtefact]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpPost(nameof(BlockDeleteArtefact))]
        public async Task<Message<Artefact>> BlockDeleteArtefact(Artefact data)
        {
            Message<Artefact> message = new Message<Artefact>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteArtefact]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new Artefact();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

    }
}
