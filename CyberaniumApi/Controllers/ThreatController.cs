﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ThreatController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ThreatController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreatController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public ThreatController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The GetAllThreat.
        /// </summary>
        /// <returns>The <see cref="Task{List{Threat}}"/>.</returns>
        [HttpGet(nameof(GetAllThreat))]
        public async Task<List<Threat>> GetAllThreat()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<Threat>("[dbo].[SP_GetAllThreat]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The SaveThreat.
        /// </summary>
        /// <param name="Threat">The Threat<see cref="Threat"/>.</param>
        /// <returns>The <see cref="Task{Threat}"/>.</returns>
        [HttpPost(nameof(SaveupdateThreat))]
        public async Task<Threat> SaveupdateThreat(Threat Threat)
        {
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("ThreatId", Threat.ThreatId, DbType.Int32);
            dbparams.Add("TTypeId", Threat.TTypeId, DbType.Int32);
            dbparams.Add("ThreatCode", Threat.ThreatCode, DbType.String);
            dbparams.Add("ThreatName", Threat.ThreatName, DbType.String);
            dbparams.Add("IP", Threat.CreatedIP, DbType.String);
            dbparams.Add("UserID", Threat.CreatedBy, DbType.Int32);
            dbparams.Add("ControlCategoryXML", obj.CreateControlCategoryXML(Threat.ControlCategoryXML), DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_Threat]", dbparams, CommandType.StoredProcedure));
            return new Threat() { TTypeId = result };
        }

        /// <summary>
        /// The UpdateThreat.
        /// </summary>
        /// <param name="Threat">The Threat<see cref="Threat"/>.</param>
        /// <returns>The <see cref="Task{Threat}"/>.</returns>
        [HttpPost(nameof(UpdateThreat))]
        public async Task<Threat> UpdateThreat(Threat Threat)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ThreatId", Threat.ThreatId, DbType.Int32);
            dbparams.Add("TTypeId", Threat.TTypeId, DbType.Int32);
            dbparams.Add("ThreatCode", Threat.ThreatCode, DbType.String);
            dbparams.Add("ThreatName", Threat.ThreatName, DbType.String);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_Update_Threat]", dbparams, CommandType.StoredProcedure));
            return new Threat() { TTypeId = result };
        }

        /// <summary>
        /// The BlockDeleteThreat.
        /// </summary>
        /// <param name="Threat">The Threat<see cref="Threat"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPost(nameof(BlockDeleteThreat))]
        public async Task<Message<Threat>> BlockDeleteThreat(Threat Threat)
        {
            Message<Threat> message = new Message<Threat>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ThreatId", Threat.ThreatId, DbType.Int32);
            dbparams.Add("ThreatName", Threat.ThreatName, DbType.String);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteThreat]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = new Threat();
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new Threat();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(GetThreatsByClientId))]
        public async Task<Message<List<Threat>>> GetThreatsByClientId(DomainBaseRiskScore model)
        {
            var message = new Message<List<Threat>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("ConCatId", model.CategoryId, DbType.Int64);
            dbparams.Add("QuestionnaireMapId", model.AnswerId, DbType.Int64);
            var result = await Task.FromResult(_dapper.GetAll<Threat>("SP_GetThreatsByClientId", dbparams, CommandType.StoredProcedure));
            message.Data = result;
            return message;
        }

        [HttpPost(nameof(GetProbabilityScore))]
        public async Task<Message<int>> GetProbabilityScore(DomainBaseRiskScore model)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("CategoryId", model.CategoryId, DbType.Int64);
            dbparams.Add("AnswerId", model.AnswerId, DbType.Int64);
            dbparams.Add("ThreatId", model.ThreatId, DbType.Int64);
            var result = await Task.FromResult(_dapper.Get<int>("SP_GetProbabilityScore", dbparams, CommandType.StoredProcedure));
            message.Data = result;
            return message;
        }

        [HttpGet(nameof(GetControlCategoriesByThreatId))]
        public async Task<List<Threat>> GetControlCategoriesByThreatId(long threatId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("threatId", threatId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<Threat>("SP_GetControlCategoriesByThreatId", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetThreatsByControlCategoryId))]
        public async Task<List<Threat>> GetThreatsByControlCategoryId(int controlCategoryId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("controlCategoryId", controlCategoryId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<Threat>("SP_GetThreatsByControlCategoryId", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(GetFirstQuesAnswersByConCatId))]
        public async Task<List<ThreatEnabler>> GetFirstQuesAnswersByConCatId(long controlCategoryId, long OrganizationId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("controlCategoryId", controlCategoryId, DbType.Int32);
            dbparams.Add("OrganizationId", OrganizationId, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<ThreatEnabler>("SP_GetFirstQuesAnswersByConCatId", dbparams, CommandType.StoredProcedure));
        }

        [HttpGet(nameof(ValidateThreatName))]
        public async Task<bool> ValidateThreatName(int ThreatTypeId, string ThreatName)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ThreatTypeId", ThreatTypeId, DbType.Int32);
            dbparams.Add("ThreatName", ThreatName, DbType.String);
            var res = await Task.FromResult(_dapper.Get<int>("SP_ValidateThreatName", dbparams, CommandType.StoredProcedure));
            return res > 0 ? true : false;
        }
    }
}
