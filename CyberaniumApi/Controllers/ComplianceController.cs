﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="ComplianceController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ComplianceController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplianceController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public ComplianceController(IDapper dapper)
        {
            _dapper = dapper;
        }

        [HttpPost(nameof(SubmitCompliance))]
        public async Task<Message<Compliance>> SubmitCompliance(Compliance data)
        {
            var CreateXML = new XMLCreater();
            var message = new Message<Compliance>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("ControlCategory", data.ControlCategory, DbType.Int32);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            dbparams.Add("UserID", data.CreatedBy, DbType.Int32);
            dbparams.Add("QuestionnaireXML", CreateXML.CreateComplainceXML(data.QuestionnaireXML), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitCompliance]", dbparams, CommandType.StoredProcedure));
            if (result > 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetCompliance))]
        public async Task<List<Compliance>> GetCompliance()
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", 0, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Compliance>("[dbo].[SP_GetCompliance]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(BlockDeleteCompliance))]
        public async Task<Message<int>> BlockDeleteCompliance(int id)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", id, DbType.Int32);
            message.Data = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteCompliance]", dbparams, CommandType.StoredProcedure));
            message.IsSuccess = message.Data > 0 ? true : false;
            message.ReturnMessage = message.Data > 0 ? "Success" : "Failed";
            return message;
        }

        [HttpGet(nameof(SetIsPrimary))]
        public async Task<Message<int>> SetIsPrimary(int id)
        {
            var message = new Message<int>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", id, DbType.Int32);
            message.Data = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_SetIsPrimary]", dbparams, CommandType.StoredProcedure));
            message.IsSuccess = message.Data > 0 ? true : false;
            message.ReturnMessage = message.Data > 0 ? "Success" : "Failed";
            return message;
        }
    }
}
