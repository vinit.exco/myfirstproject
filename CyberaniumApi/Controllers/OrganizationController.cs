﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumApi.Services;
    using CyberaniumModels;
    using CyberaniumModels.Comman;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Data.SqlClient;
    using Microsoft.Extensions.Configuration;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationController : ControllerBase
    {
        Message<Organization> message = new Message<Organization>();
        private readonly IDapper _dapper;
        private readonly IConfiguration _config;
        private string Connectionstring = "DevConnection";
        SqlConnection con;
        public OrganizationController(IDapper dapper, IConfiguration _config)
        {
            _dapper = dapper;
            con = new SqlConnection(_config.GetConnectionString(Connectionstring));
        }

        #region New Organization

        [HttpGet(nameof(GetOrganizationList))]
        public async Task<List<Organization>> GetOrganizationList() => await Task.FromResult(_dapper.GetAll<Organization>("sp_GetOrganizationList", null, default));

        [HttpGet(nameof(AssignedOrgList))]
        public async Task<List<Organization>> AssignedOrgList()
        {
            var result = await Task.FromResult(_dapper.GetAll<Organization>("proc_AssignedOrgList",null,commandType: CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetAssessmentOrganizationList))]
        public async Task<List<Organization>> GetAssessmentOrganizationList(int AssesmentValue)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("AssesmentValue", AssesmentValue, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("sp_GetAssesmentOrganizationList", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(EditAssignedOrg))]
        public async Task<List<Organization>> EditAssignedOrg(long OrgID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int64);

            var result = await Task.FromResult(_dapper.GetAll<Organization>("proc_EditAssignedOrg", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(BindFunctionProcess))]
        public async Task<List<OrganizationProcesss>> BindFunctionProcess(long OrgID,int FunId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int64);
            dbparams.Add("FunId", FunId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<OrganizationProcesss>("Sp_getProcess", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpPost(nameof(AddUpadeteOrganization))]
        public async Task<Message<Organization>> AddUpadeteOrganization(Organization model)
        {
            var message = new Message<Organization>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", model.Id, DbType.Int64);
            dbparams.Add("Name", model.Name, DbType.String);
            dbparams.Add("OrganizationXML", string.IsNullOrEmpty(model.OrganizationXML) ? null : obj.CreateOrganizationXML(model.OrganizationXML), DbType.Xml);
            dbparams.Add("UserID", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_AddUpadeteOrganization]", dbparams, CommandType.StoredProcedure));
            model.Id = result;
            message.Data = model;
            message.IsSuccess = result > 0 ? true : false;
            return message;
        }

        [HttpGet(nameof(DeleteOrganizationById))]
        public async Task<Message<Organization>> DeleteOrganizationById(long parentOrgId = 0)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("parentOrgId", parentOrgId, DbType.Int64);
            var res = await Task.FromResult(_dapper.Get<long>("sp_DeleteOrganizationById", dbparams, CommandType.StoredProcedure));
            return new Message<Organization> { IsSuccess = res > 0 ? true : false };
        }

        [HttpGet(nameof(CheckOrgDuplicate))]
        public async Task<Message<Organization>> CheckOrgDuplicate(string OrgName = "")
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgName", OrgName, DbType.String);
            var res = await Task.FromResult(_dapper.Get<long>("sp_CheckDuplicateOrganization", dbparams, CommandType.StoredProcedure));
            return new Message<Organization> { IsSuccess = res > 0 ? true : false };
        }

        [HttpGet(nameof(AssessmentListByOrgId))]
        public async Task<List<OrgAssignedAssessment>> AssessmentListByOrgId(long orgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("orgId", orgId, DbType.String);
            var res = await Task.FromResult(_dapper.GetAll<OrgAssignedAssessment>("sp_AssessmentListByOrgId", dbparams, CommandType.StoredProcedure));
            return res;
        }
        #endregion

        #region Old Organization

        [HttpPost(nameof(SaveThreatIdentified))]
        public async Task<Message<Organization>> SaveThreatIdentified(Organization organization)
        {
            Message<Organization> message = new Message<Organization>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("IP", organization.CreatedIP, DbType.String);
            dbparams.Add("UserID", organization.CreatedBy, DbType.Int32);
            dbparams.Add("Probability", CreateXML.CreateOrganogramXML(organization.Probability), DbType.Xml);
            dbparams.Add("ParentOrgName", organization.ParentOrgName, DbType.String);
            dbparams.Add("ChildOrgName", organization.ChildOrgName, DbType.String);
            dbparams.Add("ChildOrgLocation", organization.ChildOrgLocation, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SaveThreatIdentified", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = organization;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = organization;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(CheckThreatById))]
        public async Task<Message<List<Organization>>> CheckThreatById(Base model)
        {
            var message = new Message<List<Organization>>();
            var context = new DynamicParameters();
            context.Add("OrgName", model.CreatedIP, DbType.String);
            context.Add("ChildOrgName", model.UpdatedIP, DbType.String);
            message.Data = await Task.FromResult(_dapper.GetAll<Organization>("SP_CheckThreatById", context, CommandType.StoredProcedure));
            return message;
        }

        [HttpGet(nameof(GetOrganizationByUserId))]
        public async Task<List<Organization>> GetOrganizationByUserId(long userId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("UserId", userId, DbType.Int64);
            return await Task.FromResult(_dapper.GetAll<Organization>("SP_GetOrganizationByUserId", dbparams, CommandType.StoredProcedure));
        }

        [HttpPost(nameof(CheckParentOrgName))]
        public async Task<Message<int>> CheckParentOrgName(Organization data)
        {
            Message<int> message = new Message<int>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgName", data.ParentOrgName, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("proc_CheckParentOrgName", dbparams, CommandType.StoredProcedure));
            if (result > 0)
            {
                message.Data = result;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = result;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(SubmitOrganization))]
        public async Task<Message<Organization>> SubmitOrganization(Organization data)
        {
            Message<Organization> message = new Message<Organization>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("ParentOrgName", data.ParentOrgName, DbType.String);
            dbparams.Add("CorporateOfficeLocation", data.CorporateOfficeLocation, DbType.String);
            dbparams.Add("EmployeId", data.RoleId, DbType.Int32);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            dbparams.Add("UserID", data.CreatedBy, DbType.Int32);
            dbparams.Add("CountryID", data.CountryID, DbType.Int32);
            dbparams.Add("StateID", data.StateId, DbType.Int32);
            dbparams.Add("CityID", data.CityId, DbType.Int32);
            dbparams.Add("ClientAdminIds", data.ClientAdminIds, DbType.String);
            dbparams.Add("Xml", obj.xmlOrganizationParentAudity(data.xmlAudity), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitOrganization]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpPost(nameof(BlockDeleteOrganization))]
        public async Task<Message<Organization>> BlockDeleteOrganization(Organization data)
        {
            Message<Organization> message = new Message<Organization>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteOrganization]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new Organization();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetOrganizationByEmplyoyeeID))]
        public async Task<List<Organization>> GetOrganizationByEmplyoyeeID(int EmployeeId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("EmployeeId", EmployeeId.ToString(), DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("Proc_GetOrganizationByEmplyoyeeID", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetOrganizationNotAssigned))]
        public async Task<List<Organization>> GetOrganizationNotAssigned()
        {
            var result = await Task.FromResult(_dapper.GetAll<Organization>("Proc_GetOrganizationNotAssigned", null, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetAssignedOrganizationByEmplyoyeeID))]
        public async Task<List<Organization>> GetAssignedOrganizationByEmplyoyeeID(int EmployeeId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("EmployeeId", EmployeeId.ToString(), DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("Proc_GetAssignedOrganizationByEmplyoyeeID", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpPost(nameof(GetRiskDomainsByOrgId))]
        public async Task<Message<List<RiskDomain>>> GetRiskDomainsByOrgId(RiskDomain model)
        {

            var res = new Message<List<RiskDomain>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            var list = await Task.FromResult(_dapper.GetAll<RiskDomain>("SP_GetRiskDomainsByOrgId", dbparams, commandType: CommandType.StoredProcedure));
            res.Data = list;
            return res;
        }

        [HttpPost(nameof(SubmitDomainCriticalityScore))]
        public async Task<Message<int>> SubmitDomainCriticalityScore(RiskDomain model)
        {
            Message<int> message = new Message<int>();
            var dbparams = new DynamicParameters();
            var CreateXML = new XMLCreater();
            dbparams.Add("Id", model.Id, DbType.Int64);
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", model.ChildOrgId, DbType.Int64);
            dbparams.Add("FunctionId", model.FunctionId, DbType.Int64);
            dbparams.Add("ProcessId", model.ProcessId, DbType.Int64);
            dbparams.Add("DomainCriticalityScoreXML", CreateXML.CreateDomainCriticalityScoreXML(model.DomainCriticalityScoreXML), DbType.String);
            dbparams.Add("UserID", model.CreatedBy, DbType.Int32);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("SP_SubmitDomainCriticalityScore", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = 200;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = 500;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetOrganizationAudityListByID))]
        public async Task<List<ClientAudity>> GetOrganizationAudityListByID(long ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<ClientAudity>("proc_GetOrganizationAudityListByID", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetParentOrgListByEmployeeId))]
        public async Task<List<Organization>> GetParentOrgListByEmployeeId(int EmployeeId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("EmployeeId", EmployeeId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("SP_GetParentOrgListByEmployeeId", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetChildOrgListByEmployeeId))]
        public async Task<List<Organization>> GetChildOrgListByEmployeeId(int EmployeeId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("EmployeeId", EmployeeId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Organization>("SP_GetChildOrgListByEmployeeId", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetOrgFunctions))]
        public async Task<List<OrganizationFunctions>> GetOrgFunctions(int employeeId, long id)
        {
            var context = new DynamicParameters();
            context.Add("EmployeeId", employeeId, DbType.Int32);
            context.Add("Id", id, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<OrganizationFunctions>("SP_GetOrgFunctions", context, CommandType.StoredProcedure));
            return res;
        }

        [HttpGet(nameof(GetConsultants))]
        public async Task<List<Consultant>> GetConsultants()
        {
            var context = new DynamicParameters();
            var res = await Task.FromResult(_dapper.GetAll<Consultant>("SP_GetConsultants", context, CommandType.StoredProcedure));
            return res;
        }

        [HttpGet(nameof(GetAssignedConsultantsByOrgId))]
        public async Task<List<Consultant>> GetAssignedConsultantsByOrgId(long parentOrgId, long childOrgId)
        {
            var context = new DynamicParameters();
            context.Add("parentOrgId", parentOrgId, DbType.Int64);
            context.Add("childOrgId", childOrgId, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<Consultant>("SP_GetAssignedConsultantsByOrgId", context, CommandType.StoredProcedure));
            return res;
        }

        [HttpGet(nameof(GetAssignedConsultantsByFunctionId))]
        public async Task<List<Consultant>> GetAssignedConsultantsByFunctionId(int functionId)
        {
            var context = new DynamicParameters();
            context.Add("functionId", functionId, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<Consultant>("SP_GetAssignedConsultantsByFunctionId", context, CommandType.StoredProcedure));
            return res;
        }

        [HttpGet(nameof(GetAssignedConsultantsByFunctionMapId))]
        public async Task<List<Consultant>> GetAssignedConsultantsByFunctionMapId(long functionId = 0)
        {
            var context = new DynamicParameters();
            context.Add("functionId", functionId, DbType.Int64);
            var res = await Task.FromResult(_dapper.GetAll<Consultant>("SP_GetAssignedConsultantsByFunctionMapId", context, CommandType.StoredProcedure));
            return res;
        }

        [HttpGet(nameof(GetAssignedConsultantsByProcessId))]
        public async Task<List<Consultant>> GetAssignedConsultantsByProcessId(int processId)
        {
            var context = new DynamicParameters();
            context.Add("processId", processId, DbType.Int32);
            var res = await Task.FromResult(_dapper.GetAll<Consultant>("SP_GetAssignedConsultantsByProcessId", context, CommandType.StoredProcedure));
            return res;
        }
        #endregion
    }
}