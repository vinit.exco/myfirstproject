﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="RegulationController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class RegulationController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegulationController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public RegulationController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The GetAllLawRegulation.
        /// </summary>
        /// <returns>The <see cref="Task{List{LawRegulation}}"/>.</returns>
        [HttpGet(nameof(GetAllLawRegulation))]
        public async Task<List<LawRegulation>> GetAllLawRegulation()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<LawRegulation>("[dbo].[SP_GetAllLawRegulation]", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(GetAllRegulation))]
        public async Task<List<LawRegulation>> GetAllRegulation()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<LawRegulation>("[dbo].[proc_GetAllRegulation]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The SaveLawRegulation.
        /// </summary>
        /// <param name="LawRegulation">The LawRegulation<see cref="LawRegulation"/>.</param>
        /// <returns>The <see cref="Task{LawRegulation}"/>.</returns>
        [HttpPost(nameof(SaveupdateLawRegulation))]
        public async Task<Message<LawRegulation>> SaveupdateLawRegulation(LawRegulation LawRegulation)
        {
            Message<LawRegulation> message = new Message<LawRegulation>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", LawRegulation.Id, DbType.Int16);
            dbparams.Add("SectorId", LawRegulation.SectorId, DbType.Int16);
            dbparams.Add("Title", LawRegulation.Title, DbType.String);
            dbparams.Add("Code", LawRegulation.Code, DbType.String);
            dbparams.Add("CountryId", LawRegulation.CountryId, DbType.Int32);
            dbparams.Add("IP", LawRegulation.CreatedIP, DbType.String);
            dbparams.Add("UserID", LawRegulation.CreatedBy, DbType.Int32);
            dbparams.Add("XmlRegulation", obj.CreateRegulationXml(LawRegulation.XmlRegulation), DbType.Xml);

            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_Add_LawRegulation]", dbparams, CommandType.StoredProcedure));

            if (result == 0)
            {
                message.Data = LawRegulation;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = LawRegulation;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;

        }
     




        /// <summary>
        /// The BlockDeleteLawRegulation.
        /// </summary>
        /// <param name="LawRegulation">The LawRegulation<see cref="LawRegulation"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPost(nameof(BlockDeleteLawRegulation))]
        public async Task<Message<LawRegulation>> BlockDeleteLawRegulation(LawRegulation LawRegulation)
        {
            Message<LawRegulation> message = new Message<LawRegulation>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", LawRegulation.Id, DbType.Int32);
            dbparams.Add("Title", LawRegulation.Title, DbType.String);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteLawRegulation]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = new LawRegulation();
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new LawRegulation();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }

        [HttpGet(nameof(GetMapRegulationByID))]
        public async Task<List<LawRegulation>> GetMapRegulationByID(int ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", ID, DbType.Int32);
            return await Task.FromResult(_dapper.GetAll<LawRegulation>("SP_GetMapRegulationById", dbparams, CommandType.StoredProcedure));
        }
    }
}
