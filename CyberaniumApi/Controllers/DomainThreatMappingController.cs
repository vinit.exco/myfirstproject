﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="DomainThreatMappingController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DomainThreatMappingController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainThreatMappingController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public DomainThreatMappingController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The GetAllDomainThreatMapping.
        /// </summary>
        /// <returns>The <see cref="Task{List{DomainThreatMapping}}"/>.</returns>
        [HttpGet(nameof(GetDomainThreatMapping))]
        public async Task<List<DomainThreatMapping>> GetDomainThreatMapping()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<DomainThreatMapping>("[dbo].[SP_GetDomainThreatMapping]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The SaveDomainThreatMapping.
        /// </summary>
        /// <param name="DomainThreatMapping">The DomainThreatMapping<see cref="DomainThreatMapping"/>.</param>
        /// <returns>The <see cref="Task{DomainThreatMapping}"/>.</returns>
        [HttpPost(nameof(SubmitDomainThreatMapping))]
        public async Task<DomainThreatMapping> SubmitDomainThreatMapping(DomainThreatMapping DomainThreatMapping)
        {
            var dbparams = new DynamicParameters();
            var obj = new XMLCreater();
            dbparams.Add("Id", DomainThreatMapping.Id, DbType.Int64);
            dbparams.Add("DomainId", DomainThreatMapping.DomainId, DbType.Int32);
            dbparams.Add("DomainThreatMappingXML", obj.CreateDomainThreatMappingXML(DomainThreatMapping.DomainThreatMappingXML), DbType.String);
            dbparams.Add("IP", DomainThreatMapping.CreatedIP, DbType.String);
            dbparams.Add("UserID", DomainThreatMapping.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitDomainThreatMapping]", dbparams, CommandType.StoredProcedure));
            return new DomainThreatMapping() { Id = result };
        }

        [HttpGet(nameof(GetDomainThreatMapList))]
        public async Task<List<DomainThreatMapping>> GetDomainThreatMapList(string domainIds)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("domainIds", domainIds, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<DomainThreatMapping>("[dbo].[SP_GetDomainThreatMapList]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpPost(nameof(GetThreatByDomainId))]
        public async Task<Message<List<DomainThreatMapping>>> GetThreatByDomainId(DomainThreatMapping data)
        {
            var dbparams = new DynamicParameters();
            var res = new Message<List<DomainThreatMapping>>();
            dbparams.Add("DomainId", data.DomainId, DbType.Int32);
            res.Data = await Task.FromResult(_dapper.GetAll<DomainThreatMapping>("[dbo].[SP_GetThreatByDomainId]", dbparams, CommandType.StoredProcedure));
            return res;
        }
    }
}
