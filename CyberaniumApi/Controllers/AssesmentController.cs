﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="AssesmentController" />.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AssesmentController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AssesmentController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public AssesmentController(IDapper dapper)
        {
            _dapper = dapper;
        }

        [HttpPost(nameof(SubmitEnablerAssesments))]
        public async Task<Message<ClientEnablers>> SubmitEnablerAssesments(ClientEnablers data)
        {
            try
            {
                Message<ClientEnablers> message = new Message<ClientEnablers>();
                var obj = new XMLCreater();
                var dbparams = new DynamicParameters();
                dbparams.Add("Id", data.Id, DbType.Int64);
                dbparams.Add("CategoryId", data.CategoryId, DbType.Int32);
                dbparams.Add("QuestionnaireXML", obj.CreateQuestionnaireXML(data.QuestionnaireXML), DbType.Xml);
                dbparams.Add("PeopleGovnXML", obj.PeopleGovnXML(data.PeopleGovnXML), DbType.Xml);
                dbparams.Add("userID", data.CreatedBy, DbType.Int32);
                dbparams.Add("IP", data.CreatedIP, DbType.String);
                var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitAssesment]", dbparams, CommandType.StoredProcedure));
                if (result == 0)
                {
                    message.Data = data;
                    message.IsSuccess = true;
                    message.ReturnMessage = "Success";
                }
                else
                {
                    message.Data = data;
                    message.IsSuccess = false;
                    message.ReturnMessage = "Success";
                }
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpPost(nameof(SubmitAssignedAssessment))]
        public async Task<Message<OrgAssignedAssessment>> SubmitAssignedAssessment(OrgAssignedAssessment model)
        {
            var obj = new XMLCreater();
            var parameters = new DynamicParameters();
            var message = new Message<OrgAssignedAssessment>();
            parameters.Add("OrgId", model.OrgId, DbType.Int32);
            parameters.Add("userId", model.CreatedBy, DbType.Int32);
            parameters.Add("AssessmentXML", obj.CreateAssessmentXML(model.AssessmentXML), DbType.Xml);
            var result = await Task.FromResult(_dapper.Insert<int>("sp_SubmitAssignedAssessment", parameters, CommandType.StoredProcedure));
            message.ReturnMessage = result > 0 ? "Success" : "Unsuccess";
            message.IsSuccess = result > 0 ? true : false;
            message.Data = model;
            return message;
        }
        //[HttpPost(nameof(SubmitAssesment))]
        //     public async Task<Message<Assesment>> SubmitAssesment(Assesment data)
        //     {
        //         Message<Assesment> message = new Message<Assesment>();
        //         var obj = new XMLCreater();
        //         var dbparams = new DynamicParameters();
        //         dbparams.Add("Id", data.Id, DbType.Int32);
        //         dbparams.Add("OrganizationId", data.OrganizationId, DbType.Int32);
        //         dbparams.Add("QuestionnaireXML", obj.CreateQuestionnaireXML(data.QuestionnaireXML), DbType.Xml);
        //         dbparams.Add("EventType", data.EventType, DbType.Int16);
        //         dbparams.Add("ChildOrgId", data.ChildOrgId, DbType.Int32);
        //         dbparams.Add("userID", data.CreatedBy, DbType.Int32);
        //         dbparams.Add("IP", data.CreatedIP, DbType.String);
        //         var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitAssesment]", dbparams, CommandType.StoredProcedure));
        //         if (result == 0)
        //         {
        //             message.Data = data;
        //             message.IsSuccess = true;
        //             message.ReturnMessage = "Success";
        //         }
        //         else
        //         {
        //             message.Data = data;
        //             message.IsSuccess = false;
        //             message.ReturnMessage = "Success";
        //         }
        //         return message;
        //     }

        [HttpGet(nameof(GetAssesment))]
        public async Task<List<Assesment>> GetAssesment()
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", 0, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<Assesment>("[dbo].[SP_GetAssesment]", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(GetChildOrg))]
        public async Task<List<ChildOrg>> GetChildOrg()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<ChildOrg>("select * from OrganizationChildMap where isdeleted =0", dbparams, CommandType.Text));
            return result;
        }

        [HttpPost(nameof(BlockDeleteAssesment))]
        public async Task<Message<Assesment>> BlockDeleteAssesment(Assesment data)
        {
            Message<Assesment> message = new Message<Assesment>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", data.Id, DbType.Int32);
            dbparams.Add("EventType", data.EventType, DbType.Int16);
            var result = await Task.FromResult(_dapper.Update<int>("[dbo].[SP_BlockDeleteAssesment]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new Assesment();
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpGet(nameof(GetEnablerQuestionByClientCategory))]
        public async Task<Message<List<GetEnablerQuestion>>> GetEnablerQuestionByClientCategory(long parentOrgId, long childOrgId, int categoryId)
        {
            Message<List<GetEnablerQuestion>> message = new Message<List<GetEnablerQuestion>>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ParentOrgId", parentOrgId, DbType.Int64);
            dbparams.Add("ChildOrgId", childOrgId, DbType.Int64);
            dbparams.Add("CategoryId", categoryId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<GetEnablerQuestion>("[dbo].[GetEnablerQuestionByClientCategory]", dbparams, CommandType.StoredProcedure));

            if (result.Count >= 0)
            {
                message.Data = result;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = new List<GetEnablerQuestion>();
                message.IsSuccess = false;
                message.ReturnMessage = "Fail";
            }
            return message;
        }

        [HttpPost(nameof(SubmitScopeAssessment))]
        public async Task<Message<ScopeAssessments>> SubmitScopeAssessment(ScopeAssessments model)
        {
            var message = new Message<ScopeAssessments>();
            var obj = new XMLCreater();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", model.Id, DbType.Int32);
            dbparams.Add("ParentOrgId", model.ParentOrgId, DbType.Int64);
            dbparams.Add("ScopeAssessmentXml", obj.CreateScopeAssessmentXml(model.ScopeAssessmentXml), DbType.Xml);
            dbparams.Add("ScopeAssessmentRiskDomainMapXml", obj.CreateScopeAssessmentRiskDomainMapXml(model.ScopeAssessmentRiskDomainMapXml), DbType.Xml);
            dbparams.Add("ScopeAssessmentISOMapXml", obj.CreateScopeAssessmentISOMapXml(model.ScopeAssessmentISOMapXml), DbType.Xml);
            dbparams.Add("ScopeAssessmentRegulationMapXml", obj.CreateScopeAssessmentRegulationMapXml(model.ScopeAssessmentRegulationMapXml), DbType.Xml);
            dbparams.Add("UserID", model.CreatedBy, DbType.Int64);
            dbparams.Add("IP", model.CreatedIP, DbType.String);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitScopeAssessment]", dbparams, CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = model;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = model;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
    }
}
