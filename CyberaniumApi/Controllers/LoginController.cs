﻿namespace CyberaniumApi.Controllers
{
    using CyberaniumApi.Helpers;
    using CyberaniumApi.IServices;
    using CyberaniumModels;
    using Dapper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="LoginController" />.
    /// </summary>
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        /// <summary>
        /// Defines the _dapper.
        /// </summary>
        private readonly IDapper _dapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginController"/> class.
        /// </summary>
        /// <param name="dapper">The dapper<see cref="IDapper"/>.</param>
        public LoginController(IDapper dapper)
        {
            _dapper = dapper;
        }

        /// <summary>
        /// The Create.
        /// </summary>
        /// <param name="data">The data<see cref="UserLogin"/>.</param>
        /// <returns>The <see cref="Task{Message{int}}"/>.</returns>
        ///
        [AllowAnonymous]
        [HttpPost("UserCreation")]

        public async Task<Message<UserLogin>> SaveUpdateUser(UserLogin data)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", data.Id, DbType.Int32);
            dbparams.Add("UserName", data.UserName, DbType.String);
            dbparams.Add("Password", data.Password, DbType.String);
            dbparams.Add("Email", data.Email, DbType.String);
            dbparams.Add("RoleId", data.RoleId, DbType.Int32);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SaveUpdateUser]"
                , dbparams,
                commandType: CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }
        [HttpPost(nameof(SaveOrganizationUser))]
        public async Task<Message<UserLogin>> SaveOrganizationUser(UserLogin data)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", data.Id, DbType.Int32);
            dbparams.Add("UserName", data.UserName, DbType.String);
            dbparams.Add("Password", data.PasswordNew, DbType.String);
            dbparams.Add("Email", data.Email, DbType.String);
            dbparams.Add("t_UserID", data.t_UserIDNew, DbType.String);
            dbparams.Add("t_CanAccess", data.CanAccess, DbType.String);
            dbparams.Add("t_AccessRole", data.t_AccessRole, DbType.Int32);
            dbparams.Add("IP", data.CreatedIP, DbType.String);
            dbparams.Add("CreatedBy", data.CreatedBy, DbType.Int32);
            dbparams.Add("OrganizationID", data.OrganizationId, DbType.Int32);
            var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SaveOrganizationUser]", dbparams, commandType: CommandType.StoredProcedure));
            if (result == 0)
            {
                message.Data = data;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = data;
                message.IsSuccess = false;
                message.ReturnMessage = "Success";
            }
            return message;
        }


        [HttpPost(nameof(SubmitClientAdmin))]
        public async Task<Message<UserLogin>> SubmitClientAdmin(UserLogin data)
        {
            try
            {
                Message<UserLogin> message = new Message<UserLogin>();
                var obj = new XMLCreater();
                var dbparams = new DynamicParameters();
                dbparams.Add("Id", data.Id, DbType.Int32);
                dbparams.Add("OrganizationId", data.OrganizationId, DbType.Int32);
                dbparams.Add("Organization", data.Organization, DbType.String);
                dbparams.Add("ClientAdminXml", obj.ClientAdminXml(data.ClientAdminXml), DbType.Xml);
                dbparams.Add("userID", data.CreatedBy, DbType.Int32);
                dbparams.Add("IP", data.CreatedIP, DbType.String);
                var result = await Task.FromResult(_dapper.Insert<int>("[dbo].[SP_SubmitClientAdmin]", dbparams, CommandType.StoredProcedure));
                if (result == 0)
                {
                    message.Data = data;
                    message.IsSuccess = true;
                    message.ReturnMessage = "Success";
                }
                else
                {
                    message.Data = data;
                    message.IsSuccess = false;
                    message.ReturnMessage = "Success";
                }
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet(nameof(getClientadmin))]
        public async Task<List<ClientAdmin>> getClientadmin()
        {
            var dbparams = new DynamicParameters();
            var result = await Task.FromResult(_dapper.GetAll<ClientAdmin>("Sp_GetClientAdmin", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(getClientadminFillData))]
        public async Task<List<ClientAdmin>> getClientadminFillData(int OrgId)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgId, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<ClientAdmin>("Sp_getClientadminFillData", dbparams, CommandType.StoredProcedure));
            return result;
        }


        [HttpGet(nameof(DeleteClientAdmin))]
        public async Task<Message<ClientAdmin>> DeleteClientAdmin(int Id)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", Id, DbType.Int32);
            var res = await Task.FromResult(_dapper.Get<long>("Sp_DeleteClientAdmin", dbparams, CommandType.StoredProcedure));
            return new Message<ClientAdmin> { IsSuccess = res > 0 ? true : false };
        }

        [HttpGet(nameof(getOrganizationUsers))]
        public async Task<List<UserLogin>> getOrganizationUsers(int OrgID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("OrgID", OrgID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<UserLogin>("proc_getOrganizationUsers", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(EditOrganizationUsers))]
        public async Task<List<UserLogin>> EditOrganizationUsers(int ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<UserLogin>("proc_EditOrganizationUsers", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(OrganizationUsersPassword))]
        public async Task<List<UserLogin>> OrganizationUsersPassword(int ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<UserLogin>("proc_OrganizationUsersPassword", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(OrganizationUsersEmail))]
        public async Task<List<UserLogin>> OrganizationUsersEmail(string Email)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Email", Email, DbType.String);
            var result = await Task.FromResult(_dapper.GetAll<UserLogin>("proc_OrgUserEmail", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(SaveUsername))]
        public async Task<Message<UserLogin>> SaveUsername(string UserName = "")
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("UserName", UserName, DbType.String);
            var res = await Task.FromResult(_dapper.Get<long>("proc_saveUsername", dbparams, CommandType.StoredProcedure));
            return new Message<UserLogin> { IsSuccess = res > 0 ? true : false };

        }
        [HttpGet(nameof(PasswordResetOrganizationUsers))]
        public async Task<List<UserLogin>> PasswordResetOrganizationUsers(int ID,string Password, int ModifiedBy)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            dbparams.Add("Password",Password, DbType.String);
            dbparams.Add("ModifiedBy", ModifiedBy, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<UserLogin>("proc_passwordresetOrgUsers", dbparams, CommandType.StoredProcedure));
            return result;
        }

        [HttpGet(nameof(DeleteOrganizationUsers))]
        public async Task<List<UserLogin>> DeleteOrganizationUsers(int ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<UserLogin>("proc_deleteOrganizationUsers", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpGet(nameof(DisableOrganizationUsers))]
        public async Task<List<UserLogin>> DisableOrganizationUsers(int ID)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("ID", ID, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<UserLogin>("proc_DisableOrganizationUsers", dbparams, CommandType.StoredProcedure));
            return result;
        }

        /// <summary>
        /// The GetById.
        /// </summary>
        /// <param name="Id">The Id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{UserLogin}"/>.</returns>
        [HttpGet(nameof(GetById))]
        public async Task<UserLogin> GetById(int Id)
        {
            var result = await Task.FromResult(_dapper.Get<UserLogin>($"Select * from [UserLogin] where Id = {Id}", null, commandType: CommandType.Text));
            return result;
        }
        [AllowAnonymous]
        [HttpGet(nameof(GetAllUsers))]
        public async Task<List<UserLogin>> GetAllUsers()
        {
            var result = await Task.FromResult(_dapper.GetAll<UserLogin>($"Select [ID], [UserName], [Email], convert(varchar(100),DecryptByPassPhrase('1',Password )) as Password, [UserType], [RoleId], [Activated], [Deleted], [CreatedBy], [CreatedOn], [CreatedIP], [ModifiedBy], [ModifiedOn], [ModifiedIP], [LastLogin], [Hits], [Hittingtime] from [UserLogin] where Deleted=0 and OrganizationID=0 order by ID Desc", null, commandType: CommandType.Text));
            return result;
        }

        /// <summary>
        /// The GetByIds.
        /// </summary>
        /// <param name="Id">The Id<see cref="UserLogin"/>.</param>
        /// <returns>The <see cref="Task{Message{UserLogin}}"/>.</returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        [ActionName(nameof(GetByIds))]
        public async Task<Message<UserLogin>> GetByIds(UserLogin data)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Password", data.Password, DbType.String);
            dbparams.Add("UserName", data.UserName, DbType.String);
            var result = await Task.FromResult(_dapper.Get<UserLogin>($"GetLoginWithCredential", dbparams, commandType: CommandType.StoredProcedure));
            if (result != null)
            {
                message.Data = result;
                message.IsSuccess = true;
                message.ReturnMessage = "Success";
            }
            else
            {
                message.Data = result;
                message.IsSuccess = false;
                message.ReturnMessage = "Fail";
            }
            return message;
        }

        /// <summary>
        /// The Delete.
        /// </summary>
        /// <param name="Id">The Id<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpDelete(nameof(Delete))]
        public async Task<int> Delete(int Id)
        {
            var result = await Task.FromResult(_dapper.Execute($"Delete [Dummy] Where Id = {Id}", null, commandType: CommandType.Text));
            return result;
        }

        /// <summary>
        /// The Count.
        /// </summary>
        /// <param name="num">The num<see cref="int"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpGet(nameof(Count))]
        public Task<int> Count(int num)
        {
            var totalcount = Task.FromResult(_dapper.Get<int>($"select COUNT(*) from [Dummy] WHERE Age like '%{num}%'", null,
                    commandType: CommandType.Text));
            return totalcount;
        }

        /// <summary>
        /// The Update.
        /// </summary>
        /// <param name="data">The data<see cref="UserLogin"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        [HttpPatch(nameof(Update))]
        public Task<int> Update(UserLogin data)
        {
            var dbPara = new DynamicParameters();
            dbPara.Add("Id", data.Id);
            dbPara.Add("Name", data.UserName, DbType.String);

            var updateArticle = Task.FromResult(_dapper.Update<int>("[dbo].[SP_Update_Article]",
                            dbPara,
                            commandType: CommandType.StoredProcedure));
            return updateArticle;
        }
        [AllowAnonymous]
        [HttpGet(nameof(GetRoleMenuByRoleId))]
        public async Task<List<RBAC>> GetRoleMenuByRoleId(int id)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("RoleId", id, DbType.Int32);
            var result = await Task.FromResult(_dapper.GetAll<RBAC>("SP_GetRoleMenuByRoleId", dbparams, CommandType.StoredProcedure));
            return result;
        }
        [HttpPost(nameof(DeleteActiveUser))]
        public async Task<Message<UserLogin>> DeleteActiveUser(UserLogin user)
        {
            Message<UserLogin> message = new Message<UserLogin>();
            var dbparams = new DynamicParameters();
            dbparams.Add("Id", user.Id, DbType.Int32);
            dbparams.Add("Type", user.UserType, DbType.Int32);
            var result = await Task.FromResult(_dapper.Update<int>("SP_UserDeleteActive", dbparams, CommandType.StoredProcedure));
            message.Data = new UserLogin() { Id = result };
            message.IsSuccess = true;
            return message;
        }
    }
}
