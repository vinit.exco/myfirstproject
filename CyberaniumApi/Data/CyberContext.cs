﻿using CyberaniumApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberaniumApi.Data
{
    public class CyberContext : DbContext
    {
        public CyberContext() { }
        public CyberContext(DbContextOptions<CyberContext> options) : base(options) { }

    }
}
