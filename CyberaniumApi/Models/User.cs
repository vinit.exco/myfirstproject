namespace CyberaniumApi.Models
{
    using System;
    using System.Collections.Generic;

    public class User
    {
        public Guid Id { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime CreatedAt { get; set; }

        public Guid? CreatedById { get; set; }

        public virtual User CreatedBy { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public Guid? UpdatedById { get; set; }

        public virtual User UpdatedBy { get; set; }

        public Guid? RoleId { get; set; }

        public virtual Role Role { get; set; }

        public DateTime? LastLoginAt { get; set; }

        public DateTime? LoginFailedAt { get; set; }

        public int LoginFailedCount { get; set; }

        public string UnconfirmedEmail { get; set; }

        public DateTime? UnconfirmedEmailCreatedAt { get; set; }

        public string UnconfirmedEmailCode { get; set; }

        public int UnconfirmedEmailCount { get; set; }

        public DateTime? ResetPasswordCreatedAt { get; set; }

        public int ResetPasswordCount { get; set; }

        public string ResetPasswordCode { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<User> CreatedUsers { get; set; }

        public virtual ICollection<User> UpdatedUsers { get; set; }

        public virtual ICollection<Role> CreatedRoles { get; set; }

        public virtual ICollection<Role> UpdatedRoles { get; set; }
    }
}
