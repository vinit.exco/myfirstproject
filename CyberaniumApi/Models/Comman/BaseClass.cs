﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberaniumApi.Models.Comman
{
    public class BaseClass
    {
        public int ID { get; set; }
        public bool Deleted { get; set; }
        public int  CreatedBy  { get; set; }
        public DateTime  CreatedOn  { get; set; }
        public string  CreatedIP  { get; set; }
        public int  ModifiedBy  { get; set; }
        public DateTime ModifiedOn  { get; set; }

        public string ModifiedIP  { get; set; }
    }
}
