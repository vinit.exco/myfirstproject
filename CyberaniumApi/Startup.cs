using CyberaniumApi.Data;
using CyberaniumApi.Helpers;
using CyberaniumApi.IServices;
using CyberaniumApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberaniumApi
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;
        private AppSettings _appSettings;
        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        //public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireAdminRole",
                    policy => policy.RequireRole(Models.EnumerationTypes.Role.Admin.ToString()));
            });
            if (_env.IsProduction()) services.AddDbContext<CyberContext>();
            else services.AddDbContext<CyberContext>();

            //services.AddHealthChecks().AddDbContextCheck<AppDbContext>();

            services.AddCors();
            // Configure strongly typed settings objects.
            var appSettingsSection = _configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            _appSettings = appSettingsSection.Get<AppSettings>();


            // Configure JWT authentication.
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            services
                .AddAuthentication(configuration =>
                {
                    configuration.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    configuration.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(configuration =>
                {
                    //configuration.Events = new JwtBearerEvents
                    //{
                    //    OnTokenValidated = context =>
                    //    {
                    //        var db = context.HttpContext.RequestServices.GetRequiredService<AppDbContext>();
                    //        var userId = context.Principal.Identity.Name;
                    //        if (userId == null) context.Fail("Unauthorized");

                    //        var user = db.Users.AsNoTracking().Include(x => x.Role)
                    //            .FirstOrDefault(x => x.Id == Guid.Parse(userId));

                    //        if (user != null)
                    //        {
                    //            var identity = context.Principal.Identity as ClaimsIdentity;
                    //            identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.Name));
                    //        }
                    //        else
                    //        {
                    //            context.Fail("Unauthorized");
                    //        }

                    //        return Task.CompletedTask;
                    //    }
                    //};
                    configuration.RequireHttpsMetadata = false;
                    configuration.SaveToken = true;
                    configuration.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
            //services
            //    .AddControllers()
            //    .AddFluentValidation(configuration =>
            //    {
            //        configuration.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            //    });
            //services.AddSwaggerGen(
            //    c => { //<-- NOTE 'Add' instead of 'Configure'
            //        c.SwaggerDoc("v1", new OpenApiInfo
            //        {
            //            Title = "CyberaniumApi",
            //            Version = "v1"
            //        }
            //        );
            //        c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First()); //This line

            //    });

            // Add Swagger.
            services.AddSwaggerGen(configuration =>
            {
                configuration.SwaggerDoc("v1", new OpenApiInfo { Title = "CyberaniumApi", Version = "v1" });
                const string xmlFile = "CyberaniumApi.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                configuration.IncludeXmlComments(xmlPath);
                configuration.CustomSchemaIds(type => type.ToString());
                configuration.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description =
                        "JWT Authorization header using the Bearer scheme.\r\n\r\n" +
                        "Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\n" +
                        "Example: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                configuration.AddSecurityRequirement(
                    new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                },
                                Scheme = "oauth2",
                                Name = "Bearer",
                                In = ParameterLocation.Header
                            },
                            new List<string>()
                        }
                    });
            });
            services.AddControllers();
            services.AddDbContext<CyberContext>(opts => opts.UseSqlServer(_configuration.GetConnectionString("DevConnection")));


            //Register dapper in scope
            services.AddScoped<IDapper, Dapperr>();
            services.AddLocalization();

            // Configure DI for application services.
            // Transient objects are always different; a new instance is provided to every controller and every service.
            // Scoped objects are the same within a request, but different across different requests.
            // Singleton objects are the same for every object and every request.
            services.AddScoped<IPasswordHelper, PasswordHelper>();
            services.AddScoped<IAuthHelper, AuthHelper>();
            //services.AddScoped<IUserService, UserService>();
            services.AddScoped<IEmailService, EmailService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json",
                "CyberaniumApi");
            });
            app.UseHttpsRedirection();
            // The localization middleware must be configured before
            // any middleware which might check the request culture.
            var supportedCultures = new[]
            {
                new CultureInfo("en"),
                new CultureInfo("bs")
            };
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en"),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            });
            app.UseRouting();
            app.UseCors(configuration => configuration.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
