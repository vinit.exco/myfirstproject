﻿using Newtonsoft.Json.Linq;

namespace CyberaniumApi.Helpers
{
    public class XMLCreater
    {
        #region ICD XML
        public string CreateICDCategoryMapXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ICDCategoryMap>" +
                                    "<Id>" + jsonObj.Id + "</Id>" +
                                    "<CategoryId>" + jsonObj.CategoryId + "</CategoryId>" +
                                "</ICDCategoryMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateICDStandardMapXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ICDStandardMap>" +
                                    "<Id>" + jsonObj.Id + "</Id>" +
                                    "<StandardId>" + jsonObj.StandardId + "</StandardId>" +
                                "</ICDStandardMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateICDArtefactMapXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ICDArtefactMap>" +
                                    "<Id>" + jsonObj.Id + "</Id>" +
                                    "<Name>" + jsonObj.Name + "</Name>" +
                                "</ICDArtefactMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateRCMArtefactMapXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<tblRCMArtefactMap>" +
                                    "<Id>" + jsonObj.Id + "</Id>" +
                                    "<Name>" + jsonObj.Name + "</Name>" +
                                "</tblRCMArtefactMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateICDThreatMapXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ICDThreatMap>" +
                                    "<Id>" + jsonObj.Id + "</Id>" +
                                    "<ThreatId>" + jsonObj.ThreatId + "</ThreatId>" +
                                "</ICDThreatMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateRCMThreatMapXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<tblRCMThreatMap>" +
                                    "<Id>" + jsonObj.Id + "</Id>" +
                                    "<ThreatId>" + jsonObj.ThreatId + "</ThreatId>" +
                                "</tblRCMThreatMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }

        public string CreateICDRegulationMapXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ICDRegulationMap>" +
                                    "<Id>" + jsonObj.Id + "</Id>" +
                                    "<RegulationId>" + jsonObj.RegulationId + "</RegulationId>" +
                                "</ICDRegulationMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        #endregion
        #region Scope Assessment Xml
        public string CreateScopeAssessmentXml(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ScopeAssessmentXml>" +
                                    "<ChildOrgId>" + jsonObj.ChildOrgId + "</ChildOrgId>" +
                                "</ScopeAssessmentXml>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateScopeAssessmentRiskDomainMapXml(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ScopeAssessmentRiskDomainMapXml>" +
                                    "<RiskDomainId>" + jsonObj.RiskDomainId + "</RiskDomainId>" +
                                "</ScopeAssessmentRiskDomainMapXml>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateScopeAssessmentISOMapXml(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ScopeAssessmentISOMapXml>" +
                                    "<ISOId>" + jsonObj.ISOId + "</ISOId>" +
                                "</ScopeAssessmentISOMapXml>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateScopeAssessmentRegulationMapXml(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ScopeAssessmentRegulationMapXml>" +
                                    "<RegulationId>" + jsonObj.RegulationId + "</RegulationId>" +
                                "</ScopeAssessmentRegulationMapXml>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        #endregion
        public string CreateQuestionnaireXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet += "<Questionnaire>" +
                                   "<QuestionId>" + jsonObj.QuestionId + "</QuestionId>" +
                                   "<Id>" + jsonObj.Id + "</Id>" +
                                   "<Answer>" + jsonObj.Answer + "</Answer>" +
                                    "<RowNo>" + jsonObj.RowNo + "</RowNo>" +
                               "</Questionnaire>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string PeopleGovnXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet += "<Questionnaire>" +
                                   "<CategoryId>" + jsonObj.CategoryId + "</CategoryId>" +
                               "</Questionnaire>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string OrgAssigneToConsultantMap(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet += "<tblOrgAssigneToConsultantMap>" +
                                   "<OrgId>" + jsonObj.OrgId + "</OrgId>" +
                                   "<EmpIds>" + jsonObj.EmpIds + "</EmpIds>" +
                               "</tblOrgAssigneToConsultantMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string ClientAdminXml(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet += "<tblClientAdmin>" +
                                   "<ClientId>" + jsonObj.ClientId + "</ClientId>" +
                                   "<ClientName>" + jsonObj.ClientName + "</ClientName>" +
                                   "<AssignRoleClientId>" + jsonObj.AssignRoleClientId + "</AssignRoleClientId>" +
                                   "<AssignRoleClient>" + jsonObj.AssignRoleClient + "</AssignRoleClient>" +
                               "</tblClientAdmin>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateThreadScoreXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            foreach (var ques in jsonObj.QuesArray)
                            {
                                var uniqueIdNumber = ques.uniqueId;
                                foreach (var ans in ques.AnsArray)
                                {
                                    dataSet += "<tblThreatsEnablerScore>" +
                                    "<CategoryId>" + jsonObj.CategoryId + "</CategoryId>" +
                                    "<ThreatsID>" + ans.ThreatsID + "</ThreatsID>" +
                                    "<ScoreValue>" + ans.Score + "</ScoreValue>" +
                                    "<uniqueId>" + ans.uniqueId + "</uniqueId>" +
                                    "<AppID>" + ans.AppID + "</AppID>" +
                                "</tblThreatsEnablerScore>";
                                }
                            }
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string xmlProcessEnablerMapping(string Data)
        {
            string t_PackagingXml = "<NewDataSet>";
            if (Data != null)
            {
                var CPList = JArray.Parse(Data);
                if (CPList.Count > 0)
                {
                    foreach (var item in CPList)
                    {
                        dynamic CPobjc = JObject.Parse(item.ToString());
                        if (item.HasValues)
                        {
                            t_PackagingXml += "<ProcessEnablerMap><FunctionId>"
                                + CPobjc.FunctionId + "</FunctionId><ProcessId>"
                                + CPobjc.ProcessId + "</ProcessId><CategoryId>"
                                + CPobjc.CategoryId + "</CategoryId><EnablerId>"
                                + CPobjc.EnablerId + "</EnablerId></ProcessEnablerMap>";
                        }
                    }
                }
            }
            t_PackagingXml += "</NewDataSet>";
            return t_PackagingXml;
        }
        public string CreateRBACXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<RBAC>" +
                                    "<ParentMenuId>" + jsonObj.ParentMenuId + "</ParentMenuId>" +
                                "</RBAC>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateChildOrgXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ChildOrgXML>" +
                                    "<ChildOrgName>" + jsonObj.ChildOrgName + "</ChildOrgName>" +
                                    "<ChildOrgLocation>" + jsonObj.ChildOrgLocation + "</ChildOrgLocation>" +
                                "</ChildOrgXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateOrganizationFunctionXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<OrganizationFunctionMap>" +
                                        "<Id>" + obj.Id + "</Id>" +
                                        "<FunctionName>" + obj.FunctionName + "</FunctionName>" +
                                        "<Consultants>" + obj.Consultants + "</Consultants>" +
                                        "<FunctionDescription>" + obj.FunctionDescription + "</FunctionDescription>" +
                                    "</OrganizationFunctionMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string xmlOrganizationParentAudity(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<OrganizationParentAudity>" +
                                       "<Id>" + obj.Id + "</Id>" +
                                        "<UName>" + obj.UName + "</UName>" +
                                        "<Email>" + obj.Email + "</Email>" +
                                        "<UPassword>" + obj.UPassword + "</UPassword>" +
                                    "</OrganizationParentAudity>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string xmlOrganizationChildAudity(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<OrganizationChildAudity>" +
                                        "<Id>" + obj.Id + "</Id>" +
                                        "<UName>" + obj.UName + "</UName>" +
                                        "<Email>" + obj.Email + "</Email>" +
                                        "<UPassword>" + obj.UPassword + "</UPassword>" +
                                    "</OrganizationChildAudity>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateFunctionProcessXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    int sn = 1;
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            string _dataSet = "";
                            if (obj.Processses != null)
                            {
                                _dataSet = "<Function" + sn + ">";
                                foreach (var _item in obj.Processses)
                                {
                                    dynamic _obj = JObject.Parse(_item.ToString());
                                    _dataSet +=
                                    "<ProcessXML>" +
                                        "<ProcessId>" + _obj.ProcessId + "</ProcessId>" +
                                        "<ProcessName>" + _obj.ProcessName + "</ProcessName>" +
                                        "<ProcessOwner>" + _obj.ProcessOwner + "</ProcessOwner>" +
                                        "<ProcessDescription>" + _obj.ProcessDescription + "</ProcessDescription>" +
                                    "</ProcessXML>";
                                }
                                _dataSet += "</Function" + sn + ">";
                            }
                            dataSet +=
                                    "<FunctionXML>" +
                                        "<Id>" + sn + "</Id>" +
                                        "<FunctionId>" + obj.FunctionId + "</FunctionId>" +
                                        "<FunctionName>" + obj.FunctionName + "</FunctionName>" +
                                        "<FunctionOwner>" + obj.FunctionOwner + "</FunctionOwner>" +
                                        "<FunctionDescription>" + obj.FunctionDescription + "</FunctionDescription>" + _dataSet +
                                    "</FunctionXML>";
                        }
                        sn++;
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateDomainThreatMappingXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<DomainThreatMappingXML>" +
                                    "<ThreatId>" + jsonObj.ThreatId + "</ThreatId>" +
                                    "<Status>" + jsonObj.Status + "</Status>" +
                                "</DomainThreatMappingXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateOrganogramXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    int i = 0;
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            if (i % 2 == 0)
                            {
                                dataSet += "<Organogram>" +
                                    "<uniqueId>" + jsonObj.uniqueId + "</uniqueId>";
                            }
                            else
                            {
                                dataSet += "<Probability>" + jsonObj.Probability + "</Probability>" +
                                    "</Organogram>";
                            }
                        }
                        i++;
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string xmlStandardRegulationScore(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<tblStandardRegulationRCMScore>" +
                                        "<SubDomainID>" + obj.SubDomainID + "</SubDomainID>" +
                                        "<Id>" + obj.Id + "</Id>" +
                                        "<ControlDomain>" + obj.ControlDomain + "</ControlDomain>" +
                                        "<ControlID>" + obj.ControlID + "</ControlID>" +
                                        "<ControlName>" + obj.ControlName + "</ControlName>" +
                                        "<ControlDescription>" + obj.ControlDescription + "</ControlDescription>" +
                                        "<TestOfDesign>" + obj.TestOfDesign + "</TestOfDesign>" +
                                        "<TestOfOE>" + obj.TestOfOE + "</TestOfOE>" +
                                        "<OverallEffectiveness>" + obj.OverallEffectiveness + "</OverallEffectiveness>" +
                                        "<DomainEffectiveScore>" + obj.DomainEffectiveScore + "</DomainEffectiveScore>" +
                                        "<Remarks>" + obj.Remarks + "</Remarks>" +
                                    "</tblStandardRegulationRCMScore>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateControlCategoryXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ControlCategoryXML>" +
                                    "<ControlCategoryId>" + jsonObj.ControlCategoryId + "</ControlCategoryId>" +
                                "</ControlCategoryXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateEnablerXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<EnablerXML>" +
                                    "<ConCatId>" + jsonObj.ConCatId + "</ConCatId>" +
                                "</EnablerXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateThreatScoreXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ThreatScoreXML>" +
                                    "<ConCatId>" + jsonObj.ConCatId + "</ConCatId>" +
                                    "<ThreatId>" + jsonObj.ThreatId + "</ThreatId>" +
                                    "<QuestionnaireMapId>" + jsonObj.QuestionnaireMapId + "</QuestionnaireMapId>" +
                                    "<Score>" + jsonObj.Score + "</Score>" +
                                "</ThreatScoreXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateProcessEnablerXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ProcessEnablerXML>" +
                                    "<ConCatId>" + jsonObj.ConCatId + "</ConCatId>" +
                                    "<AnswerId>" + jsonObj.AnswerId + "</AnswerId>" +
                                "</ProcessEnablerXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateDomainCriticalityScoreXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            var domainScore = "";
                            foreach (var ds in jsonObj.RiskDomainScore)
                            {
                                domainScore +=
                                    "<DomainCriticalityScoreXML>" +
                                        "<ParameterId>" + jsonObj.ParameterId + "</ParameterId>" +
                                        "<RiskDomainId>" + ds.RiskDomainId + "</RiskDomainId>" +
                                        "<Score>" + ds.Score + "</Score>" +
                                    "</DomainCriticalityScoreXML>";
                            }
                            dataSet += domainScore;
                            //dataSet += "<DomainCriticalityScoreXML><FunctionName>" + jsonObj.ParameterId + "</FunctionName>" + domainScore + "</DomainCriticalityScoreXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateClientProcessEnablerScoreXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            var domainScore = "";
                            foreach (var ds in jsonObj.RiskDomainScore)
                            {
                                domainScore +=
                                    "<ClientProcessEnablerScoreXML>" +
                                        "<ConCatId>" + jsonObj.ConCatId + "</ConCatId>" +
                                        "<RiskDomainId>" + ds.RiskDomainId + "</RiskDomainId>" +
                                        "<Score>" + ds.Score + "</Score>" +
                                    "</ClientProcessEnablerScoreXML>";
                            }
                            dataSet += domainScore;
                            //dataSet += "<DomainCriticalityScoreXML><FunctionName>" + jsonObj.ParameterId + "</FunctionName>" + domainScore + "</DomainCriticalityScoreXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateClientProcessFinalScoreXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ClientProcessFinalScoreXML>" +
                                    "<RiskDomainId>" + jsonObj.RiskDomainId + "</RiskDomainId>" +
                                    "<Score>" + jsonObj.Score + "</Score>" +
                                "</ClientProcessFinalScoreXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateRegulationXml(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<REGODB>" +
                                        "<TypeId>" + obj.TypeId + "</TypeId>" +
                                        "<Sector>" + obj.Sector + "</Sector>" +
                                        "<Section>" + obj.Section + "</Section>" +
                                        "<Domain>" + obj.Domain + "</Domain>" +
                                        "<YearPublish>" + obj.YearPublish + "</YearPublish>" +
                                        "<Revision>" + obj.Revision + "</Revision>" +
                                        "<Requirement_ID>" + obj.Requirement_ID + "</Requirement_ID>" +
                                        "<Text>" + obj.Text + "</Text>" +
                                    "</REGODB>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateOrganizationXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    var count = JPList.Count;
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            string _dataSet = "";
                            if (obj.SubChildOrgArray != null)
                            {
                                foreach (var _item in obj.SubChildOrgArray)
                                {
                                    dynamic _obj = JObject.Parse(_item.ToString());
                                    _dataSet +=
                                    "<SubChildOrgXML" + count + ">" +
                                        "<SubChildOrgId>" + _obj.SubChildOrgId + "</SubChildOrgId>" +
                                        "<SubChildOrgName>" + _obj.SubChildOrgName + "</SubChildOrgName>" +
                                    "</SubChildOrgXML" + count + ">";
                                }
                            }
                            dataSet +=
                                    "<ChildOrgXML>" +
                                        "<ChildOrgId>" + obj.ChildOrgId + "</ChildOrgId>" +
                                        "<ChildOrgName>" + obj.ChildOrgName + "</ChildOrgName>" + _dataSet +
                                    "</ChildOrgXML>";
                            count--;
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string xmlGenerateRCM(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<tblRCMMap>" +
                                        "<ICDID>" + obj.ICDID + "</ICDID>" +
                                        "<DomainID>" + obj.DomainID + "</DomainID>" +
                                        "<DomainName>" + obj.DomainName + "</DomainName>" +
                                        "<SubDomainID>" + obj.SubDomainID + "</SubDomainID>" +
                                        "<SubDomainName>" + obj.SubDomainName + "</SubDomainName>" +
                                        "<ControlName>" + obj.ControlName + "</ControlName>" +
                                        "<ControlDescription>" + obj.ControlDescription + "</ControlDescription>" +
                                        "<ControlID>" + obj.ControlID + "</ControlID>" +
                                        "<ControlTypeID>" + obj.ControlTypeID + "</ControlTypeID>" +
                                        "<ControlTypeName>" + obj.ControlTypeName + "</ControlTypeName>" +
                                        "<Governance>" + obj.Governance + "</Governance>" +
                                        "<Application>" + obj.Application + "</Application>" +
                                        "<Infrastructure>" + obj.Infrastructure + "</Infrastructure>" +
                                        "<Cloud>" + obj.Cloud + "</Cloud>" +
                                        "<Networks>" + obj.Networks + "</Networks>" +
                                        "<People>" + obj.People + "</People>" +
                                        "<Location>" + obj.Location + "</Location>" +
                                    "</tblRCMMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            dataSet = dataSet.Replace("'", "\"").Replace("&", "And");
            return dataSet;
        }
        public string CreateRCMXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<RCMXML>" +
                                        "<DomainName>" + obj.domainName + "</DomainName>" +
                                        "<SubDomainName>" + obj.subDomainName + "</SubDomainName>" +
                                        "<ControlName>" + obj.controlName + "</ControlName>" +
                                        "<ControlDescription>" + obj.controlDescription + "</ControlDescription>" +
                                        "<ControlID>" + obj.controlID + "</ControlID>" +
                                        "<ControlTypeName>" + obj.controlTypeName + "</ControlTypeName>" +
                                        "<Governance>" + obj.governance + "</Governance>" +
                                        "<Application>" + obj.application + "</Application>" +
                                        "<Infrastructure>" + obj.infrastructure + "</Infrastructure>" +
                                        "<Cloud>" + obj.cloud + "</Cloud>" +
                                        "<Networks>" + obj.networks + "</Networks>" +
                                        "<People>" + obj.people + "</People>" +
                                        "<Location>" + obj.location + "</Location>" +
                                    "</RCMXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            dataSet = dataSet.Replace("'", "\"").Replace("&", "and");
            return dataSet;
        }
        public string CreateICDXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<ICDXML>" +
                                        "<DomainId>" + obj.domainId + "</DomainId>" +
                                        "<ControlTypeId>" + obj.controlTypeId + "</ControlTypeId>" +
                                        "<ControlName>" + obj.controlName + "</ControlName>" +
                                        "<ControlDescription>" + obj.controlDescription + "</ControlDescription>" +
                                        "</ICDXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            dataSet = dataSet.Replace("'", "\"").Replace("&", "and");
            return dataSet;
        }
        public string CreateProcessEnablerMapXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<ProcessEnablerMapXML>" +
                                        "<OrgId>" + obj.OrgId + "</OrgId>" +
                                        "<FunctionId>" + obj.FunctionId + "</FunctionId>" +
                                        "<ProcessId>" + obj.ProcessId + "</ProcessId>" +
                                        "<ConCatId>" + obj.ConCatId + "</ConCatId>" +
                                        "<QuestionnaireId>" + obj.QuestionnaireId + "</QuestionnaireId>" +
                                    "</ProcessEnablerMapXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateThreatProbabilityScoreXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<ThreatProbabilityScoreXML>" +
                                    "<ThreatId>" + jsonObj.ThreatId + "</ThreatId>" +
                                    "<ConCatId>" + jsonObj.ConCatId + "</ConCatId>" +
                                    "<QuestionnaireId>" + jsonObj.QuestionnaireId + "</QuestionnaireId>" +
                                    "<Score>" + jsonObj.Score + "</Score>" +
                                "</ThreatProbabilityScoreXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateControlAssessmentXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<tblControlAssessmentMap>" +
                                        "<ICDID>" + obj.ICDID + "</ICDID>" +
                                        "<ControlName>" + obj.ControlName + "</ControlName>" +
                                        "<DomainID>" + obj.DomainID + "</DomainID>" +
                                        "<ControlDescription>" + obj.ControlDescription + "</ControlDescription>" +
                                        "<DomainName>" + obj.DomainName + "</DomainName>" +
                                        "<ControlId>" + obj.ControlId + "</ControlId>" +
                                        "<SubDomainID>" + obj.SubDomainID + "</SubDomainID>" +
                                        "<ArtefactName>" + obj.ArtefactName + "</ArtefactName>" +
                                        "<SubDomainName>" + obj.SubDomainName + "</SubDomainName>" +
                                        "<Rating>" + obj.Rating + "</Rating>" +
                                        "<ControlTypeID>" + obj.ControlTypeID + "</ControlTypeID>" +
                                        "<Observation>" + obj.Observation + "</Observation>" +
                                        "<Recommendation>" + obj.Recommendation + "</Recommendation>" +
                                    "</tblControlAssessmentMap>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet.Replace("'", "\"").Replace("&", "And");
        }
        public string CreateCommentXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<tblControlAssessmentCmnt>" +
                                        "<ICDId>" + obj.ICDId + "</ICDId>" +
                                      "<ObservationCmnt>" + obj.ObservationCmnt + "</ObservationCmnt>" +
                                      "<RecomedationCmnt>" + obj.RecomedationCmnt + "</RecomedationCmnt>" +
                                    "</tblControlAssessmentCmnt>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet.Replace("'", "\"").Replace("&", "And");
        }
        public string CreateComplainceAssessmentXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            var domainScore = "";
                            foreach (var ds in jsonObj.xml)
                            {
                                domainScore +=
                                    "<tblComplianceRCMControls>" +
                                        "<DomainCompilancescore>" + jsonObj.DomainCompilancescore + "</DomainCompilancescore>" +
                                        "<ICDID>" + ds.ICDID + "</ICDID>" +
                                        "<SubDomainID>" + ds.SubDomainID + "</SubDomainID>" +
                                        "<ControlName>" + ds.controlName + "</ControlName>" +
                                        "<ControlDescription>" + ds.controlDescription + "</ControlDescription>" +
                                        "<Testofdesign>" + ds.Testofdesign + "</Testofdesign>" +
                                        "<Testofeffectiveness>" + ds.Testofeffectiveness + "</Testofeffectiveness>" +
                                        "<overalleffectiveness>" + ds.overalleffectiveness + "</overalleffectiveness>" +
                                        "<remarks>" + ds.remarks + "</remarks>" +
                                    "</tblComplianceRCMControls>";
                            }
                            dataSet += domainScore;
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateCustomRCMXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            var domainScore = "";
                            foreach (var ds in jsonObj.xml)
                            {
                                domainScore +=
                                    "<tblCustomRCMControls>" +
                                        "<DomainCompilancescore>" + jsonObj.DomainCompilancescore + "</DomainCompilancescore>" +
                                        "<ICDID>" + ds.ICDID + "</ICDID>" +
                                        "<SubDomainID>" + ds.SubDomainID + "</SubDomainID>" +
                                        "<ControlName>" + ds.controlName + "</ControlName>" +
                                        "<ControlDescription>" + ds.controlDescription + "</ControlDescription>" +
                                        "<Testofdesign>" + ds.Testofdesign + "</Testofdesign>" +
                                        "<Testofeffectiveness>" + ds.Testofeffectiveness + "</Testofeffectiveness>" +
                                        "<overalleffectiveness>" + ds.overalleffectiveness + "</overalleffectiveness>" +
                                        "<remarks>" + ds.remarks + "</remarks>" +
                                    "</tblCustomRCMControls>";
                            }
                            dataSet += domainScore;
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateStdAndRegAssessmentXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            var domainScore = "";
                            foreach (var ds in jsonObj.xml)
                            {
                                domainScore +=
                                    "<tblStandardAndRegulationRCMControls>" +
                                        "<DomainCompilancescore>" + jsonObj.DomainCompilancescore + "</DomainCompilancescore>" +
                                        "<ICDID>" + ds.ICDID + "</ICDID>" +
                                        "<SubDomainID>" + ds.SubDomainID + "</SubDomainID>" +
                                        "<ControlName>" + ds.controlName + "</ControlName>" +
                                        "<ControlDescription>" + ds.controlDescription + "</ControlDescription>" +
                                        "<Testofdesign>" + ds.Testofdesign + "</Testofdesign>" +
                                        "<Testofeffectiveness>" + ds.Testofeffectiveness + "</Testofeffectiveness>" +
                                        "<overalleffectiveness>" + ds.overalleffectiveness + "</overalleffectiveness>" +
                                        "<remarks>" + ds.remarks + "</remarks>" +
                                    "</tblStandardAndRegulationRCMControls>";
                            }
                            dataSet += domainScore;
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateDomainBaseRiskScoreXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<DomainBaseRiskScoreXML>" +
                                    "<RiskDomainId>" + jsonObj.RiskDomainId + "</RiskDomainId>" +
                                    "<Score>" + jsonObj.Score + "</Score>" +
                                "</DomainBaseRiskScoreXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateRiskTreatmentPlanXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic obj = JObject.Parse(item.ToString());
                            dataSet +=
                                    "<tblRiskTreatment>" +
                                        "<ICDID>" + obj.ICDID + "</ICDID>" +
                                        "<ControlName>" + obj.ControlName + "</ControlName>" +
                                        "<ControlId>" + obj.ControlId + "</ControlId>" +
                                        "<Rating>" + obj.Rating + "</Rating>" +
                                        "<Observation>" + obj.Observation + "</Observation>" +
                                        "<Recommendation>" + obj.Recommendation + "</Recommendation>" +
                                        "<RiskOwner>" + obj.RiskOwner + "</RiskOwner>" +
                                        "<ManagementDecision>" + obj.ManagementDecision + "</ManagementDecision>" +
                                        "<Justification>" + obj.Justification + "</Justification>" +
                                        "<ActionPlan>" + obj.ActionPlan + "</ActionPlan>" +
                                        "<Responsibility>" + obj.Responsibility + "</Responsibility>" +
                                        "<TargetclosureDate>" + obj.TargetclosureDate + "</TargetclosureDate>" +
                                        "<Status>" + obj.Status + "</Status>" +
                                    "</tblRiskTreatment>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateComplainceXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet +=
                                "<QuestionnaireXML>" +
                                    "<Id>" + jsonObj.Id + "</Id>" +
                                    "<OrderNo>" + jsonObj.OrderNo + "</OrderNo>" +
                                    "<Questionnaire>" + jsonObj.Questionnaire + "</Questionnaire>" +
                                "</QuestionnaireXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateAssessmentXML(string Data)
        {
            if (Data.Equals("null")) return null;
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet += "<AssessmentXML>" +
                                   "<AssignedId>" + jsonObj.AssignedId + "</AssignedId>" +
                               "</AssessmentXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateArtefactsXML(string Data)
        {
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet += "<ArtefactsXML>" +
                                   "<ICDId>" + jsonObj.ICDId + "</ICDId>" +
                                   "<RecFromId>" + jsonObj.RecFromId + "</RecFromId>" +
                                   "<RecByDate>" + jsonObj.RecByDate + "</RecByDate>" +
                                   "<RecStatus>" + jsonObj.RecStatus + "</RecStatus>" +
                                   "<Remark>" + jsonObj.Remark + "</Remark>" +
                               "</ArtefactsXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
        public string CreateImagesXML(string Data)
        {
            if (Data == null) return null;
            string dataSet = "<NewDataSet>";
            if (string.IsNullOrEmpty(Data))
            {
                return dataSet += "</NewDataSet>";
            }
            if (Data != null)
            {
                var JPList = JArray.Parse(Data);
                if (JPList.Count > 0)
                {
                    foreach (var item in JPList)
                    {
                        if (item.HasValues)
                        {
                            dynamic jsonObj = JObject.Parse(item.ToString());
                            dataSet += "<ImagesXML>" +
                                   "<ICDId>" + jsonObj.ICDId + "</ICDId>" +
                                   "<ImagePath>" + jsonObj.ImagePath + "</ImagePath>" +
                               "</ImagesXML>";
                        }
                    }
                }
            }
            dataSet += "</NewDataSet>";
            return dataSet;
        }
    }
}